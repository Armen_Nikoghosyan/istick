#import "ToDoManagedObject+CoreDataProperties.h"

@implementation ToDoManagedObject (CoreDataProperties)
@dynamic object_id;
@dynamic created_at;
@dynamic todos;
@dynamic appointments;

@end

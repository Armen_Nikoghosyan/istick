//
//  FileManagedObject+CoreDataProperties.m
//  iStick
//
//  Created by Falcon on 5/31/16.
//  Copyright © 2016 Levon. All rights reserved.
//

#import "FileManagedObject+CoreDataProperties.h"

@implementation FileManagedObject (CoreDataProperties)
@dynamic name;
@dynamic notes;
@dynamic upper_name;
@end

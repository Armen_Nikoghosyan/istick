//
//  FileManagedObject.h
//  iStick
//
//  Created by Falcon on 5/31/16.
//  Copyright © 2016 Levon. All rights reserved.
//
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN
@interface FileManagedObject : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "FileManagedObject+CoreDataProperties.h"
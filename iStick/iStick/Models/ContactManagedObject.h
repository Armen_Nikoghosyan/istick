#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN
@interface ContactManagedObject : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ContactManagedObject+CoreDataProperties.h"

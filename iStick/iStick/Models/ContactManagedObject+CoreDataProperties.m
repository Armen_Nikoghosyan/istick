#import "ContactManagedObject+CoreDataProperties.h"

@implementation ContactManagedObject (CoreDataProperties)
@dynamic contactId;
@dynamic contact;
@dynamic notes;
@end

#import "ToDoManagedObject.h"

@interface ToDoManagedObject (CoreDataProperties)
@property (nullable, nonatomic, retain) NSDate *created_at;
@property (nullable ,nonatomic, retain) NSSet *todos;
@property (nullable ,nonatomic, retain) NSSet *appointments;
@property (nullable, nonatomic, retain) NSString *object_id;


@end

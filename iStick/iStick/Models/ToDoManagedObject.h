#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN
@interface ToDoManagedObject : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ToDoManagedObject+CoreDataProperties.h"

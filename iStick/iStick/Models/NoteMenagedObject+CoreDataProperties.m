#import "NoteMenagedObject+CoreDataProperties.h"

@implementation NoteMenagedObject (CoreDataProperties)
@dynamic name;
@dynamic created_at;
@dynamic image;
@dynamic title;
@dynamic textY;
@dynamic color_index;
@dynamic object_id;
@dynamic priority;
@dynamic isInParse;
@dynamic fileName;
@dynamic categoryNameUpperCase;
@dynamic categoryName;
@dynamic titleUpperCase;
@dynamic updated_at;
@dynamic wasDeleted;
@dynamic star_count;
@dynamic time_sort_number;
@dynamic contact_id;
@dynamic version;
@dynamic view_at;
@dynamic backgroundImage;
@dynamic line_color_index;
@dynamic pin_count;
@dynamic opened;
@dynamic safe;
@dynamic phoneNumber;
@dynamic isInCue;
@dynamic alarm_at;
@dynamic alarm;
@dynamic document;
@dynamic gallery;
@end

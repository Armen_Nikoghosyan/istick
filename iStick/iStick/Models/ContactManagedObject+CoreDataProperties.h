#import "ContactManagedObject.h"

@interface ContactManagedObject (CoreDataProperties)
@property (nullable, nonatomic, retain) NSString *contactId;
@property (nullable, nonatomic, retain) NSData *contact;
@property (nullable ,nonatomic, retain) NSSet *notes;
@end

#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN
@interface ToDoNoteManagedObject : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ToDoNoteManagedObject+CoreDataProperties.h"

//
//  FileManagedObject+CoreDataProperties.h
//  iStick
//
//  Created by Falcon on 5/31/16.
//  Copyright © 2016 Levon. All rights reserved.
//

#import "FileManagedObject.h"
#import "NoteMenagedObject.h"

@interface FileManagedObject (CoreDataProperties)
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *upper_name;
@property (nullable ,nonatomic, retain) NSSet *notes;

@end

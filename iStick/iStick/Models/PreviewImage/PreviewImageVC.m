//
//  PreviewImageVC.m
//  iStick
//
//  Created by Armen Nikodhosyan on 08.12.17.
//  Copyright © 2017 Levon. All rights reserved.
//

#import "PreviewImageVC.h"

#import "BVCropViewController.h"
#import "BVCropPhotoView.h"
#import "BVCropPhotoOverlayView.h"
#import "btSimplePopUp.h"
#import "TOCropViewController.h"
#import "TOCropViewControllerTransitioning.h"

@interface PreviewImageVC ()<BVCropViewControllerDelegate,TOCropViewControllerDelegate>

@property()btSimplePopUP *popUp;
@property()NSNumber* type;
@end

@implementation PreviewImageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.type = [NSNumber numberWithInteger:0];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.saveAsButton.layer.cornerRadius = self.saveAsButton.frame.size.height / 2;
    self.saveAsButton.layer.borderWidth = 1;
    self.saveAsButton.layer.borderColor = [UIColor colorWithRed:255/229 green:255/195 blue:255/51 alpha:255/255].CGColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidLayoutSubviews {
    CGRect frame = self.saveAsButton.frame;
    frame.size.height = self.view.frame.size.height * 0.15;
    frame.size.width = frame.size.height;
    self.saveAsButton.frame = frame;
    self.saveAsButton.layer.cornerRadius = self.saveAsButton.frame.size.height / 2;
    [self.view layoutIfNeeded];

}






- (IBAction)cropButtonAction:(UIButton *)sender {
    
//    BVCropViewController *controller = [[BVCropViewController alloc] init];
//    controller.cropSize = CGSizeMake(260, 286);
//    controller.cropPhotoView.maximumZoomScale = 5;
//    [controller.cropPhotoView updateOverlayView:[[BVCropPhotoOverlayView alloc] initWithCropSize:controller.cropSize]];
//    controller.delegate = self;
//    controller.sourceImage = self.takedImage.image;
//    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
//    [self presentViewController:navigationController animated:YES completion:nil];
   
    
      [self presentViewController:self.takedImage.image];
   
    
    
}



- (IBAction)rotateButtonAction:(UIButton *)sender {
    
    self.takedImage.image = [self rotateUIImage:self.takedImage.image clockwise:NO];
}

- (IBAction)saveAsButtonAction:(UIButton *)sender {
   
    
    
    self.popUp = [[btSimplePopUP alloc]initWithItemImage:@[[UIImage imageNamed:@"doc"],[UIImage imageNamed:@"img"],[UIImage imageNamed:@"document512x512"]]
                                                andTitles:@[@"Note",@"Photo",@"Document"]
                                           andActionArray:@[^{[self  noteButtonPressed];},
                                                             ^{[self photoButtonPressed];},
                                                             ^{[self documentButtonPressed];}]
                                      addToViewController:self];
    [self.view addSubview:_popUp];
    [_popUp show:BTPopUPAnimateWithFade];
    
  
    
}
                  
                  
-(void)noteButtonPressed {
    self.type = [NSNumber numberWithInteger:0];
    [self.saveAsButton setTitle:@"Note" forState:UIControlStateNormal];
}
-(void)photoButtonPressed {
    self.type = [NSNumber numberWithInteger:1];
    [self.saveAsButton setTitle:@"Photo" forState:UIControlStateNormal];
}

-(void)documentButtonPressed {
    self.type = [NSNumber numberWithInteger:2];
    [self.saveAsButton setTitle:@"Document" forState:UIControlStateNormal];
}

- (IBAction)cammeraButtonAction:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:true completion:nil];
    [self.delegate cameraButtonPressed];
}


- (void)cropViewControllerDidCrop:(BVCropViewController *)sender croppedImage:(UIImage *)croppedImage{
    self.takedImage.image = croppedImage;
    [sender.navigationController dismissViewControllerAnimated:YES
                                                    completion:nil];
}



-(void)cropViewControllerDidCancel:(BVCropViewController *)sender{
    [sender dismissViewControllerAnimated:YES
                               completion:nil];
}


- (UIImage*)rotateUIImage:(UIImage*)sourceImage clockwise:(BOOL)clockwise
{
    CGSize size = sourceImage.size;
    UIGraphicsBeginImageContext(CGSizeMake(size.height, size.width));
    [[UIImage imageWithCGImage:[sourceImage CGImage] scale:1.0 orientation:clockwise ? UIImageOrientationRight : UIImageOrientationLeft] drawInRect:CGRectMake(0,0,size.height ,size.width)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
- (IBAction)saveButtonAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.delegate saveButtonPressed:self.takedImage.image type:self.type];
}


- (void)presentViewController:(UIImage*)image
{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    TOCropViewController *cropViewController = [[TOCropViewController alloc] initWithImage:image];
    cropViewController.delegate = self;
    [self presentViewController:cropViewController animated:YES completion:nil];
}


- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    self.takedImage.image = image;
    [self dismissViewControllerAnimated:YES completion:nil];
         // 'image' is the newly cropped version of the original image
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

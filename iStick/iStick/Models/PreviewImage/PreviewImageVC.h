//
//  PreviewImageVC.h
//  iStick
//
//  Created by Armen Nikodhosyan on 08.12.17.
//  Copyright © 2017 Levon. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PreviewImageVCDDelegate<NSObject>
-(void)cameraButtonPressed;
-(void)saveButtonPressed:(UIImage *)image type:(NSNumber*)type;
@end

@interface PreviewImageVC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *takedImage;
@property (weak, nonatomic) IBOutlet UIButton *saveAsButton;
@property()UIImage *image;
@property() id<PreviewImageVCDDelegate> delegate;
@end


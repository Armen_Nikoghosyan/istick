#import "NoteMenagedObject.h"

@interface NoteMenagedObject (CoreDataProperties)
@property (nullable, nonatomic, retain) NSData *backgroundImage;
@property (nullable, nonatomic, retain) NSData *image;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *phoneNumber;
@property (nullable, nonatomic, retain) NSString *fileName;
@property (nullable, nonatomic, retain) NSString *categoryName;
@property (nullable, nonatomic, retain) NSString *categoryNameUpperCase;
@property (nullable, nonatomic, retain) NSString *contact_id;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *titleUpperCase;
@property (nullable, nonatomic, retain) NSString *object_id;
@property (nullable, nonatomic, retain) NSDate *created_at;
@property (nullable, nonatomic, retain) NSDate *view_at;
@property (nullable, nonatomic, retain) NSDate *updated_at;
@property (nullable, nonatomic, retain) NSNumber *textY;
@property (nullable, nonatomic, retain) NSNumber *color_index;
@property (nullable, nonatomic, retain) NSString *line_color_index;
@property (nullable, nonatomic, retain) NSNumber *star_count;
@property (nullable, nonatomic, retain) NSNumber *pin_count;
@property (nullable, nonatomic, retain) NSNumber *opened;
@property (nullable, nonatomic, retain) NSNumber *safe;
@property (nullable, nonatomic, retain) NSNumber *isInParse;
@property (nullable, nonatomic, retain) NSNumber *wasDeleted;
@property (nullable, nonatomic, retain) NSNumber *priority;
@property (nullable, nonatomic, retain) NSNumber *time_sort_number;
@property (nullable, nonatomic, retain) NSNumber *version;
@property (nullable, nonatomic, retain) NSNumber *isInCue;
@property (nullable, nonatomic, retain) NSDate *alarm_at;
@property (nullable, nonatomic, retain) NSNumber *alarm;
@property (nullable, nonatomic, retain) NSNumber *document;
@property (nullable, nonatomic, retain) NSNumber *gallery;

@end

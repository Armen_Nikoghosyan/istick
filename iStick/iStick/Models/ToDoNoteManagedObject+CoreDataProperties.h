#import "ToDoNoteManagedObject.h"

@interface ToDoNoteManagedObject (CoreDataProperties)
@property (nullable, nonatomic, retain) NSDate *created_at;
@property (nullable, nonatomic, retain) NSString *object_id;
@property (nonatomic, nullable, retain) NSNumber *chacked;
@property (nullable, nonatomic, retain) NSData *image;
@property (nonatomic, nullable, retain) NSNumber *index;

@end

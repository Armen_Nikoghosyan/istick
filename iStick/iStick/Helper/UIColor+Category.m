#import "UIColor+Category.h"

@implementation UIColor (Category)
+ (NSString *)getStringFromColor:(UIColor *)color{
    if (color) {
    const CGFloat* components = CGColorGetComponents(color.CGColor);
        if (!((int)components[0] == 0 && (int)components[1] == 1 && (int)components[2] == 0)) {
            NSString *returnedString = [NSString stringWithFormat:@"%f,%f,%f,%f",components[0],components[1],components[2],CGColorGetAlpha(color.CGColor)];
            return returnedString;
        }
    }
    return @"0,0,0,1";
}

+ (UIColor *)getColorFromString:(NSString *)text{
    NSArray* stringComponents = [text componentsSeparatedByString:@","];
    if (stringComponents.count == 4) {
    return [UIColor colorWithRed:(CGFloat)[stringComponents[0] floatValue] green:(CGFloat)[stringComponents[1] floatValue] blue:(CGFloat)[stringComponents[2] floatValue] alpha:(CGFloat)[stringComponents[3] floatValue]];
    }else{
        return [UIColor blackColor];
    }
}
@end

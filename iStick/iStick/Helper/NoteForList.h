//
//  NoteForList.h
//  iStick
//
//  Created by Falcon on 7/1/16.
//  Copyright © 2016 Levon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NoteForList : NSObject
@property (nullable, nonatomic, retain) NSData *image;
@property (nullable, nonatomic, retain) NSData *backgroundImage;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *fileName;
@property (nullable, nonatomic, retain) NSString *contact_id;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *object_id;
@property (nullable, nonatomic, retain) NSDate *created_at;
@property (nullable, nonatomic, retain) NSDate *view_at;
@property (nullable, nonatomic, retain) NSDate *updated_at;
@property (nullable, nonatomic, retain) NSNumber *textY;
@property (nullable, nonatomic, retain) NSNumber *color_index;
@property (nullable, nonatomic, retain) NSString *line_color_index;
@property (nullable, nonatomic, retain) NSNumber *isInParse;
@property (nullable, nonatomic, retain) NSNumber *wasDeleted;
@property (nullable, nonatomic, retain) NSNumber *priority;
@property (nullable, nonatomic, retain) NSNumber *starCount;
@property (nullable, nonatomic, retain) NSNumber *pinCount;
@property (nullable, nonatomic, retain) NSNumber *time_sort_number;
@property (nullable, nonatomic, retain) NSNumber *version;

@end

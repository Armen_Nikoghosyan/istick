#import <UIKit/UIKit.h>

@interface UIImage (Resize)

+ (void)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize completion:(void (^)(UIImage * returnedImage))completion;
+ (UIImage *)imageWithImageOne:(UIImage *)image1 andImageTwo:(UIImage *)image2;
+ (UIImage *)imageFromColor:(UIColor *)color withSize:(CGSize)size;
//+ (UIImage *)cropeImage:(UIImage *)image toSize:(CGSize)size;
- (UIImage *)scaleImageToSize:(CGSize)newSize;
+ (UIImage *)scaleImage:(UIImage *)image maxWidth:(int) maxWidth maxHeight:(int) maxHeight;
+ (UIImage *)cropeImage:(UIImage *)image toSize:(CGSize)size;
+ (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size;
- (UIImage *) imageByTrimmingTransparentPixels;
@end

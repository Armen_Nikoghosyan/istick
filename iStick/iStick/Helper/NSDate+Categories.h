//
//  NSDate+Categories.h
//  iStick
//
//  Created by Falcon on 5/31/16.
//  Copyright © 2016 Levon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Categories)
+ (NSDate *)dateToMidnight:(NSDate *)date;
+ (NSInteger)minutesBetween:(NSDate *)date2 dates:(NSDate *)date1;
+ (NSString *)dateValue:(NSDate *)date;
+ (NSString *)dateValueForToDo:(NSDate *)date;
+ (NSDate *)getPrevYear:(NSDate *)date;
+ (double)dateDistanceFrom1970InSecconds:(NSDate *)localeDate;
@end

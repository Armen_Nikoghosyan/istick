#import <UIKit/UIKit.h>

@interface UIColor (Category)

+ (NSString *)getStringFromColor:(UIColor *)color;
+ (UIColor *)getColorFromString:(NSString *)color;

@end

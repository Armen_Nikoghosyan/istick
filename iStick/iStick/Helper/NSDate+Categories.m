//
//  NSDate+Categories.m
//  iStick
//
//  Created by Falcon on 5/31/16.
//  Copyright © 2016 Levon. All rights reserved.
//

#import "NSDate+Categories.h"

@implementation NSDate (Categories)
+ (NSDate *)dateToMidnight:(NSDate *)date{
    NSCalendar *const calendar = NSCalendar.currentCalendar;
    NSCalendarUnit const preservedComponents = (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay);
    NSDateComponents *const components = [calendar components:preservedComponents fromDate:date];
    NSDate *const normalizedDate = [calendar dateFromComponents:components];
    return normalizedDate;
    
}

+ (NSInteger)minutesBetween:(NSDate *)date2 dates:(NSDate *)date1{
    
    NSTimeInterval distanceBetweenDates = [date2 timeIntervalSinceDate:date1];
    double secondsInAnHour = 60;
    NSInteger minutesBetweenDates = distanceBetweenDates / secondsInAnHour;
    return minutesBetweenDates;
}

+ (NSString *)dateValue:(NSDate *)date{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyyMMddhhmmss"];
    return [formatter stringFromDate:date];
}

+ (NSString *)dateValueForToDo:(NSDate *)date{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyyMMdd"];
    return [formatter stringFromDate:date];
}

+ (NSDate *)getPrevYear:(NSDate *)date{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setYear:-1];
    NSDate *prevYear = [gregorian dateByAddingComponents:offsetComponents toDate:date options:0];
    return prevYear;
}

+ (double)dateDistanceFrom1970InSecconds:(NSDate *)localeDate{
    NSTimeInterval distanceBetweenDates = [localeDate timeIntervalSince1970];
    return distanceBetweenDates;
}


@end

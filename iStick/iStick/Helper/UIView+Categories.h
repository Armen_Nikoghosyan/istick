//
//  UIView+Categories.h
//  iStick
//
//  Created by Falcon on 6/9/16.
//  Copyright © 2016 Levon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Categories)

- (CAShapeLayer *)drawLineFromPoint:(CGPoint)start toPoint:(CGPoint)end ofColor:(UIColor *)color;
- (void)addShadowToView;

@end

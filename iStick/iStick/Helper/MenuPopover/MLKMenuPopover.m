//
//  MLKMenuPopover.m
//  MLKMenuPopover
//
//  Created by NagaMalleswar on 20/11/14.
//  Copyright (c) 2014 NagaMalleswar. All rights reserved.
//

#import "MLKMenuPopover.h"
#import <QuartzCore/QuartzCore.h>

#define RGBA(a, b, c, d) [UIColor colorWithRed:(a / 255.0f) green:(b / 255.0f) blue:(c / 255.0f) alpha:d]
#define MENU_ITEM_HEIGHT        40
#define FONT_SIZE               17
#define CELL_IDENTIGIER         @"MenuPopoverCell"
#define MENU_TABLE_VIEW_FRAME   CGRectMake(0, 0, frame.size.width, frame.size.height)
#define SEPERATOR_LINE_RECT     CGRectMake(10, MENU_ITEM_HEIGHT - 1, self.frame.size.width - 20, 1)
#define MENU_POINTER_RECT       CGRectMake(frame.origin.x, frame.origin.y, 23, 11)
#define CONTAINER_BG_COLOR      RGBA(0, 0, 0, 0.4f)
#define ZERO                    0.0f
#define ONE                     1.0f
#define ANIMATION_DURATION      0.2f
#define MENU_POINTER_TAG        1011
#define MENU_TABLE_VIEW_TAG     1012
#define LANDSCAPE_WIDTH_PADDING 50

@interface MLKMenuPopover (){
    UIView *poligonView;
    UITableView *menuItemsTableView;
}

@property(nonatomic,retain) NSArray *menuItems;
@property(nonatomic,retain) UIButton *containerButton;

- (void)hide;
- (void)addSeparatorImageToCell:(UITableViewCell *)cell;

@end

@implementation MLKMenuPopover

@synthesize menuPopoverDelegate;
@synthesize menuItems;
@synthesize containerButton;

- (id)initWithFrame:(CGRect)frame menuItems:(NSArray *)aMenuItems{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        self.menuItems = aMenuItems;
        self.isHidden = YES;
        // Adding Container Button which will take care of hiding menu when user taps outside of menu area
        self.containerButton = [[UIButton alloc] init];
        [self.containerButton setBackgroundColor:CONTAINER_BG_COLOR];
        [self.containerButton addTarget:self action:@selector(dismissMenuPopover) forControlEvents:UIControlEventTouchUpInside];
        [self.containerButton setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin];
        
        
        // Adding Menu Options Pointer
        UIImageView *menuPointerView = [[UIImageView alloc] initWithFrame:MENU_POINTER_RECT];
        menuPointerView.image = [UIImage imageNamed:@"options_pointer"];
        menuPointerView.tag = MENU_POINTER_TAG;
        [self.containerButton addSubview:menuPointerView];
        
        poligonView = [[UIView alloc] initWithFrame:CGRectMake(frame.size.width/2, 2, 20, 20)];
        [poligonView setBackgroundColor:[UIColor whiteColor]];
        [menuPointerView addSubview:poligonView];
        poligonView.transform = CGAffineTransformMakeRotation(M_PI_4);
        
        // Adding menu Items table
        menuItemsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 11, frame.size.width, frame.size.height)];
        
        menuItemsTableView.dataSource = self;
        menuItemsTableView.delegate = self;
        menuItemsTableView.scrollEnabled = NO;
        menuItemsTableView.backgroundColor = [UIColor whiteColor];
        menuItemsTableView.tag = MENU_TABLE_VIEW_TAG;
        menuItemsTableView.clipsToBounds = YES;
        [menuItemsTableView setScrollEnabled:YES];
        [menuItemsTableView setBounces:YES];
        menuItemsTableView.layer.masksToBounds = YES;
        menuItemsTableView.layer.cornerRadius = 5;

        UIImageView *bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Menu_PopOver_BG"]];
        menuItemsTableView.backgroundView = bgView;
        [self addSubview:menuItemsTableView];
        [self.containerButton addSubview:self];
        
    }
    
    return self;
}

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    menuItemsTableView.frame = CGRectMake(0, 11, frame.size.width, frame.size.height);
}

- (void)setArrowX:(CGFloat)x{
    poligonView.centerX = x;
}

- (void)hideArrow{
    [poligonView setHidden:YES];
}

#pragma mark -
#pragma mark UITableViewDatasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return MENU_ITEM_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section{
    return [self.menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = CELL_IDENTIGIER;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell.textLabel setTextAlignment:NSTextAlignmentLeft];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];

    }
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;

    cell.textLabel.text = [self.menuItems objectAtIndex:indexPath.row];
    if ((indexPath.row == 3 && [cell.textLabel.text isEqualToString:@"ALL"]) ||
        (indexPath.row == 0 && [cell.textLabel.text isEqualToString:@"by date"]) ||
        (indexPath.row == 1 && [cell.textLabel.text isEqualToString:@"by text"]) ||
        (indexPath.row == 2 && [cell.textLabel.text isEqualToString:@"by color"])) {
        [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:FONT_SIZE]];
        [cell.textLabel setTextColor:[UIColor redColor]];
    }else{
        [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:FONT_SIZE]];
        [cell.textLabel setTextColor:[UIColor blackColor]];
    }
    
    return cell;
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self hide];
    [self.menuPopoverDelegate menuPopover:self didSelectMenuItemAtIndex:indexPath.row];
}

#pragma mark -
#pragma mark Actions

- (void)dismissMenuPopover{
    [self hide];
}

- (void)showInView:(UIView *)view{
    self.containerButton.alpha = ZERO;
    self.containerButton.frame = view.bounds;
    [view addSubview:self.containerButton];
    [self removeFromSuperview];
//    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
//    gradientLayer.frame = self.containerButton.layer.bounds;
//    
//    gradientLayer.colors = [NSArray arrayWithObjects:
//                            (id)[UIColor colorWithWhite:0.3f alpha:0.7f].CGColor,
//                            (id)[UIColor colorWithWhite:0.8f alpha:0.1f].CGColor,
//                            nil];
//    gradientLayer.cornerRadius = self.containerButton.layer.cornerRadius;
//    [self.containerButton.layer addSublayer:gradientLayer];
    [self.containerButton addSubview:self];
    [UIView animateWithDuration:ANIMATION_DURATION
                     animations:^{
                         self.containerButton.alpha = ONE;
                     }
                     completion:^(BOOL finished) {
                         _isHidden = NO;
                     }];
}

- (void)hide{
    [menuPopoverDelegate menuPopoverWillHide:self];
    [UIView animateWithDuration:ANIMATION_DURATION
                     animations:^{
                         self.containerButton.alpha = ZERO;
                     }
                     completion:^(BOOL finished) {
                         [self.containerButton removeFromSuperview];
                         _isHidden = YES;
                     }];
}

#pragma mark -
#pragma mark Separator Methods

- (void)addSeparatorImageToCell:(UITableViewCell *)cell{
    UIImageView *separatorImageView = [[UIImageView alloc] initWithFrame:SEPERATOR_LINE_RECT];
    [separatorImageView setImage:[UIImage imageNamed:@"DefaultLine"]];
    separatorImageView.opaque = YES;
    [cell.contentView addSubview:separatorImageView];
}

#pragma mark -
#pragma mark Orientation Methods

- (void)layoutUIForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    BOOL landscape = (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
    
    UIImageView *menuPointerView = (UIImageView *)[self.containerButton viewWithTag:MENU_POINTER_TAG];
    UITableView *menuItemsTableView = (UITableView *)[self.containerButton viewWithTag:MENU_TABLE_VIEW_TAG];
    
    if( landscape ){
        menuPointerView.frame = CGRectMake(menuPointerView.frame.origin.x + LANDSCAPE_WIDTH_PADDING, menuPointerView.frame.origin.y, menuPointerView.frame.size.width, menuPointerView.frame.size.height);
        
        menuItemsTableView.frame = CGRectMake(menuItemsTableView.frame.origin.x + LANDSCAPE_WIDTH_PADDING, menuItemsTableView.frame.origin.y, menuItemsTableView.frame.size.width, menuItemsTableView.frame.size.height);
    }else{
        menuPointerView.frame = CGRectMake(menuPointerView.frame.origin.x - LANDSCAPE_WIDTH_PADDING, menuPointerView.frame.origin.y, menuPointerView.frame.size.width, menuPointerView.frame.size.height);
        
        menuItemsTableView.frame = CGRectMake(menuItemsTableView.frame.origin.x - LANDSCAPE_WIDTH_PADDING, menuItemsTableView.frame.origin.y, menuItemsTableView.frame.size.width, menuItemsTableView.frame.size.height);
    }
    menuItemsTableView.layer.shadowColor = [UIColor blackColor].CGColor;
    menuItemsTableView.layer.shadowOpacity = 1;
    menuItemsTableView.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, menuItemsTableView.width * 1.1, menuItemsTableView.height * 1.1)].CGPath;
    menuItemsTableView.layer.shadowRadius = 2;
    menuItemsTableView.layer.shadowOffset = CGSizeMake(menuItemsTableView.width * 1.1, menuItemsTableView.height * 1.1);
}
@end

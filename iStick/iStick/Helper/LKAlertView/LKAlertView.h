//
//  LKAlertView.h
//  iStick
//
//  Created by Falcon on 6/24/16.
//  Copyright © 2016 Levon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void(^LKAlertViewCompletion)(NSUInteger selectedOtherButtonIndex);

@interface LKAlertView : NSObject


+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
         cancelButtonTitle:(NSString *)cancelButtonTitle
         otherButtonTitles:(NSArray *)otherButtonTitles
                completion:(LKAlertViewCompletion)completion;

+ (void)showAlertWithTitleInDevMode:(NSString *)title
                   message:(NSString *)message
         cancelButtonTitle:(NSString *)cancelButtonTitle
         otherButtonTitles:(NSArray *)otherButtonTitles
                completion:(LKAlertViewCompletion)completion;

@end

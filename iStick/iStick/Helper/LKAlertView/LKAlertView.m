//
//  LKAlertView.m
//  iStick
//
//  Created by Falcon on 6/24/16.
//  Copyright © 2016 Levon. All rights reserved.
//

#import "LKAlertView.h"

@interface LKAlertView () <UIAlertViewDelegate>

@property (nonatomic, copy) LKAlertViewCompletion completion;

@end

@implementation LKAlertView

+ (void)showAlertWithTitleInDevMode:(NSString *)title
                            message:(NSString *)message
                  cancelButtonTitle:(NSString *)cancelButtonTitle
                  otherButtonTitles:(NSArray *)otherButtonTitles
                         completion:(LKAlertViewCompletion)completion{
#ifdef DEBUG
//    [LKAlertView showAlertWithTitle:title message:message cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtonTitles completion:completion];

#endif
}


+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
         cancelButtonTitle:(NSString *)cancelButtonTitle
         otherButtonTitles:(NSArray *)otherButtonTitles
                completion:(LKAlertViewCompletion)completion {
    if ([UIAlertController class] != nil) {
        __block UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                                         message:message
                                                                                  preferredStyle:UIAlertControllerStyleAlert];
        
        void (^alertActionHandler)(UIAlertAction *) = [^(UIAlertAction *action) {
            if (completion) {
                // This block intentionally retains alertController, and releases it afterwards.
                if (action.style == UIAlertActionStyleCancel) {
                    completion(NSNotFound);
                } else {
                    NSUInteger index = [alertController.actions indexOfObject:action];
                    completion(index - 1);
                }
            }
            alertController = nil;
        } copy];
        
        [alertController addAction:[UIAlertAction actionWithTitle:cancelButtonTitle
                                                            style:UIAlertActionStyleCancel
                                                          handler:alertActionHandler]];
        
        for (NSString *buttonTitle in otherButtonTitles) {
            [alertController addAction:[UIAlertAction actionWithTitle:buttonTitle
                                                                style:UIAlertActionStyleDefault
                                                              handler:alertActionHandler]];
        }
        
        UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
        UIViewController *viewController = keyWindow.rootViewController;
        while (viewController.presentedViewController) {
            viewController = viewController.presentedViewController;
        }
        
        [viewController presentViewController:alertController animated:YES completion:nil];
    } else {
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_8_0
        __block LKAlertView *lkAlertView = [[self alloc] init];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:cancelButtonTitle
                                                  otherButtonTitles:nil];
        
        for (NSString *buttonTitle in otherButtonTitles) {
            [alertView addButtonWithTitle:buttonTitle];
        }
        
        lkAlertView.completion = ^(NSUInteger index) {
            if (completion) {
                completion(index);
            }
            
            lkAlertView = nil;
        };
        
        alertView.delegate = lkAlertView;
        [alertView show];
#endif
    }
}

#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_8_0

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (self.completion) {
        if (buttonIndex == alertView.cancelButtonIndex) {
            self.completion(NSNotFound);
        } else {
            self.completion(buttonIndex - 1);
        }
    }
}

#endif

@end

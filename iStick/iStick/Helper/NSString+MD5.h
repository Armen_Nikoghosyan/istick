//
//  NSString+MD5.h
//  iStick
//
//  Created by Levon Kirakosyan on 3/14/17.
//  Copyright © 2017 Levon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *)MD5String;

@end

//
//  BMCollectionViewFlowLayout.h
//  iStick
//
//  Created by Levon Kirakosyan on 5/5/17.
//  Copyright © 2017 Levon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BMCollectionViewFlowLayout : UICollectionViewFlowLayout

@end

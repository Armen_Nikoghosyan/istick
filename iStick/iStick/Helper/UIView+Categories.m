//
//  UIView+Categories.m
//  iStick
//
//  Created by Falcon on 6/9/16.
//  Copyright © 2016 Levon. All rights reserved.
//

#import "UIView+Categories.h"

@implementation UIView (Categories)

- (CAShapeLayer *)drawLineFromPoint:(CGPoint)start toPoint:(CGPoint)end ofColor:(UIColor *)color{
    
    //design the path
    UIBezierPath *path = [[UIBezierPath alloc] init];
    [path moveToPoint:start];
    [path addLineToPoint:end];
    
    //design path in layer
    CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] init];
    shapeLayer.path = path.CGPath;
    shapeLayer.strokeColor = color.CGColor;
    shapeLayer.lineWidth = 1.0f;

    [self.layer addSublayer:shapeLayer];
    return shapeLayer;
    
}


- (void)addShadowToView{
    
    self.layer.masksToBounds = NO;
    self.layer.shadowOffset = CGSizeMake(1, 1);
    self.layer.shadowColor = [UIColor colorWithWhite:0.1 alpha:0.7].CGColor;
    self.layer.shadowOpacity = 0.5;
    self.layer.shadowRadius = 1;
}
@end

#import <UIKit/UIKit.h>
#import "MCManager.h"
#import "NotesViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;  
@property (strong, nonatomic) NSArray *colorsArray;
@property (strong, nonatomic) NSMutableArray *array;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic) BOOL amIMessageSender;
@property (nonatomic, strong) MCManager *mcManager;
@property (nonatomic, strong) NotesViewController *notesViewController;

- (NSURL *)applicationDocumentsDirectory;
@end


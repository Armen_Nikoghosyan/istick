

#import <UIKit/UIKit.h>


@interface InfColorSquareView : UIImageView

@property (nonatomic) float hue;

@end


@interface InfColorSquarePicker : UIControl

@property (nonatomic) float hue;
@property (nonatomic) CGPoint value;

@end


#import "Contact.h"
#import "AppDelegate.h"
#import <Contacts/Contacts.h>
#import "NoteMenagedObject.h"
#import "NotePFObject.h"

@implementation Contact
+ (instancetype)sharedInstance {
    static Contact *contact = nil;
    if (!contact) {
        contact = [[Contact alloc] init];
    }
    return contact;
}

- (void)syncAllContacts{
    CNContactStore *store = [[CNContactStore alloc] init];
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted == YES) {
            NSArray *keys = @[CNContactNamePrefixKey,
                              CNContactGivenNameKey,
                              CNContactMiddleNameKey,
                              CNContactFamilyNameKey,
                              CNContactPreviousFamilyNameKey,
                              CNContactNameSuffixKey,
                              CNContactNicknameKey,
                              CNContactPhoneticGivenNameKey,
                              CNContactPhoneticMiddleNameKey,
                              CNContactPhoneticFamilyNameKey,
                              CNContactOrganizationNameKey,
                              CNContactDepartmentNameKey,
                              CNContactJobTitleKey,
                              CNContactBirthdayKey,
                              CNContactNonGregorianBirthdayKey,
                              CNContactNoteKey,
                              CNContactImageDataKey,
                              CNContactThumbnailImageDataKey,
                              CNContactImageDataAvailableKey,
                              CNContactTypeKey,
                              CNContactPhoneNumbersKey,
                              CNContactEmailAddressesKey,
                              CNContactPostalAddressesKey,
                              CNContactDatesKey,
                              CNContactUrlAddressesKey,
                              CNContactRelationsKey,
                              CNContactSocialProfilesKey,
                              CNContactInstantMessageAddressesKey];
            NSString *containerId = store.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *error;
            NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
            if (error) {
                NSLog(@"error fetching contacts %@", error);
            } else {
                NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
                for (CNContact *cont in [NSMutableArray arrayWithArray:cnContacts]) {
                    ContactManagedObject *contactManagedObject = [self getContactById:cont.identifier];
                    if (contactManagedObject) {
                        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:cont];
                        contactManagedObject.contact = data;
                        NSError *error;
                        [context save:&error];
                    }else{
                        ContactManagedObject *contactManagedObject  = [NSEntityDescription insertNewObjectForEntityForName:@"Contact" inManagedObjectContext:context];
                        contactManagedObject.contactId = cont.identifier;
                        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:cont];
                        contactManagedObject.contact = data;
                        NSError *error;
                        [context save:&error];
                    }
               }
           }
        }        
    }];

}

- (NSMutableArray *)getAllContacts{
    NSMutableArray *allCNContacts = nil;
    if (!allCNContacts) {
        allCNContacts = [[NSMutableArray alloc] init];
        NSError *error;
        NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
        NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
        NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Contact" inManagedObjectContext:context];
        [fetReq setEntity:entityLink];
        NSMutableArray *fetchResults = [[context executeFetchRequest:fetReq error:&error] mutableCopy];
        for (ContactManagedObject *contactManagedObject in fetchResults) {
            CNContact *contact = [NSKeyedUnarchiver unarchiveObjectWithData:contactManagedObject.contact];
            if (contact) {
                [allCNContacts addObject:contact];
            }
        }
    }
    return allCNContacts;
}

- (NSMutableArray *)getAllCoreDataContactsContacts{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Contact" inManagedObjectContext:context];
    [fetReq setEntity:entityLink];
    return [[context executeFetchRequest:fetReq error:&error] mutableCopy];
}

- (ContactManagedObject *)getContactById:(NSString *)contactId{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Contact" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"contactId == %@", contactId];
    [fetReq setPredicate:predicate];
    [fetReq setEntity:entityLink];
    NSMutableArray *fetchResults = [[context executeFetchRequest:fetReq error:&error] mutableCopy];
    if (fetchResults && fetchResults.count) {
        return fetchResults[0];
    }else{
        return nil;
    }
}

- (void)syncAllContactsWithParse{
    NSArray *allParseContacts = [ContactPFObject getAllContactsFromParse];
    for (ContactPFObject *contactPFObject in allParseContacts) {
        ContactManagedObject *contactManagedObject = [self getContactById:[contactPFObject getId]];
        if (!contactManagedObject) {
            [self createNewContactWithParseObject:contactPFObject];
        }else{
            [self updateContact:contactManagedObject withParseObject:contactPFObject];
        }
    }
    NSArray *allCoreDataContacts = [self getAllCoreDataContactsContacts];
    for (ContactManagedObject *contactManagedObject in allCoreDataContacts) {
        ContactPFObject *contactPFObject = [ContactPFObject getContactById:contactManagedObject.contactId];
        if (!contactPFObject) {
            [self createNewContactWithCoreDataObject:contactManagedObject];
        }else{
            [self updateContact:contactPFObject withCoreDataObject:contactManagedObject];
        }
    }
}

- (void)createNewContactWithParseObject:(ContactPFObject *)contactPFObject{
    //TODO
}

- (void)updateContact:(ContactManagedObject *)contactManagedObject withParseObject:(ContactPFObject *)contactPFObject{
    //TODO
}

- (void)createNewContactWithCoreDataObject:(ContactManagedObject *)contactManagedObject{
//    NSArray *notesArray = [Note getObjectsWithContactId:contactManagedObject.contactId];
//    ContactPFObject *contactObjectToSave = [[ContactPFObject alloc] initWithClassName:@"Contact"];
//    [contactObjectToSave setId:contactManagedObject.contactId];
//    PFFile *file = [PFFile fileWithData:contactManagedObject.contact];
//    [file saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
//        if (succeeded) {
//            [contactObjectToSave setContact:file];
//            [contactObjectToSave setUserId:[PFUser currentUser].objectId];
//            [contactObjectToSave saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
//                if (succeeded) {
//                    PFRelation *notesRelation = [contactObjectToSave relationForKey:@"notes"];
//                    for(NoteMenagedObject *noteManagedObject in notesArray){
//                        NotePFObject *notePFObject = [NotePFObject getNoteById:noteManagedObject.object_id];
//                        [notesRelation addObject:notePFObject];
//                    }
//                    [contactObjectToSave saveInBackground];
//                }
//            }];
//        }
//    }];
}

- (void)updateContact:(ContactPFObject *)contactPFObject withCoreDataObject:(ContactManagedObject *)contactManagedObject{
//    //TODO
//    NSArray *notesArray = [Note getObjectsWithContactId:contactManagedObject.contactId];
//    PFFile *file = [PFFile fileWithData:contactManagedObject.contact];
//    [file saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
//        if (succeeded) {
//            [contactPFObject setContact:file];
//            [contactPFObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
//                if (succeeded) {
//                    PFRelation *notesRelation = [contactPFObject relationForKey:@"notes"];
//                    for(NoteMenagedObject *noteManagedObject in notesArray){
//                        NotePFObject *notePFObject = [NotePFObject getNoteById:noteManagedObject.object_id];
//                        [notesRelation addObject:notePFObject];
//                    }
//                    [contactPFObject saveInBackground];
//                }
//            }];
//        }
//    }];
}

@end

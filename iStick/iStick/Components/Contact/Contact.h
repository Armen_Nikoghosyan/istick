#import <Foundation/Foundation.h>
#import "ContactManagedObject.h"
#import "ContactPFObject.h"

@interface Contact : NSObject
+ (instancetype)sharedInstance;
- (void)syncAllContacts;
- (NSMutableArray *)getAllContacts;
- (ContactManagedObject *)getContactById:(NSString *)contactId;
- (void)syncAllContactsWithParse;
- (NSMutableArray *)getAllCoreDataContactsContacts;

@end

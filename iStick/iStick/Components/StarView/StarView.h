#import <UIKit/UIKit.h>

@protocol StarViewDelegate <NSObject>

- (void)starCountDidChange:(NSInteger)count;

@end

@interface StarView : UIView
@property (nonatomic, weak) id<StarViewDelegate> delegate;
- (void)setStarCount:(NSInteger)count;
@end

#import "StarView.h"

@implementation StarView{
    int count;
}

- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        [self configure];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self configure];
    }
    return self;
}

- (void)configure{
    CGFloat width = self.frame.size.width/4;
    for (int i = 0; i < 4; ++i) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(i * width, 0, width, self.frame.size.height)];
        [btn setImage:[[UIImage imageNamed:@"star_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        [btn setTag:i];
        [btn setTintColor:[UIColor colorWithWhite:1 alpha:0.9]];
        [btn addTarget:self action:@selector(didSelectStar:) forControlEvents:UIControlEventTouchUpInside];
        //[btn addShadowToView];
        [self addSubview:btn];
    }
    
}

- (void)didSelectStar:(UIButton *)sender{
    [self setStarCount:sender.tag];
    [_delegate starCountDidChange:sender.tag];
}

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    [self changeFrame:frame];

}

- (void)changeFrame:(CGRect)frame{
    CGFloat width = frame.size.width/5<self.frame.size.height?frame.size.width/5:self.frame.size.height;
    CGFloat diferance = (frame.size.width - 4 * (width + 4))/2;
    for (UIButton *btn in self.subviews) {
        [btn setFrame:CGRectMake( 2 + btn.tag * (width + 4) + diferance, 0, width, width)];
    }
}

- (void)setStarCount:(NSInteger)count{
    for (UIButton *btn in self.subviews) {
        if (btn.tag <= count) {
            [btn setTintColor:[UIColor colorWithRed:1 green:1 blue:0 alpha:0.9]];
        }else{
            [btn setTintColor:[UIColor colorWithWhite:1 alpha:0.9]];
        }
    }
}

- (void)changeStarCount:(NSUInteger)count{
    for (UIButton *btn in self.subviews) {

        if (count == 0 && (btn.tag == 0 && [btn.tintColor isEqual:[UIColor yellowColor]])) {
            [btn setTintColor:[UIColor whiteColor]];

        }else{
            if (btn.tag <= count) {
                [btn setTintColor:[UIColor yellowColor]];
            }else{
                [btn setTintColor:[UIColor whiteColor]];
            }
        }
    }
}


@end

#import <UIKit/UIKit.h>

@protocol ColorCollectionViewDelegate <NSObject>

- (void)collectionView:(UICollectionView *)collectionView didSelectObjectAtIndex:(NSInteger)index;

@end

@interface ColorCollectionViewController : UICollectionViewController
@property (nonatomic, strong) NSArray *colorsArray;
@property (nonatomic, strong) UIColor *currentColor;
@property (nonatomic, weak) id<ColorCollectionViewDelegate> delegate;

@end

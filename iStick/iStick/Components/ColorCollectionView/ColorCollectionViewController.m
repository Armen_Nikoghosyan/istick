#import "ColorCollectionViewController.h"
#import "ColorCollectionViewCell.h"

@interface ColorCollectionViewController (){
    NSInteger lastIndex;
}

@end

@implementation ColorCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    lastIndex = -1;
    [self.collectionView registerClass:[ColorCollectionViewCell class] forCellWithReuseIdentifier:@"ColorCollectionViewCell"];
    UINib *nib = [UINib nibWithNibName:@"ColorCollectionViewCell" bundle:nil];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:@"ColorCollectionViewCell"];
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    BOOL happen = NO;
    for (UIColor *color in _colorsArray) {
        if ([color isEqual:_currentColor]){
            happen = YES;
        }
    }
    if (!happen) {
        _currentColor = _colorsArray[0];
        [self.collectionView reloadData];
    }
    return _colorsArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ColorCollectionViewCell *cell = (ColorCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ColorCollectionViewCell" forIndexPath:indexPath];
    [cell.contentView setBackgroundColor:_colorsArray[indexPath.row]];
    cell.contentView.layer.masksToBounds = NO;
    cell.contentView.layer.borderColor = [UIColor colorWithWhite:0.2 alpha:1].CGColor;
    cell.contentView.layer.borderWidth = 1;
    cell.contentView.layer.cornerRadius = 15;
    if (lastIndex == indexPath.row || ([_currentColor isEqual:_colorsArray[indexPath.row]] && lastIndex == -1)) {
        cell.contentView.layer.borderWidth = 5;
        cell.contentView.layer.cornerRadius = 20;
        if (lastIndex == -1) {
            lastIndex = indexPath.row;
            _currentColor = _colorsArray[indexPath.row];
        }
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (lastIndex == indexPath.row || ([_currentColor isEqual:_colorsArray[indexPath.row]] && lastIndex == -1)) {
        return CGSizeMake(40, 40);
    }
    return CGSizeMake(30, 30);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 10, 0, 10);
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [_delegate collectionView:collectionView didSelectObjectAtIndex:indexPath.row];
    lastIndex = indexPath.row;
    [collectionView reloadData];
}
@end

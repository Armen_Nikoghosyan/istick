#define NOTES @"notesiStick"
#import "AppDelegate.h"
#import "Contact.h"
#import "NSDate+Categories.h"
#import "RNDecryptor.h"
#import "Reachability.h"

@implementation Note
    static NSMutableArray *notesArray;
    static NSMutableArray *oldArray;
    static int syncCount = 0;
    static int loadidImageCount = 0;
    static int colorSortCount = 0;

- (void)encodeWithCoder:(NSCoder *)coder{

}

- (id)initWithCoder:(NSCoder *)coder{
    self = [super init];
    if (self != nil){

    }
    return self;
}

+ (void)updateCoreDataObjectInParse:(NoteMenagedObject *)note{
    if (note.safe.integerValue != 1) {
        PFQuery *query = [NotePFObject query];
        [query whereKey:@"object_id" equalTo:note.object_id];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
            if (!error) {
                NotePFObject *pfObject = (NotePFObject *)object;
                [pfObject saveMenagedObjectToParse:note];
            }else{
                NotePFObject *pfObject = [[NotePFObject alloc] init];
                [pfObject saveMenagedObjectToParse:note];
            }
        }];
    }
}

+ (void)updateCoreDataObject:(NoteMenagedObject *)noteCoreDataObject inParseObject:(NotePFObject *)notePfObject{
    if (noteCoreDataObject.safe.integerValue != 1) {
        
        if(noteCoreDataObject.alarm.integerValue == 1) {
            if([noteCoreDataObject.alarm_at compare:[NSDate date]] == NSOrderedDescending){
                
            }else if ([noteCoreDataObject.alarm_at compare:[NSDate date]] == NSOrderedAscending){
                [Note chnageAlarm:noteCoreDataObject alarm:[NSNumber numberWithInteger:0]];
                noteCoreDataObject.alarm = [NSNumber numberWithInteger:0];
            }
        }
        [notePfObject saveMenagedObjectToParse:noteCoreDataObject];
    }
}

+ (void)changeItems:(NSArray *)items title:(NSString *)title{
    for (NoteMenagedObject *note in  items) {
        [Note chnagetitleInNote:note title:title];
    }
}

+ (void)removeAllObjects{

}

+ (NoteMenagedObject *)getNoteByObject_id:(NSString *)object_id{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"object_id == %@", object_id];
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    [fetReq setEntity:entityLink];
    [fetReq setPredicate:predicate];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    return [fetchResults firstObject];
}

+ (NoteMenagedObject *)getNoteBydate:(NSDate *)date{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyyMMddhhmmss"];
    NSString *strdate=[formatter stringFromDate:date];
    return [Note getNoteByObject_id:strdate];
}

+ (NSArray *)getAllObjects{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"wasDeleted == %i",0];
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES];

    [fetReq setPredicate:predicate];
    [fetReq setEntity:entityLink];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    [fetReq setReturnsObjectsAsFaults:NO];
    notesArray = [fetchResults mutableCopy];
    return notesArray;
}

+ (NSArray *)getAllUndeletedObjects{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"wasDeleted != %i",1];
    [fetReq setPredicate:predicate];
    [fetReq setEntity:entityLink];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    [fetReq setReturnsObjectsAsFaults:NO];
    notesArray = [fetchResults mutableCopy];
    return notesArray;
}

+ (NSArray *)getAllUndeletedObjectsForCategoryName:(NSString *)categoryName{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    NSEntityDescription *entityLink = [NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"wasDeleted != %i && categoryNameUpperCase == %@",1,categoryName.uppercaseString];
    [fetReq setPredicate:predicate];
    [fetReq setEntity:entityLink];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    [fetReq setReturnsObjectsAsFaults:NO];
    notesArray = [fetchResults mutableCopy];
    return notesArray;
}

+ (NSArray *)getAllTitles{
    NSArray *allObjectArray = [Note getAllUndeletedObjects];
    NSArray *allTitles = [allObjectArray valueForKey:@"title"];
    NSMutableArray *returnedArray  = [[NSMutableArray alloc] init];
    for (NSString *string in allTitles) {
        BOOL isHappen = NO;
        for (NSString *existingInArrayString in returnedArray) {
            if ([[string uppercaseString] isEqualToString:[existingInArrayString uppercaseString]]) {
                isHappen = YES;
                break;
            }
        }
        if (!isHappen && ![string isEqualToString:@""]) {
            [returnedArray addObject:string];
        }
    }
    [returnedArray sortUsingSelector:@selector(localizedCompare:)];
    NSInteger size = returnedArray.count;
    NSInteger count = 0;
    for (int i = 0; i < size; ++i) {
        NSString *str = returnedArray[i];
        if([str characterAtIndex:0] >= '0' && [str characterAtIndex:0] <= '9'){
            [returnedArray removeObjectAtIndex:i];
            [returnedArray insertObject:str atIndex:size - 1];
            --i;
            count++;
            if (count + i >=size) {
                return returnedArray;
            }
        }
    }
    return returnedArray;
}

+ (NSArray *)getAllTitlesWithoutCategory{
    NSArray *allObjectArray = [Note getAllUndeletedObjects];
    NSMutableArray *returnedArray  = [[NSMutableArray alloc] init];
    for (NoteMenagedObject *obj in allObjectArray) {
        BOOL isHappen = NO;
        NSString *title = obj.title;
        if (![obj.categoryName isEqualToString:@""] && obj.categoryName != nil) {
            isHappen = YES;
            break;
        }
        for (NSString *existingInArrayString in returnedArray) {
            if ([[title uppercaseString] isEqualToString:[existingInArrayString uppercaseString]]) {
                isHappen = YES;
                break;
            }
        }
        if (!isHappen && ![title isEqualToString:@""]) {
            [returnedArray addObject:title];
        }
    }
    [returnedArray sortUsingSelector:@selector(localizedCompare:)];
    NSInteger size = returnedArray.count;
    NSInteger count = 0;
    for (int i = 0; i < size; ++i) {
        NSString *str = returnedArray[i];
        if([str characterAtIndex:0] >= '0' && [str characterAtIndex:0] <= '9'){
            [returnedArray removeObjectAtIndex:i];
            [returnedArray insertObject:str atIndex:size - 1];
            --i;
            count++;
            if (count + i >=size) {
                return returnedArray;
            }
        }
    }
    return returnedArray;
}

+ (NSArray *)getAllTitlesForCategoryName:(NSString *)categoryName{
    NSArray *allObjectArray = [Note getAllUndeletedObjectsForCategoryName:categoryName];
    NSArray *allTitles = [allObjectArray valueForKey:@"title"];
    NSMutableArray *returnedArray  = [[NSMutableArray alloc] init];
    for (NSString *string in allTitles) {
        BOOL isHappen = NO;
        for (NSString *existingInArrayString in returnedArray) {
            if ([[string uppercaseString] isEqualToString:[existingInArrayString uppercaseString]]) {
                isHappen = YES;
                break;
            }
        }
        if (!isHappen && ![string isEqualToString:@""]) {
            [returnedArray addObject:string];
        }
    }
    [returnedArray sortUsingSelector:@selector(localizedCompare:)];
    NSInteger size = returnedArray.count;
    NSInteger count = 0;
    for (int i = 0; i < size; ++i) {
        NSString *str = returnedArray[i];
        if([str characterAtIndex:0] >= '0' && [str characterAtIndex:0] <= '9'){
            [returnedArray removeObjectAtIndex:i];
            [returnedArray insertObject:str atIndex:size - 1];
            --i;
            count++;
            if (count + i >=size) {
                return returnedArray;
            }
        }
    }
    return returnedArray;
}

+ (NSString *)getCategoryForTitle:(NSString *)title{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    NSEntityDescription *entityLink = [NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"title == %@",title];
    [fetReq setPredicate:predicate];
    [fetReq setEntity:entityLink];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    [fetReq setReturnsObjectsAsFaults:NO];
    if (fetchResults.count > 0) {
        NoteMenagedObject *note = fetchResults[0];
        return note.categoryName;
    }
    return @"";
}

+ (BOOL)isTitle:(NSString *)title inCategory:(NSString *)category{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"title == %@ && categoryNameUpperCase == %@",title,category.uppercaseString];
    [fetReq setPredicate:predicate];
    [fetReq setEntity:entityLink];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    [fetReq setReturnsObjectsAsFaults:NO];
    if (fetchResults.count > 0) {
        return YES;
    }
    return NO;
}

+ (NSArray *)getAllCategories{
    NSArray *allObjectArray = [Note getAllUndeletedObjects];
    NSMutableArray *allTitles = [allObjectArray valueForKey:@"categoryName"];
    NSArray *categories = [[NSUserDefaults standardUserDefaults] arrayForKey:@"CATEGORIES"];
    if (categories) {
        allTitles = [[allTitles arrayByAddingObjectsFromArray:categories] mutableCopy];
    }
    NSMutableArray *returnedArray  = [[NSMutableArray alloc] init];
    for (NSString *string in allTitles) {
        if ([string class] != [NSNull class]) {
            BOOL isHappen = NO;
            for (NSString *existingInArrayString in returnedArray) {
                if ([[string uppercaseString] isEqualToString:[existingInArrayString uppercaseString]]) {
                    isHappen = YES;
                    break;
                }
            }
            if (!isHappen && ![string isEqualToString:@""]) {
                [returnedArray addObject:string];
            }
        }
    }
    [returnedArray sortUsingSelector:@selector(localizedCompare:)];
    NSInteger size = returnedArray.count;
    NSInteger count = 0;
    for (int i = 0; i < size; ++i) {
        NSString *str = returnedArray[i];
        if([str characterAtIndex:0] >= '0' && [str characterAtIndex:0] <= '9'){
            [returnedArray removeObjectAtIndex:i];
            [returnedArray insertObject:str atIndex:size - 1];
            --i;
            count++;
            if (count + i >=size) {
                return returnedArray;
            }
        }
    }
    return returnedArray;
}

+ (NSArray *)getAllObjectsUsingSortDescriptor:(SortDescriptortype)type  fromArchive:(NSNumber *)fromArchive withTitle:(NSString *)title{
    if(type == SortDescriptortypeColor){
        
        NSArray *array1 = [self getAllObjectsWhereColorIndex:colorSortCount%5 isEqual:NO fromArchive:fromArchive withTitle:title];
        NSArray *array2 = [self getAllObjectsWhereColorIndex:colorSortCount%5 isEqual:YES fromArchive:fromArchive withTitle:title];
        ++colorSortCount;
        return [array1 arrayByAddingObjectsFromArray:array2];

    }else{
        NSError *error;
        NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
        NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
        NSPredicate *predicate;
        if ([title isEqualToString:@""]) {
            if ([fromArchive integerValue] == 1) {
                predicate = [NSPredicate predicateWithFormat: @"wasDeleted == %i",1];
            }else{
                predicate = [NSPredicate predicateWithFormat: @"wasDeleted == %i",0];
            }
        }else{
            if ([fromArchive integerValue] == 1) {
                predicate = [NSPredicate predicateWithFormat: @"wasDeleted == %i  && title ==[c] %@",1,title];
            }else{
                predicate = [NSPredicate predicateWithFormat: @"wasDeleted != %i  && title ==[c] %@",1,title];
            }
        }
        NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
        NSSortDescriptor *sortDescriptor;
        if (type == SortDescriptortypeViewDate) {
            sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"view_at" ascending:YES];
        }else{
            sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES];
        }
        [fetReq setEntity:entityLink];
        [fetReq setPredicate:predicate];
        [fetReq setSortDescriptors:@[sortDescriptor]];
        NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
        [fetReq setReturnsObjectsAsFaults:NO];
        notesArray = [fetchResults mutableCopy];
        if (fetchResults.count>0) {
            if (type == SortDescriptortypeName) {
                NSArray *sortedArray = [fetchResults sortedArrayUsingComparator:
                                        ^NSComparisonResult(NoteMenagedObject *not1, NoteMenagedObject *not2) {
                                            
                                            if ([not1.title isEqualToString:@""] || [not2.title isEqualToString:@""]) {
                                                return [not1.title compare:not2.title];

                                            }
                                            return [[not2.title uppercaseString] compare:[not1.title uppercaseString]];
                                        }
                                        ];
                return sortedArray;
            }
            else if(type == SortDescriptortypePriority){
                NSArray *sortedArray = [fetchResults sortedArrayUsingComparator:
                                        ^NSComparisonResult(NoteMenagedObject *not1, NoteMenagedObject *not2) {
                                            
                                            return [not2.star_count doubleValue] < [not1.star_count doubleValue];
                                        }];
                return sortedArray;
            }else if (type == SortDescriptortypePossition){
                NSArray *sortedArray = [fetchResults sortedArrayUsingComparator:
                                        ^NSComparisonResult(NoteMenagedObject *not1, NoteMenagedObject *not2) {
                                            
                                            return [not2.priority doubleValue] < [not1.priority doubleValue];
                                        }];
                return sortedArray;
            }
//            else if(type == SortDescriptortypeTime){
//                NSArray *sortedArray = [fetchResults sortedArrayUsingComparator:
//                                        ^NSComparisonResult(NoteMenagedObject *not1, NoteMenagedObject *not2) {
//                                            
//                                            return [not2.time_sort_number doubleValue] < [not1.time_sort_number doubleValue];
//                                        }];
//                return sortedArray;
//            }
            
            return [[NSMutableArray alloc] initWithArray:fetchResults];
            
        }else{
            return nil;
        }
        return notesArray;
    }
}

+ (NSArray *)getAllObjectsWhereColorIndex:(int)index isEqual:(BOOL)isEqual fromArchive:(NSNumber *)fromArchive withTitle:(NSString *)title{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSPredicate *predicate;
    if (isEqual) {
        if ([title isEqualToString:@""]) {
            if ([fromArchive integerValue] == 1) {
                predicate = [NSPredicate predicateWithFormat: @"color_index == %i && wasDeleted == %i",index,1];
            }else{
                predicate = [NSPredicate predicateWithFormat: @"color_index == %i && wasDeleted == %i",index,0];
            }
        }else{
            if ([fromArchive integerValue] == 1) {
                predicate = [NSPredicate predicateWithFormat: @"color_index == %i && wasDeleted == %i && title ==[c] %@",index,1,title];
            }else{
                predicate = [NSPredicate predicateWithFormat: @"color_index == %i && wasDeleted != %i && title ==[c] %@",index,1,title];
            }
        }
    }else{
        if ([title isEqualToString:@""]) {
            if ([fromArchive integerValue] == 1) {
                predicate = [NSPredicate predicateWithFormat: @"color_index != %i && wasDeleted == %i",index,1];
            }else{
                predicate = [NSPredicate predicateWithFormat: @"color_index != %i && wasDeleted == %i",index,0];
            }        }else{
            if ([fromArchive integerValue] == 1) {
                predicate = [NSPredicate predicateWithFormat: @"color_index != %i && wasDeleted == %i && title ==[c] %@",index,1,title];
            }else{
                predicate = [NSPredicate predicateWithFormat: @"color_index != %i && wasDeleted != %i && title ==[c] %@",index,1,title];
            }
        }
    }
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES];
    [fetReq setEntity:entityLink];
    [fetReq setPredicate:predicate];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    NSArray *sortedArray = [fetchResults sortedArrayUsingComparator:
                            ^NSComparisonResult(NoteMenagedObject *not1, NoteMenagedObject *not2) {
                                return [not2.color_index intValue] > [not1.color_index intValue];

                            }];
    return sortedArray;
}

+ (NSArray *)getAllObjectsWhereColorIndex:(int)index isEqual:(BOOL)isEqual withFolderName:(NSString *)name{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSPredicate *predicate;
    if (isEqual) {
        predicate = [NSPredicate predicateWithFormat: @"color_index == %i && fileName ==[c] %@ && wasDeleted == 0",index,name];
        
    }else{
        predicate = [NSPredicate predicateWithFormat: @"color_index != %i && fileName ==[c] %@ && wasDeleted == 0",index,name];
        
    }
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES];
    [fetReq setEntity:entityLink];
    [fetReq setPredicate:predicate];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    NSArray *sortedArray = [fetchResults sortedArrayUsingComparator:
                            ^NSComparisonResult(NoteMenagedObject *not1, NoteMenagedObject *not2) {
                                return [not2.color_index intValue] > [not1.color_index intValue];
                                
                            }];
    return sortedArray;
    
}

+ (NoteMenagedObject *)getNextObject:(NSDate *)date{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"created_at < %@ && wasDeleted == 0", date];
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES];
    [fetReq setEntity:entityLink];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    [fetReq setPredicate:predicate];
    
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    notesArray = [fetchResults mutableCopy];
    if (fetchResults.count>0) {
        return fetchResults[fetchResults.count - 1];
        
    }else{
        return nil;
    }
}

+ (NoteMenagedObject *)getFirstNote{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"wasDeleted == 0"];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES];
    [fetReq setEntity:entityLink];
    [fetReq setPredicate:predicate];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    [fetReq setReturnsObjectsAsFaults:NO];
    notesArray = [fetchResults mutableCopy];
    if (notesArray.count) {
        return notesArray[0];
        
    }
    return nil;
}

+ (NoteMenagedObject *)getLastNote{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"wasDeleted == 0"];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES];
    [fetReq setEntity:entityLink];
    [fetReq setPredicate:predicate];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    [fetReq setReturnsObjectsAsFaults:NO];
    notesArray = [fetchResults mutableCopy];
    if (notesArray.count) {
        return notesArray[notesArray.count - 1];

    }
    return nil;
}

+ (NoteMenagedObject *)getFirstNoteWithFolderName:(NSString *)name{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"wasDeleted == 0 && fileName ==[c] %@",name];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES];
    [fetReq setEntity:entityLink];
    [fetReq setPredicate:predicate];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    [fetReq setReturnsObjectsAsFaults:NO];
    notesArray = [fetchResults mutableCopy];
    if (notesArray.count) {
        return notesArray[0];
        
    }
    return nil;
}

+ (NoteMenagedObject *)getLastNoteWithFolderName:(NSString *)name{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"wasDeleted == 0 && fileName ==[c] %@",name];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES];
    [fetReq setEntity:entityLink];
    [fetReq setPredicate:predicate];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    [fetReq setReturnsObjectsAsFaults:NO];
    notesArray = [fetchResults mutableCopy];
    if (notesArray.count) {
        return notesArray[notesArray.count - 1];
        
    }
    return nil;
}

+ (NoteMenagedObject *)getPrevObject:(NSDate *)date{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"created_at > %@ && wasDeleted == 0", date];
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES];
    [fetReq setEntity:entityLink];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    [fetReq setPredicate:predicate];
    
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    notesArray = [fetchResults mutableCopy];
    if (fetchResults.count>0) {
        return fetchResults[0];
        
    }else{
        return nil;
    }
}

+ (NoteMenagedObject *)getNextObject:(NSDate *)date withTitle:(NSString *)name{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"created_at < %@ && wasDeleted == 0 && title ==[c] %@", date,name];
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES];
    [fetReq setEntity:entityLink];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    [fetReq setPredicate:predicate];
    
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    notesArray = [fetchResults mutableCopy];
    if (fetchResults.count>0) {
        return fetchResults[fetchResults.count - 1];
        
    }else{
        return nil;
    }
}

+ (NoteMenagedObject *)getPrevObject:(NSDate *)date withTitle:(NSString *)name{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"created_at > %@ && wasDeleted == 0 && title ==[c] %@", date,name];
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES];
    [fetReq setEntity:entityLink];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    [fetReq setPredicate:predicate];
    
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    return fetchResults.firstObject;
    
    
    
    notesArray = [fetchResults mutableCopy];
    if (fetchResults.count>0) {
        return fetchResults[0];
        
    }else{
        if ([self getNoteBydate:date]) {
            return nil;
        }
        [Note getNoteByObject_id:@""];
        return [Note getFirstNoteWithTitle:name];
    }
}

+ (NoteMenagedObject *)getFirstNoteWithTitle:(NSString *)name{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"wasDeleted == 0 && title ==[c] %@",name];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES];
    [fetReq setEntity:entityLink];
    [fetReq setPredicate:predicate];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    [fetReq setReturnsObjectsAsFaults:NO];
    notesArray = [fetchResults mutableCopy];
    if (notesArray.count) {
        return notesArray[0];
        
    }
    return nil;
}

+ (NoteMenagedObject *)getLastNoteWithTitle:(NSString *)name{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"wasDeleted == 0 && title ==[c] %@",name];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:NO];
    [fetReq setEntity:entityLink];
    [fetReq setPredicate:predicate];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    [fetReq setReturnsObjectsAsFaults:NO];
    notesArray = [fetchResults mutableCopy];
    return [notesArray firstObject];
}

+ (NoteMenagedObject *)getNextObject:(NSDate *)date withFolderName:(NSString *)name{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"created_at > %@ && wasDeleted == 0 && fileName ==[c] %@", date,name];
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES];
    [fetReq setEntity:entityLink];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    [fetReq setPredicate:predicate];
    
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    notesArray = [fetchResults mutableCopy];
    return [notesArray firstObject];
}

+ (NoteMenagedObject *)getPrevObject:(NSDate *)date withFolderName:(NSString *)name{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"created_at < %@ && wasDeleted == 0 && fileName ==[c] %@", date,name];
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES];
    [fetReq setEntity:entityLink];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    [fetReq setPredicate:predicate];
    
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    notesArray = [fetchResults mutableCopy];
    if (fetchResults.count>0) {
        return fetchResults[fetchResults.count - 1];
        
    }else{
        return nil;
    }
}

+ (NSArray *)getFavorites{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"wasDeleted == 0 && star_count > 0"];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES];
    [fetReq setEntity:entityLink];
    [fetReq setPredicate:predicate];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    [fetReq setReturnsObjectsAsFaults:NO];
    return [fetchResults mutableCopy];
}

+ (NSArray *)getPins{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"wasDeleted == 0 && pin_count > 0"];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"pin_count" ascending:YES];
    [fetReq setEntity:entityLink];
    [fetReq setPredicate:predicate];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    [fetReq setReturnsObjectsAsFaults:NO];
    return [fetchResults mutableCopy];
}

+ (NSArray *)getOpened{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"wasDeleted == 0 && opened > 0"];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"opened" ascending:YES];
    [fetReq setEntity:entityLink];
    [fetReq setPredicate:predicate];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    [fetReq setReturnsObjectsAsFaults:NO];
    return [fetchResults mutableCopy];
}

+ (NSArray *)getObjectsWithContactId:(NSString *)contactId{
    NSSet*set = [[[Contact sharedInstance] getContactById:contactId] valueForKey:@"notes"];
    return [set allObjects];
}

+ (NSDate *)getOldestObjectDate{
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
    
    NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES];
    [fetReq setEntity:entityLink];
    [fetReq setSortDescriptors:@[sortDescriptor]];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    [fetReq setReturnsObjectsAsFaults:NO];

    if (fetchResults && fetchResults.count) {
        NoteMenagedObject *oldestNote = [fetchResults firstObject];
        if (oldestNote.created_at) {
            return oldestNote.created_at;
        }
    }
    
    return  [NSDate getPrevYear:[NSDate new]];

}

+ (void)removeObjectFinally:(NoteMenagedObject *)note{
    PFQuery *query = [NotePFObject query];
    [query whereKey:@"object_id" equalTo:note.object_id];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
        if (!error) {
            NotePFObject *parseNote = (NotePFObject *)object;
            //[parseNote deleteEventually];
        }
    }];
    //[[APPDELEGATE managedObjectContext] deleteObject:note];
    [LKAlertView showAlertWithTitleInDevMode:@"DELETE !!!!" message:@"" cancelButtonTitle:@"OK" otherButtonTitles:nil completion:nil];
    NSError *error = nil;
    if (![[APPDELEGATE managedObjectContext] save:&error]){
        NSLog(@"Error deleting note, %@", [error userInfo]);
    }
}

+ (void)removeObjectsFinally:(NSArray *)objects{
    for (NoteMenagedObject * note in objects) {
        [Note removeObjectFinally:note];
    }
}

+ (void)removeObject:(NoteMenagedObject *)note{
    if ([note.wasDeleted integerValue] == 1) {
        [Note removeObjectFinally:note];
    }else{
        PFQuery *query = [NotePFObject query];
        [query whereKey:@"object_id" equalTo:note.object_id];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
            if (!error) {
                NotePFObject *parseNote = (NotePFObject *)object;
                parseNote.wasDeleted = [NSNumber numberWithInt:1];
                [parseNote saveEventually];
            }
        }];
        note.wasDeleted = [NSNumber numberWithInt:1];
        NSError *error = nil;
        if (![[APPDELEGATE managedObjectContext] save:&error]){
            NSLog(@"Error deleting note, %@", [error userInfo]);
        }
    }
}

+ (void)removeObjectNotAll:(NoteMenagedObject *)note{
    if ([note.wasDeleted integerValue] == 1) {
        [Note removeObjectFinally:note];
    }else{
        PFQuery *query = [NotePFObject query];
        [query whereKey:@"object_id" equalTo:note.object_id];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
            if (!error) {
                NotePFObject *parseNote = (NotePFObject *)object;
                parseNote.wasDeleted = [NSNumber numberWithInt:2];
                [parseNote saveEventually];
            }
        }];
        note.wasDeleted = [NSNumber numberWithInt:2];
        NSError *error = nil;
        if (![[APPDELEGATE managedObjectContext] save:&error]){
            NSLog(@"Error deleting note, %@", [error userInfo]);
        }
    }
}

+ (void)updateParseObjectToCoreData:(NotePFObject *)object{
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NoteMenagedObject *note = [Note getNoteByObject_id:object.getObject_id];
    note.updated_at = [object getUpdatedAt];
    note.view_at = [object viewAt];
    note.wasDeleted = object.wasDeleted;
    note.color_index = object.getColorIndex;
    note.line_color_index = object.getLineColorIndex;
    note.time_sort_number = [NSNumber numberWithFloat:object.time_sort_number];
    note.version = object.version;
    note.star_count = object.star_count;
    note.pin_count = object.pin_count;
    note.fileName = object.getFileName;
    note.name = object.getName;
    note.title = object.getTitle;
    note.textY = object.getTextY;
    note.priority = object.priority;
    note.categoryNameUpperCase = object.getCategoryNameUpperCase;
    note.categoryName = object.getCategoryName;
    note.phoneNumber = object.getPhoneNumber;
    note.isInCue = object.getIsInCue;
    note.alarm_at = object.getAlarmAt;
    note.alarm = object.alarm;
    note.document = object.getDocument;
    note.gallery = object.getGallery;
    
   
    
    [object.getImage getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if (!error) {
            NSData *imgData = data;//[Note decodeData:data];// data;//[Note decodeData:data];
            note.image = imgData;
            NSError *error;
            if (![context save:&error]) {

                NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
            }else{
                 [Note updateAlarmForNote:note];
//                NSDate *date = note.created_at;
//                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//                [formatter setDateFormat:@"ss mm HH yy MM dd"];
//                [formatter setTimeZone:[NSTimeZone defaultTimeZone]];
//                NSString *stringFromDate = [formatter stringFromDate:date];
//                UIImage *image = [UIImage imageWithData:data];
            }
        }else{
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
            }
        }
        loadidImageCount++;
        if (loadidImageCount == syncCount) {
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"NOTES_DID_SYNC" object:nil];
            loadidImageCount = syncCount = 0;
            
        }
    }];
//    [object.getBackgroundImage getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
//        if (!error) {
//            NSData *imgData = data;
//            note.backgroundImage = imgData;
//            NSError *error;
//            if (![context save:&error]) {
//                NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
//            }
//        }else{
//            NSError *error;
//            if (![context save:&error]) {
//                NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
//            }
//        }
//    }];
}

+ (void)saveParseObjectToCoreData:(NotePFObject *)object{
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NoteMenagedObject *noteMenagedObject = [NSEntityDescription insertNewObjectForEntityForName:@"Note" inManagedObjectContext:context];
    noteMenagedObject.created_at = object.getCreatedAt;
    noteMenagedObject.updated_at = object.updatedAt;
    noteMenagedObject.view_at = object.viewAt;
    noteMenagedObject.color_index = object.getColorIndex;
    noteMenagedObject.line_color_index = object.getLineColorIndex;
    noteMenagedObject.name = object.getName;
    noteMenagedObject.object_id = object.getObject_id;
    noteMenagedObject.title = object.getTitle;
    noteMenagedObject.priority = object.priority;
    noteMenagedObject.star_count = object.star_count;
    noteMenagedObject.pin_count = object.pin_count;
    noteMenagedObject.time_sort_number = [NSNumber numberWithFloat:object.time_sort_number];
    noteMenagedObject.version = object.version;
    noteMenagedObject.isInParse = [NSNumber numberWithInt:4];
    noteMenagedObject.wasDeleted = object.wasDeleted;
    noteMenagedObject.fileName = object.getFileName;
    noteMenagedObject.categoryNameUpperCase = object.getCategoryNameUpperCase;
    noteMenagedObject.categoryName = object.getCategoryName;
    noteMenagedObject.isInCue = object.getIsInCue;
    noteMenagedObject.alarm_at = object.getAlarmAt;
    noteMenagedObject.alarm = object.alarm;
    noteMenagedObject.document = object.getDocument;
    noteMenagedObject.gallery = object.getGallery;
    
    
    
    if (object.getImage) {
        [object.getImage getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if (!error) {
                NSData *imgData = data;// [Note decodeData:data];// data;//[Note decodeData:data];
                noteMenagedObject.image = imgData;
                NSError *error;
                if (![context save:&error]) {
                    NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
                } else {
                    [Note updateAlarmForNote:noteMenagedObject];
                }
            }else{
                NSError *error;
                if (![context save:&error]) {
                    NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
                }
            }
            loadidImageCount++;
            if (loadidImageCount == syncCount) {
               // [[NSNotificationCenter defaultCenter] postNotificationName:@"NOTES_DID_SYNC" object:nil];
                loadidImageCount = syncCount = 0;
                
            }
        }];

    }else{
        NSLog(@"Whoops, image is NULL");

    }
   //    [object.getBackgroundImage getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
//        if (!error) {
//            NSData *imgData = data;
//            noteMenagedObject.backgroundImage = imgData;
//            NSError *error;
//            if (![context save:&error]) {
//                NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
//            }
//        }else{
//            NSError *error;
//            if (![context save:&error]) {
//                NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
//            }
//        }
//    }];
}


+(void)updateAlarmForNote:(NoteMenagedObject*)note {
    
    if(note.alarm.integerValue == 1) {
        if([note.alarm_at compare:[NSDate date]] == NSOrderedDescending){
            [Note setupNotificationForNote:note];
            
        }else if ([note.alarm_at compare:[NSDate date]] == NSOrderedAscending){
            [Note chnageAlarm:note alarm:[NSNumber numberWithInteger:0]];
        }
    }
}



+(void)setupNotificationForNote:(NoteMenagedObject*)note{
    UILocalNotification *notification = [[UILocalNotification alloc]init];
    //notification.repeatInterval = NSDayCalendarUnit;
    [notification setAlertBody:@"Reminder"];
    [notification setFireDate:note.alarm_at];
    [notification setTimeZone:[NSTimeZone  defaultTimeZone]];
    notification.soundName = UILocalNotificationDefaultSoundName;
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyyMMddhhmmss"];
    NSString *strdate = [formatter stringFromDate:note.alarm_at];
    NSMutableArray *scheduledObjects = [[NSUserDefaults standardUserDefaults] objectForKey:@"scheduled_objects_object_id"];
    if (!scheduledObjects || scheduledObjects.count == 0) {
        scheduledObjects = [[NSMutableArray alloc] init];
    }else{
        scheduledObjects = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"scheduled_objects_object_id"]];
    }
    if (![scheduledObjects containsObject:strdate]) {
    [scheduledObjects addObject:strdate];
    [[NSUserDefaults standardUserDefaults] setObject:scheduledObjects forKey:@"scheduled_objects_object_id"];
    NSString * str = [note.object_id isEqualToString:@"0"] ? strdate : note.object_id ;
    notification.userInfo = [NSDictionary dictionaryWithObject:str forKey:@"object_id"];
    //notification.applicationIconBadgeNumber++;
    NSMutableArray *oldNotificationsArray = [[[UIApplication sharedApplication] scheduledLocalNotifications] mutableCopy];
    if (oldNotificationsArray && oldNotificationsArray.count > 0) {
        [oldNotificationsArray addObject:notification];
        [[UIApplication sharedApplication] setScheduledLocalNotifications:oldNotificationsArray];
        
    }else{
        [[UIApplication sharedApplication] setScheduledLocalNotifications:[NSArray arrayWithObject:notification]];
    }
    notification.applicationIconBadgeNumber = [[[UIApplication sharedApplication] scheduledLocalNotifications] count] + 1;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LOCALE_NOTIFICATION_STATE_CHANGED" object:note.alarm_at];
    }
}

+ (NSData *)decodeData:(NSData *)data{
    NSString *aPassword = [[NSUserDefaults standardUserDefaults] objectForKey:@"ENCODEKEY"];
    NSError *error;
    NSData *encryptedData = [RNDecryptor decryptData:data
                                        withPassword:aPassword
                                               error:&error];
    if (!error) {
        return encryptedData;
    }
    return nil;
}

+ (void)syncData{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
    }
    else
    {
        //connection available
        if ([PFUser currentUser]) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                PFRelation *relation = [[PFUser currentUser] relationForKey:@"Notes"];
                PFQuery *query = [relation query];
                [query setLimit:1000];
                [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
                    
                    for (NotePFObject *obj in objects) {
                        NoteMenagedObject *coreDataNote = [Note getNoteByObject_id:[obj getObject_id]];
                        
                        if(!coreDataNote){
                            syncCount ++;
                            [Note saveParseObjectToCoreData:obj];
                            NSLog(@"notes xxx %@",obj.getTitle);
                        }else if ([coreDataNote.version intValue] == [obj.version intValue]){
                            NSLog(@"notes have the same VERSION NUMBER %@",obj.getTitle);
                        }
                        if ([coreDataNote.version intValue] > [obj.version intValue]) {
                            [Note updateCoreDataObject:coreDataNote inParseObject:obj];
                            NSLog(@"parse have LESSSS VERSION NUMBER");
                        } else if ([coreDataNote.version intValue] < [obj.version intValue]) {
                            syncCount ++;
                            [Note updateParseObjectToCoreData:obj];
                            NSLog(@"parse have MORRRRE VERSION NUMBER");
                        } else {
                            
                        }
                    }
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"NOTES_DID_SYNC" object:nil];
                }];
                NSError *error;
                NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
                NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
                [fetReq setReturnsObjectsAsFaults:NO];
                NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
                [fetReq setEntity:entityLink];
                NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
                
                for (NoteMenagedObject *obj in fetchResults) {
                    if (obj.isInParse.integerValue > 3 && obj.object_id) {
                        //                    PFQuery *localeQuery = [NotePFObject query];
                        //                    [localeQuery whereKey:@"object_id" equalTo:obj.object_id];
                        //                    [localeQuery whereKey:@"userId" equalTo:[PFUser currentUser].objectId];
                        //                    [localeQuery getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
                        //                        if (error) {
                        //                            if (error.code == 101) {
                        //                                //                                    [backgroundContext deleteObject:obj];
                        //                                //                                    NSError *error;
                        //                                //                                    [backgroundContext save:&error];
                        //                            }
                        //                        }
                        //                    }];
                    } else if(obj.object_id && obj.safe.integerValue != 1){
                        [Note changeCoreDataObjectIsInParseProparty:obj];
                        PFRelation *relation = [[PFUser currentUser] relationForKey:@"Notes"];
                        PFQuery *localeQuery = [relation query];
                        [localeQuery whereKey:@"object_id" equalTo:obj.object_id];
                        [localeQuery getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
                            if (error) {
                                if (error.code == 101) {
                                    NotePFObject *notePfObject = [[NotePFObject alloc] init];
                                    [notePfObject saveMenagedObjectToParse:obj];
                                }
                            }
                        }];
                    }
                }
                // [[NSNotificationCenter defaultCenter] postNotificationName:@"NOTES_DID_SYNC" object:nil];
            });
        }
    }
  
}

+ (void)changeCoreDataObjectIsInParseProparty:(NoteMenagedObject *)object{
    object.isInParse = [NSNumber numberWithInt:4];
    NSError *error;
    [[APPDELEGATE managedObjectContext] save:&error];
}

//            NSManagedObjectContext *backgroundContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
//            [backgroundContext performBlock:^{
//                backgroundContext.persistentStoreCoordinator = [APPDELEGATE persistentStoreCoordinator];
//                
//                NSError *error = nil;
//                NSFetchRequest *fetReq=[[NSFetchRequest alloc] init];
//                [fetReq setReturnsObjectsAsFaults:NO];
//                NSEntityDescription *entityLink=[NSEntityDescription entityForName:@"Note" inManagedObjectContext:backgroundContext];
//                [fetReq setEntity:entityLink];
//                NSArray *fetchedObjects = [backgroundContext executeFetchRequest:fetReq error:&error];

+ (void)saveAllUnsavedObjectToParse{
    for (NoteMenagedObject *obj in [Note getAllObjects]) {
        if (![obj.isInParse boolValue]) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                if (obj.safe.integerValue != 1) {
                    NotePFObject *notePfObject = [[NotePFObject alloc] init];
                    [notePfObject saveMenagedObjectToParse:obj];
                }
            });
        }
    }
}

+ (void)chnageStarCountInNote:(NoteMenagedObject *)note count:(NSInteger)count{
    note.updated_at = [NSDate new];
    note.view_at = [NSDate new];
    note.version = [NSNumber numberWithInt:[note.version intValue] + 1];
    note.star_count = [NSNumber numberWithInteger:count];
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
    }else{
        if ([PFUser currentUser]) {
            [Note updateCoreDataObjectInParse:note];
        }
    }
}

+ (void)chnagePinCountInNote:(NoteMenagedObject *)note count:(NSNumber *)count{
    note.updated_at = [NSDate new];
    note.view_at = [NSDate new];
    note.opened = count;
    note.pin_count = count;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
    }else{
        
    }
}

+ (void)chnageIsInCueInNote:(NoteMenagedObject *)note count:(NSNumber *)count{
    note.updated_at = [NSDate new];
    note.view_at = [NSDate new];
    note.isInCue = count;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
    }else{
        
    }
}


+ (void)chnageAlarmDate:(NoteMenagedObject *)note date:(NSDate*)date{
    note.alarm_at = date;
     NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
    }else{
        
    }
}
+ (void)chnageAlarm:(NoteMenagedObject *)note alarm:(NSNumber*)alarm{
    note.alarm = alarm;
    note.version = [NSNumber numberWithInt:[note.version intValue] + 1];
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
    }else{
        
    }
}

+ (void)removeContactIFromNote:(NoteMenagedObject *)note{
    note.updated_at = [NSDate new];
    note.view_at = [NSDate new];
    note.contact_id = nil;
    note.phoneNumber = nil;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
    }else{
        
    }
}

+ (void)chnageOpenedInNote:(NoteMenagedObject *)note count:(NSInteger)count{
    note.updated_at = [NSDate new];
    note.view_at = [NSDate new];
    note.pin_count = [NSNumber numberWithInteger:count];
    note.priority = [NSNumber numberWithDouble:[NSDate dateDistanceFrom1970InSecconds:[NSDate new]]];
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
    }else{
        
    }
}

+ (void)chnagetitleInNote:(NoteMenagedObject *)note title:(NSString *)title{
    note.updated_at = [NSDate new];
    note.title = title;
    note.version = [NSNumber numberWithInt:[note.version intValue] + 1];
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
    }else{
        
    }
}

+ (void)chnageCategoryInNote:(NoteMenagedObject *)note category:(NSString *)category{
    note.updated_at = [NSDate new];
    note.categoryName = category;
    note.categoryNameUpperCase = category.uppercaseString;
    note.version = [NSNumber numberWithInt:[note.version intValue] + 1];
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
    }else{
        
    }
}

+ (void)moveAllNotesWithTitle:(NSString *)title tocategory:(NSString *)category{
    if (category != nil && title != nil) {
        NSArray *noteWithThatTitle = [Note getNotes:nil
                                                pin:nil
                                             opened:nil
                                           sortType:SortDescriptortypeCreatedDate
                                           fromDate:nil
                                             toDate:nil
                                        fromArchive:[NSNumber numberWithInt:0]
                                              title:title
                                           category:nil
                                           objectId:nil
                                              phone:nil
                                        fromGallery:nil
                                      fromDocuments:nil];
        for (NoteMenagedObject *note in noteWithThatTitle) {
            [Note chnageCategoryInNote:note category:category];
        }
    }
}

+ (void)getNotesUsingNoteHelperUsingBlock:(void (^)(NSArray *))block{
    if (block) {
        block([Note getNotes:[NoteHelper sharedInstance].favorite
                          pin:[NoteHelper sharedInstance].pin
                       opened:[NoteHelper sharedInstance].opened
                     sortType:[NoteHelper sharedInstance].sortType
                     fromDate:[NoteHelper sharedInstance].fromDate
                       toDate:[NoteHelper sharedInstance].toDate
                  fromArchive:[NoteHelper sharedInstance].fromArchive
                        title:[NoteHelper sharedInstance].title
                     category:[NoteHelper sharedInstance].category
                     objectId:[NoteHelper sharedInstance].objecrtId
                        phone:[NoteHelper sharedInstance].phoneNumber
                  fromGallery:[NoteHelper sharedInstance].gallery
                fromDocuments:[NoteHelper sharedInstance].document]);
    }
}

+ (void)getNotes:(SortDescriptortype)sortType usingNoteHelperUsingBlock:(void (^) (NSArray *notes))block{
    if (block) {
        block([Note getNotes:[NoteHelper sharedInstance].favorite
                         pin:[NoteHelper sharedInstance].pin
                      opened:[NoteHelper sharedInstance].opened
                    sortType:sortType
                    fromDate:[NoteHelper sharedInstance].fromDate
                      toDate:[NoteHelper sharedInstance].toDate
                 fromArchive:[NoteHelper sharedInstance].fromArchive
                       title:[NoteHelper sharedInstance].title
                    category:[NoteHelper sharedInstance].category
                    objectId:[NoteHelper sharedInstance].objecrtId
                       phone:[NoteHelper sharedInstance].phoneNumber
                 fromGallery:[NoteHelper sharedInstance].gallery
               fromDocuments:[NoteHelper sharedInstance].document]);
    }
}


+ (NSArray *)getNotes:(NSNumber *)favorite
                  pin:(NSNumber *)pin
               opened:(NSNumber *)opened
             sortType:(SortDescriptortype)sortType
             fromDate:(NSDate *)fromDate
               toDate:(NSDate *)toDate
          fromArchive:(NSNumber *)fromArchive
                title:(NSString *)title
             category:(NSString *)category
             objectId:(NSString *)objecrtId
                phone:(BOOL)phoneNumber
          fromGallery:(NSNumber *)fromGallery
        fromDocuments:(NSNumber *)fromDocuments{
    NSPredicate *archivePredicate = [NSPredicate predicateWithFormat: @"wasDeleted == %i",[fromArchive integerValue]];
    NSMutableArray *predicatesArray = [@[archivePredicate] mutableCopy];
    NSPredicate *pinPredicate = nil;
    NSPredicate *openedPredicate = nil;
    NSPredicate *favoritePredicate = nil;
    NSPredicate *titlePredicate = nil;
    NSPredicate *betweenDatePredicate = nil;
    NSPredicate *objecrtIdPredicate = nil;
    NSPredicate *datesPredicate = nil;
    NSPredicate *categoryPredicate = nil;
    NSPredicate *phonePredicate = nil;
    NSPredicate *galletyPredicate = nil;
    NSPredicate *documentPredicate = nil;
    
    if (fromGallery){
        if(fromGallery == [NSNumber numberWithInt:0]) {
            galletyPredicate = [NSPredicate predicateWithFormat:@"gallery == 0"];
            [predicatesArray addObject:galletyPredicate];
        }else {
        galletyPredicate = [NSPredicate predicateWithFormat:@"gallery > 0"];
        [predicatesArray addObject:galletyPredicate];
        }
    } 
    
    if (fromDocuments){
        if(fromDocuments == [NSNumber numberWithInt:0]){
            documentPredicate = [NSPredicate predicateWithFormat:@"document == 0"];
            [predicatesArray addObject:documentPredicate];
        }else {
        documentPredicate = [NSPredicate predicateWithFormat:@"document > 0"];
        [predicatesArray addObject:documentPredicate];
        }
    }

    if (phoneNumber) {
        phonePredicate = [NSPredicate predicateWithFormat:@"isInCue > 0"];
        [predicatesArray addObject:phonePredicate];
    }
    if ([pin integerValue] > 0) {
        pinPredicate = [NSPredicate predicateWithFormat: @"pin_count > 0"];
        [predicatesArray addObject:pinPredicate];
    }
    if ([opened integerValue] > 0) {
        openedPredicate = [NSPredicate predicateWithFormat: @"opened > 0"];
        [predicatesArray addObject:openedPredicate];
    }
    if (favorite) {
        favoritePredicate = [NSPredicate predicateWithFormat: @"star_count > 0"];
        [predicatesArray addObject:favoritePredicate];
    }
    if (title && ![title isEqualToString:@""]) {
        titlePredicate = [NSPredicate predicateWithFormat:@"title ==[c] %@",title];
        [predicatesArray addObject:titlePredicate];
    }
    if (category && ![category isEqualToString:@""]) {
        categoryPredicate = [NSPredicate predicateWithFormat:@"categoryNameUpperCase == %@",category.uppercaseString];
        [predicatesArray addObject:categoryPredicate];
    }
    if (fromDate && toDate) {
        betweenDatePredicate = [NSPredicate predicateWithFormat:@"(created_at >= %@) AND (created_at <= %@)", [NSDate dateToMidnight:fromDate], [[NSDate dateToMidnight:toDate] dateByAddingTimeInterval:60*60*24]];
        [predicatesArray addObject:betweenDatePredicate];
    }
    if (objecrtId) {
        objecrtIdPredicate = [NSPredicate predicateWithFormat: @"object_id == %@", objecrtId];
        [predicatesArray addObject:objecrtIdPredicate];
    }
    if (fromDate != nil && toDate != nil) {
        datesPredicate = [NSPredicate predicateWithFormat:@"(created_at >= %@) AND (created_at <= %@)", [NSDate dateToMidnight:fromDate], [[NSDate dateToMidnight:toDate] dateByAddingTimeInterval:60*60*24]];
    }
    NSError *error;
    NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
    NSFetchRequest *fetReq = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityLink = [NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    NSPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicatesArray];
    NSSortDescriptor *sortDescriptor;
    if (sortType == SortDescriptortypeViewDate) {
        sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"view_at" ascending:NO];
    }else if (sortType == SortDescriptortypeCreatedDate) {
        sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:NO];
    }else if (sortType == SortDescriptortypePriority) {
        sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"star_count" ascending:NO];
    }else if (sortType == SortDescriptortypeColor) {
        sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"color_index" ascending:NO];
    }else if (sortType == SortDescriptortypePossition){
        sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"pin_count" ascending:NO];
    }
    if (!oldArray) {
        oldArray = [[NSMutableArray alloc] init];
    }
    if ([NoteHelper sharedInstance].isNewSearch) {
        [oldArray removeAllObjects];
    }
    if (sortDescriptor) {
        [fetReq setSortDescriptors:@[sortDescriptor]];
    }
    [fetReq setEntity:entityLink];
    [fetReq setPredicate:compoundPredicate];
    [fetReq setFetchLimit:20];
    NSUInteger offset = oldArray.count;
    if (!offset) {
        offset = 0;
    }
    [fetReq setFetchOffset:offset];
    NSArray *fetchResults = [context executeFetchRequest:fetReq error:&error];
    [fetReq setReturnsObjectsAsFaults:YES];
    NSArray *newArray = [fetchResults mutableCopy];
    NSSortDescriptor *sortDescriptorForArray;
    sortDescriptorForArray = [[NSSortDescriptor alloc] initWithKey:@"view_at"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptorForArray];
    NSMutableArray *sortedArray = [[newArray sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    [sortedArray addObjectsFromArray:oldArray];
    oldArray =  sortedArray;

//    if (sortType == SortDescriptortypeName) {
//        NSArray *sortedArray = [fetchResults sortedArrayUsingComparator:
//                                ^NSComparisonResult(NoteMenagedObject *not1, NoteMenagedObject *not2) {
//                                    
//                                    if ([not1.title isEqualToString:@""] || [not2.title isEqualToString:@""]) {
//                                        return [not1.title compare:not2.title];
//                                        
//                                    }
//                                    return [[not2.title uppercaseString] compare:[not1.title uppercaseString]];
//                                }
//                                ];
//        return sortedArray;
//    }
//    NSArray *sortedArray = [fetchResults sortedArrayUsingComparator:^NSComparisonResult(NoteMenagedObject *not1, NoteMenagedObject *not2) {
//                return not1.object_id > not2.object_id;
//            }
//            ];

    return oldArray;
}


@end

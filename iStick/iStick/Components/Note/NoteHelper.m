#import "NoteHelper.h"

@implementation NoteHelper
+ (instancetype)sharedInstance{
    static NoteHelper *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance              = [[NoteHelper alloc] init];
        sharedInstance.fromArchive  = [NSNumber numberWithInt:0];
        sharedInstance.title        = nil;
        sharedInstance.favorite     = nil;
        sharedInstance.pin          = nil;
        sharedInstance.fromDate     = sharedInstance.toDate = nil;
        sharedInstance.objecrtId    = nil;
        sharedInstance.sortType     = SortDescriptortypeViewDate;
        sharedInstance.category     = nil;
        sharedInstance.opened       = nil;
        sharedInstance.isNewSearch  = YES;
        sharedInstance.phoneNumber  = NO;
        sharedInstance.gallery  = [NSNumber numberWithInt:0];
        sharedInstance.document  = [NSNumber numberWithInt:0];

    });
    return sharedInstance;
}
@end

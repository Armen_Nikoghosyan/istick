#import <Foundation/Foundation.h>

@interface NoteHelper : NSObject
+ (instancetype)sharedInstance;

@property (nonatomic, strong) NSNumber *favorite;
@property (nonatomic, strong) NSNumber *pin;
@property (nonatomic) BOOL phoneNumber;
@property (nonatomic, strong) NSNumber *opened;
@property (nonatomic) SortDescriptortype sortType;
@property (nonatomic, strong) NSDate *fromDate;
@property (nonatomic, strong) NSDate *toDate;
@property (nonatomic, strong) NSNumber *fromArchive;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *objecrtId;
@property (nonatomic) BOOL isNewSearch;
@property (nonatomic, strong) NSNumber *gallery;
@property (nonatomic, strong) NSNumber *document;


@end

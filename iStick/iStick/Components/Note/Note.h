

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NotePFObject.h"

typedef enum {
    SortDescriptortypeViewDate,
    SortDescriptortypeCreatedDate,
    SortDescriptortypePriority,
    SortDescriptortypeName,
    SortDescriptortypeColor,
    SortDescriptortypePossition,
} SortDescriptortype;


@interface Note : NSObject

+ (NSArray *)getFavorites;
+ (NSArray *)getPins;
+ (NSArray *)getOpened;

- (void)encodeWithCoder:(NSCoder *)encode;
- (id)initWithCoder:(NSCoder *)decode;
+ (void)removeAllObjects;
+ (void)saveAllUnsavedObjectToParse;
+ (void)changeItems:(NSArray *)items title:(NSString *)title;

+ (NSArray *)getObjectsWithContactId:(NSString *)contactId;
+ (NSDate *)getOldestObjectDate;

+ (NSArray *)getAllObjectsUsingSortDescriptor:(SortDescriptortype)type fromArchive:(NSNumber *)fromArchive withTitle:(NSString *)title;
+ (NSArray *)getAllObjects;

+ (NSArray *)getAllTitles;
+ (NSArray *)getAllTitlesForCategoryName:(NSString *)categoryName;
+ (NSArray *)getAllTitlesWithoutCategory;
+ (NSString *)getCategoryForTitle:(NSString *)title;
+ (BOOL)isTitle:(NSString *)title inCategory:(NSString *)category;
+ (NSArray *)getAllCategories;
+ (void)removeObjectNotAll:(NoteMenagedObject *)note;
+ (void)removeObject:(NoteMenagedObject *)note;
+ (void)removeObjectFinally:(NoteMenagedObject *)note;
+ (void)removeObjectsFinally:(NSArray *)objects;
+ (void)updateCoreDataObjectInParse:(NoteMenagedObject *)note;
+ (void)updateCoreDataObject:(NoteMenagedObject *)noteCoreDataObject inParseObject:(NotePFObject *)notePfObject;
+ (NoteMenagedObject *)getNoteByObject_id:(NSString *)object_id;
+ (NoteMenagedObject *)getNextObject:(NSDate *)date;
+ (NoteMenagedObject *)getPrevObject:(NSDate *)date;
+ (NoteMenagedObject *)getNextObject:(NSDate *)date withFolderName:(NSString *)name;
+ (NoteMenagedObject *)getPrevObject:(NSDate *)date withFolderName:(NSString *)name;
+ (NoteMenagedObject *)getFirstNote;
+ (NoteMenagedObject *)getLastNote;
+ (NoteMenagedObject *)getFirstNoteWithFolderName:(NSString *)name;
+ (NoteMenagedObject *)getLastNoteWithFolderName:(NSString *)name;
+ (NoteMenagedObject *)getNextObject:(NSDate *)date withTitle:(NSString *)name;
+ (NoteMenagedObject *)getPrevObject:(NSDate *)date withTitle:(NSString *)name;
+ (NoteMenagedObject *)getFirstNoteWithTitle:(NSString *)name;
+ (NoteMenagedObject *)getLastNoteWithTitle:(NSString *)name;
+ (void)saveParseObjectToCoreData:(NotePFObject *)object;
+ (void)syncData;

+ (void)chnageStarCountInNote:(NoteMenagedObject *)note count:(NSInteger)count;
+ (void)chnagetitleInNote:(NoteMenagedObject *)note title:(NSString *)title;
+ (void)chnageAlarmDate:(NoteMenagedObject *)note date:(NSDate*)date;
+ (void)chnageAlarm:(NoteMenagedObject *)note alarm:(NSNumber*)alarm;
+ (void)chnagePinCountInNote:(NoteMenagedObject *)note count:(NSNumber *)count;
+ (void)chnageIsInCueInNote:(NoteMenagedObject *)note count:(NSNumber *)count;
+ (void)removeContactIFromNote:(NoteMenagedObject *)note;
+ (void)getNotesUsingNoteHelperUsingBlock:(void (^) (NSArray *notes))block;
+ (void)getNotes:(SortDescriptortype)sortType usingNoteHelperUsingBlock:(void (^) (NSArray *notes))block;

+ (void)moveAllNotesWithTitle:(NSString *)title tocategory:(NSString *)category;

+ (NSArray *)getNotes:(NSNumber *)favorite
                  pin:(NSNumber *)pin
                opened:(NSNumber *)opened
             sortType:(SortDescriptortype)sortType
             fromDate:(NSDate *)fromDate
               toDate:(NSDate *)toDate
          fromArchive:(NSNumber *)fromArchive
                title:(NSString *)title
             category:(NSString *)category
             objectId:(NSString *)objecrtId
                phone:(BOOL)phoneNumber;

@end

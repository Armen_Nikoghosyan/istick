#import "drawingView.h"
#import "drawingTools.h"
#import "Note.h"
#import <CoreText/CoreText.h>
#import <UITextView_Placeholder/UITextView+Placeholder.h>
#import <QuartzCore/QuartzCore.h>
#import "UIImage+Resize.h"
#import "DrawingViewManager.h"

@interface DrawingView () {
    CGPoint currentPoint;
    CGPoint previousPoint1;
    CGPoint previousPoint2;
    UITouch *touch;
    BOOL willShowPlaceholder;
    NSLayoutConstraint *constTop;
    NSLayoutConstraint *constLeft;
    NSLayoutConstraint *constWidth;
    NSLayoutConstraint *constHeight;
}

@property (nonatomic, strong) NSMutableArray *pathArray;
@property (nonatomic, strong) NSMutableArray *bufferArray;
@property (nonatomic, strong) id<DrawingTool> currentTool;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, assign) CGFloat originalFrameYPos;

@end

#pragma mark -

@implementation DrawingView


- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        [self configure];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self configure];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    constWidth.constant = self.width;
}

- (void)configure{
    
    // init the private arrays
    self.pathArray = [NSMutableArray array];
    self.bufferArray = [NSMutableArray array];
    
    // set the default values for the public properties
    self.lineColor = kDefaultLineColor;
    self.lineWidth = kDefaultLineWidth;
    self.lineAlpha = kDefaultLineAlpha;

    self.drawMode = DrawingModeOriginalSize;
    
    willShowPlaceholder = YES;
    
    // set the transparent background //
    
    self.backgroundColor = [UIColor clearColor];
    
    self.originalFrameYPos = self.frame.origin.y;
    _textY = 60;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textModeEnd) name:@"TEXT_MODE_END" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(voiceSentText:) name:@"VOICE_SENT_TEXT" object:nil];
}

- (UIImage *)prev_image {
    return self.backgroundImage;
}

- (void)setPrev_image:(UIImage *)prev_image {
    [self setBackgroundImage:prev_image];
}


#pragma mark - Drawing

- (void)drawRect:(CGRect)rect{
    
#if PARTIAL_REDRAW
    [self drawPath];
#else
    switch (self.drawMode) {
        case DrawingModeOriginalSize:
            [self.image drawAtPoint:CGPointZero];
            break;
            
        case DrawingModeScale:
            [self.image drawInRect:self.bounds];
            break;
    }
    [self.currentTool draw];
#endif
}

- (void)commitAndDiscardToolStack{
    
    [self updateCacheImage:YES];
    self.backgroundImage = self.image;
    [self.pathArray removeAllObjects];
}

- (void)updateCacheImage:(BOOL)redraw{
    
    // init a context
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 1.0);
    
    if (redraw) {
        // erase the previous image
        self.image = nil;
        
        // load previous image (if returning to screen)
        
        switch (self.drawMode) {
            case DrawingModeOriginalSize:
                [[self.backgroundImage copy] drawAtPoint:CGPointZero];
                break;
            case DrawingModeScale:
                [[self.backgroundImage copy] drawInRect:CGRectMake(0, 0, self.image.size.width, self.image.size.width)];
                break;
        }
        
        // I need to redraw all the lines
        for (id<DrawingTool> tool in self.pathArray) {
            [tool draw];
        }
        
    } else {
        // set the draw point
        [self.image drawAtPoint:CGPointZero];
        [self.currentTool draw];
    }
    
    // store the image
    self.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

- (void)finishDrawing{
    // call the delegate
    if ([self.delegate respondsToSelector:@selector(drawingViewDidEndDrawUsingTool:)]) {
        [self.delegate drawingViewDidEndDrawUsingTool:self.currentTool];
    }
    
    // update the image
    [self updateCacheImage:NO];
    
    // clear the redo queue
    [self.bufferArray removeAllObjects];

    // clear the current tool
    self.currentTool = nil;
    
    //[self setNeedsDisplay];

}

- (void)setDrawTool:(DrawingToolType)drawTool{
    _drawTool = drawTool;
    if (_drawTool == DrawingToolTypeText || _drawTool == DrawingToolTypeMultilineText) {
        self.currentTool = [DrawingTextTool new];
        self.currentTool.lineWidth = self.lineWidth;
        self.currentTool.lineColor = self.lineColor;
        self.currentTool.lineAlpha = self.lineAlpha;
        [self initializeTextBox:CGPointMake(0, _textY) WithMultiline:NO];
        [self resizeTextViewFrame: CGPointMake(SCREEN_WIDTH, 120)];
        self.textView.y = _textY;

        [_textView becomeFirstResponder];
        _textView.autocorrectionType = UITextAutocorrectionTypeNo;
    }
}

- (void)setCustomDrawTool:(id<DrawingTool>)customDrawTool{
    _customDrawTool = customDrawTool;
    if (customDrawTool != nil) {
        self.drawTool = DrawingToolTypeCustom;
    }
}

- (id<DrawingTool>)toolWithCurrentSettings{
    switch (self.drawTool) {
        case DrawingToolTypePen:{
            return [DrawingPenTool new];
        }
            
        case DrawingToolTypeLine:{
            return [DrawingLineTool new];
        }
       
        case DrawingToolTypeText:{
            return [DrawingTextTool new];

        }

        case DrawingToolTypeMultilineText:{
            return [DrawingMultilineTextTool new];
        }

        case DrawingToolTypeEraser:{
            return [DrawingEraserTool new];
        }
            
        case DrawingToolTypeCustom:{
            return self.customDrawTool;
        }
    }
}

//#pragma mark - Touch Methods
//
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    if (self.textView && !self.textView.hidden) {
//        [self commitAndHideTextEntry];
//        return;
//    }
//    // add the first touch
//    UITouch *touch = [touches anyObject];
//    previousPoint1 = [touch previousLocationInView:self];
//    currentPoint = [touch locationInView:self];
//    // init the bezier path
//    self.currentTool = [self toolWithCurrentSettings];
//    self.currentTool.lineWidth = self.lineWidth;
//    self.currentTool.lineColor = self.lineColor;
//    self.currentTool.lineAlpha = self.lineAlpha;
//    if ([self.currentTool class] == [DrawingTextTool class]) {
//        [self initializeTextBox:currentPoint WithMultiline:NO];
//    } else if([self.currentTool class] == [DrawingMultilineTextTool class]) {
//        [self initializeTextBox:currentPoint WithMultiline:YES];
//    } else {
//        [self.pathArray addObject:self.currentTool];
//        [self.currentTool setInitialPoint:currentPoint];
//    }
//    // call the delegate
//    if ([self.delegate respondsToSelector:@selector(drawingViewWillBeginDrawUsingTool:)]) {
//        [self.delegate drawingViewWillBeginDrawUsingTool:self.currentTool];
//    }
//}
//
//- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    // save all the touches in the path
//    UITouch *touch = [touches anyObject];
//    previousPoint2 = previousPoint1;
//    previousPoint1 = [touch previousLocationInView:self];
//    currentPoint = [touch locationInView:self];
//    if ([self.currentTool isKindOfClass:[DrawingPenTool class]]) {
//        CGRect bounds = [(DrawingPenTool*)self.currentTool addPathPreviousPreviousPoint:previousPoint2 withPreviousPoint:previousPoint1 withCurrentPoint:currentPoint];
//        CGRect drawBox = bounds;
//        drawBox.origin.x -= self.lineWidth * 2.0;
//        drawBox.origin.y -= self.lineWidth * 2.0;
//        drawBox.size.width += self.lineWidth * 4.0;
//        drawBox.size.height += self.lineWidth * 4.0;
//        [self setNeedsDisplayInRect:drawBox];
//    }
//    else if ([self.currentTool isKindOfClass:[DrawingTextTool class]]) {
//        [self resizeTextViewFrame: currentPoint];
//    }else {
//        [self.currentTool moveFromPoint:previousPoint1 toPoint:currentPoint];
//        [self setNeedsDisplay];
//    }
//    self.lastPoint = self.lastPoint.y < currentPoint.y?currentPoint:self.lastPoint;
//}
//
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
//    // make sure a point is recorded
//    [self touchesMoved:touches withEvent:event];
//    
//    if ([self.currentTool isKindOfClass:[DrawingTextTool class]]) {
//        [self startTextEntry];
//    }
//    else {
//        [self finishDrawing];
//    }
//}
//
//- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
//    // make sure a point is recorded
//    [self touchesEnded:touches withEvent:event];
//}

#pragma mark - Touch Methods


//if (touch.type == UITouchTypeStylus) {
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"PEN_STYLE"] integerValue] == 1) {
//        touch = t;
//    }else{
//        touch = nil;
//    }
//}else{
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"PEN_STYLE"] integerValue] == 2 || [[[NSUserDefaults standardUserDefaults] objectForKey:@"PEN_STYLE"] integerValue] == 0) {
//        touch = t;
//    }else{
//        touch = nil;
//    }
//}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{

    if (self.textView && !self.textView.hidden) {
        [self commitAndHideTextEntry];
        return;
    }
    BOOL isPen;
    @try {
        isPen = touch.type == UITouchTypeStylus;
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    if (![DrawingViewManager sharedInstance].isPen) {
        NSLog(@"pennnnnnnn");
        if ([self.delegate respondsToSelector:@selector(drawingViewWillBeginDrawUsingTool:)]) {
            [self.delegate drawingViewWillBeginDrawUsingTool:self.currentTool];
        }
        if (![[event allTouches] containsObject:touch]) {
            for (UITouch *t in [event allTouches]) {
                touch = t;
                if ([self.currentTool isKindOfClass:[DrawingTextTool class]]) {
                    [self startTextEntry];
                }
                else {
                    //[self finishDrawing];
                }
                break;
            }
            for (UITouch *t in [event allTouches]) {
                BOOL isPen;
                @try {
                    isPen = t.type == UITouchTypeStylus;
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                if (isPen) {
                    touch = t;
                    if ([self.currentTool isKindOfClass:[DrawingTextTool class]]) {
                        [self startTextEntry];
                    }
                    else {
                        //[self finishDrawing];
                    }
                    break;
                }
            }
            BOOL isPen;
            @try {
                isPen = touch.type == UITouchTypeStylus;
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            if (((isPen && [DrawingViewManager sharedInstance].isPen) || ![DrawingViewManager sharedInstance].isPen)) {
                previousPoint1 = [touch previousLocationInView:self];
                currentPoint = [touch locationInView:self];
                UIScrollView *scrollView = (UIScrollView *)self.superview;
                if ([scrollView isKindOfClass:[UIScrollView class]]) {
                    //if (currentPoint.x - scrollView.contentOffset.x < [UIScreen mainScreen].bounds.size.width*0.8 || currentPoint.y - scrollView.contentOffset.y < [UIScreen mainScreen].bounds.size.height - [UIScreen mainScreen].bounds.size.width*0.2) {
                    
                    // init the bezier path
                    self.currentTool = [self toolWithCurrentSettings];
                    self.currentTool.lineWidth = self.lineWidth;
                    self.currentTool.lineColor = self.lineColor;
                    self.currentTool.lineAlpha = self.lineAlpha;
                    
                    if ([self.currentTool class] == [DrawingTextTool class]) {
                        [self initializeTextBox:currentPoint WithMultiline:NO];
                    } else if([self.currentTool class] == [DrawingMultilineTextTool class]) {
                        [self initializeTextBox:currentPoint WithMultiline:YES];
                    } else {
                        [self.pathArray addObject:self.currentTool];
                        
                        [self.currentTool setInitialPoint:currentPoint];
                    }
                    
                    // call the delegate
                    
                    //}
                    
                }else{
                    
                    self.currentTool = [self toolWithCurrentSettings];
                    self.currentTool.lineWidth = self.lineWidth;
                    self.currentTool.lineColor = self.lineColor;
                    self.currentTool.lineAlpha = self.lineAlpha;
                    
                    if ([self.currentTool class] == [DrawingTextTool class]) {
                        [self initializeTextBox:currentPoint WithMultiline:NO];
                    } else if([self.currentTool class] == [DrawingMultilineTextTool class]) {
                        [self initializeTextBox:currentPoint WithMultiline:YES];
                    } else {
                        [self.pathArray addObject:self.currentTool];
                        
                        [self.currentTool setInitialPoint:currentPoint];
                    }
                    
                    // call the delegate
                    if ([self.delegate respondsToSelector:@selector(drawingViewWillBeginDrawUsingTool:)]) {
                        [self.delegate drawingViewWillBeginDrawUsingTool:self.currentTool];
                    }
                }
            }
        }
    } else if ([event allTouches].count < 2) {
    
  
        //[self touchesMoved:touches withEvent:event];
    }
        // add the first touch
    

    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    BOOL isPen;
    @try {
        isPen = touch.type == UITouchTypeStylus;
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    if (![DrawingViewManager sharedInstance].isPen) {
        NSLog(@"pennnnnnnn");
        
        if ([[event allTouches] containsObject:touch]) {
            previousPoint2 = previousPoint1;
            previousPoint1 = [touch previousLocationInView:self];
            currentPoint = [touch locationInView:self];
            if ([self.currentTool isKindOfClass:[DrawingPenTool class]]) {
                CGRect bounds = [(DrawingPenTool*)self.currentTool addPathPreviousPreviousPoint:previousPoint2 withPreviousPoint:previousPoint1 withCurrentPoint:currentPoint];
                
                CGRect drawBox = bounds;
                drawBox.origin.x -= self.lineWidth * 2.0;
                drawBox.origin.y -= self.lineWidth * 2.0;
                drawBox.size.width += self.lineWidth * 4.0;
                drawBox.size.height += self.lineWidth * 4.0;
                
                [self setNeedsDisplayInRect:drawBox];
            }
            else if ([self.currentTool isKindOfClass:[DrawingTextTool class]]) {
                [self resizeTextViewFrame: currentPoint];
                
            }
            else {
                [self.currentTool moveFromPoint:previousPoint1 toPoint:currentPoint];
                [self setNeedsDisplay];
            }
            if ([self.delegate respondsToSelector:@selector(drawingViewDidDraw:)]) {
                //[self.delegate drawingViewDidDraw:self];
                
            }
            
            self.lastPoint = self.lastPoint.y < currentPoint.y?currentPoint:self.lastPoint;
            
        }
    } else  if ([event allTouches].count < 2) {
    // save all the touches in the path
   

    }
   
   
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{

    // make sure a point is recorded
    //[self touchesMoved:touches withEvent:event];
    //if (touch.phase == UITouchPhaseEnded) {

        if ([self.currentTool isKindOfClass:[DrawingTextTool class]]) {
            [self startTextEntry];
        }
        else {
            if (![DrawingViewManager sharedInstance].isPen) {
            [self finishDrawing];
            }
        }
    //}

    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self touchesEnded:touches withEvent:event];
}

#pragma mark - Text Entry

- (void)initializeTextBox:(CGPoint)startingPoint WithMultiline:(BOOL)multiline{
    if (!self.textView) {
        self.textView = [[UITextView alloc] init];
        if (willShowPlaceholder) {
            self.textView.placeholder = @"Type here";
            willShowPlaceholder = NO;
        }
        self.textView.delegate = self;
        if(!multiline) {
            self.textView.returnKeyType = UIReturnKeyDone;
        }
        self.textView.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textView.backgroundColor = [UIColor clearColor];
//        self.textView.layer.borderWidth = 1.0f;
//        self.textView.layer.borderColor = [[UIColor grayColor] CGColor];
//        self.textView.layer.cornerRadius = 8;
        [self.textView setContentInset: UIEdgeInsetsZero];
        [self addSubview:self.textView];
    }
    int calculatedFontSize = self.lineWidth * 8; //3 is an approximate size factor
    [self.textView setFont:[UIFont systemFontOfSize:calculatedFontSize]];
    self.textView.textColor = self.lineColor;
    self.textView.alpha = 1;
    int defaultWidth = SCREEN_WIDTH;
    int defaultHeight = calculatedFontSize * 3;
    int initialYPosition = startingPoint.y - (defaultHeight/2);
    CGRect frame = CGRectMake(0, initialYPosition, defaultWidth, defaultHeight);
    frame = [self adjustFrameToFitWithinDrawingBounds:frame];
    self.textView.frame = frame;
    self.textView.text = @"";
    self.textView.hidden = NO;
    constTop = [NSLayoutConstraint
                                    constraintWithItem:self.textView attribute:NSLayoutAttributeTop
                                    relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                    NSLayoutAttributeTop multiplier:1.0f constant:_textView.y];
    constLeft = [NSLayoutConstraint
                                    constraintWithItem:self.textView attribute:NSLayoutAttributeLeft
                                    relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                     NSLayoutAttributeLeft multiplier:1.0f constant:0];
    constWidth = [NSLayoutConstraint constraintWithItem:self.textView
                                                                  attribute:NSLayoutAttributeWidth
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                                 multiplier:1.0
                                                                   constant:_textView.superview.frame.size.width];
    constHeight = [NSLayoutConstraint constraintWithItem:self.textView
                                                                   attribute:NSLayoutAttributeHeight
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:nil
                                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                                  multiplier:1.0
                                                                    constant:_textView.height];
    [self addConstraint:constTop];
    [self addConstraint:constLeft];
    [_textView addConstraint:constWidth];
    [_textView addConstraint:constHeight];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"START_TEXT_ENTRY" object:nil];

}

- (void) startTextEntry{
    if (!self.textView.hidden) {
        [self.textView becomeFirstResponder];
        self.textView.autocorrectionType = UITextAutocorrectionTypeNo;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"]) {//([self.currentTool class] == [DrawingTextTool  class]) &&
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView{
    CGRect frame = self.textView.frame;
    //if (self.textView.contentSize.height > frame.size.height) {
        frame.size.height = self.textView.contentSize.height + 70;
    //}
    self.textView.frame = frame;
    [self setNeedsUpdateConstraints];
    [_textView setNeedsUpdateConstraints];
   // [self layoutIfNeeded];
  //  [[NSNotificationCenter defaultCenter] postNotificationName:@"START_TEXT_ENTRY" object:nil];
//    int calculatedFontSize = self.lineWidth * 8;
//    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//    paragraphStyle.lineHeightMultiple = 50.0f;
//    paragraphStyle.maximumLineHeight = 60.0f;
//    paragraphStyle.minimumLineHeight = 60.0f;
//    NSString *string = textView.text;
//    NSDictionary *attribute = @{
//                                NSParagraphStyleAttributeName : paragraphStyle,
//                                NSFontAttributeName : [UIFont fontWithName:textView.font.fontName size:calculatedFontSize],
//                                
//                                };
//    textView.attributedText = [[NSAttributedString alloc] initWithString:string attributes:attribute];
}

- (void)textModeEnd{
    [self textViewDidEndEditing:self.textView];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [self commitAndHideTextEntry];
    if ([self.delegate respondsToSelector:@selector(drawingViewDidEndDrawUsingTool:)]) {
        [self.delegate drawingViewDidEndDrawUsingTool:self.currentTool];
    }
    _lastPoint.y = _textY>_lastPoint.y?_textY:_lastPoint.y;
}

-(void)resizeTextViewFrame: (CGPoint)adjustedSize{
    
    int minimumAllowedHeight = self.textView.font.pointSize * 2;
    int minimumAllowedWidth = self.textView.font.pointSize * 0.5;
    
    CGRect frame = self.textView.frame;
    
    //adjust height
    int adjustedHeight = adjustedSize.y - self.textView.frame.origin.y;
    if (adjustedHeight > minimumAllowedHeight) {
        frame.size.height = adjustedHeight;
    }
    
    //adjust width
    int adjustedWidth = adjustedSize.x - self.textView.frame.origin.x;
    if (adjustedWidth > minimumAllowedWidth) {
        frame.size.width = adjustedWidth;
    }
    frame = [self adjustFrameToFitWithinDrawingBounds:frame];
    self.textView.frame = frame;
    [self setNeedsUpdateConstraints];
    [_textView setNeedsUpdateConstraints];
    [self layoutIfNeeded];

}

- (CGRect)adjustFrameToFitWithinDrawingBounds: (CGRect)frame{
    
    //check that the frame does not go beyond bounds of parent view
    if ((frame.origin.x + frame.size.width) > self.frame.size.width) {
        frame.size.width = self.frame.size.width - frame.origin.x;
    }
    if ((frame.origin.y + frame.size.height) > self.frame.size.height) {
        frame.size.height = self.frame.size.height - frame.origin.y;
    }
    return frame;
}

- (void)commitAndHideTextEntry{
    [self.textView resignFirstResponder];
    
    if ([self.textView.text length]) {
        UIEdgeInsets textInset = self.textView.textContainerInset;
        CGFloat additionalXPadding = 5;
        CGPoint start = CGPointMake(self.textView.frame.origin.x + textInset.left + additionalXPadding, self.textView.frame.origin.y + textInset.top);
        CGPoint end = CGPointMake(self.textView.frame.origin.x + self.textView.frame.size.width - additionalXPadding, self.textView.frame.origin.y + self.textView.frame.size.height);
        
        ((DrawingTextTool*)self.currentTool).attributedText = [self.textView.attributedText copy];
        
        [self.pathArray addObject:self.currentTool];
        
        [self.currentTool setInitialPoint:start]; //change this for precision accuracy of text location
        [self.currentTool moveFromPoint:start toPoint:end];
        [self setNeedsDisplay];
        
        [self addText:self.textView.text];
        _textY = self.textView.y + self.textView.height - 86;
        
        [self finishDrawing];
        
    }
    
    self.currentTool = nil;
    self.textView.hidden = YES;
    self.textView = nil;
}

- (void)addText:(NSString *)text{
    if (!_name) {
        _name = [[NSString alloc] init];
    }
    _name = [_name stringByAppendingString:[NSString stringWithFormat:@" %@",text]];
}

#pragma mark - Keyboard Events

- (void)keyboardDidShow:(NSNotification *)notification{
    self.originalFrameYPos = self.frame.origin.y;

    if (IOS8_OR_ABOVE) {
        [self adjustFramePosition:notification];
    }
    else {
        if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
            [self landscapeChanges:notification];
        } else {
            [self adjustFramePosition:notification];
        }
    }
}

- (void)landscapeChanges:(NSNotification *)notification {
    CGPoint textViewBottomPoint = [self convertPoint:self.textView.frame.origin toView:self];
    CGFloat textViewOriginY = textViewBottomPoint.y;
    CGFloat textViewBottomY = textViewOriginY + self.textView.frame.size.height;

    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

    CGFloat offset = (self.frame.size.height - keyboardSize.width) - textViewBottomY;

    if (offset < 0) {
        CGFloat newYPos = self.frame.origin.y + offset;
        self.frame = CGRectMake(self.frame.origin.x,newYPos, self.frame.size.width, self.frame.size.height);

    }
}

- (void)adjustFramePosition:(NSNotification *)notification{
    CGPoint textViewBottomPoint = [self convertPoint:self.textView.frame.origin toView:nil];
    textViewBottomPoint.y += self.textView.frame.size.height;

    CGRect screenRect = [[UIScreen mainScreen] bounds];

    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

    CGFloat offset = (screenRect.size.height - keyboardSize.height) - textViewBottomPoint.y;

    if (offset < 0) {
        CGFloat newYPos = self.frame.origin.y + offset;
        self.frame = CGRectMake(self.frame.origin.x,newYPos, self.frame.size.width, self.frame.size.height);

    }
}

-(void)keyboardDidHide:(NSNotification *)notification{
    
    self.frame = CGRectMake(self.frame.origin.x,self.originalFrameYPos,self.frame.size.width,self.frame.size.height);
}


#pragma mark - Load Image

- (void)loadImage:(UIImage *)image{
    
    self.image = image;
    self.lastPoint = CGPointMake(0, image.size.height);
    
    //save the loaded image to persist after an undo step
    self.backgroundImage = [image copy];
    
    // when loading an external image, I'm cleaning all the paths and the undo buffer
    [self.bufferArray removeAllObjects];
    [self.pathArray removeAllObjects];
    [self updateCacheImage:YES];
    [self setNeedsDisplay];

}

- (void)loadImageData:(NSData *)imageData{
    
    CGFloat imageScale;
    int width = [UIImage imageWithData:imageData].size.width;
    imageScale = 0;
    UIImage *image = [UIImage imageWithData:imageData] ;
    [self loadImage:image];
}

- (void)resetTool{
    
    if ([self.currentTool isKindOfClass:[DrawingTextTool class]]) {
        self.textView.text = @"";
        [self commitAndHideTextEntry];
    }
    self.currentTool = nil;
}

#pragma mark - Actions

- (void)clear{
    self.lastPoint = CGPointMake(0, 0);
    _textY = 60;
    [self resetTool];
    [self.bufferArray removeAllObjects];
    [self.pathArray removeAllObjects];
    self.backgroundImage = nil;
    [self updateCacheImage:YES];
    [self setNeedsDisplay];
}


#pragma mark - Undo / Redo

- (NSUInteger)undoSteps{
    
    return self.bufferArray.count;
}

- (BOOL)canUndo{
    
    return self.pathArray.count > 0;
}

- (void)undoLatestStep{
    
    if ([self canUndo]) {
        [self resetTool];
        id<DrawingTool>tool = [self.pathArray lastObject];
        [self.bufferArray addObject:tool];
        [self.pathArray removeLastObject];
        [self updateCacheImage:YES];
        [self setNeedsDisplay];
    }
}

- (BOOL)canRedo{
    
    return self.bufferArray.count > 0;
}

- (void)redoLatestStep{
    
    if ([self canRedo]) {
        [self resetTool];
        id<DrawingTool>tool = [self.bufferArray lastObject];
        [self.pathArray addObject:tool];
        [self.bufferArray removeLastObject];
        [self updateCacheImage:YES];
        [self setNeedsDisplay];
    }
}

- (void)dealloc{
    
    self.pathArray = nil;
    self.bufferArray = nil;
    self.currentTool = nil;
    self.image = nil;
    self.backgroundImage = nil;
    self.customDrawTool = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];

}

#pragma mark --Notification Metods

- (void)voiceSentText:(NSNotification *)notif{
    [_textView setText:notif.object];
}

@end

#import <Foundation/Foundation.h>
#import "NotesCollectionViewController.h"


@interface DrawingViewManager : NSObject
+ (instancetype)sharedInstance;
@property (nonatomic, strong) UIColor *lineColor;
@property (nonatomic) NSInteger bgColorIndex;
@property (nonatomic) BOOL isLineSelected;
@property (nonatomic, strong) NoteForList *noteForList;
@property (nonatomic) BOOL willCreateNewNote;
@property (nonatomic, strong) NoteMenagedObject *showingNote;
@property (nonatomic, strong) NSString *contactId;
@property (nonatomic, strong) UIColor *buttonsColor;
@property (nonatomic, strong) NSString *prevTitle;
@property (nonatomic, strong) NSString *prevCategory;
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) NSData *takenPhoto;
@property (nonatomic, strong) NSData *photoFromCamera;
@property (nonatomic) BOOL isPen;
@property (nonatomic) NSNumber* saveType;

- (void)setIsPen:(BOOL)isPen;


@end

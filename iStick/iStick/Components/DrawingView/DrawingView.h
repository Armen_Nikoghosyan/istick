#import <UIKit/UIKit.h>

#define drawingViewVersion   1.3.7

typedef enum {
    DrawingToolTypePen,
    DrawingToolTypeLine,
    DrawingToolTypeEraser,
    DrawingToolTypeText,
    DrawingToolTypeMultilineText,
    DrawingToolTypeCustom,
} DrawingToolType;

typedef NS_ENUM(NSUInteger, DrawingMode) {
    DrawingModeScale,
    DrawingModeOriginalSize
};

@protocol DrawingTool;
@protocol DrawingViewDelegate <NSObject>

@optional
- (void)drawingViewWillBeginDrawUsingTool:(id<DrawingTool>)tool;
- (void)drawingViewDidEndDrawUsingTool:(id<DrawingTool>)tool;
- (void)drawingViewDidDraw;
- (void)drawingViewDidScroll:(BOOL)direction different:(double)diff;

@end
@interface DrawingView : UIView<UITextViewDelegate>

@property (nonatomic, assign) DrawingToolType drawTool;
@property (nonatomic, assign) id<DrawingTool> customDrawTool;
@property (nonatomic, weak) id<DrawingViewDelegate> delegate;

// public properties
@property (nonatomic, strong) UIColor *lineColor;
@property (nonatomic, assign) CGFloat lineWidth;
@property (nonatomic, assign) CGFloat lineAlpha;
@property (nonatomic, assign) CGPoint lastPoint;
@property (nonatomic, assign) DrawingMode drawMode;

// get the current drawing
@property (nonatomic, strong, readonly) UIImage *image;
@property (nonatomic, strong) UIImage *backgroundImage;
@property (nonatomic, strong) NSString *name;
@property (nonatomic) CGFloat textY;
@property (nonatomic, strong) UITextView *textView;

@property (nonatomic, readonly) NSUInteger undoSteps;

// load external image
- (void)loadImage:(UIImage *)image;
- (void)loadImageData:(NSData *)imageData;

// erase all
- (void)clear;

// undo / redo
- (BOOL)canUndo;
- (void)undoLatestStep;

- (BOOL)canRedo;
- (void)redoLatestStep;

- (void)commitAndDiscardToolStack;

@end

#pragma mark - 

@interface DrawingView (Deprecated)
@property (nonatomic, strong) UIImage *prev_image DEPRECATED_MSG_ATTRIBUTE("Use 'backgroundImage' instead.");
@end

#import <UIKit/UIKit.h>

@protocol DrawingTool <NSObject>

@property (nonatomic, strong) UIColor *lineColor;
@property (nonatomic, assign) CGFloat lineAlpha;
@property (nonatomic, assign) CGFloat lineWidth;

- (void)setInitialPoint:(CGPoint)firstPoint;
- (void)moveFromPoint:(CGPoint)startPoint toPoint:(CGPoint)endPoint;

- (void)draw;

@end

#pragma mark -var x:Bool = false

@interface DrawingPenTool : UIBezierPath<DrawingTool> {
    CGMutablePathRef path;
}

- (CGRect)addPathPreviousPreviousPoint:(CGPoint)p2Point withPreviousPoint:(CGPoint)p1Point withCurrentPoint:(CGPoint)cpoint;

@end

#pragma mark -

@interface DrawingEraserTool : DrawingPenTool

@end

#pragma mark -

@interface DrawingLineTool : NSObject<DrawingTool>

@end

#pragma mark -

@interface DrawingTextTool : NSObject<DrawingTool>
@property (strong, nonatomic) NSAttributedString* attributedText;
@end

@interface DrawingMultilineTextTool : DrawingTextTool
@end



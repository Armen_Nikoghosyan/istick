#import "DrawingViewManager.h"

@implementation DrawingViewManager
+ (instancetype)sharedInstance {
    static DrawingViewManager *drawingViewManager = nil;
    if (!drawingViewManager) {
        BOOL isPen = [[[NSUserDefaults standardUserDefaults] objectForKey:@"PENORFINGER"]  isEqual: [NSNumber numberWithInt:0]]?NO:YES;
        drawingViewManager = [[DrawingViewManager alloc] init];
        drawingViewManager.lineColor = [UIColor blackColor];
        drawingViewManager.backgroundColor = [UIColor colorWithRed:230/255.0f green:195/255.0f blue:51/255.0f alpha:1];
        drawingViewManager.bgColorIndex = 2;
        drawingViewManager.isLineSelected = NO;
        drawingViewManager.noteForList = nil;
        drawingViewManager.willCreateNewNote = NO;
        drawingViewManager.showingNote = nil;
        drawingViewManager.contactId = @"";
        drawingViewManager.buttonsColor = [UIColor blackColor];
        drawingViewManager.prevTitle = @"";
        drawingViewManager.takenPhoto = nil;
        drawingViewManager.prevCategory = @"";
        drawingViewManager.isPen = isPen;
        drawingViewManager.photoFromCamera = nil;
        drawingViewManager.saveType = nil;
    }
    return drawingViewManager;
}

- (void)setIsPen:(BOOL)isPen{
    _isPen = isPen;
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:isPen] forKey:@"PENORFINGER"];
}

@end

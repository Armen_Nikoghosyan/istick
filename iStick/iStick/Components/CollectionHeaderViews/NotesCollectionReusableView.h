#import <UIKit/UIKit.h>
#import "FileManagedObject.h"

@interface NotesCollectionReusableView : UICollectionReusableView

- (void)configureWithFile:(FileManagedObject *)file;

@end

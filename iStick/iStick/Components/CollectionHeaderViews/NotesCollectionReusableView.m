#import "NotesCollectionReusableView.h"

@implementation NotesCollectionReusableView{
    UILabel *label;
}
- (void)configureWithFile:(FileManagedObject *)file{
    [self setBackgroundColor:[UIColor redColor]];
    if (!label) {
        label = [[UILabel alloc] initWithFrame:self.bounds];
        [self addSubview:label];

    }
    [label setText:file.name];
    [label sizeToFit];
    label.left = 5;
    label.centerY = 15;
    
}

@end

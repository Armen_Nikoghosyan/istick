#import <UIKit/UIKit.h>

@protocol SortByDelegate <NSObject>

@optional

- (void)didTapOnSortButton:(UIButton *)sender;
- (void)didslectItemAtIndex:(SortDescriptortype)type withButton:(UIButton *)button;


@end

@interface SortBy : UIView
- (void)configureViewWithItems:(NSArray *)items;
@property (nonatomic, weak) id<SortByDelegate> delegate;
@end

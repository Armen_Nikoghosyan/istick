#import <UIKit/UIKit.h>

@protocol TitleTableViewControllerDelegate <NSObject>

@optional

- (void)titleTableViewControllerDidSelectObject:(NSString *)object;

@end

@interface TitleTableViewController : UIViewController

- (void)setArray:(NSArray *)array;
@property (nonatomic, weak) id<TitleTableViewControllerDelegate> delegate;
@end

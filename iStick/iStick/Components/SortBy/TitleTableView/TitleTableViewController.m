#import "TitleTableViewController.h"

@interface TitleTableViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UITableView *mainTableView;
    NSArray *cellsArray;
}

@end

@implementation TitleTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    cellsArray = [[NSMutableArray alloc] init];
    mainTableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    [self.view addSubview:mainTableView];
    // Do any additional setup after loading the view.
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    mainTableView.frame = self.view.bounds;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setArray:(NSArray *)array{
    cellsArray = array;
    [mainTableView reloadData];
}

#pragma mark - TbaleView Delegate/DataSourse metods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return cellsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"TableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = cellsArray[indexPath.row];
    [cell.textLabel setTextAlignment:NSTextAlignmentLeft];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_delegate titleTableViewControllerDidSelectObject:cellsArray[indexPath.row]];
}

@end

#import "SortBy.h"
#define defaultColorForSortBy [UIColor whiteColor]

@implementation SortBy{
    UIView *itemsView;
    UIButton *sortByButton;
    int count;
}

- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        [self configure];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self configure];
    }
    return self;
}

- (void)configure{
    [self setBackgroundColor:defaultColorForSortBy];

}

- (void)configureViewWithItems:(NSArray *)items{
    sortByButton = [[UIButton alloc] initWithFrame:self.frame];
    sortByButton.y = 0;
    [sortByButton setTitle:@"Sort by" forState:UIControlStateNormal];
    [sortByButton setBackgroundColor:[UIColor redColor]];
    [sortByButton addTarget:self action:@selector(didTapOnSortBy:) forControlEvents:UIControlEventTouchUpInside];
    
    itemsView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.width, 40 * items.count)];
    [itemsView setBackgroundColor:[UIColor lightGrayColor]];
    for (int i = 0; i < items.count; ++i) {
        NSString *itemName = items[i];
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, i * 40, self.width, 40)];
        [btn setTitle:itemName forState:UIControlStateNormal];
        btn.tag = i;
        [btn addTarget:self action:@selector(didSeletItem:) forControlEvents:UIControlEventTouchUpInside];
        [itemsView addSubview:btn];
    }
    
    count = (int)items.count;
    
    self.height = count * 40 + 40;
    
    [self addSubview:itemsView];
    [self addSubview:sortByButton];
    
}

- (void)didTapOnSortBy:(UIButton *)sortButtom{
    [_delegate didTapOnSortButton:sortByButton];
}

- (void)didSeletItem:(UIButton *)button{
    [_delegate didslectItemAtIndex:(int)button.tag withButton:sortByButton];
}

@end

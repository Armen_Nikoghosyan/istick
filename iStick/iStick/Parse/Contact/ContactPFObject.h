#import <Parse/Parse.h>

@interface ContactPFObject : PFObject <PFSubclassing>

- (void)setId:(NSString *)contactId;
- (void)setContact:(PFFile *)contact;
- (void)setUserId:(NSString *)userId;

- (NSString *)getUserId;
- (PFFile *)getContact;
- (NSString *)getId;
- (PFRelation *)getNotes;

+ (ContactPFObject *)getContactById:(NSString *)contacId;
+ (NSArray *)getAllContactsFromParse;
@end

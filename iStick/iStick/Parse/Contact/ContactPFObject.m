#import "ContactPFObject.h"

@implementation ContactPFObject
+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"Contact";
}

- (void)setId:(NSString *)contactId{
    [self setObject:contactId forKey:@"id"];
}

- (void)setContact:(PFFile *)contact{
    if (contact) {
        [self setObject:contact forKey:@"contact"];
    }
}

- (void)setUserId:(NSString *)userId{
    [self setObject:userId forKey:@"userId"];
}

- (NSString *)getUserId{
    return [self objectForKey:@"userId"];
}

- (PFRelation *)getNotes{
    return [self objectForKey:@"notes"];
}

- (PFFile *)getContact{
    return (PFFile *)self[@"contact"];
}

- (NSString *)getId{
    return [self objectForKey:@"id"];
}

+ (ContactPFObject *)getContactById:(NSString *)contacId{
    PFQuery *query = [ContactPFObject query];
    [query whereKey:@"id" equalTo:contacId];
    [query whereKey:@"userId" equalTo:[PFUser currentUser].objectId];
    NSArray *contacts = [query findObjects];
    return [contacts firstObject];
}

+ (NSArray *)getAllContactsFromParse{
    PFQuery *query = [ContactPFObject query];
    [query whereKey:@"userId" equalTo:[PFUser currentUser].objectId];
    return [query findObjects];
}

@end

#import "NotePFObject.h"
#import "AppDelegate.h"
#import "RNEncryptor.h"

@implementation NotePFObject

+ (void)load {
    [self registerSubclass];
}


+ (NSString *)parseClassName {
    return @"Note";
}

- (void)setName:(NSString *)name{
    [self setObject:name forKey:@"name"];
}

- (void)setCreatedAt:(NSDate *)date{
    [self setObject:date forKey:@"created_at"];
}

- (void)setAlarmAt:(NSDate *)date {
     [self setObject:date forKey:@"alarm_at"];
}
- (void)setImage:(PFFile *)image{
    if (image) {
        [self setObject:image forKey:@"image"];
    }
}

- (void)setBackgroundImage:(PFFile *)image{
    if (image) {
        [self setObject:image forKey:@"backgroundImage"];
    }
}

- (void)setUserId:(NSString *)userId{
    [self setObject:userId forKey:@"userId"];
}

- (void)setCategoryName:(NSString *)categoryName{
    [self setObject:categoryName forKey:@"categoryName"];
}

- (void)setCategoryNameUpperCase:(NSString *)categoryNameUpperCase{
    [self setObject:categoryNameUpperCase forKey:@"categoryNameUpperCase"];
}

- (void)setTitle:(NSString *)title{
    [self setObject:title forKey:@"title"];
}

- (void)setObject_id:(NSString *)object_id{
    [self setObject:object_id forKey:@"object_id"];
}

- (void)setColorIndex:(NSNumber *)index{
    [self setObject:index forKey:@"colorIndex"];
}

- (void)setLineColorIndex:(NSString *)index{
    [self setObject:index forKey:@"lineColorIndex"];
}

- (void)setTextY:(NSNumber *)textY{
    [self setObject:textY forKey:@"textY"];
}

- (void)setPriority:(NSNumber *)priority{
    [self setObject:priority forKey:@"priority"];
}

- (void)setFileName:(NSString *)name{
    [self setObject:name forKey:@"fileName"];
}

- (void)setUpdatedAt:(NSDate *)date{
    [self setObject:date forKey:@"updated_at"];
}

- (void)setWasDeleted:(NSNumber *)wasDeleted{
    [self setObject:wasDeleted forKey:@"wasDeleted"];
}

- (void)setStar_count:(NSNumber *)star_count{
    [self setObject:star_count forKey:@"star_count"];
}

- (void)setPin_count:(NSNumber *)pin_count{
    [self setObject:pin_count forKey:@"pin_count"];
}

- (void)setTime_sort_number:(CGFloat)time_sort_number{
    [self setObject:[NSNumber numberWithFloat:time_sort_number] forKey:@"time_sort_number"];
}

- (void)setVersion:(NSNumber *)version{
    [self setObject:version forKey:@"version"];
}

- (void)setViewAt:(NSDate *)date{
    [self setObject:date forKey:@"viewAt"];
}

- (void)setPhoneNumber:(NSString *)phoneNamber{
    [self setObject:phoneNamber forKey:@"phoneNumber"];
}

- (void)setIsInCue:(NSNumber *)isInCue{
    [self setObject:isInCue forKey:@"isInCue"];

}
- (void)setAlarm:(NSNumber *)alarm{
     [self setObject:alarm forKey:@"alarm"];
}

- (void)setDocument:(NSNumber *)document{
     [self setObject:document forKey:@"document"];
}

- (void)setGallery:(NSNumber *)gallery {
    [self setObject:gallery forKey:@"gallery"];
}

///////////////////////////////////////////////////////////////////////////////////

- (NSNumber *)getIsInCue{
    return [self objectForKey:@"isInCue"];
}

- (NSDate *)getAlarmAt{
    return [self objectForKey:@"alarm_at"];
}

- (NSNumber *)alarm{
    return [self objectForKey:@"alarm"];
}

- (NSString *)getPhoneNumber{
    return [self objectForKey:@"phoneNumber"];
}

- (NSDate *)viewAt{
    return [self objectForKey:@"viewAt"];
}

- (NSNumber *)version;{
    return [self objectForKey:@"version"];
}

- (CGFloat)time_sort_number{
    NSNumber *number = [self objectForKey:@"time_sort_number"];
    return [number floatValue];
}

- (NSNumber *)star_count{
    return [self objectForKey:@"star_count"];
}

- (NSNumber *)pin_count{
    return [self objectForKey:@"pin_count"];
}

- (NSNumber *)wasDeleted{
    return [self objectForKey:@"wasDeleted"];
}

- (NSDate *)getUpdatedAt{
    return [self objectForKey:@"updated_at"];
}

- (NSString *)getCategoryNameUpperCase{
    return [self objectForKey:@"categoryNameUpperCase"];
}

- (NSString *)getCategoryName{
    return [self objectForKey:@"categoryName"];
}

- (NSString *)getFileName{
    return [self objectForKey:@"fileName"];
}

- (NSNumber *)priority{
    return [self objectForKey:@"priority"];
}

- (NSString *)getName{
    return [self objectForKey:@"name"];
}

- (NSDate *)getCreatedAt{
    return [self objectForKey:@"created_at"];
}

- (PFFile *)getImage{
    return (PFFile *)self[@"image"];
}

- (PFFile *)getBackgroundImage{
    return (PFFile *)self[@"backgroundImage"];
}

- (NSString *)getUserId{
    return [self objectForKey:@"userId"];
}

- (NSString *)getTitle{
    return [self objectForKey:@"title"];
}

- (NSNumber *)getColorIndex{
    return [self objectForKey:@"colorIndex"];
}

- (NSString *)getLineColorIndex{
    return [self objectForKey:@"lineColorIndex"];

}

- (NSNumber *)getTextY{
    return [self objectForKey:@"TextY"];
}

- (NSString *)getObject_id{
    return self[@"object_id"];
}
- (NSNumber *)getDocument{
    return [self objectForKey:@"document"];
}
- (NSNumber *)getGallery{
    return [self objectForKey:@"gallery"];
}


///////////////////////////////////////////////////////////////////////////////////

- (void)saveMenagedObjectToParse:(NoteMenagedObject *)object{
    if (object.created_at != nil) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self setCreatedAt:object.created_at];
            [self setObject_id:object.object_id];
            if (object.name != nil) {
                [self setName:object.name];
            }
            if (object.alarm != nil) {
                [self setAlarm:object.alarm];
            }
            if (object.alarm_at != nil) {
                [self setAlarmAt:object.alarm_at];
            }
            if (object.phoneNumber != nil) {
                [self setPhoneNumber:object.phoneNumber];
            }
            if (object.categoryName != nil) {
                [self setCategoryName:object.categoryName];
            }
            if (object.categoryNameUpperCase != nil) {
                [self setCategoryNameUpperCase:object.categoryNameUpperCase];
            }
            if (object.fileName != nil) {
                [self setFileName:object.fileName];
            }
            if (object.title != nil) {
                [self setTitle:object.title];
            }
            if (object.updated_at != nil) {
                [self setUpdatedAt:object.updated_at];
            }
            if (object.view_at != nil) {
                [self setViewAt:object.view_at];
            }
            if ([PFUser currentUser] != nil) {
                [self setUserId:[PFUser currentUser].objectId];
            }
            if (object.color_index != nil) {
                [self setColorIndex:object.color_index];
            }
            if (object.line_color_index != nil) {
                [self setLineColorIndex:object.line_color_index];
            }
            if (object.textY != nil) {
                [self setTextY:object.textY];
            }
            if (object.priority != nil) {
                [self setPriority:object.priority];
            }
            if (object.star_count != nil) {
                [self setStar_count:object.star_count];
            }
            if (object.pin_count != nil) {
                [self setPin_count:object.pin_count];
            }
            if (object.wasDeleted != nil) {
                [self setWasDeleted:object.wasDeleted];
            }
            if (object.time_sort_number != nil) {
                [self setTime_sort_number:[object.time_sort_number floatValue]];
            }
            if (object.version != nil) {
                [self setVersion:object.version];
            }
            if (object.isInCue != nil) {
                [self setIsInCue:object.isInCue];
            }
            if (object.document != nil) {
                [self setDocument:object.document];
            }
            if (object.gallery != nil) {
                [self setGallery:object.gallery];
            }
            if (object.image != nil) {
                NSData *data = object.image; //[self encodedData:object.image];//[self encodedData:object.image];
                if (data) {
                    PFFile *file = [PFFile fileWithData:data];
                    if (file != nil) {
                        [file saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                            if (!error) {
                                [self setImage:file];
                                if (object.backgroundImage) {
                                    PFFile *fileBackgroundImage = [PFFile fileWithData:object.backgroundImage];
                                    if (fileBackgroundImage) {
                                        [fileBackgroundImage saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                                            if (!error) {
                                                [self setBackgroundImage:fileBackgroundImage];
                                                [self saveInParse:object];////save
                                            }else{
                                                [self saveInParse:object];////save
                                            }
                                        }];
                                    }else{
                                        [self saveInParse:object];////save
                                    }
                                }else{
                                    [self saveInParse:object];////save
                                }
                            }else{
                                [self saveInParse:object];////save
                            }
                        }];
                    }else{
                        [self saveInParse:object];////save
                    }
                }else{
                    [self saveInParse:object];////save
                }
            }else{
                [self saveInParse:object];////save
            }
        });
    }
}

- (NSData *)encodedData:(NSData *)data{
    NSString *aPassword = [[NSUserDefaults standardUserDefaults] objectForKey:@"ENCODEKEY"];
    NSError *error;
    NSData *encryptedData = [RNEncryptor encryptData:data
                                        withSettings:kRNCryptorAES256Settings
                                            password:aPassword
                                               error:&error];
    if (!error) {
        return encryptedData;
    }
    return nil;
}

- (void)saveInParse:(NoteMenagedObject *)object{
    [self changeCoreDataObjectIsInParseProparty:object];
    [self saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        if (!error) {
            PFRelation *relation = [[PFUser currentUser] relationForKey:@"Notes"];
            [relation addObject:self];
            [[PFUser currentUser] saveEventually];
        }else{
            [self decreseCoreDataObjectIsInParseProparty:object];
            [LKAlertView showAlertWithTitleInDevMode:@"Whoops, note couldn't save (Globale)"
                                             message:[NSString stringWithFormat:@"%@", [error localizedDescription]]
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:nil
                                          completion:nil];
        }
    }];

}

- (void)changeCoreDataObjectIsInParseProparty:(NoteMenagedObject *)object{
    object.isInParse = [NSNumber numberWithInt:4];
    NSError *error;
    [[APPDELEGATE managedObjectContext] save:&error];
}

- (void)decreseCoreDataObjectIsInParseProparty:(NoteMenagedObject *)object{
    object.isInParse = [NSNumber numberWithInt:0];
    NSError *error;
    [[APPDELEGATE managedObjectContext] save:&error];
}

+ (NotePFObject *)getNoteById:(NSString *)noteId{
    PFQuery *query = [NotePFObject query];
    [query whereKey:@"object_id" equalTo:noteId];
    return [[query findObjects] firstObject];
}


@end

//
//  NotePFObject.h
//  iStick
//
//  Created by Falcon on 5/23/16.
//  Copyright © 2016 Levon. All rights reserved.
//
#import <Parse/Parse.h>
#import "NoteMenagedObject.h"

@interface NotePFObject : PFObject <PFSubclassing>
- (void)setName:(NSString *)name;
- (void)setPhoneNumber:(NSString *)phoneNamber;
- (void)setCreatedAt:(NSDate *)date;
- (void)setImage:(PFFile *)image;
- (void)setUserId:(NSString *)userId;
- (void)setObject_id:(NSString *)object_id;
- (void)setTitle:(NSString *)title;
- (void)setColorIndex:(NSNumber *)index;
- (void)setTextY:(NSNumber *)textY;
- (void)setPriority:(NSNumber *)priority;
- (void)setFileName:(NSString *)name;
- (void)setUpdatedAt:(NSDate *)date;
- (void)setWasDeleted:(NSNumber *)wasDeleted;
- (void)setStar_count:(NSNumber *)star_count;
- (void)setPin_count:(NSNumber *)pin_count;
- (void)setTime_sort_number:(CGFloat)time_sort_number;
- (void)setVersion:(NSNumber *)version;
- (void)setViewAt:(NSDate *)date;
- (void)setBackgroundImage:(PFFile *)image;
- (void)setLineColorIndex:(NSString *)index;
- (void)setCategoryName:(NSString *)categoryName;
- (void)setCategoryNameUpperCase:(NSString *)categoryNameUpperCase;
- (void)setIsInCue:(NSNumber *)isInCue;
- (void)setAlarmAt:(NSDate *)date;
- (void)setAlarm:(NSNumber *)alarm;
- (void)setDocument:(NSNumber *)document;
- (void)setGallery:(NSNumber *)gallery;

- (NSNumber *)getIsInCue;
- (NSString *)getPhoneNumber;
- (NSString *)getCategoryNameUpperCase;
- (NSString *)getCategoryName;
- (NSString *)getLineColorIndex;
- (PFFile *)getBackgroundImage;
- (NSDate *)viewAt;
- (NSNumber *)version;
- (CGFloat)time_sort_number;
- (NSNumber *)star_count;
- (NSNumber *)pin_count;
- (NSNumber *)wasDeleted;
- (NSDate *)getUpdatedAt;
- (NSString *)getFileName;
- (NSString *)getName;
- (NSString *)getObject_id;
- (NSDate *)getCreatedAt;
- (PFFile *)getImage;
- (NSString *)getUserId;
- (NSString *)getTitle;
- (NSNumber *)getColorIndex;
- (NSNumber *)getTextY;
- (NSNumber *)priority;
- (NSDate *)getAlarmAt;
- (NSNumber *)alarm;
- (NSNumber *)getDocument;
- (NSNumber *)getGallery;


- (void)saveMenagedObjectToParse:(NoteMenagedObject *)object;
+ (NotePFObject *)getNoteById:(NSString *)noteId;

@end

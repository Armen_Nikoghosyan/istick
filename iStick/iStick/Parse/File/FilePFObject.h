//
//  FilePFObject.h
//  iStick
//
//  Created by Falcon on 6/16/16.
//  Copyright © 2016 Levon. All rights reserved.
//

#import <Parse/Parse.h>

@interface FilePFObject : PFObject <PFSubclassing>
- (void)setName:(NSString *)name;
- (void)setNotes:(PFRelation *)notes;
- (NSString *)getName;
- (PFRelation *)getNotes;

- (void)saveMenagedObjectToParse:(FileManagedObject *)object;
@end

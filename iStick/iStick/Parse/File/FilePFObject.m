//
//  FilePFObject.m
//  iStick
//
//  Created by Falcon on 6/16/16.
//  Copyright © 2016 Levon. All rights reserved.
//

#import "FilePFObject.h"

@implementation FilePFObject

+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"File";
}

- (void)setName:(NSString *)name{
    [self setObject:name forKey:@"name"];

}

- (void)setNotes:(PFRelation *)notes{
    [self setObject:notes forKey:@"Notes"];

}

- (NSString *)getName{
    return [self objectForKey:@"name"];
}

- (PFRelation *)getNotes{
    return [self objectForKey:@"Notes"];

}

- (void)saveMenagedObjectToParse:(FileManagedObject *)object{
    [self setName:object.name];
    for (NotePFObject *notePFObject in [object.notes allObjects]) {
        [[self getNotes] addObject:notePFObject];
    }
    [self saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        
    }];
}

@end

#import "AppDelegate.h"
#import "DrawViewController.h"
#import <Parse/Parse.h>
#import <GameKit/GameKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "MVYSideMenuController.h"
#import "DrawingViewManager.h"
#import <AddressBook/ABAddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "Contact.h"
#import "MainTabBarController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "NSString+MD5.h"
#import "HDNotificationView.h"

@interface AppDelegate (){
    MainTabBarController *mainTabBarController;
}

@end

@implementation AppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (void)sync{
    if([PFUser currentUser]){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [Note syncData];
        });
    }
}

- (void)loginToParseWithUsername:(NSString *)username{
    if([PFUser currentUser] == nil) {
        PFQuery *query = [PFUser query];
        [query whereKey:@"username" equalTo:username];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (object != nil) {
                [PFUser logInWithUsernameInBackground:username password:@"password" block:^(PFUser * _Nullable user, NSError * _Nullable error) {
                    if (!error) {
                        [Note saveAllUnsavedObjectToParse];
                        [self saveEncodingKey:username password:@"password"];
                    }
                }];
            }else{
                PFUser *user = [PFUser user];
                user.username = username;
                user.password = @"password";
                [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                    if (!error) {
                        [Note saveAllUnsavedObjectToParse];
                        [Parse enableLocalDatastore];
                        [self saveEncodingKey:username password:@"password"];

                    }
                }];
            }
        }];
    }
}

- (void)saveEncodingKey:(NSString *)username password:(NSString *)password{
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@ %@",username,password] forKey:@"ENCODEKEY"];
}

- (void) logUser {
    if ([PFUser currentUser]) {
        [CrashlyticsKit setUserIdentifier:@"12345"];
        [CrashlyticsKit setUserEmail:@"user@fabric.io"];
        [CrashlyticsKit setUserName:[PFUser currentUser].username];
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    NSDate *startDate = [NSDate new];
    
   
    [[Contact sharedInstance] syncAllContacts];
    NSLog(@"Seconds -------->1 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    [Fabric with:@[[Crashlytics class]]];
    NSLog(@"Seconds -------->2 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    [self logUser];
    NSLog(@"Seconds -------->3 %f",[[NSDate date] timeIntervalSinceDate: startDate]);


    [APPDELEGATE setAmIMessageSender:NO];
    NSLog(@"Seconds -------->4 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    UILocalNotification *userInfo = [launchOptions valueForKey:@"UIApplicationLaunchOptionsLocalNotificationKey"];
    NSLog(@"Seconds -------->5 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    _array = [[NSMutableArray alloc] init];
    NSLog(@"Seconds -------->6 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    _colorsArray = @[[UIColor colorWithRed:1 green:0 blue:0 alpha:1],
                     [UIColor colorWithRed:0 green:0 blue:1 alpha:1],
                     [UIColor colorWithRed:230/255.0f green:195/255.0f blue:51/255.0f alpha:1],
                     [UIColor colorWithRed:0 green:0 blue:0 alpha:0.9],
                     [UIColor colorWithRed:1 green:1 blue:1 alpha:1]
                     ];
    NSLog(@"Seconds -------->7 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    UIWindow *window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    NSLog(@"Seconds -------->8 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    if (userInfo) {
        [self application:application didReceiveLocalNotification:userInfo];
    }
    NSLog(@"Seconds -------->9 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    UIUserNotificationType types =  UIUserNotificationTypeBadge |
                                    UIUserNotificationTypeSound |
                                    UIUserNotificationTypeAlert;
    NSLog(@"Seconds -------->10 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    NSLog(@"Seconds -------->11 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    NSLog(@"Seconds -------->12 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    NSLog(@"Seconds -------->13 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    ///BUDDY-------------------------
        [Parse initializeWithConfiguration:[ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration> configuration) {
            configuration.applicationId = @"e7973710-cb94-47bf-8fd2-fb4b7532df90";
            configuration.clientKey = @"H680cSxRGR8n6vFyLoTCjiW4WlTnFH6C";
            configuration.server = @"https://parse.buddy.com/parse";
            configuration.localDatastoreEnabled = YES; // If you need to enable local data store
        }]];
    NSLog(@"Seconds -------->14 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    
//    ///BACK4APP-------------------------
//    [Parse initializeWithConfiguration:[ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration> configuration) {
//        configuration.applicationId = @"OfPwsuorP395PZ5W7IcgdGYEIFytIMx7pIKfwQ9O";
//        configuration.clientKey = @"uvE9WIsJZxtiSjOAD45bPrvEgOhN2Qha0bSuMXiX";
//        configuration.server = @"https://parseapi.back4app.com/";
//        configuration.localDatastoreEnabled = YES; // If you need to enable local data store
//    }]];
    
    
    
    ///UNUSED------------------------
//    [Parse enableLocalDatastore];
//    [Parse initializeWithConfiguration:[ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration> configuration) {
//        configuration.applicationId = @"tVDZuyA3QjjqutPNwWZAMF3SowBCnIJukBSCcxha";
//        configuration.clientKey = @"";
//        configuration.server = @"http://localhost:1337/parse";
//    }]];
    
    
    
    
//    //SASHIDO-----------------------
//    [Parse initializeWithConfiguration:[ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration> configuration) {
//        configuration.applicationId = @"tVDZuyA3QjjqutPNwWZAMF3SowBCnIJukBSCcxha";
//        configuration.clientKey = @"OKZxVlaLB5wIhBTSVAAE5UsUuC6FQ90vy8wUBJjB";
//        configuration.server = @"https://pg-app-neo6w8x6iavxgcub1i53eedxdd359f.scalabl.cloud/1/";
//        configuration.localDatastoreEnabled = YES; // If you need to enable local data store
//    }]];
    
    
    
    
//    [Parse initializeWithConfiguration:[ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration> configuration) {
//        configuration.applicationId = @"tVDZuyA3QjjqutPNwWZAMF3SowBCnIJukBSCcxha";
//        configuration.clientKey = @"";
//        configuration.server = @"https://repository.boomermemo.com";
//    }]];
    
//    [Parse setApplicationId:@"tVDZuyA3QjjqutPNwWZAMF3SowBCnIJukBSCcxha" clientKey:@"OKZxVlaLB5wIhBTSVAAE5UsUuC6FQ90vy8wUBJjB"];
    
//    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    


    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    NSLog(@"Seconds -------->15 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    _mcManager = [[MCManager alloc] init];
    NSLog(@"Seconds -------->16 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    [_mcManager setupPeerAndSessionWithDisplayName:[UIDevice currentDevice].name];
    [_mcManager advertiseSelf:YES];
    NSLog(@"Seconds -------->17 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    mainTabBarController = [[MainTabBarController alloc] init];
    [mainTabBarController.tabBar setHidden:YES];
    [window setRootViewController:mainTabBarController];
    NSLog(@"Seconds -------->18 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    [window makeKeyAndVisible];
    NSLog(@"Seconds -------->19 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    NSLog(@"Seconds -------->20 %f",[[NSDate date] timeIntervalSinceDate: startDate]);
    //[PFUser logOut];
   // [self loginToParseWithUsername:@"Levon"];
    NSLog(@"Seconds -------->21 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    [self sync];
    NSLog(@"Seconds -------->22 %f",[[NSDate date] timeIntervalSinceDate: startDate]);
    [self setWindow:window];
    
    
    
    

    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    return handled;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"APLICATION_WILL_RESIGN_ACTIVE" object:nil];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    mainTabBarController.selectedIndex = 4;

}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [self sync];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LOCALE_NOTIFICATION_STATE_CHANGED" object:nil];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
    //[self sync];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    if ( application.applicationState == UIApplicationStateActive){
        NSLog(@"Active %@",notification.userInfo);
        NSString *object_id = [notification.userInfo objectForKey:@"object_id"];
        NoteMenagedObject *obj = [Note getNoteByObject_id:object_id];
        [Note chnageAlarm:obj alarm:[NSNumber numberWithInteger:0]];
        [Note syncData];
        NSString* title;
        NSLog(@"obj == %@",obj);
        if([obj.title isEqualToString:@""]) {
            title = @"iStick";
        } else {
            title = obj.title;
        }
        [HDNotificationView showNotificationViewWithImage:[UIImage imageNamed:@"fill_X_icon"]
                                                    title:title
                                                  message:@"" isAutoHide:YES onTouch:^{
                                                      
                                                  }];
       
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LOCALE_NOTIFICATION_STATE_CHANGED" object:nil];
    }else if( application.applicationState == UIApplicationStateBackground){
        NSLog(@"Background");
        [self redirectToPage:notification.userInfo];
    }else if( application.applicationState == UIApplicationStateInactive){
        NSLog(@"Inactive");
        [self redirectToPage:notification.userInfo];
    }
    NSMutableArray *scheduledObjects = [[NSUserDefaults standardUserDefaults] objectForKey:@"scheduled_objects_object_id"];
    NSMutableArray *newArray = [[NSMutableArray alloc] initWithArray:scheduledObjects];
    for (NSString *str in scheduledObjects) {
        if ([[notification.userInfo objectForKey:@"object_id"] isEqualToString:str]) {
            [newArray removeObject:str];
        }
    }
    if (newArray && newArray.count) {
        [[NSUserDefaults standardUserDefaults] setObject:newArray forKey:@"scheduled_objects_object_id"];
    }
}

- (void)redirectToPage:(NSDictionary *)info{
    UIPageViewController *pageViewController = [[UIPageViewController alloc] init];
    DrawViewController *viewController;
    if (IS_IPHONE) {
        viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewControlleriPhone" bundle:nil];
    }else{
        viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewController" bundle:nil];
    }

    NSString *object_id = [info objectForKey:@"object_id"];
    NoteMenagedObject *obj = [Note getNoteByObject_id:object_id];
    viewController.note = obj;
    [pageViewController setViewControllers:@[viewController] direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
    [mainTabBarController.viewControllers[0] pushViewController:pageViewController animated:NO];
}

- (void)saveContext{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (NSManagedObjectContext *)managedObjectContext{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"DataModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator{
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES],
                             NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES],
                             NSInferMappingModelAutomaticallyOption,
                             nil];
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"DataModelFor3Version.sqlite"];
    NSLog(@"Documents Directory: %@", [self applicationDocumentsDirectory]);
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

- (NSURL *)applicationDocumentsDirectory{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end

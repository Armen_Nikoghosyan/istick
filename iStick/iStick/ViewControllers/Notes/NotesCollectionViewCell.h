#import <UIKit/UIKit.h>
#import "StarView.h"

@protocol NotesCollectionViewCellDelegate <NSObject>

- (void)didSelectDeleteButtonWithObject:(NoteMenagedObject *)note;
- (void)didSelectDeleteButtonWithObjectNotAll:(NoteMenagedObject *)note;
- (void)didChackNote:(NoteMenagedObject *)note;

@end


@interface NotesCollectionViewCell : UICollectionViewCell<StarViewDelegate,UITextFieldDelegate,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

- (void)configureCellWithNote:(NoteMenagedObject *)note isInDeleteState:(BOOL)isInDeleteState withChakedItems:(NSMutableArray *)array willShowStars:(BOOL)willShowStars;
@property (nonatomic, weak) id<NotesCollectionViewCellDelegate> delegate;
@end

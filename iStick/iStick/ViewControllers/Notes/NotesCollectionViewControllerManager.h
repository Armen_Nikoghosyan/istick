#import <Foundation/Foundation.h>
#import "NotesCollectionViewCell.h"

@interface NotesCollectionViewControllerManager : NSObject
+ (instancetype)sharedInstance;
@property (nonatomic, strong) NotesCollectionViewCell *firstResponderCell;
@end

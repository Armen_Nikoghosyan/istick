#import "NotesCollectionViewController.h"
#import "NotesCollectionViewCell.h"
#import "LXReorderableCollectionViewFlowLayout.h"
#import "AppDelegate.h"
#import "DrawViewController.h"
#import "NotesCollectionReusableView.h"
#import "DrawingViewManager.h"
#import "NotesCollectionViewControllerManager.h"

@interface NotesCollectionViewController ()<NotesCollectionViewCellDelegate,LXReorderableCollectionViewDataSource,LXReorderableCollectionViewDelegateFlowLayout,UIScrollViewDelegate>{
    BOOL istouchEnded;
    float lastY;
    BOOL isSortByInEditState;
    NSInteger dragedObjectFirstPosition;
    SortDescriptortype currentSortType;
    BOOL willRemoveState;
    BOOL willShowStarsState;
    CGFloat oldContentHeight;
    NoteMenagedObject *firstNote;
    UIDeviceOrientation lastOrientation;
}

@end

@implementation NotesCollectionViewController

static NSString * const reuseIdentifier = @"NotesCollectionViewCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    lastOrientation = [UIDevice  currentDevice].orientation;
    willRemoveState = NO;
    willShowStarsState = NO;    
    lastY = 0;
    istouchEnded = NO;
    isSortByInEditState = NO;
    currentSortType = SortDescriptortypeCreatedDate;
    _checkedItemsArray = [[NSMutableArray alloc] init];
    [self.collectionView registerClass:[NotesCollectionViewCell class] forCellWithReuseIdentifier:@"NotesCollectionViewCell"];
    UINib *nib = [UINib nibWithNibName:@"NotesCollectionViewCell" bundle:nil];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:@"NotesCollectionViewCell"];
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    if (((NSArray *)_itemsArray[0]).count > 20 && lastOrientation != [UIDevice  currentDevice].orientation) {
        self.collectionView.contentOffsetY = 0;
    }
    lastOrientation = [UIDevice  currentDevice].orientation;

}
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if (_itemsArray.count > 0) {
        NSInteger count = ((NSArray *)_itemsArray[0]).count;
        if (count >= 20) {
            if (scrollView.contentSize.height + 1 < scrollView.contentOffsetY + SCREEN_HEIGHT && scrollView.contentSize.height != oldContentHeight) {
                //oldContentHeight = scrollView.contentSize.height;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"GETMORENOTES" object:nil];
            }
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
    

}

- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    self.collectionView.contentSize = CGSizeMake(self.collectionView.contentSize.width, self.collectionView.contentSize.height + SCREEN_HEIGHT/2);
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    self.collectionView.contentSize = CGSizeMake(self.collectionView.contentSize.width, self.collectionView.contentSize.height - SCREEN_HEIGHT/2);

}

- (void)setItemsArray:(NSMutableArray *)itemsArray{
    _itemsArray = itemsArray;
}

#pragma mark CollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (_itemsArray.count > 0) {
        NSInteger count = ((NSArray *)_itemsArray[0]).count;
        return count;
    }
    return 0;

}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (void)didSelectDeleteButtonWithObject:(NoteMenagedObject *)note{
     NSMutableArray *newArray = [_itemsArray mutableCopy];
     NSMutableArray *newArrayCopy = [_itemsArray mutableCopy];
     BOOL willBeNewArray = NO;
     for (int i = 0; i < newArrayCopy.count; ++i) {
         [((NSMutableArray *)newArrayCopy[i]) removeObject:note];
         if (((NSMutableArray *)newArrayCopy[i]).count == 0) {
             [newArray removeObject:newArray[i]];
             willBeNewArray = YES;
         }
     }
     if (willBeNewArray) {
         [self setArray:newArray willRemove:YES willShowStars:willShowStarsState];
     }else{
         [self setArray:newArrayCopy willRemove:YES willShowStars:willShowStarsState];
     }
     [Note removeObject:note];
}

- (void)didSelectDeleteButtonWithObjectNotAll:(NoteMenagedObject *)note{
    NSMutableArray *newArray = [_itemsArray mutableCopy];
    NSMutableArray *newArrayCopy = [_itemsArray mutableCopy];
    BOOL willBeNewArray = NO;
    for (int i = 0; i < newArrayCopy.count; ++i) {
        [((NSMutableArray *)newArrayCopy[i]) removeObject:note];
        if (((NSMutableArray *)newArrayCopy[i]).count == 0) {
            [newArray removeObject:newArray[i]];
            willBeNewArray = YES;
        }
    }
    if (willBeNewArray) {
        [self setArray:newArray willRemove:YES willShowStars:willShowStarsState];
    }else{
        [self setArray:newArrayCopy willRemove:YES willShowStars:willShowStarsState];
    }
    [Note removeObjectNotAll:note];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)view cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NoteMenagedObject *note;
    NSInteger index = ((NSArray *)(_itemsArray[0])).count - indexPath.row - 1;
    if (index > 100000000000 || index < 0) {
        note = firstNote;
    }else{
        note = (NoteMenagedObject *)((NSArray *)_itemsArray[0])[index];
    }
    NotesCollectionViewCell *cell = (NotesCollectionViewCell *)[view dequeueReusableCellWithReuseIdentifier:@"NotesCollectionViewCell" forIndexPath:indexPath];
    cell.delegate = self;
    [cell configureCellWithNote:note isInDeleteState:willRemoveState withChakedItems:_checkedItemsArray willShowStars:willShowStarsState];
    cell.layer.masksToBounds = YES;
    cell.layer.borderWidth = 1;
    cell.layer.borderColor = [UIColor colorWithRed:230/255.0f green:195/255.0f blue:51/255.0f alpha:1].CGColor;
    [cell layoutIfNeeded];
    
    return cell;


}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((SCREEN_MIN_LENGTH - 60)/2, SCREEN_MAX_LENGTH/3.5);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 20, 5, 20);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5.0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NoteMenagedObject *selectedNote = (NoteMenagedObject *)((NSArray *)_itemsArray[indexPath.section])[((NSArray *)(_itemsArray[0])).count - indexPath.row - 1];
    [[DrawingViewManager sharedInstance] setShowingNote:selectedNote];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_SELECT_TABBAR_ITEM" object:[NSNumber numberWithInt:0]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_SELECT_NOTE" object:nil];
}

- (void)setArray:(NSArray *)localeArray willRemove:(BOOL)willRemove willShowStars:(BOOL)willShowStars{
    willRemoveState = willRemove;
    willShowStarsState = willShowStars;
    _itemsArray = [localeArray mutableCopy];
    //[self.collectionView performBatchUpdates:^{
    if (_itemsArray.count > 0) {
        NSInteger count = ((NSArray *)_itemsArray[0]).count;
        if (count > 0) {
            firstNote = _itemsArray[0][0];
        }
        if (count < 20) {
            self.collectionView.contentOffsetY = 0;
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self.collectionView reloadData];
    });

        //[self.collectionView reloadData];
//    } completion:^(BOOL finished) {
//        
//    }];
}

- (void)didChackNote:(NoteMenagedObject *)note{
    if ([_checkedItemsArray containsObject:note]) {
        [_checkedItemsArray removeObject:note];
    }else{
        [_checkedItemsArray addObject:note];
    }
}

#pragma mark - LXReorderableCollectionViewDelegateFlowLayout methods


- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath {
    return _willAllowChangePossition;
}

- (BOOL)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath canMoveToIndexPath:(NSIndexPath *)toIndexPath {
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath willMoveToIndexPath:(NSIndexPath *)toIndexPath {
    NoteMenagedObject *currentObject = _itemsArray[0][((NSArray *)(_itemsArray[0])).count - fromIndexPath.row - 1];
    [_itemsArray[0] removeObject:currentObject];
    [_itemsArray[0] insertObject:currentObject atIndex:((NSArray *)(_itemsArray[0])).count - toIndexPath.row];
    
}

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout willBeginDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
    dragedObjectFirstPosition = indexPath.row;
    NSLog(@"will begin drag");
}

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout didBeginDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"did begin drag%ld",(long)indexPath.row);
}

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout willEndDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"will end drag");
    NoteMenagedObject *currentObject = _itemsArray[0][((NSArray *)(_itemsArray[0])).count - indexPath.row - 1];
    if (dragedObjectFirstPosition != indexPath.row) {
        if ((((NSArray *)(_itemsArray[0])).count - indexPath.row) == 1) {
            NoteMenagedObject *oldestObject = _itemsArray[0][1];
            currentObject.priority = [NSNumber numberWithDouble:([oldestObject.priority doubleValue] - 30)];
        }else if ((((NSArray *)(_itemsArray[0])).count - indexPath.row) == ((NSArray *)(_itemsArray[0])).count){
            NoteMenagedObject *newerObject = _itemsArray[0][((NSArray *)(_itemsArray[0])).count - 2];
            currentObject.priority = [NSNumber numberWithDouble:([newerObject.priority doubleValue] + 30)];
            
        }else{
            NoteMenagedObject *nextObject = _itemsArray[0][((NSArray *)(_itemsArray[0])).count - indexPath.row];
            NoteMenagedObject *prevObject = _itemsArray[0][((NSArray *)(_itemsArray[0])).count - indexPath.row - 2];
            currentObject.priority = [NSNumber numberWithDouble:(([nextObject.priority doubleValue] + [prevObject.priority doubleValue])/2)];
        }
        NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
        }else{
            @try {
                [Note updateCoreDataObjectInParse:currentObject];
                
            } @catch (NSException *exception) {
            } @finally {
            }
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout didEndDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"did end drag %ld",(long)indexPath.row);
}

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout draggingItemAtIndexPath:(NSIndexPath *)indexPath withGesture:(UILongPressGestureRecognizer *)gestureRecognizer{
}

@end

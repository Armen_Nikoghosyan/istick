#import <UIKit/UIKit.h>

@interface NotesViewController : UIViewController
@property (nonatomic, strong)UIColor *navigationColor;
- (void)navigateToHome;
@end

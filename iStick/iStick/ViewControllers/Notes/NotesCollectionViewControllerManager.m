#import "NotesCollectionViewControllerManager.h"

@implementation NotesCollectionViewControllerManager
+ (instancetype)sharedInstance {
    static NotesCollectionViewControllerManager *notesCollectionViewControllerManager = nil;
    if (!notesCollectionViewControllerManager) {
        notesCollectionViewControllerManager = [[NotesCollectionViewControllerManager alloc] init];
        notesCollectionViewControllerManager.firstResponderCell = nil;

    }
    return notesCollectionViewControllerManager;
}
@end

#import "NotesViewController.h"
#import "NotesCollectionViewCell.h"
#import "AppDelegate.h"
#import "SortBy.h"
#import "LXReorderableCollectionViewFlowLayout.h"
#import "MLKMenuPopover.h"
#import "NMRangeSlider.h"
#import "MVYSideMenuController.h"
#import "NotesCollectionViewController.h"
#import "UIView+Categories.h"
#import "DrawViewController.h"
#import "ColorCollectionViewController.h"
#import "DrawingViewManager.h"
#import "LKAlertView.h"
#import "MainTabBarController.h"
#import "SearchViewController.h"
#import <Masonry/Masonry.h>
#import <RSLoadingView/RSLoadingView-Swift.h>

@interface NotesViewController ()<UISearchBarDelegate,UIScrollViewDelegate,SortByDelegate,MLKMenuPopoverDelegate,NMRangeSliderDelegate,UIGestureRecognizerDelegate,ColorCollectionViewDelegate>{
    __weak IBOutlet UIButton *documentsButton;
    __weak IBOutlet UIButton *starButton;
    __weak IBOutlet UIButton *queButton;
    __weak IBOutlet UIButton *plussButton;
    __weak IBOutlet UIView *navigationView;
    __weak IBOutlet UIButton *editOkButton;
    __weak IBOutlet UIButton *backRemoveAllButton;
    __weak IBOutlet UIButton *searchButton;
    __weak IBOutlet UIButton *menuButton;
    __weak IBOutlet UIButton *homeButton;
    __weak IBOutlet UILabel *dateLabel;
    __weak IBOutlet UIButton *galleryButton;
    MLKMenuPopover *serchMenuPopover;
    MLKMenuPopover *sortMenuPopover;
    UIView *dateSliderView;
    UIView *colorPickerView;
    NMRangeSlider *slider;
    NSDate *lowerDate;
    NSDate *upperDate;
    UILabel *lowerDateLabel;
    UILabel *upperDateLabel;
    NSDate *lastDate;
    NotesCollectionViewController *notesCollectionViewController;
    NoteMenagedObject *note_file;
    UIView * backgroundColorPopoverView;
    BOOL willRemove;
    NSNumber *fromArchive;
    __weak IBOutlet UIView *fileView;
    __weak IBOutlet UITextField *fileTextField;
    UISearchBar *searchBar;
    SortBy *sortByView;
    NSArray *titleMenuItems;
    NSString *selectedTitle;
    NSInteger *selectedColor;
    UIDeviceOrientation lastOrientation;
    UIColor *allButtonsColor;

}

@property (nonatomic, strong) NSMutableArray *searchArray;
@property ()RSLoadingView *rSLoadingView;
@end

@implementation NotesViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin) name:@"DIDLOGIN" object:nil];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    notesCollectionViewController.willAllowChangePossition = NO;
    allButtonsColor = [UIColor colorWithRed:230/255.0f green:195/255.0f blue:51/255.0f alpha:1];
    [navigationView setBackgroundColor:[UIColor colorWithRed:230/255.0f green:195/255.0f blue:51/255.0f alpha:1]];
    [searchButton addShadowToView];
    [editOkButton addShadowToView];
    [plussButton addShadowToView];
    [starButton addShadowToView];
    [backRemoveAllButton addShadowToView];
    [menuButton addShadowToView];
    [homeButton addShadowToView];
    [searchButton setImage:[[UIImage imageNamed:@"search_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                  forState:UIControlStateNormal];
    [homeButton setImage:[homeButton.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                forState:UIControlStateNormal];
    [editOkButton setImage:[[UIImage imageNamed:@"close_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                  forState:UIControlStateNormal];
    [plussButton setImage:[plussButton.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                 forState:UIControlStateNormal];
    [backRemoveAllButton setImage:[backRemoveAllButton.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                         forState:UIControlStateNormal];
    [starButton setImage:[starButton.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                forState:UIControlStateNormal];
    [starButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [menuButton setImage:[menuButton.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                forState:UIControlStateNormal];
    [menuButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [starButton setTintColor:[UIColor whiteColor]];
    [backRemoveAllButton setTintColor:[UIColor whiteColor]];
    [searchButton setTintColor:[UIColor whiteColor]];
    [editOkButton setTintColor:[UIColor whiteColor]];
    [plussButton setTintColor:[UIColor whiteColor]];
    [menuButton setTintColor:[UIColor whiteColor]];
    [homeButton setTintColor:[UIColor whiteColor]];
    
    fromArchive = [NSNumber numberWithInt:0];
    
    notesCollectionViewController = [[NotesCollectionViewController alloc]initWithNibName:@"NotesCollectionViewController" bundle:nil];
    [notesCollectionViewController.collectionView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self addChildViewController:notesCollectionViewController];
    [self.view addSubview:notesCollectionViewController.collectionView];
    [notesCollectionViewController.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(80);
        make.centerX.equalTo(self.view.mas_centerX);
        make.width.equalTo(self.view.mas_width);
        make.height.equalTo(self.view.mas_height).offset(-40);
    }];
//    [self configureDateSliderView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notesDidSync) name:@"NOTES_DID_SYNC" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectObject:) name:@"COLLECTION_DID_DID_SELECT_OBJECT" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sortButtonPressed) name:@"ORDER_BUTTON_PRESSED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(trashButtonPressed) name:@"TRASH_BUTTON_PRESSED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(homeButtonPressed:) name:@"HOME_BUTTON_PRESSED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(starButtonPressed) name:@"STAR_BUTTON_PRESSED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(titleItemSelected:) name:@"TITLE_ITEM_SELECTED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectSearch) name:@"DID_SELECT_SEARCH" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didCloseSearch) name:@"DID_CLOSE_SEARCH" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pinItemSelected:) name:@"PIN_BUTTON_PRESSED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(phoneItemSelected) name:@"PHONE_BUTTON_PRESSED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openedItemSelected:) name:@"OPED_BUTTON_PRESSED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(menuButtonPressed:) name:@"MENU_BUTTON_PRESSED" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(galleryButtonPressed:) name:@"GALLERY_BUTTON_PRESSED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(documentsButtonPressed:) name:@"DOC_BUTTON_PRESSED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showDateSliderView) name:@"SEARCH_BY_DATE_BUTTON_PRESSED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showSearchBar) name:@"SEARCH_BY_NAME_BUTTON_PRESSED" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSavedNote:) name:@"DID_SAVED_NOTE" object:nil];

    [self setdefaultNavigationState];
    [self addSearchBar];
    [self configureColorPickerView];
    [self.view bringSubviewToFront:fileView];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [notesCollectionViewController.collectionView addSubview:refreshControl];
    [self homeButtonPressed:homeButton];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(orientationChanged:)
     name:UIDeviceOrientationDidChangeNotification
     object:[UIDevice currentDevice]];
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateTime) userInfo:nil repeats:YES];
    return  self;

}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    [notesCollectionViewController viewWillLayoutSubviews];
//    [notesCollectionViewController.collectionView setFrame:CGRectMake(0, 50, SCREEN_WIDTH, SCREEN_HEIGHT - 40)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    notesCollectionViewController.willAllowChangePossition = NO;
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCollectionView) name:@"COLLECTION_DID_CHANGE" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMoreNotes) name:@"GETMORENOTES" object:nil];
    notesCollectionViewController.collectionView.contentOffsetY = 0;
    
   

}

-(void)didSavedNote:(NSNotification *)notInfo {
    NSDictionary * notificationDictionary = (NSDictionary*)notInfo.object;
 
    [[DrawingViewManager sharedInstance] setPrevTitle:selectedTitle];
    [[DrawingViewManager sharedInstance] setPrevCategory:[NoteHelper sharedInstance].category == nil?@"":[NoteHelper sharedInstance].category];
    [[DrawingViewManager sharedInstance] setWillCreateNewNote:YES];
    [[DrawingViewManager sharedInstance] setPhotoFromCamera:UIImageJPEGRepresentation([notificationDictionary valueForKey:@"image"], 1)];
    [[DrawingViewManager sharedInstance] setSaveType:[notificationDictionary valueForKey:@"type"]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_SELECT_TABBAR_ITEM" object:[NSNumber numberWithInt:0]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_TAKED_IMAGE" object:[NSNumber numberWithInt:0]];
}




- (void)refreshCollectionView{
    [Note syncData]; //Armen
    [NoteHelper sharedInstance].isNewSearch = YES;
    [Note getNotesUsingNoteHelperUsingBlock:^(NSArray *notes) {
        _searchArray = [@[notes] mutableCopy];
        notesCollectionViewController.collectionView.contentOffsetY = 0;
        [notesCollectionViewController setArray:@[notes] willRemove:willRemove willShowStars:starButton.selected];
    }];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (willRemove) {
        [self ok];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [(MainTabBarController *)self.tabBarController setTrashButtonColor:[UIColor blackColor]];
    [(MainTabBarController *)self.tabBarController setStarButtonColor:[UIColor blackColor]];
    [(MainTabBarController *)self.tabBarController setPinButtonColor:[UIColor blackColor]];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"COLLECTION_DID_CHANGE" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GETMORENOTES" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DIDLOGIN" object:nil];
    
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)setNavigationColor:(UIColor *)navigationColor{
    [navigationView setBackgroundColor:[UIColor clearColor]];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    [Note syncData];
    [refreshControl endRefreshing];
}

- (void)didSelectObject:(NSNotification *)notif{
    [[DrawingViewManager sharedInstance] setShowingNote:(NoteMenagedObject *)notif.object];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_SELECT_TABBAR_ITEM" object:[NSNumber numberWithInt:0]];
}

- (void)notesDidSync{
    
    
    [NoteHelper sharedInstance].isNewSearch = YES;
    [Note getNotesUsingNoteHelperUsingBlock:^(NSArray *notes) {
        _searchArray = [@[notes] mutableCopy];
        if (self.rSLoadingView != nil ) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.rSLoadingView hide];
                self.rSLoadingView = nil;
            });
            
        }
        [notesCollectionViewController setArray:@[notes] willRemove:willRemove willShowStars:starButton.selected];
    }];
}

- (void)configureSearchMenuPopover{
    NSArray *searchArrayItems = @[@"by date",@"by text"];
    titleMenuItems = [Note getAllTitles];
    NSMutableArray *upperCaseTitlesArray = [[NSMutableArray alloc] init];
    for (NSString *title in titleMenuItems) {
        [upperCaseTitlesArray addObject:[title uppercaseString]];
    }
    if (notesCollectionViewController.checkedItemsArray.count) {
        serchMenuPopover = [[MLKMenuPopover alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/4, 40, 250, (titleMenuItems.count + 4) * 40  > SCREEN_HEIGHT - 100 ? SCREEN_HEIGHT - 100 : (titleMenuItems.count + 4) * 40)
                                                       menuItems: [searchArrayItems arrayByAddingObjectsFromArray: upperCaseTitlesArray]];
    }else{
        serchMenuPopover = [[MLKMenuPopover alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/4, 40, 250, (titleMenuItems.count + 4) * 40  > SCREEN_HEIGHT - 100 ? SCREEN_HEIGHT - 100 : (titleMenuItems.count + 4) * 40)
                                                       menuItems: [searchArrayItems arrayByAddingObjectsFromArray:upperCaseTitlesArray]];
    }
    serchMenuPopover.centerX = SCREEN_WIDTH/2;
    [serchMenuPopover hideArrow];
    serchMenuPopover.tag = 0;
    serchMenuPopover.menuPopoverDelegate = self;
    [serchMenuPopover showInView:self.view];
}

- (void)configureSortMenuPopover{
    NSArray *sortArrayItems = @[@"by view date",@"by created date",@"by priority",@"by title",@"by color"];
    sortMenuPopover = [[MLKMenuPopover alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 150)/2, 50, 250, sortArrayItems.count * 40) menuItems:sortArrayItems];
    sortMenuPopover.centerX = SCREEN_WIDTH/2;
    [sortMenuPopover hideArrow];
    sortMenuPopover.tag = 1;
    sortMenuPopover.menuPopoverDelegate = self;
}

- (IBAction)modeButtonPressed:(id)sender {
    [sender setTintColor:[UIColor redColor]];
}

- (void) didSelectSearch{
    [searchButton setTintColor:[UIColor whiteColor]];
}

- (void)didCloseSearch{
    //[searchButton setTintColor:[UIColor whiteColor]];
}

- (void)titleItemSelected:(NSNotification *)notif{
    NSArray *array = notif.object;
    [NoteHelper sharedInstance].opened = nil;
    if (array.count > 1) {
        [NoteHelper sharedInstance].category = array[1];
    }else{
        [NoteHelper sharedInstance].category = nil;
    }
    [self didSelectTitle:array.firstObject];
}

- (void)phoneItemSelected{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_SELECT_TABBAR_ITEM" object:[NSNumber numberWithInt:1]];
    [self setLowDate:nil upperDate:nil];
    [NoteHelper sharedInstance].favorite = nil;
    [NoteHelper sharedInstance].phoneNumber = YES;
    [NoteHelper sharedInstance].opened = nil;
    [NoteHelper sharedInstance].fromArchive = [NSNumber numberWithInt:0];
    [NoteHelper sharedInstance].pin = nil;
    [NoteHelper sharedInstance].gallery = [NSNumber numberWithInt:0];
    [NoteHelper sharedInstance].document = [NSNumber numberWithInt:0];
    if ([SearchViewController sharedInstance].view) {
        [[SearchViewController sharedInstance].view removeFromSuperview];
    }
    [self reloadTableView];
}

- (void)pinItemSelected:(NSNotification *)notif{
    [self setLowDate:nil upperDate:nil];
    [NoteHelper sharedInstance].favorite = nil;
    [NoteHelper sharedInstance].phoneNumber = NO;
    [NoteHelper sharedInstance].opened = nil;
    [NoteHelper sharedInstance].fromArchive = [NSNumber numberWithInt:0];
    [NoteHelper sharedInstance].pin = [NSNumber numberWithInt:1];
    [NoteHelper sharedInstance].gallery = nil;
    [NoteHelper sharedInstance].document = nil;
    if ([SearchViewController sharedInstance].view) {
        [[SearchViewController sharedInstance].view removeFromSuperview];
    }
    [self reloadTableView];
}

- (void)openedItemSelected:(NSNotification *)notif{
    [self setLowDate:nil upperDate:nil];
    [NoteHelper sharedInstance].title = nil;
    [NoteHelper sharedInstance].category = nil;
    [NoteHelper sharedInstance].favorite = nil;
    [NoteHelper sharedInstance].phoneNumber = NO;
    [NoteHelper sharedInstance].pin = nil;
    [NoteHelper sharedInstance].fromArchive = [NSNumber numberWithInt:0];
    [NoteHelper sharedInstance].opened = [NSNumber numberWithInt:1];
    [NoteHelper sharedInstance].gallery = nil;
    [NoteHelper sharedInstance].document = nil;
    [self reloadTableView];
}

- (void)starButtonPressed{
    [self setLowDate:nil upperDate:nil];
    [NoteHelper sharedInstance].pin = nil;
    [NoteHelper sharedInstance].opened = nil;
    [NoteHelper sharedInstance].fromArchive = [NSNumber numberWithInt:0];;
    [NoteHelper sharedInstance].favorite = [NSNumber numberWithInt:1];
    [NoteHelper sharedInstance].phoneNumber = NO;
    [NoteHelper sharedInstance].gallery = nil;
    [NoteHelper sharedInstance].document = nil;
    if ([SearchViewController sharedInstance].view) {
        [[SearchViewController sharedInstance].view removeFromSuperview];
    }
    [self reloadTableView];
}

- (void)didSelectTitle:(NSString *)title{
    self.tabBarController.selectedIndex = 1;
    [self setLowDate:nil upperDate:nil];
    [NoteHelper sharedInstance].title = title;
    [NoteHelper sharedInstance].favorite = nil;
    [NoteHelper sharedInstance].phoneNumber = NO;
    [NoteHelper sharedInstance].opened = nil;
    [NoteHelper sharedInstance].pin = nil;
    [NoteHelper sharedInstance].fromArchive = [NSNumber numberWithInt:0];
    [NoteHelper sharedInstance].gallery = nil;
    [NoteHelper sharedInstance].document = nil;
    [self reloadTableView];
    notesCollectionViewController.collectionView.contentOffsetY = 0;
}

- (void)trashButtonPressed{
    [NoteHelper sharedInstance].title = nil;
    [NoteHelper sharedInstance].category = nil;
    [NoteHelper sharedInstance].favorite = nil;
    [NoteHelper sharedInstance].phoneNumber = NO;
    [NoteHelper sharedInstance].opened = nil;
    [NoteHelper sharedInstance].pin = nil;
    [NoteHelper sharedInstance].gallery = nil;
    [NoteHelper sharedInstance].document = nil;
    [self setLowDate:nil upperDate:nil];
    [NoteHelper sharedInstance].fromArchive = [NSNumber numberWithInt:1];
    if ([SearchViewController sharedInstance].view) {
        [[SearchViewController sharedInstance].view removeFromSuperview];
    }
    [self reloadTableView];
}
- (IBAction)queButtonPressed:(id)sender {
    [self phoneItemSelected];
}
    
- (IBAction)documentsButtonPressed:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_SELECT_TABBAR_ITEM" object:[NSNumber numberWithInt:1]];
    [NoteHelper sharedInstance].fromArchive = [NSNumber numberWithInt:0];
    [self setLowDate:nil upperDate:nil];
    [NoteHelper sharedInstance].favorite = nil;
    [NoteHelper sharedInstance].phoneNumber = NO;
    [NoteHelper sharedInstance].opened = nil;
    [NoteHelper sharedInstance].title = nil;
    [NoteHelper sharedInstance].category = nil;
    [NoteHelper sharedInstance].pin = nil;
    [NoteHelper sharedInstance].gallery =  [NSNumber numberWithInt:0];
    [NoteHelper sharedInstance].document = [NSNumber numberWithInt:1];
    if ([SearchViewController sharedInstance].view) {
        [[SearchViewController sharedInstance].view removeFromSuperview];
    }
     [self reloadTableView];
     [documentsButton setTintColor:[UIColor redColor]];
}
    
    
- (IBAction)galleryButtonPressed:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_SELECT_TABBAR_ITEM" object:[NSNumber numberWithInt:1]];
    [NoteHelper sharedInstance].fromArchive = [NSNumber numberWithInt:0];
    [self setLowDate:nil upperDate:nil];
    [NoteHelper sharedInstance].favorite = nil;
    [NoteHelper sharedInstance].phoneNumber = NO;
    [NoteHelper sharedInstance].opened = nil;
    [NoteHelper sharedInstance].title = nil;
    [NoteHelper sharedInstance].category = nil;
    [NoteHelper sharedInstance].pin = nil;
    [NoteHelper sharedInstance].gallery =  [NSNumber numberWithInt:1];
    [NoteHelper sharedInstance].document = [NSNumber numberWithInt:0];
    if ([SearchViewController sharedInstance].view) {
        [[SearchViewController sharedInstance].view removeFromSuperview];
    }
    [self reloadTableView];
    [galleryButton setTintColor:[UIColor redColor]];
}

- (IBAction)menuButtonPressed:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_SELECT_TABBAR_ITEM" object:[NSNumber numberWithInt:1]];
    [NoteHelper sharedInstance].title = nil;
    [NoteHelper sharedInstance].category = nil;
    [NoteHelper sharedInstance].favorite = nil;
    [NoteHelper sharedInstance].phoneNumber = NO;
    [NoteHelper sharedInstance].opened = nil;
    [NoteHelper sharedInstance].pin = nil;
    [NoteHelper sharedInstance].gallery = [NSNumber numberWithInt:0];
    [NoteHelper sharedInstance].document = [NSNumber numberWithInt:0];
    [self setLowDate:nil upperDate:nil];
    [NoteHelper sharedInstance].fromArchive = [NSNumber numberWithInt:0];
    [self reloadTableView];

//    NSArray *notes = [Note getAllObjectsUsingSortDescriptor:[NoteHelper sharedInstance].sortType fromArchive:@0 withTitle:@""];
//    [notesCollectionViewController setArray:@[notes] willRemove:willRemove willShowStars:starButton.selected];
//    [self setButtonsColor];
}
-(void)didLogin{
   
    if ([PFUser currentUser]) {
        self.rSLoadingView = [[RSLoadingView alloc]init];
        [self.rSLoadingView showOn:self.view];
        [Note syncData];
    }

}


- (void)sliderButtonPressed{
    [self reloadTableView];
}

- (void)reloadTableView{
    [Note syncData];
    [NoteHelper sharedInstance].isNewSearch = YES;
    [Note getNotesUsingNoteHelperUsingBlock:^(NSArray *notes) {
        _searchArray = [@[notes] mutableCopy];
        notesCollectionViewController.collectionView.contentOffsetY = 0;
        [notesCollectionViewController setArray:@[notes] willRemove:willRemove willShowStars:starButton.selected];
    }];
    [self setButtonsColor];
}

static NoteHelper * extracted() {
    return [NoteHelper sharedInstance];
}

- (void)setButtonsColor{
    
    if ([NoteHelper sharedInstance].opened.integerValue > 0) {
        [menuButton setTintColor:[UIColor whiteColor]];
        [(MainTabBarController *)self.tabBarController setStarButtonColor:[UIColor whiteColor]];
        [(MainTabBarController *)self.tabBarController setPinButtonColor:[UIColor whiteColor]];
        [searchButton.layer removeAllAnimations];
        [searchButton setTintColor:[UIColor whiteColor]];
         [galleryButton setTintColor:[UIColor whiteColor]];
        [documentsButton setTintColor:[UIColor whiteColor]];
        [(MainTabBarController *)self.tabBarController setTrashButtonColor:[UIColor whiteColor]];
        [(MainTabBarController *)self.tabBarController setOpenedColor:[UIColor redColor]];
    }else{
        [(MainTabBarController *)self.tabBarController setOpenedColor:[UIColor whiteColor]];
    }
    if ([[NoteHelper sharedInstance].fromArchive integerValue] == 0) {
        [(MainTabBarController *)self.tabBarController setTrashButtonColor:[UIColor blackColor]];
        [(MainTabBarController *)self.tabBarController setStarButtonColor:[UIColor blackColor]];
        [(MainTabBarController *)self.tabBarController setPinButtonColor:[UIColor blackColor]];
        [searchButton.layer removeAllAnimations];
        [searchButton setTintColor:[UIColor whiteColor]];
        [menuButton setTintColor:[UIColor redColor]];
        [galleryButton setTintColor:[UIColor whiteColor]];
         [documentsButton setTintColor:[UIColor whiteColor]];
    }else{
        [menuButton setTintColor:[UIColor whiteColor]];
        [galleryButton setTintColor:[UIColor whiteColor]];
         [documentsButton setTintColor:[UIColor whiteColor]];
        [(MainTabBarController *)self.tabBarController setStarButtonColor:[UIColor blackColor]];
        [(MainTabBarController *)self.tabBarController setPinButtonColor:[UIColor blackColor]];
        [searchButton.layer removeAllAnimations];
        [searchButton setTintColor:[UIColor whiteColor]];
        [(MainTabBarController *)self.tabBarController setTrashButtonColor:[UIColor redColor]];
    }
    if ([NoteHelper sharedInstance].gallery && [NoteHelper sharedInstance].gallery != [NSNumber numberWithInt:0]) {
        [(MainTabBarController *)self.tabBarController setPinButtonColor:[UIColor blackColor]];
        [menuButton setTintColor:[UIColor whiteColor]];
        [galleryButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [(MainTabBarController *)self.tabBarController setTrashButtonColor:[UIColor blackColor]];
        [(MainTabBarController *)self.tabBarController setStarButtonColor:[UIColor redColor]];
    } else {
        [menuButton setTintColor:[UIColor whiteColor]];
         [galleryButton setTintColor:[UIColor whiteColor]];
        [documentsButton setTintColor:[UIColor whiteColor]];
        [(MainTabBarController *)self.tabBarController setStarButtonColor:[UIColor blackColor]];
        [(MainTabBarController *)self.tabBarController setPinButtonColor:[UIColor blackColor]];
        [searchButton.layer removeAllAnimations];
        [searchButton setTintColor:[UIColor whiteColor]];
        [(MainTabBarController *)self.tabBarController setTrashButtonColor:[UIColor redColor]];
    }
    if ([NoteHelper sharedInstance].document && [NoteHelper sharedInstance].document != [NSNumber numberWithInt:0]) {
        [(MainTabBarController *)self.tabBarController setPinButtonColor:[UIColor blackColor]];
        [menuButton setTintColor:[UIColor whiteColor]];
        [documentsButton setTintColor:[UIColor redColor]];
        [(MainTabBarController *)self.tabBarController setTrashButtonColor:[UIColor blackColor]];
        [(MainTabBarController *)self.tabBarController setStarButtonColor:[UIColor redColor]];
    } else {
        [menuButton setTintColor:[UIColor whiteColor]];
        [documentsButton setTintColor:[UIColor whiteColor]];
        [(MainTabBarController *)self.tabBarController setStarButtonColor:[UIColor blackColor]];
        [(MainTabBarController *)self.tabBarController setPinButtonColor:[UIColor blackColor]];
        [searchButton.layer removeAllAnimations];
        [searchButton setTintColor:[UIColor whiteColor]];
        [(MainTabBarController *)self.tabBarController setTrashButtonColor:[UIColor redColor]];
    }
    if ([NoteHelper sharedInstance].favorite) {
        [(MainTabBarController *)self.tabBarController setPinButtonColor:[UIColor blackColor]];
        [menuButton setTintColor:[UIColor whiteColor]];
        [galleryButton setTintColor:[UIColor whiteColor]];
        [documentsButton setTintColor:[UIColor whiteColor]];
        [(MainTabBarController *)self.tabBarController setTrashButtonColor:[UIColor blackColor]];
        [(MainTabBarController *)self.tabBarController setStarButtonColor:[UIColor redColor]];
    }else{
        [(MainTabBarController *)self.tabBarController setStarButtonColor:[UIColor blackColor]];
    }
    
    if (extracted().pin) {
        [(MainTabBarController *)self.tabBarController setStarButtonColor:[UIColor blackColor]];
        [menuButton setTintColor:[UIColor whiteColor]];
        
        [(MainTabBarController *)self.tabBarController setTrashButtonColor:[UIColor blackColor]];
        [(MainTabBarController *)self.tabBarController setPinButtonColor:[UIColor redColor]];
    }else{
        [(MainTabBarController *)self.tabBarController setPinButtonColor:[UIColor blackColor]];
    }
    if ([NoteHelper sharedInstance].phoneNumber) {
        [(MainTabBarController *)self.tabBarController setStarButtonColor:[UIColor blackColor]];
        [menuButton setTintColor:[UIColor whiteColor]];
        [galleryButton setTintColor:[UIColor whiteColor]];
        [documentsButton setTintColor:[UIColor whiteColor]];
        [(MainTabBarController *)self.tabBarController setTrashButtonColor:[UIColor blackColor]];
        [(MainTabBarController *)self.tabBarController setPinButtonColor:[UIColor blackColor]];
        [queButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
//        [(MainTabBarController *)self.tabBarController setPhoneButtonColor:[UIColor redColor]];
    }else{
//        [(MainTabBarController *)self.tabBarController setPhoneButtonColor:[UIColor blackColor]];
        [queButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    if ([NoteHelper sharedInstance].title == nil || [[NoteHelper sharedInstance].title isEqualToString:@""]) {
        [searchButton.layer removeAllAnimations];
        [searchButton setTintColor:[UIColor whiteColor]];
        selectedTitle = @"";

    }else{
        [menuButton setTintColor:[UIColor whiteColor]];
        [galleryButton setTintColor:[UIColor whiteColor]];
        [documentsButton setTintColor:[UIColor whiteColor]];
        [(MainTabBarController *)self.tabBarController setTrashButtonColor:[UIColor blackColor]];
        
        [searchButton setTintColor:[UIColor redColor]];
        [self animateSearchButton];
        selectedTitle = [NoteHelper sharedInstance].title;
    }
    plussButton.tintColor = [UIColor whiteColor];
    homeButton.tintColor = [UIColor whiteColor];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.view endEditing:YES];

}

- (void)getMoreNotes{
    NSLog(@"WSSSSSssssssSSSSSSSssSSsSSSSSSSSSSSSSSS");
    [NoteHelper sharedInstance].isNewSearch = NO;
    [Note getNotesUsingNoteHelperUsingBlock:^(NSArray *notes) {
        _searchArray = [@[notes] mutableCopy];
        [notesCollectionViewController setArray:@[notes] willRemove:willRemove willShowStars:starButton.selected];
        [NoteHelper sharedInstance].isNewSearch = YES;

    }];
}

#pragma mark DateSlider

- (void)configureDateSliderView{
    dateSliderView = [[UIView alloc] initWithFrame:CGRectMake(50, 0, SCREEN_WIDTH - 50, 40)];
    [dateSliderView setBackgroundColor:[UIColor colorWithWhite:199.0f/255.0f alpha:1]];
    dateSliderView.clipsToBounds = NO;
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(dateSliderView.width - 60,0, 60, 40)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(hideDateSliderView) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitleColor:[UIColor colorWithRed:0 green:0.5 blue:1 alpha:1] forState:UIControlStateNormal];
    slider = [[NMRangeSlider alloc] initWithFrame:CGRectMake(10, 5, dateSliderView.width - 80, 30)];
    slider.sliderDelegate = self;
    lastDate = [Note getOldestObjectDate];
    lowerDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 35, 150, 20)];
    lowerDateLabel.right = slider.right;
    [lowerDateLabel setTextAlignment:NSTextAlignmentRight];
    [lowerDateLabel setText:[self stringFromDate:lastDate]];
    upperDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 35, 150, 20)];
    [upperDateLabel setText:[self stringFromDate:[NSDate new]]];
    [dateSliderView addSubview:cancelButton];
    cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
    [dateSliderView addConstraint:[NSLayoutConstraint constraintWithItem:cancelButton
                                                               attribute:NSLayoutAttributeTop
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:dateSliderView
                                                               attribute:NSLayoutAttributeTop
                                                              multiplier:1.0f
                                                                constant:0.0f]];
    [dateSliderView addConstraint:[NSLayoutConstraint constraintWithItem:cancelButton
                                                               attribute:NSLayoutAttributeRight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:dateSliderView
                                                               attribute:NSLayoutAttributeRight
                                                              multiplier:1.0f
                                                                constant:0.0f]];
    [cancelButton addConstraint:[NSLayoutConstraint constraintWithItem:cancelButton
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1.0f
                                                              constant:60.0f]];
    [cancelButton addConstraint:[NSLayoutConstraint constraintWithItem:cancelButton
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1.0f
                                                              constant:40.0f]];
    
    [dateSliderView addSubview:lowerDateLabel];
    lowerDateLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [dateSliderView addConstraint:[NSLayoutConstraint constraintWithItem:lowerDateLabel
                                                               attribute:NSLayoutAttributeTop
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:dateSliderView
                                                               attribute:NSLayoutAttributeTop
                                                              multiplier:1.0f
                                                                constant:35.0f]];
    [dateSliderView addConstraint:[NSLayoutConstraint constraintWithItem:lowerDateLabel
                                                               attribute:NSLayoutAttributeRight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:cancelButton
                                                               attribute:NSLayoutAttributeLeft
                                                              multiplier:1.0f
                                                                constant:5.0f]];
    [lowerDateLabel addConstraint:[NSLayoutConstraint constraintWithItem:lowerDateLabel
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1.0f
                                                              constant:150.0f]];
    [lowerDateLabel addConstraint:[NSLayoutConstraint constraintWithItem:lowerDateLabel
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1.0f
                                                              constant:20.0f]];
    
    [dateSliderView addSubview:upperDateLabel];
    upperDateLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [dateSliderView addConstraint:[NSLayoutConstraint constraintWithItem:upperDateLabel
                                                               attribute:NSLayoutAttributeTop
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:dateSliderView
                                                               attribute:NSLayoutAttributeTop
                                                              multiplier:1.0f
                                                                constant:35.0f]];
    [dateSliderView addConstraint:[NSLayoutConstraint constraintWithItem:upperDateLabel
                                                               attribute:NSLayoutAttributeLeft
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:dateSliderView
                                                               attribute:NSLayoutAttributeLeft
                                                              multiplier:1.0f
                                                                constant:0.0f]];
    [upperDateLabel addConstraint:[NSLayoutConstraint constraintWithItem:upperDateLabel
                                                               attribute:NSLayoutAttributeWidth
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeNotAnAttribute
                                                              multiplier:1.0f
                                                                constant:150.0f]];
    [upperDateLabel addConstraint:[NSLayoutConstraint constraintWithItem:upperDateLabel
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeNotAnAttribute
                                                              multiplier:1.0f
                                                                constant:20.0f]];
    
    [dateSliderView addSubview:slider];
    slider.translatesAutoresizingMaskIntoConstraints = NO;
    [dateSliderView addConstraint:[NSLayoutConstraint constraintWithItem:slider
                                                               attribute:NSLayoutAttributeTop
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:dateSliderView
                                                               attribute:NSLayoutAttributeTop
                                                              multiplier:1.0f
                                                                constant:5.0f]];
    [dateSliderView addConstraint:[NSLayoutConstraint constraintWithItem:slider
                                                               attribute:NSLayoutAttributeLeft
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:dateSliderView
                                                               attribute:NSLayoutAttributeLeft
                                                              multiplier:1.0f
                                                                constant:5.0f]];
    [dateSliderView addConstraint:[NSLayoutConstraint constraintWithItem:slider
                                                               attribute:NSLayoutAttributeRight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:cancelButton
                                                               attribute:NSLayoutAttributeLeft
                                                              multiplier:1.0f
                                                                constant:5.0f]];
    [slider addConstraint:[NSLayoutConstraint constraintWithItem:slider
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeNotAnAttribute
                                                              multiplier:1.0f
                                                                constant:30.0f]];
}

- (void)showDateSliderView{
    [self.view addSubview:dateSliderView];
    [self setLowDate:nil upperDate:nil];
    dateSliderView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:dateSliderView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:dateSliderView
                                                          attribute:NSLayoutAttributeRight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeRight
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:dateSliderView
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1.0f
                                                           constant:50.0f]];
    [dateSliderView addConstraint:[NSLayoutConstraint constraintWithItem:dateSliderView
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0f
                                                           constant:40.0f]];

}

- (void)hideDateSliderView{
    [dateSliderView removeFromSuperview];
}

- (void)sliderValueDidChange{
    NSDate *last = lastDate;
    int daysToAdd = (int)((double)[self daysBetween:[NSDate new] dates:lastDate] * (1 - slider.upperValue));
    NSLog(@"%i",daysToAdd);
    lowerDate = [last dateByAddingTimeInterval:60*60*24*daysToAdd];
    [lowerDateLabel setText:[self stringFromDate:lowerDate]];
    NSDate *now = [NSDate new];
    daysToAdd = [self daysBetween:[NSDate new] dates:lastDate] * slider.lowerValue;
    upperDate = [now dateByAddingTimeInterval:-60*60*24*daysToAdd];
    [upperDateLabel setText:[self stringFromDate:upperDate]];
    [self setLowDate:lowerDate upperDate:upperDate];
}

-(void)sliderDidEndDragging{
    [self sliderButtonPressed];
}

- (void)setLowDate:(NSDate *)lowdate upperDate:(NSDate *)upDate{
    [NoteHelper sharedInstance].fromDate = lowerDate;
    [NoteHelper sharedInstance].toDate = upDate;
}

- (NSInteger)daysBetween:(NSDate *)date2 dates:(NSDate *)date1{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:date1
                                                          toDate:[date2 dateByAddingTimeInterval:60*60*24]
                                                         options:NSCalendarWrapComponents];
    return [components day];
}

- (NSString *)stringFromDate:(NSDate *)date{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"M/d/YY"];
    [formatter setTimeZone:[NSTimeZone defaultTimeZone]];
    return [formatter stringFromDate:date];
}

#pragma mark ColorPicker

- (void)configureColorPickerView{
    colorPickerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
    [colorPickerView setBackgroundColor:[UIColor colorWithWhite:199.0f/255.0f alpha:1]];
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 60,0, 60, 40)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(hideColorPickerView) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitleColor:[UIColor colorWithRed:0 green:0.5 blue:1 alpha:1] forState:UIControlStateNormal];
    NSArray *colorArray = [APPDELEGATE colorsArray];
    for (int i = 0; i < colorArray.count; i++) {
        UIButton *bgColorButton = [[UIButton alloc] initWithFrame:CGRectMake(50 * i + 25, 5, 30, 30)];
        bgColorButton.tag = i;
        [bgColorButton setBackgroundColor:colorArray[i]];
        if (i == 0) {
            [bgColorButton setBackgroundColor:[APPDELEGATE colorsArray][i]];
        }
        [bgColorButton addTarget:self action:@selector(didSelectBgColor:) forControlEvents:UIControlEventTouchUpInside];
        [colorPickerView addSubview:bgColorButton];
    }
    [colorPickerView addSubview:cancelButton];
}

- (void)showColorPickerView{
    [self.view addSubview:colorPickerView];
}

- (void)hideColorPickerView{
    [colorPickerView removeFromSuperview];
}

- (void)didSelectBgColor:(UIButton *)sender{
    
}

#pragma mark Search Bar

- (void)showSearchBar{
    [searchBar becomeFirstResponder];
    searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    [self.view addSubview:searchBar];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:searchBar
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:searchBar
                                                          attribute:NSLayoutAttributeRight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeRight
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:searchBar
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1.0f
                                                           constant:50.0f]];
    [searchBar addConstraint:[NSLayoutConstraint constraintWithItem:searchBar
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0f
                                                           constant:40.0f]];
}

- (void)hideSearchBar{
    [searchBar setText:@""];
    [self searchBar:searchBar textDidChange:@""];
    [searchBar resignFirstResponder];
    [searchBar removeFromSuperview];
}

- (void)addSearchBar{
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(50, 0, [UIScreen mainScreen].bounds.size.width - 50, 40)];
    searchBar.translatesAutoresizingMaskIntoConstraints = NO;
    searchBar.delegate = self;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)search{
    [search setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)search{
    [search setShowsCancelButton:YES animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)search{
    [self hideSearchBar];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.view endEditing:YES];
}

- (void)searchBar:(UISearchBar *)search textDidChange:(NSString *)searchText{
    NSMutableArray *destArray = [[NSMutableArray alloc] init];
    for (NSMutableArray *array in _searchArray) {
        NSMutableArray *destArrayNotes = [[NSMutableArray alloc] init];
        for (NoteMenagedObject *obj in array) {
            if ([[obj.name lowercaseString] containsString:[searchText lowercaseString]] || [[obj.title lowercaseString] containsString:[searchText lowercaseString]]) {
                [destArrayNotes addObject:obj];
            }
        }
        if (destArrayNotes && destArrayNotes.count) {
            [destArray addObject:destArrayNotes];
        }
    }
    if ([searchText isEqualToString:@""]) {
        [destArray addObjectsFromArray:_searchArray];
    }
    [notesCollectionViewController setArray:destArray willRemove:willRemove willShowStars:starButton.selected];
}

#pragma mark	Navigation

- (void)setdefaultNavigationState{
    [editOkButton removeTarget:self action:@selector(ok) forControlEvents:UIControlEventTouchUpInside];
    [editOkButton addTarget:self action:@selector(edit) forControlEvents:UIControlEventTouchUpInside];
    [editOkButton setTintColor:[UIColor whiteColor]];
    willRemove = NO;
}

- (void)setRemoveNavigationState{
    [editOkButton setTintColor:[UIColor redColor]];
    [editOkButton removeTarget:self action:@selector(edit) forControlEvents:UIControlEventTouchUpInside];
    [editOkButton addTarget:self action:@selector(ok) forControlEvents:UIControlEventTouchUpInside];
    willRemove = YES;
}

- (void)back{
    _searchArray = nil;
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_SELECT_TABBAR_ITEM" object:[NSNumber numberWithInt:0]];
}

- (void)edit{
    [LKAlertView showAlertWithTitle:@"" message:@"Remove ..." cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"All",@"Custom"] completion:^(NSUInteger selectedOtherButtonIndex) {
        switch (selectedOtherButtonIndex) {
            case 0:{
                for (NoteMenagedObject *note in _searchArray[0]) {
                    [Note removeObject:note];
                }
                [_searchArray removeAllObjects];
                break;
            }
            case 1:{
                
                if (notesCollectionViewController.checkedItemsArray.count) {
                    [LKAlertView showAlertWithTitle:@"" message:@"Remove selected items ..." cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"From all places",@"From file cabinet only"] completion:^(NSUInteger selectedOtherButtonIndex) {
                        NSLog(@"%lu",(unsigned long)selectedOtherButtonIndex);
                        switch (selectedOtherButtonIndex) {
                            case 0:{
                                for (NoteMenagedObject *note in notesCollectionViewController.checkedItemsArray) {
                                    [Note removeObject:note];
                                }
                                [notesCollectionViewController.checkedItemsArray removeAllObjects];
                                break;
                            }
                            case 1:{
                                for (NoteMenagedObject *note in notesCollectionViewController.checkedItemsArray) {
                                    [Note removeObjectNotAll:note];
                                }
                                [notesCollectionViewController.checkedItemsArray removeAllObjects];
                                break;
                            }
                            default:
                                break;
                        }
                    }];
                }else{
                    [self setRemoveNavigationState];
                    [self reloadTableView];
                }
            }
        }
    }];
}

- (void)ok{
    [self setdefaultNavigationState];
    [self reloadTableView];
}

- (void)removeAll{
    [Note removeAllObjects];
}

- (IBAction)sortButtonPressed:(id)sender {
    [sender setTintColor:[UIColor redColor]];
    [self sortButtonPressed];
}

- (void)sortButtonPressed{
    if (sortMenuPopover) {
        [sortMenuPopover dismissMenuPopover];
        sortMenuPopover = nil;
    }else{
        [self configureSortMenuPopover];
        [sortMenuPopover showInView:self.view];
    }
}

- (IBAction)searchButtonPressed:(id)sender {
    if (!backgroundColorPopoverView) {
        SearchViewController *search = [SearchViewController sharedInstance];
        [self addChildViewController:search];
        search.view.frame = self.view.bounds;
        [self.view addSubview:search.view];
        [search didMoveToParentViewController:self];
    }else{
        [self hidePopover];
    }
}

- (IBAction)plussButtonPressed:(id)sender {
    [[DrawingViewManager sharedInstance] setPrevTitle:selectedTitle];
   
    [[DrawingViewManager sharedInstance] setPrevCategory:[NoteHelper sharedInstance].category == nil?@"":[NoteHelper sharedInstance].category];
    [[DrawingViewManager sharedInstance] setWillCreateNewNote:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_SELECT_TABBAR_ITEM" object:[NSNumber numberWithInt:0]];
}

- (IBAction)homeButtonPressed:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_SELECT_TABBAR_ITEM" object:[NSNumber numberWithInt:1]];
    [NoteHelper sharedInstance].fromArchive = [NSNumber numberWithInt:0];
    [self setLowDate:nil upperDate:nil];
    [NoteHelper sharedInstance].favorite = nil;
    [NoteHelper sharedInstance].phoneNumber = NO;
    [NoteHelper sharedInstance].opened = nil;
    [NoteHelper sharedInstance].title = nil;
    [NoteHelper sharedInstance].category = nil;
    [NoteHelper sharedInstance].fromArchive = [NSNumber numberWithInt:0];
    [NoteHelper sharedInstance].pin = [NSNumber numberWithInt:1];
    [NoteHelper sharedInstance].gallery = [NSNumber numberWithInt:0];
    [NoteHelper sharedInstance].document = [NSNumber numberWithInt:0];
    if ([SearchViewController sharedInstance].view) {
        [[SearchViewController sharedInstance].view removeFromSuperview];
    }
    [self reloadTableView];
    [homeButton setTintColor:[UIColor redColor]];
    [(MainTabBarController *)self.tabBarController setPinButtonColor:[UIColor whiteColor]];

    
}

- (void)navigateToHome{
    [self homeButtonPressed:nil];
}



#pragma mark menuPopoverDelegate

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex{
    switch (menuPopover.tag) {
        case 0:{
            switch (selectedIndex) {
                case 0:{
                    [self showDateSliderView];
                    break;
                }
                case 1:{
                    [self showSearchBar];
                    break;
                }
                default:{
                    NSString *title = nil;
                    if (notesCollectionViewController.checkedItemsArray.count) {
                        [Note changeItems:notesCollectionViewController.checkedItemsArray title:titleMenuItems[selectedIndex]];
                        [notesCollectionViewController.checkedItemsArray removeAllObjects];
                    }else{
                        if (selectedIndex > 1) {
                            title = titleMenuItems[selectedIndex  - 2];
                        }
                    }
                    [self didSelectTitle:title];

                }
                    break;
            }
            break;
        }
        case 1:{
            [NoteHelper sharedInstance].sortType = (SortDescriptortype)selectedIndex;
            [self reloadTableView];
            break;
        }
        case 2:{
            break;
        }
        default:
            break;
    }
}

- (void)animateSearchButton{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.duration = 10.0f;
    animation.autoreverses = YES;
    animation.repeatCount = MAXFLOAT;
    animation.fromValue = [NSNumber numberWithFloat:1.0];
    animation.toValue = [NSNumber numberWithFloat:0.1];
    animation.duration = 0.5;
    [searchButton.layer addAnimation:animation forKey:@"opacity"];
}

- (void)menuPopoverWillHide:(MLKMenuPopover *)menuPopover{
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
}

- (void)changeBackgroundColorButtonPressed{
    if (!backgroundColorPopoverView) {
        [menuButton setTintColor:[UIColor whiteColor]];
        [(MainTabBarController *)self.tabBarController setTrashButtonColor:[UIColor blackColor]];
        backgroundColorPopoverView = [[UIView alloc] initWithFrame:CGRectMake(searchButton.superview.x + searchButton.x - 90, 60, 220, 50)];
        ColorCollectionViewController *bgColorCollectionViewController = [[ColorCollectionViewController alloc]initWithNibName:@"ColorCollectionViewController" bundle:nil];
        bgColorCollectionViewController.delegate = self;
        bgColorCollectionViewController.collectionView.tag = 0;
        bgColorCollectionViewController.colorsArray = [APPDELEGATE colorsArray];
        [bgColorCollectionViewController.collectionView setFrame:CGRectMake(5, 0, backgroundColorPopoverView.width - 10 > bgColorCollectionViewController.colorsArray.count * 44?bgColorCollectionViewController.colorsArray.count * 44: backgroundColorPopoverView.width - 10, 50)];
        bgColorCollectionViewController.collectionView.centerX = backgroundColorPopoverView.width/2;
        [self addChildViewController:bgColorCollectionViewController];
        backgroundColorPopoverView.alpha = 0.1;
        backgroundColorPopoverView.backgroundColor = [UIColor whiteColor];
        backgroundColorPopoverView.layer.cornerRadius = 5;
        backgroundColorPopoverView.layer.masksToBounds = YES;
        backgroundColorPopoverView.clipsToBounds = NO;
        [backgroundColorPopoverView addShadowToView];
        UIView *poligonView = [[UIView alloc] initWithFrame:CGRectMake(110, -10, 20, 20)];
        [poligonView addShadowToView];
        [poligonView setBackgroundColor:[UIColor whiteColor]];
        poligonView.transform = CGAffineTransformMakeRotation(M_PI_4);
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(20, 0, backgroundColorPopoverView.width - 40, 20)];
        [view setBackgroundColor:backgroundColorPopoverView.backgroundColor];
        [backgroundColorPopoverView addSubview:view];
        [backgroundColorPopoverView addSubview:bgColorCollectionViewController.collectionView];
        [self.view addSubview:backgroundColorPopoverView];
        backgroundColorPopoverView.centerX = SCREEN_WIDTH/2;
        [UIView animateWithDuration:0.4 animations:^{
            [backgroundColorPopoverView setAlpha:1.0];
        } completion:^(BOOL finished) {
            
        }];
    }else{
        [self hidePopover];
    }
}

- (void)hidePopover{
    [UIView animateWithDuration:0.4 animations:^{
        [backgroundColorPopoverView setAlpha:0];
    } completion:^(BOOL finished) {
        [backgroundColorPopoverView removeFromSuperview];
        backgroundColorPopoverView = nil;
    }];
    [searchButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectObjectAtIndex:(NSInteger)index{
}

- (void) orientationChanged:(NSNotification *)note{
    UIDevice * device = note.object;
    if (device.orientation != lastOrientation) {
        [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(setupNewOrientation) userInfo:nil repeats:NO];
        lastOrientation = device.orientation;
    }
}

- (void)setupNewOrientation{
    serchMenuPopover.centerX = SCREEN_WIDTH/2;
    sortMenuPopover.centerX = SCREEN_WIDTH/2;
    backgroundColorPopoverView.centerX = SCREEN_WIDTH/2;
    serchMenuPopover.frame = CGRectMake(SCREEN_WIDTH/4,40,250, (titleMenuItems.count + 4) * 40  > SCREEN_HEIGHT - 100 ? SCREEN_HEIGHT - 100 : (titleMenuItems.count + 4) * 40);
    serchMenuPopover.centerX = SCREEN_WIDTH/2;
}

- (void)updateTime{
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    dateFormatter.dateFormat = @"M/d/YY H:mm";
    [dateLabel setText:[dateFormatter stringFromDate:now]];
}

@end

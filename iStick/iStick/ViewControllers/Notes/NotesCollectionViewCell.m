#import "NotesCollectionViewCell.h"
#import "DrawingView.h"
#import "UIImage+Resize.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "LKAlertView.h"
#import "NotesCollectionViewControllerManager.h"

@implementation NotesCollectionViewCell{
    __weak IBOutlet UIView *phoneBackgrounView;
    __weak IBOutlet UIButton *phoneButton;
    UIButton *archiveButton;
    __weak IBOutlet UIButton *deletebutton;
    __weak IBOutlet UIView *titileBackgroundView;
    __weak IBOutlet UIButton *pinButton;
    __weak IBOutlet UIButton *queButton;
    __weak IBOutlet UIButton *starButton;
    __weak IBOutlet UILabel *dateLabel;
    __weak IBOutlet UIImageView *alarmBigIconImageView;
    __weak IBOutlet UITextField *titleTextField;
    __weak IBOutlet UIImageView *checkImageView;
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UIImageView *imageView;
    __weak IBOutlet NSLayoutConstraint *imageWidthC;
    __weak IBOutlet UILabel *phoneNumberLabel;
    __weak IBOutlet NSLayoutConstraint *phoneViewHeightConstraint;
    UIColor *buttonColorForCell;
    NoteMenagedObject *currentNote;
    __weak IBOutlet UIView *lineView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [[pinButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [pinButton setImage:[pinButton.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    starButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    scrollView.delegate = self;
    [starButton setImage:[starButton.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [deletebutton setImage:[UIImage imageNamed:@"fill_X_icon"] forState:UIControlStateNormal];
    [deletebutton addTarget:self action:@selector(delete) forControlEvents:UIControlEventTouchUpInside];
    deletebutton.imageView.contentMode = UIViewContentModeScaleAspectFit;

    archiveButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 30)];
    [archiveButton setTitle:@"Archive" forState:UIControlStateNormal];
    [archiveButton setBackgroundColor:[UIColor blackColor]];
    archiveButton.layer.masksToBounds = YES;
    archiveButton.layer.cornerRadius = 15;
    [archiveButton addTarget:self action:@selector(archiveButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:archiveButton];
    
    [self.contentView bringSubviewToFront:deletebutton];

    titleTextField.delegate = self;
    titleTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    UILongPressGestureRecognizer *checkGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(checkNote:)];
    checkGesture.minimumPressDuration = 0.2;
    if ([titleTextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1];
        titleTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Title" attributes:@{NSForegroundColorAttributeName: color}];
    }
    [titleTextField setTextColor:[UIColor redColor]];
}

- (void)prepareForReuse{
    [super prepareForReuse];
    [titleTextField setText:@""];
    [imageView setImage:[UIImage new]];
}

- (void)didSelectNote{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"COLLECTION_DID_DID_SELECT_OBJECT" object:currentNote];
}

- (void)checkNote:(UILongPressGestureRecognizer *)sender{
    if (sender.state == UIGestureRecognizerStateEnded) {
        
    }else if (sender.state == UIGestureRecognizerStateBegan){
        [_delegate didChackNote:currentNote];
        if (checkImageView.hidden) {
            checkImageView.hidden = NO;
            [UIView animateWithDuration:0.2 animations:^{
                checkImageView.transform = CGAffineTransformMakeScale(0.7, 0.7);

            }];
        }else{
            [UIView animateWithDuration:0.2 animations:^{
                checkImageView.transform = CGAffineTransformMakeScale(0.01, 0.01);
            } completion:^(BOOL finished) {
                checkImageView.hidden = YES;
            }];
        }
    }
}

- (void)delete:(UILongPressGestureRecognizer *)sender{
    if (sender.state == UIGestureRecognizerStateBegan){
        [_delegate didSelectDeleteButtonWithObject:currentNote];
    }
}

- (void)delete{
    
    
    
    [LKAlertView showAlertWithTitle:@""
                            message:@"Do you want to clear this note?"
                  cancelButtonTitle:@"No"
                  otherButtonTitles:@[@"Yes"]
                         completion:^(NSUInteger selectedOtherButtonIndex) {
                             if (selectedOtherButtonIndex == 0) {
                                 [_delegate didSelectDeleteButtonWithObject:currentNote];
                             }
                         }];
    //pakac
    //if ([currentNote.title isEqualToString:@""]) {
        //[_delegate didSelectDeleteButtonWithObject:currentNote];
//    }else{
//        [LKAlertView showAlertWithTitle:@"" message:@"You" cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"All places",@"File cabinet only"] completion:^(NSUInteger selectedOtherButtonIndex) {
//            NSLog(@"%lu",(unsigned long)selectedOtherButtonIndex);
//            switch (selectedOtherButtonIndex) {
//                case 0:{
//                    [_delegate didSelectDeleteButtonWithObject:currentNote];
//                    break;
//                }
//                case 1:{
//                    [_delegate didSelectDeleteButtonWithObjectNotAll:currentNote];
//                    break;
//                }
//                default:
//                    break;
//            }
//        }];
//    }
}
- (IBAction)callButtonPressed:(id)sender {
    NSString *phoneNumber = [@"tel://" stringByAppendingString:currentNote.phoneNumber];
    NSString *facetimePhoneNumber = [@"facetime://" stringByAppendingString:currentNote.phoneNumber];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:phoneNumber]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:facetimePhoneNumber]];
    }
}

- (void)archiveButtonClicked{
    
}

- (void)configureCellWithNote:(NoteMenagedObject *)note isInDeleteState:(BOOL)isInDeleteState withChakedItems:(NSMutableArray *)array willShowStars:(BOOL)willShowStars{
    currentNote = note;
    [imageView setImage:[UIImage new]];
    if (![currentNote.phoneNumber isEqualToString:@""] && currentNote.phoneNumber != nil) {
        phoneViewHeightConstraint.constant = 40;
        phoneNumberLabel.text = currentNote.phoneNumber;
    }else{
        phoneViewHeightConstraint.constant = 0;
    }
    const CGFloat *_components = CGColorGetComponents(((UIColor *)[APPDELEGATE colorsArray][[note.color_index intValue]]).CGColor);
    CGFloat red     = _components[0];
    CGFloat green = _components[1];
    CGFloat blue   = _components[2];
    if (red == 0 && green == 0 && blue == 0) {
        [checkImageView setImage:[[checkImageView image]
                                  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
        [checkImageView setTintColor:[UIColor whiteColor]];
        [dateLabel setTextColor:[UIColor whiteColor]];
        [phoneNumberLabel setTextColor:[UIColor whiteColor]];
        [phoneButton setImage:[phoneButton.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]forState:UIControlStateNormal];
        [phoneButton setTintColor:[UIColor whiteColor]];
        [phoneBackgrounView setBackgroundColor:[UIColor clearColor]];
        [lineView setHidden:NO];
    }else{
        [checkImageView setImage:[[checkImageView image] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
        [checkImageView setTintColor:[UIColor blackColor]];
        [dateLabel setTextColor:[UIColor blackColor]];
        [phoneNumberLabel setTextColor:[UIColor blackColor]];
        [phoneButton setImage:[phoneButton.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]forState:UIControlStateNormal];
        [phoneButton setTintColor:[UIColor blackColor]];
        [phoneBackgrounView setBackgroundColor:[UIColor clearColor]];
        [lineView setHidden:YES];
    }
    [titileBackgroundView setBackgroundColor:[UIColor blackColor]];
    [starButton setTintColor:[UIColor whiteColor]];
    [pinButton setTintColor:[UIColor whiteColor]];
    buttonColorForCell = [UIColor whiteColor];
    [self.contentView setBackgroundColor:[UIColor colorWithRed:red green:green blue:blue alpha:1]];
    
    NSMutableArray *scheduledObjects = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"scheduled_objects_object_id"]];
    [alarmBigIconImageView setHidden:YES];
    for (NSString *str in scheduledObjects) {
        if ([note.object_id isEqualToString:str]) {
            [alarmBigIconImageView setHidden:NO];
        }
    }
    if(note.alarm.integerValue == 1){
         [alarmBigIconImageView setHidden:NO];
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"M/d/yy H:mm"];
    NSString *stringFromDate1 = [formatter stringFromDate:note.created_at];
    dateLabel.text = stringFromDate1;
    [titleTextField setText:note.title];
    if (note.backgroundImage) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            __block UIImage *cacheImage = [UIImage imageWithData:note.backgroundImage];
            if (cacheImage) {
                cacheImage = [UIImage imageWithImage:cacheImage convertToSize:CGSizeMake((SCREEN_MIN_LENGTH - 120)/2, cacheImage.size.height/ (cacheImage.size.width/((SCREEN_MIN_LENGTH - 120)/3)))];
                dispatch_async(dispatch_get_main_queue(), ^{
                    imageWidthC.constant = cacheImage.size.width;
                    //[_backgroundImageView setNeedsDisplay];
                    //_backgroundImageView.image = cacheImage; pakac
                    NSLog(@"_backgroundImageView");
             
                });
            }
            
        });
    }
    if (note.image) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{

            __block UIImage *cacheImage = [UIImage imageWithData:note.image];
           
              dispatch_async(dispatch_get_main_queue(), ^{
              imageView.contentMode = UIViewContentModeScaleToFill;
              imageView.image = cacheImage;
              });
            //pakac
//            if (cacheImage) {
//                cacheImage = [UIImage imageWithImage:cacheImage convertToSize:CGSizeMake((SCREEN_MIN_LENGTH - 120)/2, cacheImage.size.height/ (cacheImage.size.width/((SCREEN_MIN_LENGTH - 120)/2)))];
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    imageWidthC.constant = cacheImage.size.width;
//                    [imageView setNeedsDisplay];
//                    imageView.image = cacheImage;
//                    NSLog(@"mtav");
//                    scrollView.minimumZoomScale = scrollView.width/cacheImage.size.width;
//                    scrollView.zoomScale = scrollView.minimumZoomScale;
//                    scrollView.contentOffsetY = 0;
//                });
//            }

            });
    }else{
        [imageView setImage:[UIImage new]];
    }
    [archiveButton setHidden:YES];
    if ([array containsObject:note]) {
        checkImageView.transform = CGAffineTransformMakeScale(0.7, 0.7);
        checkImageView.hidden = NO;
    }else{
        checkImageView.transform = CGAffineTransformMakeScale(0, 0);
        checkImageView.hidden = YES;
    }
    if (note.star_count.integerValue > 0) {
        [starButton setAlpha:1];
        [starButton setTintColor:[UIColor yellowColor]];
    }else{
        [starButton setAlpha:0.3];
    }
    if (note.pin_count.integerValue > 0) {
        [pinButton setAlpha:1];
        [pinButton setTintColor:[UIColor yellowColor]];
    }else{
        [pinButton setAlpha:0.3];
    }
    if (note.isInCue.integerValue > 0) {
        [queButton setAlpha:1];
        [queButton setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    }else{
        [queButton setTitleColor:buttonColorForCell forState:UIControlStateNormal];
        [queButton setAlpha:0.3];
    }
}

- (void)starCountDidChange:(NSInteger)count{
    [Note chnageStarCountInNote:currentNote count:count + 1];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.contentView endEditing:YES];
    [Note chnagetitleInNote:currentNote title:textField.text];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [Note chnagetitleInNote:currentNote title:textField.text];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return imageView;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [NotesCollectionViewControllerManager sharedInstance].firstResponderCell = self;
}

- (IBAction)starButtonClicked:(id)sender {
    if (currentNote.star_count.integerValue > 0) {
        [Note chnageStarCountInNote:currentNote count:0];
        [starButton setTintColor:buttonColorForCell];
        [starButton setAlpha:0.3];
    }else{
        [Note chnageStarCountInNote:currentNote count:1];
        [starButton setTintColor:[UIColor yellowColor]];
        [starButton setAlpha:1];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"COLLECTION_DID_CHANGE" object:nil];
}
    
- (IBAction)pinButtonClicked:(id)sender {
    if (currentNote.pin_count.integerValue > 0) {
        [Note chnagePinCountInNote:currentNote count:[NSNumber numberWithInteger:0]];
        [pinButton setTintColor:buttonColorForCell];
        [pinButton setAlpha:0.3];
    }else{
        NSString *countString = [NSString stringWithFormat:@"%i%@",1,[NSDate dateValue:[NSDate new]]];
        NSNumber *count = [NSNumber numberWithLongLong:[countString longLongValue]];
        [Note chnagePinCountInNote:currentNote count:[NSNumber numberWithInt:1]];
        [pinButton setTintColor:[UIColor yellowColor]];
        [pinButton setAlpha:1];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"COLLECTION_DID_CHANGE" object:nil];
}

- (IBAction)removeFromCuePressed:(id)sender {
    if (currentNote.isInCue.integerValue > 0) {
        [Note chnageIsInCueInNote:currentNote count:[NSNumber numberWithInteger:0]];
        [queButton setTitleColor:buttonColorForCell forState:UIControlStateNormal];
        [queButton setAlpha:0.3];
    }else{
        [Note chnageIsInCueInNote:currentNote count:[NSNumber numberWithInt:1]];
        [queButton setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
        [queButton setAlpha:1];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"COLLECTION_DID_CHANGE" object:nil];
}

- (IBAction)removeContactPressed:(id)sender{
    [Note removeContactIFromNote:currentNote];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"COLLECTION_DID_CHANGE" object:nil];
}
@end

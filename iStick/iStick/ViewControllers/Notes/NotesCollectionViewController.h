#import <UIKit/UIKit.h>

@interface NotesCollectionViewController : UICollectionViewController
@property (nonatomic, strong) NSMutableArray *itemsArray;
@property (nonatomic, strong) NSMutableArray *checkedItemsArray;
@property (nonatomic) BOOL willAllowChangePossition;
- (void)setArray:(NSArray *)localeArray willRemove:(BOOL)willRemove willShowStars:(BOOL)willShowStars;

@end

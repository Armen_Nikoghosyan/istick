#import "MCManager.h"
#import "AppDelegate.h"

@implementation MCManager

-(id)init{
    self = [super init];
    if (self) {
        _peerID = nil;
        _session = nil;
        _browser = nil;
        _advertiser = nil;
    }
    return self;
}

#pragma mark - Public method implementation

-(void)setupPeerAndSessionWithDisplayName:(NSString *)displayName{
    _peerID = [[MCPeerID alloc] initWithDisplayName:displayName];
    _session = [[MCSession alloc] initWithPeer:_peerID];
    _session.delegate = self;
}

-(void)setupMCBrowser{
    _browser = [[MCBrowserViewController alloc] initWithServiceType:@"chat-files" session:_session];
}

-(void)advertiseSelf:(BOOL)shouldAdvertise{
    if (shouldAdvertise) {
        _advertiser = [[MCAdvertiserAssistant alloc] initWithServiceType:@"chat-files"
                                                           discoveryInfo:nil
                                                                 session:_session];
        [_advertiser start];
    }else{
        [_advertiser stop];
        _advertiser = nil;
    }
}


-(void)sendMyMessage:(NSString *)username{
    NSData *dataToSend = [username dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *allPeers =  [APPDELEGATE mcManager].session.connectedPeers;
    NSError *error;
    [[APPDELEGATE mcManager].session sendData:dataToSend
                                     toPeers:allPeers
                                    withMode:MCSessionSendDataReliable
                                       error:&error];
}

- (NSString *)randomStringWithLength:(int)len{
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((int)[letters length])]];
    }
    return randomString;
}

- (void)loginToParseWithUsername:(NSString *)username{
    if([PFUser currentUser] == nil) {
        PFQuery *query = [PFUser query];
        [query whereKey:@"username" equalTo:username];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (object != nil) {
                [PFUser logInWithUsernameInBackground:username password:@"password" block:^(PFUser * _Nullable user, NSError * _Nullable error) {
                    if ([APPDELEGATE amIMessageSender]) {
                        [APPDELEGATE setAmIMessageSender:NO];
                        [self sendMyMessage:[PFUser currentUser].username];


                    }else{
                        [PFUser logOutInBackgroundWithBlock:^(NSError * _Nullable error) {
                            [PFUser logInWithUsernameInBackground:username password:@"password" block:^(PFUser * _Nullable user, NSError * _Nullable error) {
                                [Note saveAllUnsavedObjectToParse];

                            }];
                        }];
                    }
                    
                }];
            }else{
                PFUser *user = [PFUser user];
                user.username = username;
                user.password = @"password";
                [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                    [Note saveAllUnsavedObjectToParse];
                    if ([APPDELEGATE amIMessageSender]) {
                        [APPDELEGATE setAmIMessageSender:NO];
                        [self sendMyMessage:username];
                    }
                }];
            }
        }];
    }else{
        if ([APPDELEGATE amIMessageSender]) {
            [APPDELEGATE setAmIMessageSender:NO];
            [self sendMyMessage:[PFUser currentUser].username];
        }else{
            [PFUser logOutInBackgroundWithBlock:^(NSError * _Nullable error) {
                [PFUser logInWithUsernameInBackground:username password:@"password" block:^(PFUser * _Nullable user, NSError * _Nullable error) {
                    [Note saveAllUnsavedObjectToParse];
                    
                }];
            }];
        }
    }
}

#pragma mark - MCSession Delegate method implementation


-(void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state{
    if (state == MCSessionStateConnected  && [APPDELEGATE amIMessageSender]) {
        [self loginToParseWithUsername:[self randomStringWithLength:20]];
    }
    NSDictionary *dict = @{@"peerID": peerID,
                           @"state" : [NSNumber numberWithInt:state]
                           };
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MCDidChangeStateNotification"
                                                        object:nil
                                                      userInfo:dict];
}


-(void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID{
    
    [self loginToParseWithUsername:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
    NSLog(@"aaaaaaabbbbb %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    NSDictionary *dict = @{@"data": data,
                           @"peerID": peerID
                           };
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MCDidReceiveDataNotification"
                                                        object:nil
                                                      userInfo:dict];
}


-(void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress{
    NSDictionary *dict = @{@"resourceName"  :   resourceName,
                           @"peerID"        :   peerID,
                           @"progress"      :   progress
                           };
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MCDidStartReceivingResourceNotification"
                                                        object:nil
                                                      userInfo:dict];
    dispatch_async(dispatch_get_main_queue(), ^{
        [progress addObserver:self
                   forKeyPath:@"fractionCompleted"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    });
}


-(void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error{
    
    NSDictionary *dict = @{@"resourceName"  :   resourceName,
                           @"peerID"        :   peerID,
                           @"localURL"      :   localURL
                           };
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishReceivingResourceNotification"
                                                        object:nil
                                                      userInfo:dict];
    
}


-(void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID{
    
}


-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MCReceivingProgressNotification"
                                                        object:nil
                                                      userInfo:@{@"progress": (NSProgress *)object}];
}

@end

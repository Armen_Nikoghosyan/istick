#import "LoginViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>
#import "RNEncryptor.h"
#import "RNDecryptor.h"
#import <RSLoadingView/RSLoadingView-Swift.h>

@interface LoginViewController  ()<UITextFieldDelegate>{
    NSString *serviceType;
    
    MCBrowserViewController *browser;
    MCAdvertiserAssistant *assistant;
    MCSession *session;
    MCPeerID *peerID;
    __weak IBOutlet UILabel *alreadyLogedInlbel;
    __weak IBOutlet UITextField *emailTextField;
    __weak IBOutlet UITextField *passwordTextField;
    __weak IBOutlet UIButton *loginButton;
    __weak IBOutlet UIView *loginView;
    __weak IBOutlet UITextField *reenterPasswordTextField;
    
    __weak IBOutlet UISegmentedControl *segmentView;
    
    __weak IBOutlet UIButton *logOutButton;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    loginButton.layer.masksToBounds = YES;
    loginButton.layer.cornerRadius = 5;
    if ([PFUser currentUser] != nil) {
        [alreadyLogedInlbel setHidden:NO];
        [loginView setHidden:YES];
        [segmentView setHidden:YES];
        [logOutButton setHidden:NO];
    }
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}




- (IBAction)closePage:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DIDLOGIN" object:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)facebookLoginClicked:(id)sender {
    [self loginButtonClicked];
}

- (IBAction)loginButtonPressed:(id)sender {
    RSLoadingView * rSLoadingView  = [[RSLoadingView alloc]init];
    [rSLoadingView showOn:self.view];
    NSString *username = emailTextField.text;
    NSString *password = passwordTextField.text;
    if (![self isValidEmail:username]) {
        [rSLoadingView hide];
        [LKAlertView showAlertWithTitle:@"Error" message:@"You typped incorrect email address." cancelButtonTitle:@"OK" otherButtonTitles:nil completion:nil];
        return;
    }
    if ([password length] < 10) {
        [rSLoadingView hide];
        [LKAlertView showAlertWithTitle:@"Error" message:@"Please Enter at least 10 characters." cancelButtonTitle:@"OK" otherButtonTitles:nil completion:nil];
        return;
    }
    UIButton * btn = (UIButton*)sender;
    if (btn.tag == 0) {
    if (![password isEqualToString:reenterPasswordTextField.text]) {
        [rSLoadingView hide];
        [LKAlertView showAlertWithTitle:@"Error" message:@"Passwords are not the same" cancelButtonTitle:@"OK" otherButtonTitles:nil completion:nil];
        return;
    }
    }
    if([PFUser currentUser] == nil) {
        PFQuery *query = [PFUser query];
        [query whereKey:@"username" equalTo:username];
        
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (object == nil && btn.tag != 0 ){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [rSLoadingView hide];
                });
                
                [LKAlertView showAlertWithTitle:@"Erorr" message:@"please try again" cancelButtonTitle:@"OK" otherButtonTitles:nil completion:nil];
            }
            if (object != nil) {
                [PFUser logInWithUsernameInBackground:username password:password block:^(PFUser * _Nullable user, NSError * _Nullable error) {
                    if (error) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                              [rSLoadingView hide];
                        });
                      
                        [LKAlertView showAlertWithTitle:@"Erorr" message:@"please try again" cancelButtonTitle:@"OK" otherButtonTitles:nil completion:nil];
                    }else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [rSLoadingView hide];
                        });
                        [Note saveAllUnsavedObjectToParse];
                        
                        [self closePage:nil];
                    }
                }];
            }else if(btn.tag == 0) {
                PFUser *user = [PFUser user];
                user.username = username;
                user.password = password;
                [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                    if (error) {
                        [rSLoadingView hide];
                        [LKAlertView showAlertWithTitle:@"Erorr" message:@"please try again" cancelButtonTitle:@"OK" otherButtonTitles:nil completion:nil];
                    }else{
                        [rSLoadingView hide];
                        [self saveEncodingKey:username password:password];
                        [Note saveAllUnsavedObjectToParse];
                        [self closePage:nil];
                    }
                }];
            }
        }];
    }
}

- (void)saveEncodingKey:(NSString *)username password:(NSString *)password{
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@ %@",username,password] forKey:@"ENCODEKEY"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(BOOL) isValidEmail:(NSString *)checkString{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void)loginButtonClicked{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             if ([result.grantedPermissions containsObject:@"email"])
             {
                 [self getFBResult];
             }
         }
     }];
}

-(void)getFBResult{
    if ([FBSDKAccessToken currentAccessToken]){
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, first_name, last_name, picture.type(large), email"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error){
                 [self loginToParseWithUsername:result[@"id"]];
             }else{
                 NSLog(@"error : %@",error);
             }
         }];
    }
}

- (void)loginToParseWithUsername:(NSString *)username{
    if([PFUser currentUser] == nil) {
        PFQuery *query = [PFUser query];
        [query whereKey:@"username" equalTo:username];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (object != nil) {
                [PFUser logInWithUsernameInBackground:username password:@"password" block:^(PFUser * _Nullable user, NSError * _Nullable error) {
                    [Note saveAllUnsavedObjectToParse];
                    [self closePage:nil];
                }];
            }else{
                PFUser *user = [PFUser user];
                user.username = username;
                user.password = @"password";
                [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                    [Note saveAllUnsavedObjectToParse];
                    [self closePage:nil];
                }];
            }
        }];
    }
}


- (NSData *)encodedData:(NSData *)data{
    NSString *aPassword = [[NSUserDefaults standardUserDefaults] objectForKey:@"ENCODEKEY"];
    NSError *error;
    NSData *encryptedData = [RNEncryptor encryptData:data
                                        withSettings:kRNCryptorAES256Settings
                                            password:aPassword
                                               error:&error];
    if (!error) {
        return encryptedData;
    }
    return nil;
}
- (IBAction)segmentViewAction:(UISegmentedControl *)sender {
    
    NSInteger index = sender.selectedSegmentIndex;
    if (index == 0) {
        [reenterPasswordTextField setHidden:NO];
        [loginButton setTitle:@"Sign Up" forState:UIControlStateNormal];
        loginButton.tag = 0;
        
    } else {
        [reenterPasswordTextField setHidden:YES];
        [loginButton setTitle:@"Sign In" forState:UIControlStateNormal];
        loginButton.tag = 1;
    }
    
}


-(NSData *)decodeData:(NSData *)data{
    NSString *aPassword = [[NSUserDefaults standardUserDefaults] objectForKey:@"ENCODEKEY"];
    NSError *error;
    NSData *encryptedData = [RNDecryptor decryptData:data
                                        withPassword:aPassword
                                               error:&error];
    if (!error) {
        return encryptedData;
    }
    return nil;
}
- (IBAction)logOutAction:(UIButton *)sender {
    if([PFUser currentUser] != nil) {
        [PFUser logOut];
        [alreadyLogedInlbel setHidden:YES];
        [loginView setHidden:NO];
        [segmentView setHidden:NO];
        [logOutButton setHidden:YES];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end

#import <UIKit/UIKit.h>
#import "InfColorPicker.h"

@interface DrawViewController : UIViewController <InfColorPickerControllerDelegate>

@property (nonatomic,strong) NoteMenagedObject *note;
@property (nonatomic,strong) NSString *contactId;
-(void)setisPen;
@end

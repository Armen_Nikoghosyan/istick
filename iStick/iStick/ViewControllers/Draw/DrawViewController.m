#import "DrawViewController.h"
#import "DrawingView.h"
#import "NotesViewController.h"
#import "AlarmViewController.h"
#import "AppDelegate.h"
#import "UIImage+Resize.h"
#import "NotePFObject.h"
#import "LoginViewController.h"
#import "ConnectionsViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "DrawingViewManager.h"
#import "ColorCollectionViewController.h"
#import "MLKMenuPopover.h"
#import "FileManagedObject.h"
#import "StarView.h"
#import "ContactsListViewController.h"
#import "Contact.h"
#import "ContactManagedObject.h"
#import <Contacts/Contacts.h>
#import "ImageSliderViewController.h"
#import "BottomImageSliderViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <Crashlytics/Crashlytics.h>
#import "TitleTableViewController.h"
#import "MainTabBarController.h"
#import "UIColor+Category.h"
#import "SearchViewController.h"
#import <Masonry/Masonry.h>
#import "HDNotificationView.h"

@interface DrawViewController ()<UIGestureRecognizerDelegate,DrawingViewDelegate,UIScrollViewDelegate,UITextFieldDelegate,ColorCollectionViewDelegate,MLKMenuPopoverDelegate,StarViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAdaptivePresentationControllerDelegate,CLLocationManagerDelegate,TitleTableViewControllerDelegate,ContactsListViewControllerDelegate>{
    
    UIDeviceOrientation lastOrientation;
    UIView *colorView;
    UIScrollView *sliderButtonsView;
    UIView *mainButtonsView;
    UIButton *hideShowButton;
    UIButton *alarmButton;
    UIButton *colorButton;
    UIView *titleBackgroundView;
    UIColor *currentColor;
    CGFloat currentWidth;
    UIView *colorPopoverView;
    UIView *backgroundColorPopoverView;
    UIButton *linesButton;
    NSNumber *starCount;
    NSNumber *pinCount;
    NSNumber *opened;
    NSNumber *isInCue;
    StarView *starView;
    UIButton *starButton;
    UIButton *selectQueBtn;
    UIButton *pinButton;
    NSMutableArray *lineLayers;
    NSArray *lineColorsArrray;
    NSMutableArray *mainButtonsArray;
    NSMutableArray *titleMenuItems;
    NSMutableArray *allbuttonsArray;
    NSTimer *titleTimer;
    MLKMenuPopover *titleMenuPopover;
    UIView *contactVCView;
    NSNumber *wasDeleted;
    __weak IBOutlet UITextField *phoneNumberTextField;
    __weak IBOutlet UIImageView *pagingImageView;
    __weak IBOutlet UIView *pagingView;
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UIView *homeButton;
    __weak IBOutlet UIButton *queButton;
    BottomImageSliderViewController *bootpmImgaeSliderVC;
    UIImageView *_backgroundImageView;
    TitleTableViewController *titleTableViewController;
    UIView *viewForTitleTableView;
    UIButton *penOrFinger;
    UIButton *safeButton;
    __weak IBOutlet UIImageView *scrallUpImageView;
    __weak IBOutlet UIImageView *scrollDownImageView;
    __weak IBOutlet UILabel *dateLabel;
    __weak IBOutlet UIButton *nextButton;
    __weak IBOutlet UIButton *backButton;
    __weak IBOutlet UIButton *menuButton;
    __weak IBOutlet UIButton *searchButton;
    __weak IBOutlet UIButton *plussButton;
    __weak IBOutlet UIButton *minimizeButton;
    __weak IBOutlet UIButton *rubberButton;
    __weak IBOutlet UIButton *textButton;
    __weak IBOutlet UIButton *clearButton;
    __weak IBOutlet UITextField *titletextField;
    __weak IBOutlet UITextField *categoryTextField;
    __weak IBOutlet UIButton *mainHomeButton;
    __weak IBOutlet UIButton *bottomButton;
    __weak IBOutlet UIButton *topButton;
    __weak IBOutlet UIView *bottomImageSliderView;
    UIButton *showImageSliderButton;
    __weak IBOutlet NSLayoutConstraint *imageSliderHeightC;
    __weak IBOutlet NSLayoutConstraint *imageSliderTopC;
    __weak IBOutlet NSLayoutConstraint *drawingViewWidthC;
    __weak IBOutlet UIButton *micraphoneButton;
    BOOL didPaged;
    int count;
    float sliderHeight;
    CGFloat keyboardHeight;
    NSString* selectedNotObjectId;
    NSDate* alarmDate;
    NSNumber *alarm;
    NSNumber *document;
    NSNumber *gallery;

}
@property (nonatomic) NSInteger colorIndex;
@property (weak, nonatomic) IBOutlet UIView *backgroundViewForColor;
@property (weak, nonatomic) IBOutlet DrawingView *drowingView;
@property (weak, nonatomic) IBOutlet UIView *createFileView;
@property (weak, nonatomic) IBOutlet UITextField *fileNameTextField;
@property (weak, nonatomic) IBOutlet UIView *signatureView;
@property (weak, nonatomic) IBOutlet DrawingView *signatureDrawingView;
@property (weak, nonatomic) IBOutlet UIView *signatureBackgroundView;
@property (nonatomic, strong) NSDate *date;


@end

@implementation DrawViewController
@synthesize colorIndex;
@synthesize date;

- (void)viewDidLoad {
    [super viewDidLoad];
    phoneNumberTextField.delegate = self;
    scrollView.delegate = self;
    sliderHeight = [UIScreen mainScreen].bounds.size.height + 20;
    scrollView.panGestureRecognizer.minimumNumberOfTouches = 2;
    imageSliderHeightC.constant = SCREEN_MAX_LENGTH/3.5;
    drawingViewWidthC.constant = SCREEN_WIDTH;
    [self.view layoutIfNeeded];
    bootpmImgaeSliderVC = [[BottomImageSliderViewController alloc] initWithNibName:@"BottomImageSliderViewController" bundle:nil];
    [self setDate: [NSDate new]];
    starCount = [NSNumber numberWithInt:0];
    pinCount = [NSNumber numberWithInt:0];
    isInCue = [NSNumber numberWithInt:0];
    lineLayers = [[NSMutableArray alloc] init];
    wasDeleted = [NSNumber numberWithInt:0];
    document = [NSNumber numberWithInt:0];
    gallery = [NSNumber numberWithInteger:0];
    lineColorsArrray = @[[UIColor colorWithRed:0 green:0 blue:0 alpha:1],
                         [UIColor colorWithRed:1 green:1 blue:1 alpha:1],
                         [UIColor colorWithRed:1 green:0 blue:0 alpha:1],
                         [UIColor colorWithRed:0 green:0 blue:1 alpha:1],
                         [UIColor colorWithRed:0 green:1 blue:1 alpha:1],
                         [UIColor colorWithRed:0 green:0.9999 blue:0 alpha:1],
                         [UIColor colorWithRed:1 green:0 blue:1 alpha:1],
                         [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1],
                         [UIColor colorWithRed:0.5 green:0 blue:0 alpha:1],
                         [UIColor colorWithRed:1 green:1 blue:0 alpha:1],
                         [UIColor colorWithRed:0.5 green:0.5 blue:0 alpha:1],
                         [UIColor colorWithRed:0 green:0.5 blue:0 alpha:1],
                         [UIColor colorWithRed:0 green:0.5 blue:0.5 alpha:1],
                         [UIColor colorWithRed:0 green:0 blue:0.5 alpha:1],
                         [UIColor colorWithRed:0.5 green:0 blue:0.5 alpha:1]];
    [self configureView];
    NSLog(@"prevTitle = %@",[[DrawingViewManager sharedInstance] prevTitle]);
   // [self setTitleTextFieldText:[[DrawingViewManager sharedInstance] prevTitle]];
    [self setCategoryName:[[DrawingViewManager sharedInstance] prevCategory]];
    [titletextField.superview bringSubviewToFront:titletextField];
    [self displayContentController:bootpmImgaeSliderVC];
    homeButton.transform = CGAffineTransformMakeRotation(M_PI_4);
    UITapGestureRecognizer *tapForHome = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(homeButtonPressed)];
    [homeButton addGestureRecognizer:tapForHome];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didSelectObject) name:@"DID_SELECT_NOTE"
                                               object:nil];
    return self;
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    sliderHeight = [UIScreen mainScreen].bounds.size.height + 20;
    CGRect frame = sliderButtonsView.frame;
    frame.size.height = sliderHeight;
    sliderButtonsView.frame = frame;
    sliderButtonsView.bottom = 40;
    sliderButtonsView.contentOffsetY = 40 * (count + 1) + 20 - sliderHeight;
    [self hideSliderMenu:sliderButtonsView];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    contactVCView.frame = CGRectMake(phoneNumberTextField.x, phoneNumberTextField.y + phoneNumberTextField.height, phoneNumberTextField.width, SCREEN_HEIGHT - phoneNumberTextField.y - phoneNumberTextField.height - keyboardHeight);
}

- (void)callTitleMenuPopover{
    titleMenuItems = [[Note getAllTitles] mutableCopy];
    NSMutableArray *upperCaseTitlesArray = [[NSMutableArray alloc] initWithObjects:@"by date",@"by text", nil];
    for (NSString *title in titleMenuItems) {
        [upperCaseTitlesArray addObject:[title uppercaseString]];
    }
    if (titletextField.width < 200) {
        titleMenuPopover = [[MLKMenuPopover alloc] initWithFrame:CGRectMake(titletextField.x + titletextField.superview.x + titletextField.superview.superview.x, 50, 250, titleMenuItems.count * 40  > SCREEN_HEIGHT - 100 ? SCREEN_HEIGHT - 100 : titleMenuItems.count * 40)
                                                       menuItems:upperCaseTitlesArray];
        titleMenuPopover.centerX = self.view.centerX;
    }else{
        titleMenuPopover = [[MLKMenuPopover alloc] initWithFrame:CGRectMake(titletextField.x + titletextField.superview.x + titletextField.superview.superview.x, 50, 250, titleMenuItems.count * 40  > SCREEN_HEIGHT - 100 ? SCREEN_HEIGHT - 100 : titleMenuItems.count * 40)
                                                       menuItems:upperCaseTitlesArray];
    }
    titleMenuPopover.centerX = SCREEN_WIDTH/2;
    titleMenuPopover.tag = 1;
    [titleMenuPopover hideArrow];
    titleMenuPopover.menuPopoverDelegate = self;
    [titleMenuPopover showInView:self.view];
}

- (void)toggleLabelAlpha{
    static float alphaCount = -0.01;
    if (titletextField.alpha < 0.1) {
        alphaCount = 0.01;
    }else if(titletextField.alpha > 0.9){
        alphaCount = -0.01;
    }
    titletextField.alpha = titletextField.alpha + alphaCount;
}

- (void)viewWillAppear:(BOOL)animated{
    selectedNotObjectId = @"0";
    alarmDate = [[NSDate alloc]init];
    alarm = [NSNumber numberWithInt:0];
    [super viewWillAppear:animated];
    [self setisPen];
    didPaged = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(createNewNoteFromList)
                                                 name:@"WILL_CREATE_NEW_NOTE_FROM_LIST"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setScheduleLocaleNotification:)
                                                 name:@"LOCALE_NOTIFICATION_STATE_CHANGED"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(startTextEntry)
                                                 name:@"START_TEXT_ENTRY"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(createOrUpdateNote) name:@"APLICATION_WILL_RESIGN_ACTIVE"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didCloseSearch) name:@"DID_CLOSE_SEARCH"
                                               object:nil];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didTakePhoto)
                                                 name:@"DID_TAKED_IMAGE"
                                               object:nil];
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(orientationChanged:)
     name:UIDeviceOrientationDidChangeNotification
     object:[UIDevice currentDevice]];
    //if (_drowingView.lastPoint.y != 0) {
        if ([[DrawingViewManager sharedInstance] willCreateNewNote]) {
            [self createNewNoteFromList];
        }
        if ([[DrawingViewManager sharedInstance] showingNote]) {
            [self didSelectObject];
        }
        if (![[[DrawingViewManager sharedInstance] contactId] isEqualToString:@""]) {
            [self askForSavingNoteWithCompletion:^(BOOL finished) {
                UIPageViewController *pageViewController = self.navigationController.viewControllers[self.navigationController.viewControllers.count - 1];
                DrawViewController *viewController;
                if (IS_IPHONE) {
                    viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewControlleriPhone" bundle:nil];
                }else{
                    viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewController" bundle:nil];
                }
                viewController.contactId = [[DrawingViewManager sharedInstance] contactId];
                [[DrawingViewManager sharedInstance] setContactId:@""];
                [pageViewController setViewControllers:@[viewController] direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
            }];
        }
//    }else{
//        [self setTitleTextFieldText:[[DrawingViewManager sharedInstance] prevTitle]];
//        [self setCategoryName:[[DrawingViewManager sharedInstance] prevCategory]];
//        if ([[DrawingViewManager sharedInstance] showingNote]) {
//            [self didSelectNote];
//        }
//        if (![[[DrawingViewManager sharedInstance] contactId] isEqualToString:@""]) {
//            self.contactId = [[DrawingViewManager sharedInstance] contactId];
//            [[DrawingViewManager sharedInstance] setContactId:@""];
//        }
//    }
    if (_note) {
        [self setTitleTextFieldText:_note.title];
        [self setCategoryName:_note.categoryName];
    }
    [self.view setNeedsDisplay];
}

-(void)didTakePhoto{
    if ([DrawingViewManager sharedInstance].photoFromCamera) {
        [self loadImageData:[DrawingViewManager sharedInstance].photoFromCamera];
        if( [DrawingViewManager sharedInstance].saveType == [NSNumber numberWithInteger:1]){
            gallery = [NSNumber numberWithInteger:1];
         }
        if( [DrawingViewManager sharedInstance].saveType == [NSNumber numberWithInteger:2]){
            document = [NSNumber numberWithInteger:1];
        }
    
    }
}

-(UIImage *)resizeImage:(UIImage *)image {
    CGRect rect = CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT);
    UIGraphicsBeginImageContext( rect.size );
    [image drawInRect:rect];
    UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *imageData = UIImagePNGRepresentation(picture1);
    UIImage *img=[UIImage imageWithData:imageData];
    return img;
}

- (void)displayContentController:(UIViewController*)content{
    [self addChildViewController:content];
    [content didMoveToParentViewController:self];
    [self.view addSubview:bottomImageSliderView];
    content.view.frame = bottomImageSliderView.bounds;
    [bottomImageSliderView addSubview:content.view];
    [self.view bringSubviewToFront:bottomImageSliderView];
}

- (void)setContactId:(NSString *)contactId{
    _contactId = contactId;
    CNContact *contact;
    NSData *cData = [[Contact sharedInstance] getContactById:contactId].contact;
    if (cData) {
        contact = [NSKeyedUnarchiver unarchiveObjectWithData:cData];
    }
    if (contact) {
        isInCue = [NSNumber numberWithInt:1];
        if (contact.phoneNumbers.count) {
            [phoneNumberTextField setText:[contact.phoneNumbers[0].value stringValue]];
        }
        if (![contact.familyName isEqualToString:@""] || ![contact.givenName isEqualToString:@""]){
            if([titletextField.text isEqual: @""]) {
                [self setTitleTextFieldText:[NSString stringWithFormat:@"%@ %@",contact.givenName,contact.familyName]];
            }
        }else{
            if (contact.phoneNumbers.count) {
                [self setTitleTextFieldText:[contact.phoneNumbers[0].value stringValue]];
            }
        }
    }
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [titleTimer invalidate];
    [starView setHidden:YES];
    //[self hideSliderMenu:sliderButtonsView];
    [[DrawingViewManager sharedInstance] setWillCreateNewNote:NO];
    [[DrawingViewManager sharedInstance] setContactId:@""];
    self.contactId = @"";
   // titletextField.text = @"";
    [[DrawingViewManager sharedInstance] setShowingNote:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didTakePhoto)
                                                 name:@"DID_TAKED_IMAGE"
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    titletextField.alpha = 1;
    [pagingView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didPaging:)]];
    NSMutableArray *scheduledObjects = [[NSMutableArray alloc] init];
    if ([[[NSUserDefaults standardUserDefaults] dictionaryRepresentation].allKeys containsObject:@"scheduled_objects_object_id"]) {
      scheduledObjects = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"scheduled_objects_object_id"]];
    }
    NSMutableArray *newArray = [[NSMutableArray alloc] initWithArray:scheduledObjects];
    for (NSString *str in scheduledObjects) {
        if ([_note.object_id isEqualToString:str]) {
            [newArray removeObject:str];
        }
    }
    if (newArray && newArray.count) {
        [[NSUserDefaults standardUserDefaults] setObject:newArray forKey:@"scheduled_objects_object_id"];
    }
    [self colorPickerDidSelectObjectAtindex:(int)colorIndex];
    if ([titletextField.text isEqualToString:@""]) {
        [titleTimer invalidate];
        titleTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(toggleLabelAlpha) userInfo:nil repeats:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)createNewNoteFromList{
    [self askForSavingNoteWithCompletion:^(BOOL finished) {
        if (finished) {
            [[DrawingViewManager sharedInstance] setWillCreateNewNote:NO];
            UIPageViewController *pageViewController = self.navigationController.viewControllers[self.navigationController.viewControllers.count - 1];
            DrawViewController *viewController;
            if (IS_IPHONE) {
                viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewControlleriPhone" bundle:nil];
            }else{
                viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewController" bundle:nil];
            }
            [pageViewController setViewControllers:@[viewController] direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
        }
    }];
}

#pragma mark Configure

- (void)configureView{
    allbuttonsArray = [[NSMutableArray alloc] init];
    [self.navigationController setNavigationBarHidden:YES];
    [pagingView setUserInteractionEnabled:YES];
    [pagingView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didPaging:)]];
    _drowingView.delegate = self;
    _backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    [_signatureView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeSignatureView:)]];
    [_signatureBackgroundView addShadowToView];
    [_drowingView.superview addSubview:_backgroundImageView];
    [_backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_backgroundImageView.superview.mas_top);
        make.centerX.equalTo(_backgroundImageView.superview.mas_centerX);
    }];
    [_drowingView.superview bringSubviewToFront:_drowingView];
    [self createTitleButton];
    [self createFunctionButtons];
    if ([[DrawingViewManager sharedInstance] isLineSelected]) {

        [self linesButtonClicked:linesButton];
    }
    colorView.backgroundColor = _drowingView.lineColor = [[DrawingViewManager sharedInstance] lineColor];
    colorIndex = [[DrawingViewManager sharedInstance] bgColorIndex];
    [self configureStarMenuPopover];
    if ([[[DrawingViewManager sharedInstance] prevTitle] isEqualToString:@""]) {
        _drowingView.lineColor = [UIColor blackColor];
        colorIndex = 2;
    }
    [self colorPickerDidSelectObjectAtindex:(int)colorIndex];
    if (_note) {
        _drowingView.name = _note.name;
       // [self setTitleTextFieldText:_note.title];
        [self setCategoryName:_note.categoryName];
        [self colorPickerDidSelectObjectAtindex:[_note.color_index intValue]];
        if ([_note.wasDeleted integerValue] == 1) {
            wasDeleted = [NSNumber numberWithInt:0];
        }else{
            wasDeleted = _note.wasDeleted;
        }
        [self setDate: _note.created_at];
        if (!_note.image) {
            [self loadImageData:nil];
        }else{
            [self loadImageData:_note.image];
        }
        [self setLineColor];
        [self setBackgroundImage:[UIImage imageWithData:_note.backgroundImage]];
    }
    if (!contactVCView) {
        ContactsListViewController *contactVC = [[ContactsListViewController alloc] init];
        [self addChildViewController:contactVC];
        contactVC.delegate = self;
        contactVC.view.frame = CGRectMake(200, 200, 300, 600);
        contactVC.view.hidden = YES;
        contactVCView = contactVC.view;
        [self.view addSubview:contactVCView];
    }

}

- (void)configureStarMenuPopover{
    starView = [[StarView alloc] initWithFrame:CGRectMake(50, 50, 100, 20)];
    [starView setFrame:starView.frame];
    starView.delegate = self;
    if (IS_IPHONE) {
        starView.x = starButton.right + starButton.superview.x + 10;
        starView.y = starButton.y + starButton.superview.y;
    }else{
        starView.x = starButton.x + starButton.superview.x - 10;
    }
    [starView addShadowToView];
    [starView setHidden:YES];
    [self.view addSubview:starView];
}

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex{
    switch (menuPopover.tag) {
        case 0:{
            if (selectedIndex == 0) {
                [_createFileView setHidden:NO];
                [_fileNameTextField becomeFirstResponder];
                _fileNameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
            }else{
                [self createOrUpdateNote];//?????????
                UIPageViewController* p = (UIPageViewController *)self.navigationController.visibleViewController;
                DrawViewController *viewController;
                if (IS_IPHONE) {
                    viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewControlleriPhone" bundle:nil];
                }else{
                    viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewController" bundle:nil];
                }
                [p setViewControllers:@[viewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
            }
            break;
        }
        case 1:{
            switch (selectedIndex) {
                case 0:{
                    [(MainTabBarController *)self.tabBarController searchByDateButtonPressed];
                    break;
                }
                case 1:{
                    [(MainTabBarController *)self.tabBarController searchByNameButtonPressed];
                    break;
                }
                default:{
                    [self createOrUpdateNote];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"TITLE_ITEM_SELECTED" object:@[titleMenuItems[selectedIndex - 2]]];
                    break;

                }
            }
        }
        default:
            break;
    }
}

- (void)menuPopoverWillHide:(MLKMenuPopover *)menuPopover{
    
}

- (void)configureScrollViewWithImage:(UIImage *)image{
    float scaleSize = 1;//MAX(SCREEN_WIDTH/image.size.width, SCREEN_HEIGHT/image.size.height);
    float lineWidth = kDefaultLineWidth;
    _drowingView.lineWidth = lineWidth / scaleSize;
}

- (void)createFunctionButtons{
    NSMutableArray *sliderButtonsArray = [[NSMutableArray alloc] init];
    UIButton *phoneButton = [[UIButton alloc] init];
    [phoneButton setImage:[[UIImage imageNamed:@"phone_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [phoneButton setTintColor:[UIColor colorWithRed:199.0f/255.0f green:92.0f/255.0f blue:92.0f/255.0f alpha:1.0f]];
    [phoneButton addTarget:self action:@selector(phoneButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    selectQueBtn = [[UIButton alloc] init];
    [selectQueBtn setTitle:@"q" forState:UIControlStateNormal];
    [selectQueBtn.titleLabel setFont:[UIFont systemFontOfSize:25 weight:UIFontWeightBold]];
    [selectQueBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [selectQueBtn addTarget:self action:@selector(selectQueBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    
    //[sliderButtonsArray addObject:phoneButton];
    UIButton *addressBookButton = [[UIButton alloc] init];
    [addressBookButton setImage:[[UIImage imageNamed:@"adress_book_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [addressBookButton setTintColor:[UIColor colorWithRed:199.0f/255.0f green:92.0f/255.0f blue:92.0f/255.0f alpha:1.0f]];
    [addressBookButton addTarget:self action:@selector(addressBookButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    //[sliderButtonsArray addObject:addressBookButton];
    UIButton *localeShareButton = [[UIButton alloc] init];
    [localeShareButton setImage:[UIImage imageNamed:@"locale_share_icon"] forState:UIControlStateNormal];
    [localeShareButton addTarget:self action:@selector(localeShareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *signatureButton = [[UIButton alloc] init];
    [signatureButton setImage:[UIImage imageNamed:@"signature_icon"] forState:UIControlStateNormal];
    [signatureButton addTarget:self action:@selector(signatureButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //[sliderButtonsArray addObject:signatureButton];
    UIButton *shareButton = [[UIButton alloc] init];
    [shareButton setImage:[UIImage imageNamed:@"share_icon"] forState:UIControlStateNormal];
    [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    alarmButton = [[UIButton alloc] init];
    if ([[UIApplication sharedApplication] scheduledLocalNotifications].count > 0) {
        [alarmButton setImage:[UIImage imageNamed:@"alarm_icon_selected"] forState:UIControlStateNormal];
    }else{
        [alarmButton setImage:[UIImage imageNamed:@"alarm_icon"] forState:UIControlStateNormal];
    }
    [alarmButton addTarget:self action:@selector(alarmButtonPressed:) forControlEvents:UIControlEventTouchUpInside];

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    mainButtonsArray = [[NSMutableArray alloc] init];
    [clearButton setImage:[[UIImage imageNamed:@"close_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [clearButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [clearButton addTarget:self action:@selector(clearButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
    [mainButtonsArray addObject:clearButton];
    [textButton setImage:[[UIImage imageNamed:@"letter_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [textButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [textButton addTarget:self action:@selector(textButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [mainButtonsArray addObject:textButton];
    
    [micraphoneButton setImage:[[UIImage imageNamed:@"microphone"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [micraphoneButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [micraphoneButton addTarget:self action:@selector(microphoneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //[mainButtonsArray addObject:micraphoneButton];

    [rubberButton setImage:[[UIImage imageNamed:@"eraser_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [rubberButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [rubberButton addTarget:self action:@selector(rubberButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [mainButtonsArray addObject:rubberButton];
    
    [plussButton setImage:[[UIImage imageNamed:@"pluss_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [plussButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [plussButton addTarget:self action:@selector(plussButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [mainButtonsArray addObject:plussButton];
    
    [minimizeButton setTitle:@"__" forState:UIControlStateNormal];
    [minimizeButton setTitleColor:[[DrawingViewManager sharedInstance] buttonsColor] forState:UIControlStateNormal];
    [minimizeButton addTarget:self action:@selector(minimizeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [minimizeButton setUserInteractionEnabled:NO];
    [mainButtonsArray addObject:minimizeButton];
    [self setMinimizeOn:opened.integerValue];
    
    [searchButton setImage:[[UIImage imageNamed:@"search_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [searchButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [searchButton addTarget:self action:@selector(searchButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [mainButtonsArray addObject:searchButton];
    
    colorButton = [[UIButton alloc] init];
    [colorButton setImage:[[UIImage imageNamed:@"color_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [colorButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [colorButton setTag:1];
    [colorButton addTarget:self action:@selector(changeBackgroundColorButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [mainButtonsArray addObject:colorButton];
    
    UIButton *noteButton = [[UIButton alloc] init];
    [noteButton setImage:[[UIImage imageNamed:@"note_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [noteButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [noteButton addTarget:self action:@selector(noteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //[mainButtonsArray addObject:noteButton];
    [menuButton setImage:[[UIImage imageNamed:@"menu_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [menuButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [menuButton addTarget:self action:@selector(itemsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [mainButtonsArray addObject:menuButton];
    
    [queButton setTitleColor:[[DrawingViewManager sharedInstance] buttonsColor] forState:UIControlStateNormal];
    [queButton addTarget:self action:@selector(queButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [mainButtonsArray addObject:queButton];
    
    [mainHomeButton setImage:[[UIImage imageNamed:@"home"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [mainHomeButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [mainHomeButton addTarget:self action:@selector(homeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [mainButtonsArray addObject:mainHomeButton];
    
    starButton = [[UIButton alloc] init];
    [starButton setImage:[[UIImage imageNamed:@"star_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [starButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [starButton addTarget:self action:@selector(starButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [mainButtonsArray addObject:starButton];
    pinButton = [[UIButton alloc] init];
    [pinButton setImage:[[UIImage imageNamed:@"icon_pin"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [pinButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [pinButton addTarget:self action:@selector(pinButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    pinButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [mainButtonsArray addObject:pinButton];
    UIButton *cameraButton = [[UIButton alloc] init];
    [cameraButton setImage:[[UIImage imageNamed:@"camera_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [cameraButton addTarget:self action:@selector(cameraButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [cameraButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    UIButton *artButton = [[UIButton alloc] init];
    [artButton setTitle:@"Art" forState:UIControlStateNormal];
    [artButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [artButton setTitleColor:[[DrawingViewManager sharedInstance] buttonsColor] forState:UIControlStateNormal];
    UIButton *galButton = [[UIButton alloc] init];
    //[galButton setTitle:@"Gall" forState:UIControlStateNormal];
    [galButton setImage:[[UIImage imageNamed:@"img1"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [galButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [galButton setTitleColor:[[DrawingViewManager sharedInstance] buttonsColor] forState:UIControlStateNormal];
     [galButton addTarget:self action:@selector(galleryButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *docButton = [[UIButton alloc] init];
    //[galButton setTitle:@"Gall" forState:UIControlStateNormal];
    [docButton setImage:[[UIImage imageNamed:@"doc"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [docButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [docButton setTitleColor:[[DrawingViewManager sharedInstance] buttonsColor] forState:UIControlStateNormal];
    [docButton addTarget:self action:@selector(documentsButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    linesButton = [[UIButton alloc] init];
    [linesButton setImage:[[UIImage imageNamed:@"lines_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [linesButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [linesButton addTarget:self action:@selector(linesButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //[mainButtonsArray addObject:linesButton];
    
    UIButton *sortButton = [[UIButton alloc] init];
    [sortButton setImage:[[UIImage imageNamed:@"sort"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [sortButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    
    UIButton *trashButton = [[UIButton alloc] init];
    [trashButton setTitle:@"Arc" forState:UIControlStateNormal];
    [trashButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [trashButton setTitleColor:[[DrawingViewManager sharedInstance] buttonsColor] forState:UIControlStateNormal];
    
    penOrFinger = [[UIButton alloc] init];
    if ([[DrawingViewManager sharedInstance] isPen]) {
        [penOrFinger setImage:[[UIImage imageNamed:@"pen"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    }else{
        [penOrFinger setImage:[[UIImage imageNamed:@"finger"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    }
    //[penOrFinger setTintColor:[UIColor redColor]];
    [penOrFinger addTarget:self action:@selector(penOrFingerButtonPressed) forControlEvents:UIControlEventTouchUpInside];
  //  [self penOrFingerButtonPressed];
    showImageSliderButton = [[UIButton alloc] init];
    [showImageSliderButton setImage:[[UIImage imageNamed:@"next_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [showImageSliderButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    showImageSliderButton.transform = CGAffineTransformMakeRotation(-M_PI_2);
    [showImageSliderButton addTarget:self action:@selector(bottomImageSliderButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //[sliderButtonsArray addObject:toDoButton];
    [sliderButtonsArray addObject:penOrFinger];
    
    safeButton = [[UIButton alloc] init];
    [safeButton setImage:[[UIImage imageNamed:@"unlocked"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [safeButton setImage:[[UIImage imageNamed:@"locked"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate ] forState:UIControlStateSelected];
    [safeButton addTarget:self action:@selector(safeButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [sliderButtonsArray addObject:safeButton];
    
    

    if (IS_IPHONE) {
        [sliderButtonsArray addObject:mainHomeButton];
        [sliderButtonsArray addObject:menuButton];
        [sliderButtonsArray addObject:queButton];
        [sliderButtonsArray addObject:galButton];
        [sliderButtonsArray addObject:docButton];
        [sliderButtonsArray addObject:searchButton];
        [sliderButtonsArray addObject:plussButton];
        [sliderButtonsArray addObject:rubberButton];
        [sliderButtonsArray addObject:textButton];
        
        //[sliderButtonsArray addObject:micraphoneButton];
    } else {
        [sliderButtonsArray addObject:galButton];
        [sliderButtonsArray addObject:docButton];
    }
    
    
    [sliderButtonsArray addObject:shareButton];
    [sliderButtonsArray addObject:alarmButton];
    [sliderButtonsArray addObject:colorButton];
    [sliderButtonsArray addObject:starButton];
    [sliderButtonsArray addObject:pinButton];
    [sliderButtonsArray addObject:selectQueBtn];
    [sliderButtonsArray addObject:phoneButton];
    [sliderButtonsArray addObject:trashButton];
    [sliderButtonsArray addObject:minimizeButton];
    [sliderButtonsArray addObject:localeShareButton];
    [sliderButtonsArray addObject:sortButton];
    [sliderButtonsArray addObject:showImageSliderButton];
    [sliderButtonsArray addObject:artButton];
   // [sliderButtonsArray addObject:cameraButton];
    [sliderButtonsArray addObject:addressBookButton];


    [self createSlideButtonsWithArray:sliderButtonsArray];

    nextButton.centerY = SCREEN_HEIGHT/2;
    nextButton.right = SCREEN_WIDTH;
    [nextButton setImage:[[UIImage imageNamed:@"nav_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [nextButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    nextButton.transform = CGAffineTransformMakeRotation(M_PI);
    [nextButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [nextButton addShadowToView];
    
    backButton.centerY = SCREEN_HEIGHT/2;
    backButton.left = 0;
    [backButton setImage:[[UIImage imageNamed:@"nav_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [backButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [backButton addTarget:self action:@selector(nextButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [backButton addShadowToView];
    
    bottomButton.centerX = SCREEN_WIDTH/2 - 30;
    bottomButton.bottom = SCREEN_HEIGHT;
    [bottomButton setImage:[[UIImage imageNamed:@"nav_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [bottomButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    bottomButton.transform = CGAffineTransformMakeRotation(-M_PI_2);
    [bottomButton addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bottomButton addShadowToView];
    
    topButton.centerX = SCREEN_WIDTH/2 + 30;
    topButton.bottom = SCREEN_HEIGHT;
    [topButton setImage:[[UIImage imageNamed:@"nav_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [topButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    topButton.transform = CGAffineTransformMakeRotation(M_PI_2);
    [topButton addTarget:self action:@selector(topButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [topButton addShadowToView];
    
    UITapGestureRecognizer *tapForScrollingUp = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapForScrollingUp:)];
    UILongPressGestureRecognizer *panForScrollingUp = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(panForScrollingUp:)];
    scrallUpImageView.userInteractionEnabled = YES;
    scrallUpImageView.image = [[UIImage imageNamed:@"nav_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    scrallUpImageView.transform = CGAffineTransformMakeRotation(M_PI_2);
    scrallUpImageView.gestureRecognizers = @[panForScrollingUp,tapForScrollingUp];
    
    UITapGestureRecognizer *tapForScrollingDown = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapForScrollingDown:)];
    UILongPressGestureRecognizer *panForScrollingDown = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(panForScrollingDown:)];
    scrollDownImageView.userInteractionEnabled = YES;
    scrollDownImageView.image = [[UIImage imageNamed:@"nav_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    scrollDownImageView.transform = CGAffineTransformMakeRotation(-M_PI_2);
    scrollDownImageView.gestureRecognizers = @[panForScrollingDown,tapForScrollingDown];

    [allbuttonsArray addObjectsFromArray:sliderButtonsArray];
    [allbuttonsArray addObjectsFromArray:mainButtonsArray];
    [allbuttonsArray addObject:phoneButton];
    [allbuttonsArray addObject:addressBookButton];
  //  [allbuttonsArray addObject:cameraButton];
    [allbuttonsArray addObject:artButton];
    //[allbuttonsArray addObject:galButton];
    [allbuttonsArray addObject:nextButton];
    [allbuttonsArray addObject:backButton];
    [allbuttonsArray addObject:scrollDownImageView];
    [allbuttonsArray addObject:scrallUpImageView];
    //[allbuttonsArray addObject:bottomButton];
    //[allbuttonsArray addObject:topButton];
    [allbuttonsArray addObject:colorButton];
    [allbuttonsArray addObject:starButton];
    [allbuttonsArray removeObject:titletextField];
    //[allbuttonsArray addObject:dateLabel];
    [allbuttonsArray addObject:mainHomeButton];
    [allbuttonsArray addObject:sortButton];
    [allbuttonsArray addObject:trashButton];
    [allbuttonsArray addObject:penOrFinger];
    [allbuttonsArray addObject:showImageSliderButton];
    [allbuttonsArray addObject:selectQueBtn];
    
    [self setButtons:allbuttonsArray wilthColor:[[DrawingViewManager sharedInstance] buttonsColor]];
}

- (void)panForScrollingUp:(UILongPressGestureRecognizer *)panGesture{
    static NSTimer *upTimer;
    if (panGesture.state == UIGestureRecognizerStateBegan) {
       upTimer = [NSTimer scheduledTimerWithTimeInterval:0.005 target:self selector:@selector(scrollUpAuto) userInfo:nil repeats:YES];
        
    } else if (panGesture.state == UIGestureRecognizerStateEnded) {
        [upTimer invalidate];
        upTimer = nil;
    }
}


-(void)galleryButtonClicked {
    [self createOrUpdateNote];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GALLERY_BUTTON_PRESSED" object:nil];
}

-(void)documentsButtonClicked {
    [self createOrUpdateNote];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DOC_BUTTON_PRESSED" object:nil];
}

- (void)scrollUpAuto{
    scrollView.contentOffsetY = scrollView.contentOffsetY + 2 > _drowingView.height - SCREEN_HEIGHT?_drowingView.height - SCREEN_HEIGHT:scrollView.contentOffsetY + 2;
}

- (void)tapForScrollingUp:(UITapGestureRecognizer *)tapGesture{
       scrollView.contentOffsetY = scrollView.contentOffsetY + 20 > _drowingView.height - SCREEN_HEIGHT?_drowingView.height - SCREEN_HEIGHT:scrollView.contentOffsetY + 20;
}

- (void)panForScrollingDown:(UILongPressGestureRecognizer *)panGesture{
    static NSTimer *upTimer;
    if (panGesture.state == UIGestureRecognizerStateBegan) {
        upTimer = [NSTimer scheduledTimerWithTimeInterval:0.005 target:self selector:@selector(scrollDownAuto) userInfo:nil repeats:YES];
        
    } else if (panGesture.state == UIGestureRecognizerStateEnded) {
        [upTimer invalidate];
        upTimer = nil;
    }
}

- (void)scrollDownAuto{
    scrollView.contentOffsetY = scrollView.contentOffsetY - 2 < 0?0:scrollView.contentOffsetY - 2;
}

-(void)drawingViewDidScroll:(BOOL)direction different:(double)diff{
//    CGPoint offset = scrollView.contentOffset;
//    if (direction){
//        offset.y = offset.y + diff;
//        [scrollView setContentOffset:offset animated:NO];
//    } else {
//        offset.y = offset.y - diff;
//        [scrollView setContentOffset:offset animated:NO];
//        
//    }
}

- (void)tapForScrollingDown:(UITapGestureRecognizer *)tapGesture{
    scrollView.contentOffsetY = scrollView.contentOffsetY - 20 < 0 ? 0:scrollView.contentOffsetY - 20;
}

- (void)setButtons:(NSMutableArray *)buttons wilthColor:(UIColor *)color{
    for (UIButton *btn in buttons) {
        if(![btn.tintColor isEqual:[UIColor redColor]]){
            if ([btn isKindOfClass:[UIButton class]] && btn.tag != 99) {
                [btn setImage:[btn.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
                [btn setTintColor:color];
                [btn setTitleColor:color forState:UIControlStateNormal];
            }
            if ([btn isKindOfClass:[UILabel class]]) {
                UILabel *lbl = (UILabel *)btn;
                [lbl setTextColor:color];
            }
            if ([btn isKindOfClass:[UIImageView class]]) {
                UIImageView *imgView = (UIImageView *)btn;
                imgView.tintColor = color;
            }
        }
    }
    [titletextField setBackgroundColor:color];
    [hideShowButton setTintColor:color];
    if ([starCount intValue] > 0) {
        [starButton setTintColor:[UIColor yellowColor]];
    }
    if ([pinCount intValue] > 0) {
        [pinButton setTintColor:[UIColor yellowColor]];
    }
}

- (void)bottomImageSliderButtonClicked:(UIButton *)sender{
    if ([showImageSliderButton isSelected]) {
        [self hideBottomImageSlider];
    }else{
        [self showBottomImageSlider];
    }
}

- (void)showBottomImageSlider{
    NSArray *objects;
    if ([titletextField.text isEqualToString:@""]) {
        objects = [Note getAllObjects];
    }else{
        objects = [Note getAllObjectsUsingSortDescriptor:SortDescriptortypeCreatedDate fromArchive:[NSNumber numberWithInt:0] withTitle:titletextField.text];
    }
    if (objects.count) {
        [bootpmImgaeSliderVC configureWithNotes:objects withObjetcId:[NSDate dateValue:date] marge:5];
    }
    imageSliderTopC.constant = - bottomImageSliderView.height;
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
    [showImageSliderButton setSelected:YES];
}

- (void)hideBottomImageSlider{
    imageSliderTopC.constant = 0;
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
    [showImageSliderButton setSelected:NO];
}

- (void)createSlideButtonsWithArray:(NSArray *)array{
    count = (int)array.count;
    sliderButtonsView = [[UIScrollView alloc] initWithFrame:CGRectMake(-20, sliderHeight - 40, 70, sliderHeight)];
    sliderButtonsView.contentSize = CGSizeMake(0, 0);
    sliderButtonsView.contentOffsetY = 40 * (count + 1) + 20 - sliderHeight;
    [sliderButtonsView setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:0]];
    for (int i = 0; i < count; ++i) {
        UIButton *btn = array[i];
        [btn addShadowToView];
        [btn setFrame:CGRectMake(25, i*40 + 20, 40, 40)];
        [sliderButtonsView addSubview:btn];
        [btn setNeedsDisplay];
    }
    hideShowButton = [[UIButton alloc] initWithFrame:CGRectMake(25, count * 40 + 21, 40, 40)];
    [hideShowButton setImage:[[UIImage imageNamed:@"down_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [hideShowButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [hideShowButton addShadowToView];
    [hideShowButton addTarget:self action:@selector(hideshowSliderMenuButtons:) forControlEvents:UIControlEventTouchUpInside];
    [sliderButtonsView addSubview:hideShowButton];
    sliderButtonsView.layer.masksToBounds = YES;
    sliderButtonsView.layer.cornerRadius = 25;
    [self.view addSubview:sliderButtonsView];
    [self.view setNeedsDisplay];
}

- (void)didSelectObject{
    [self askForSavingNoteWithCompletion:^(BOOL finished) {
        [self didSelectNote];
    }];
}

- (void)didSelectNote{
    [self setisPen];
    NoteMenagedObject *note = [[DrawingViewManager sharedInstance] showingNote];
    if (note.alarm.integerValue == 1) {
        [alarmButton setTintColor:[UIColor redColor]];
    }else{
        [alarmButton setTintColor:[DrawingViewManager sharedInstance].buttonsColor];
    }
    self.note = note;
    alarmDate = note.alarm_at;
    alarm = note.alarm;
    isInCue = note.isInCue;
    document = note.document;
    gallery = note.gallery;
    phoneNumberTextField.text = note.phoneNumber;
    starCount = note.star_count;
    pinCount = note.pin_count;
    _drowingView.name = note.name;
    [self setTitleTextFieldText:note.title];
    [self setCategoryName:note.categoryName];
    titletextField.alpha = 1;
    [self textFieldDidEndEditing:titletextField];
    _drowingView.textY = [note.textY floatValue];
    colorIndex = [note.color_index intValue];
    [[DrawingViewManager sharedInstance] setBgColorIndex:colorIndex];
    [[DrawingViewManager sharedInstance] setPrevTitle:note.title];
    [[DrawingViewManager sharedInstance] setPrevCategory:note.categoryName];
    [self setDate: note.created_at];
    [self colorPickerDidSelectObjectAtindex:(int)colorIndex];
    [safeButton setSelected:_note.safe.boolValue];
    selectedNotObjectId = note.object_id;
    NSLog(@"note.safe == %i",_note.safe.boolValue);
    if ([_note.wasDeleted integerValue] == 1) {
        wasDeleted = [NSNumber numberWithInt:0];
    }else{
        wasDeleted = _note.wasDeleted;
    }
    [self setLineColor];
    if (!note.image) {
        [self loadImageData:nil];
    }else{
        [self loadImageData:note.image];
    }
    [self setBackgroundImage:[UIImage imageWithData:note.backgroundImage]];
    if ([showImageSliderButton isSelected]) {
        [self showBottomImageSlider];
    }
    opened = note.opened;
    [self setMinimizeOn:note.opened.integerValue];
}

- (void)didPaging:(UIPanGestureRecognizer *)sender{
    if ((([sender locationInView:self.view].x + SCREEN_MIN_LENGTH*0.21 < SCREEN_MIN_LENGTH && [sender locationInView:self.view].y + SCREEN_MIN_LENGTH*0.21 < SCREEN_MAX_LENGTH)
        ||
        ([sender locationInView:self.view].x + SCREEN_MIN_LENGTH*0.21 < SCREEN_MIN_LENGTH && [sender locationInView:self.view].y + SCREEN_MIN_LENGTH*0.21 > SCREEN_MAX_LENGTH)
        ||
        ([sender locationInView:self.view].x + SCREEN_MIN_LENGTH*0.21 > SCREEN_MIN_LENGTH && [sender locationInView:self.view].y + SCREEN_MIN_LENGTH*0.21 < SCREEN_MAX_LENGTH))
         && !didPaged){
        didPaged = YES;
        [[DrawingViewManager sharedInstance] setPrevTitle:@""];
        [[DrawingViewManager sharedInstance] setPrevCategory:@""];
        [[DrawingViewManager sharedInstance] setShowingNote:nil];
        UIPageViewController *pageViewController = [[UIPageViewController alloc] init];
        DrawViewController *viewController;
        if (IS_IPHONE) {
            viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewControlleriPhone" bundle:nil];
        }else{
            viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewController" bundle:nil];
        }
        [pageViewController setViewControllers:@[viewController] direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
        CATransition* transition = [CATransition animation];
        transition.duration = 0.5;
        transition.type = @"pageCurl";
        transition.subtype = kCATransitionFromBottom;
        [self.navigationController.view.layer
         addAnimation:transition forKey:kCATransition];
        [pagingView removeGestureRecognizer:sender];
        for (UIGestureRecognizer *gest in pagingView.gestureRecognizers) {
            gest.enabled = YES;
        }
        [self createOrUpdateNote];
        [[DrawingViewManager sharedInstance] setPrevCategory:@""];
        [self setCategoryName:@""];
        self.navigationController.viewControllers = @[pageViewController];
    }
}

#pragma mark Signature

- (void)signatureButtonClicked:(UIButton *)sender{
    [self hideSliderMenu:sliderButtonsView];
    [self.view bringSubviewToFront:_signatureView];
    [_signatureView setAlpha:0];
    _signatureDrawingView.lineColor = _drowingView.lineColor;
    [_signatureView setHidden:NO];
    [UIView animateWithDuration:0.5 animations:^{
        [_signatureView setAlpha:1];
    }];
}

- (IBAction)newSignature:(id)sender {
    [_signatureDrawingView clear];
}

- (IBAction)saveSignature:(id)sender {
    if (_signatureDrawingView.image) {
        NSData* pngdata = UIImagePNGRepresentation (_signatureDrawingView.image); //PNG wrap
        UIImage* img = [UIImage imageWithData:pngdata];
        UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil);
        [UIView animateWithDuration:0.5 animations:^{
            [_signatureView setAlpha:0];
        } completion:^(BOOL finished) {
            [_signatureView setHidden:YES];
            [self signitureSaved];
        }];
    }
}

- (void)signitureSaved{
    [LKAlertView showAlertWithTitle:@""
                            message:@"Signature saved"
                  cancelButtonTitle:@"OK"
                  otherButtonTitles:nil
                         completion:nil];
}

- (void)closeSignatureView:(UITapGestureRecognizer *)gesture{
    CGPoint location = [gesture locationInView:_signatureDrawingView];
    NSLog(@"%f,%f",location.x,location.y);
    if(!(location.x > -10 && location.x < _signatureDrawingView.width + 10 && location.y > -10 && location.y < _signatureDrawingView.height + 10) ){
        [UIView animateWithDuration:0.5 animations:^{
            [_signatureView setAlpha:0];
        } completion:^(BOOL finished) {
            [_signatureView setHidden:YES];
        }];
    }
}

#pragma mark Alarm

- (void)alarmButtonPressed:(id)sender{
    AlarmViewController *datePickerViewController = [[AlarmViewController alloc]init];
    //[datePickerViewController configureViewWithSizes:CGRectMake(0, SCREEN_HEIGHT - 300, SCREEN_WIDTH, 300)];
    datePickerViewController.view.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0];
    datePickerViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    datePickerViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    datePickerViewController.date = date;
    datePickerViewController.objectId = selectedNotObjectId;
    //[self hideSliderMenu:sliderButtonsView];
    [self presentViewController:datePickerViewController animated:YES completion:nil];
}

- (void)setScheduleLocaleNotification:(NSNotification*)info{
    NSDate* dic = info.object;
    alarmDate = dic;
    alarm = [NSNumber numberWithInt:1];
    if ([[UIApplication sharedApplication] scheduledLocalNotifications].count > 0) {
        [alarmButton setTintColor:[UIColor redColor]];
    }else{
        [alarmButton setTintColor:[DrawingViewManager sharedInstance].buttonsColor];
    }
}

- (double)dateDistanceFrom1970InSecconds:(NSDate *)localeDate{
    NSTimeInterval distanceBetweenDates = [localeDate timeIntervalSince1970];
    return distanceBetweenDates;
}

#pragma mark HideShow

- (void)startTextEntry{
    [self hideSliderMenu:sliderButtonsView];
}

- (void)hideshowSliderMenuButtons:(UIButton *)sender{
    if (sender.tag == 0) {
        [self showSliderMenu:sender.superview];
    }else{
        [self hideSliderMenu:sender.superview];
    }
}

- (void)showSliderMenu:(UIView *)menuView{
    hideShowButton.tag = 1;
    sliderButtonsView.contentSize = CGSizeMake(0, 40 * (count + 1) + 20);
    sliderButtonsView.contentOffsetY = 40 * (count + 1) + 20 - sliderHeight;
    [UIView animateWithDuration:0.4 animations:^{
        menuView.y = -20;
        hideShowButton.transform = CGAffineTransformMakeRotation(M_PI);
        [self checkIsInQue];
    }];
}

- (void)hideSliderMenu:(UIView *)menuView{
    hideShowButton.tag = 0;
    sliderButtonsView.contentSize = CGSizeMake(0, 0);
    [UIView animateWithDuration:0.4 animations:^{
        menuView.bottom = 40;
        hideShowButton.transform = CGAffineTransformMakeRotation(0);
        sliderButtonsView.contentOffsetY = 40 * (count + 1) + 20 - sliderHeight;
    } completion:^(BOOL finished) {
    }];
    [starView setHidden:YES];
    [self hideBottomImageSlider];
}

- (void) colorPickerControllerDidFinish: (InfColorPickerController*) picker{
    colorView.backgroundColor = [DrawingViewManager sharedInstance].lineColor =_drowingView.lineColor = picker.resultColor;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)drawingViewWillBeginDrawUsingTool:(id<DrawingTool>)tool{
    [self.view endEditing:YES];
    if (gallery.integerValue == [NSNumber numberWithInteger:1].integerValue){
    [HDNotificationView showNotificationViewWithImage:[UIImage imageNamed:@"Icon-App-40x40"] title:@"" message:@"Your photo did moved into notes"];
    gallery = [NSNumber numberWithInteger:0];
    }
    //[self hideSliderMenu:sliderButtonsView];
    [self hidePopover];
    [starView setHidden:YES];
    if (!phoneNumberTextField.hidden) {
        [self phoneButtonClicked];
    }
}

- (void)drawingViewDidEndDrawUsingTool:(id<DrawingTool>)tool{
}

- (void)drawingViewDidDraw:(DrawingView *)view{
}

- (void)hideMenus{
    [self hideSliderMenu:sliderButtonsView];
    [self hidePopover];
    hideShowButton.tag = 0;
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame1 = mainButtonsView.frame;
        frame1.origin.y = -frame1.size.height;
        mainButtonsView.frame = frame1;
        nextButton.left = SCREEN_WIDTH;
        backButton.right = 0;
    }];
}

#pragma mark - Actions

- (void)createOrUpdateNote{
    [_drowingView endEditing:YES];
    if (_drowingView.lastPoint.y != 0 || ![phoneNumberTextField.text isEqualToString:@""]) {
        NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
        NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyyMMddhhmmss"];
        NSString *strdate = [formatter stringFromDate:date];
        if (_note) {
            NoteMenagedObject *noteMenagedObject = _note;
            UIImage *im = _drowingView.image;
            im = [im imageByTrimmingTransparentPixels];
            NSData *imgData = UIImagePNGRepresentation(im);
            noteMenagedObject.image = imgData;
            if ([DrawingViewManager sharedInstance].takenPhoto) {
                noteMenagedObject.backgroundImage = [DrawingViewManager sharedInstance].takenPhoto;
            }
            [[DrawingViewManager sharedInstance] setTakenPhoto:nil];
            noteMenagedObject.gallery = gallery;
            noteMenagedObject.document = document;
            noteMenagedObject.alarm_at = alarmDate;
            noteMenagedObject.alarm = alarm;
            noteMenagedObject.wasDeleted = wasDeleted;
            noteMenagedObject.phoneNumber = phoneNumberTextField.text;
            noteMenagedObject.isInCue = isInCue;
            noteMenagedObject.color_index = [NSNumber numberWithInteger:colorIndex];
            [noteMenagedObject setTextY:[NSNumber numberWithFloat:_drowingView.textY]];
            noteMenagedObject.name = _drowingView.name;
            noteMenagedObject.updated_at = [NSDate new];
            noteMenagedObject.view_at = [NSDate new];
            noteMenagedObject.line_color_index = [UIColor getStringFromColor:_drowingView.lineColor];
            noteMenagedObject.title = titletextField.text;
            noteMenagedObject.titleUpperCase = titletextField.text.uppercaseString;
            noteMenagedObject.categoryName = categoryTextField.text;
            noteMenagedObject.categoryNameUpperCase = categoryTextField.text.uppercaseString;            noteMenagedObject.version = [NSNumber numberWithInt:[noteMenagedObject.version intValue] + 1];
            noteMenagedObject.priority = [NSNumber numberWithDouble:[self dateDistanceFrom1970InSecconds:date]];
            noteMenagedObject.safe = [NSNumber numberWithBool:safeButton.isSelected];
            if ([starCount integerValue] > 0) {
                noteMenagedObject.star_count = starCount;
            }else{
                noteMenagedObject.star_count = [NSNumber numberWithInt:0];
            }
            if ([pinCount integerValue] > 0) {
                noteMenagedObject.pin_count = pinCount;
            }else{
                noteMenagedObject.pin_count = [NSNumber numberWithInt:0];
            }
            if (opened.integerValue > 0) {
                noteMenagedObject.opened = opened;
            }else{
                noteMenagedObject.opened = [NSNumber numberWithInt:0];
            }
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
                [LKAlertView showAlertWithTitleInDevMode:@"Whoops, note couldn't save (Locale)"
                                        message:[NSString stringWithFormat:@"%@", [error localizedDescription]]
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil
                                     completion:nil];
            }
            if ([PFUser currentUser]) {
                //[Note updateCoreDataObjectInParse:noteMenagedObject]; //Armen
                [Note syncData];
                NSLog(@"update parse___");
            }
            [formatter setDateFormat:@"ss mm HH yy MM dd"];
            [formatter setTimeZone:[NSTimeZone defaultTimeZone]];
            NSString *stringFromDate = [formatter stringFromDate:date];
        }else{
            NoteMenagedObject *noteMenagedObject = [NSEntityDescription insertNewObjectForEntityForName:@"Note" inManagedObjectContext:context];
            UIImage *im = _drowingView.image;
            im = [im imageByTrimmingTransparentPixels];
            NSData *imgData = UIImagePNGRepresentation(im);
            noteMenagedObject.image = imgData;
            if ([DrawingViewManager sharedInstance].takenPhoto) {
                noteMenagedObject.backgroundImage = [DrawingViewManager sharedInstance].takenPhoto;
            }
            [[DrawingViewManager sharedInstance] setTakenPhoto:nil];
            noteMenagedObject.document = document;
            noteMenagedObject.gallery = gallery;
            noteMenagedObject.created_at = date;
            noteMenagedObject.alarm_at = alarmDate;
            noteMenagedObject.alarm = alarm;
            noteMenagedObject.updated_at = date;
            noteMenagedObject.view_at = date;
            noteMenagedObject.isInCue = isInCue;
            noteMenagedObject.phoneNumber = phoneNumberTextField.text;
            noteMenagedObject.color_index = [NSNumber numberWithInteger:colorIndex];
            [noteMenagedObject setTextY:[NSNumber numberWithFloat:_drowingView.textY]];
            noteMenagedObject.name = _drowingView.name;
            noteMenagedObject.title = titletextField.text;
            noteMenagedObject.titleUpperCase = titletextField.text.uppercaseString;
            noteMenagedObject.categoryName = categoryTextField.text;
            noteMenagedObject.categoryNameUpperCase = categoryTextField.text.uppercaseString;
            noteMenagedObject.object_id = strdate;
            noteMenagedObject.contact_id = _contactId;
            noteMenagedObject.version = [NSNumber numberWithInt:0];
            noteMenagedObject.line_color_index = [UIColor getStringFromColor:_drowingView.lineColor];
            noteMenagedObject.safe = [NSNumber numberWithBool:safeButton.isSelected];
            noteMenagedObject.priority = [NSNumber numberWithDouble:[self dateDistanceFrom1970InSecconds:date]];
            if ([starCount integerValue] > 0) {
                noteMenagedObject.star_count = starCount;
            }else{
                noteMenagedObject.star_count = [NSNumber numberWithInt:0];
            }
            if ([pinCount integerValue] > 0) {
                noteMenagedObject.pin_count = pinCount;
            }else{
                noteMenagedObject.pin_count = [NSNumber numberWithInt:0];
            }
            if (opened.integerValue > 0) {
                noteMenagedObject.opened = opened;
            }else{
                noteMenagedObject.opened = [NSNumber numberWithInt:0];
            }
            noteMenagedObject.isInParse = [NSNumber numberWithInt:0];
            noteMenagedObject.wasDeleted = wasDeleted;
            noteMenagedObject.time_sort_number = [self getTimeSortNumberUsingDate:date];
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
                [LKAlertView showAlertWithTitleInDevMode:@"Whoops, note couldn't save (Locale)"
                                                 message:[NSString stringWithFormat:@"%@", [error localizedDescription]]
                                       cancelButtonTitle:@"OK"
                                       otherButtonTitles:nil
                                              completion:nil];
            }else{
                _note = noteMenagedObject;
                [self saveNoteForContact:noteMenagedObject];
                if ([PFUser currentUser] && _note.safe.integerValue != 1) {
                    NotePFObject *notePfObject = [[NotePFObject alloc] init];
                   // [notePfObject saveMenagedObjectToParse:noteMenagedObject];//Armen
                    [Note syncData];
                    NSLog(@"new parse___");
                }
            }
            [formatter setDateFormat:@"ss mm HH yy MM dd"];
            [formatter setTimeZone:[NSTimeZone defaultTimeZone]];
            NSString *stringFromDate = [formatter stringFromDate:date];
        }
    }else{
        [LKAlertView showAlertWithTitleInDevMode:@"Whoops, note dont saved , becouse it is empty"
                                         message:@""
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil
                                      completion:nil];
        [_backgroundImageView setImage:nil];
    }
    [[DrawingViewManager sharedInstance] setPrevCategory:categoryTextField.text];
}

- (void)saveNoteForContact:(NoteMenagedObject *)noteMenagedObject{
    ContactManagedObject *contactManagedObject = [[Contact sharedInstance] getContactById:noteMenagedObject.contact_id];
    NSMutableSet *notes = [contactManagedObject mutableSetValueForKey:@"notes"];
    if (!notes) {
        notes = [[NSMutableSet alloc] init];
        [notes addObject:noteMenagedObject];
        [contactManagedObject setValue:contactManagedObject forKey:@"notes"];

    }else{
        [notes addObject:noteMenagedObject];
    }
    NSError *error;
    if (![contactManagedObject.managedObjectContext save:&error]) {
        NSLog(@"Whoops, Contacte couldn't save: %@", [error localizedDescription]);
    }
}

- (NSNumber *)getTimeSortNumberUsingDate:(NSDate *)localeDate{
    NoteMenagedObject *prevNote = [Note getPrevObject:localeDate];
    if (prevNote) {
        NSLog(@"%li ",(long)[NSDate minutesBetween:localeDate dates:prevNote.created_at]);
        if ([NSDate minutesBetween:localeDate dates:prevNote.created_at] < 10) {
            CGFloat lastFloatNumber = [prevNote.time_sort_number floatValue];
            return [NSNumber numberWithFloat:lastFloatNumber - 0.001];
        }else{
            int lastIntNumber = [prevNote.time_sort_number intValue];
            return [NSNumber numberWithInt:lastIntNumber + 2];
        }
        return [NSNumber numberWithInteger:[NSDate minutesBetween:localeDate dates:prevNote.created_at]];
    }else{
        return [NSNumber numberWithInt:1];
    }
}

- (void)askForSavingNoteWithCompletion:(void (^ __nullable)(BOOL finished))completion{
    NoteForList *existingObject = [[DrawingViewManager sharedInstance] noteForList];
    if (existingObject) {
        NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
        NoteMenagedObject *noteMenagedObject = [NSEntityDescription insertNewObjectForEntityForName:@"Note" inManagedObjectContext:context];
        noteMenagedObject.backgroundImage = existingObject.backgroundImage;
        noteMenagedObject.image = existingObject.image;
        noteMenagedObject.created_at = existingObject.created_at;
        noteMenagedObject.updated_at = existingObject.updated_at;
        noteMenagedObject.view_at = existingObject.view_at;
        noteMenagedObject.color_index = existingObject.color_index;
        noteMenagedObject.textY = existingObject.textY;
        noteMenagedObject.name = existingObject.name;
        noteMenagedObject.title = existingObject.title;
        noteMenagedObject.object_id = existingObject.object_id;
        noteMenagedObject.priority = existingObject.priority;
        noteMenagedObject.star_count = existingObject.starCount;
        noteMenagedObject.pin_count = existingObject.pinCount;
        noteMenagedObject.time_sort_number = existingObject.time_sort_number;
        noteMenagedObject.version = existingObject.version;
        noteMenagedObject.isInParse = [NSNumber numberWithInt:0];
        noteMenagedObject.wasDeleted = wasDeleted;
        noteMenagedObject.fileName = existingObject.fileName;
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Whoops, note couldn't save: %@", [error localizedDescription]);
        }else{
            if ([PFUser currentUser]) {
                //NotePFObject *notePfObject = [[NotePFObject alloc] init];
                //[notePfObject saveMenagedObjectToParse:noteMenagedObject];
            }
        }
        completion(YES);
    }else{
        completion(YES);
    }
}

- (void)bottomButtonClicked:(UIButton *)sender{
    if (scrollView.contentSize.height - SCREEN_HEIGHT - scrollView.contentOffsetY - 50 > 0) {
        [scrollView setContentOffsetY:scrollView.contentOffsetY + 50];
    }else{
        [scrollView setContentOffsetY:scrollView.contentSize.height - SCREEN_HEIGHT];
    }
}

- (void)topButtonClicked:(UIButton *)sender{
    if (scrollView.contentOffsetY - 50 > 0) {
        [scrollView setContentOffsetY:scrollView.contentOffsetY - 50];
    }else{
        [scrollView setContentOffsetY:0];
    }
}

- (void)backButtonClicked:(UIButton *)sender{
    [[DrawingViewManager sharedInstance] setShowingNote:nil];
    [self createOrUpdateNote];
     NSLog(@"titletextField = %@",titletextField.text);
    NoteMenagedObject *prevNote = [Note getPrevObject:date withTitle:titletextField.text];
    if(prevNote){
        UIPageViewController* p = (UIPageViewController *)self.navigationController.visibleViewController;
        DrawViewController *viewController;
        if (IS_IPHONE) {
            viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewControlleriPhone" bundle:nil];
        }else{
            viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewController" bundle:nil];
        }
        viewController.note = prevNote;
        [[DrawingViewManager sharedInstance] setPrevTitle:prevNote.title];
        [p setViewControllers:@[viewController] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
    }else{
       // [self plussButtonClicked:plussButton];
    }
}

- (void)showCreateFileView{
    _createFileView.left = SCREEN_WIDTH;
    _createFileView.hidden = NO;
    [_fileNameTextField becomeFirstResponder];
    _fileNameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    [UIView animateWithDuration:0.5 animations:^{
        _createFileView.left = 0;
    }];
}

- (void)nextButtonClicked:(UIButton *)sender{
    [[DrawingViewManager sharedInstance] setShowingNote:nil];
    [self createOrUpdateNote];
    NSLog(@"titletextField = %@",titletextField.text);
    NoteMenagedObject *nextNote = [Note getNextObject:date withTitle:titletextField.text];
    if(nextNote){
        UIPageViewController* p = (UIPageViewController *)self.navigationController.visibleViewController;
        DrawViewController *viewController;
        if (IS_IPHONE) {
            viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewControlleriPhone" bundle:nil];
        }else{
            viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewController" bundle:nil];
        }
        viewController.note = nextNote;
        [[DrawingViewManager sharedInstance] setPrevTitle:nextNote.title];
        [p setViewControllers:@[viewController] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    }
}

- (void)localeShareButtonClicked:(id)sender{
    [(MainTabBarController *)self.tabBarController logInButtonPressed];
}

- (void)addressBookButtonClicked{
}

- (IBAction)shareButtonClicked:(id)sender {
    UIImage *shareImage = _drowingView.image;
    NSArray *activityItems = [NSArray arrayWithObjects:[UIImage imageWithImageOne:[UIImage imageWithImageOne:[UIImage imageFromColor:_backgroundViewForColor.backgroundColor withSize:shareImage.size] andImageTwo:shareImage] andImageTwo:[UIImage imageNamed:@"pin"]], nil];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewController.excludedActivityTypes = @[ UIActivityTypeAssignToContact ];
    activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    //[self hideSliderMenu:sliderButtonsView];
    if (IS_IPHONE) {
        [self presentViewController:activityViewController animated:YES completion:nil];
    }else{
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityViewController];
        [popup presentPopoverFromRect:CGRectMake(0, 60, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}

- (void)phoneButtonClicked{
    //TODO
    //activate field for typping phone number
    //[self presentViewController:contactVC animated:YES completion:nil];

    if (phoneNumberTextField.hidden) {
        phoneNumberTextField.hidden = NO;
        phoneNumberTextField.alpha = 1;
        [phoneNumberTextField becomeFirstResponder];
        phoneNumberTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    }else{
        phoneNumberTextField.hidden = YES;
        phoneNumberTextField.alpha = 0;
        [phoneNumberTextField resignFirstResponder];
    }
    contactVCView.hidden = phoneNumberTextField.hidden;
}

- (void)selectQueBtnClicked{
    NSLog(@"selectQueBtnClicked");
    if (isInCue.integerValue == 0) {
        isInCue = [NSNumber numberWithInt:1];
        [selectQueBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    }else{
        isInCue = [NSNumber numberWithInt:0];;
        [selectQueBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
}
-(void)checkIsInQue {
    if (isInCue.integerValue != 0) {
        [selectQueBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    }
}

- (void)didSelectContact:(CNContact *)contact{
    [self setContactId:contact.identifier];
    [self phoneButtonClicked];
    [titleTimer invalidate];
}

- (IBAction)microphoneButtonClicked:(id)sender {
    [sender setSelected:![sender isSelected]];
    CGFloat lineW = kDefaultLineWidth;
    _drowingView.lineWidth = lineW;
    [rubberButton setSelected:NO];
    [textButton setSelected:NO];
    [rubberButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [textButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    if ([sender isSelected]) {
        _drowingView.drawTool = DrawingToolTypeText;
        [sender setTintColor:[UIColor redColor]];
    }else{
        _drowingView.drawTool = DrawingToolTypePen;
        [sender setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TEXT_MODE_END" object:nil];
    }
    [(MainTabBarController *)self.tabBarController microphoneTapped];
    [self.view endEditing:YES];
    [_drowingView.textView setUserInteractionEnabled:NO];
}

- (IBAction)textButtonClicked:(UIButton *)sender {
    [_drowingView.textView setUserInteractionEnabled:YES];
    [sender setSelected:![sender isSelected]];
    CGFloat lineW = kDefaultLineWidth;
    _drowingView.lineWidth = lineW;
    [rubberButton setSelected:NO];
    [micraphoneButton setSelected:NO];
    [(MainTabBarController *)self.tabBarController endVoice];
    [rubberButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [micraphoneButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    if ([sender isSelected]) {
        _drowingView.drawTool = DrawingToolTypeText;
        [sender setTintColor:[UIColor redColor]];
    }else{
        _drowingView.drawTool = DrawingToolTypePen;
        [sender setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TEXT_MODE_END" object:nil];
    }
}

- (IBAction)rubberButtonClicked:(UIButton *)sender {
    [sender setSelected:![sender isSelected]];
    [textButton setSelected:NO];
    [textButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    [micraphoneButton setSelected:NO];
    [micraphoneButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    CGFloat lineW = kDefaultLineWidth;
    [(MainTabBarController *)self.tabBarController endVoice];
    if ([sender isSelected]) {
        _drowingView.drawTool = DrawingToolTypeEraser;
        _drowingView.lineWidth = lineW * 7;
        [sender setTintColor:[UIColor redColor]];
    }else{
        _drowingView.drawTool = DrawingToolTypePen;
        _drowingView.lineWidth = lineW;
        [sender setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    }
}

- (void)itemsButtonClicked:(id)sender {
    [self createOrUpdateNote];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MENU_BUTTON_PRESSED" object:nil];
}

- (void)queButtonClicked:(id)sender {
    [self createOrUpdateNote];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PHONE_BUTTON_PRESSED" object:nil];
}

- (void)homeButtonClicked:(id)sender {
    [self createOrUpdateNote];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"HOME_BUTTON_PRESSED" object:nil];

}

- (void)noteButtonClicked:(id)sender{
}

- (void)plussButtonClicked:(UIButton *)sender{
    [self createOrUpdateNote];
    UIPageViewController *pageViewController = self.navigationController.viewControllers[self.navigationController.viewControllers.count - 1];
    DrawViewController *viewController;
    if (IS_IPHONE) {
        viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewControlleriPhone" bundle:nil];
    }else{
        viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewController" bundle:nil];
    }
    [pageViewController setViewControllers:@[viewController] direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
}


- (void)minimizeButtonClicked:(id)sender {
    UIButton *btn = (UIButton *)sender;
    [btn setSelected:!btn.isSelected];
    [self setMinimizeOn:btn.isSelected];
}

- (void)setMinimizeOn:(BOOL)on{
    if (on) {
        [minimizeButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        opened = [NSNumber numberWithInteger:[[NSString stringWithFormat:@"%i%@",1,[NSDate dateValue:[NSDate new]]] integerValue]];
    }else{
        opened = [NSNumber numberWithInt:0];
        [minimizeButton setTitleColor:[DrawingViewManager sharedInstance].buttonsColor forState:UIControlStateNormal];
    }
}

- (void)starButtonClicked:(UIButton *)sender{
    int count;
    [starView setStarCount:[starCount integerValue] - 1];
    count = (int)[starCount integerValue];
    if (count == 0) {
        starCount = [NSNumber numberWithInt:1];
        [starButton setTintColor:[UIColor yellowColor]];
    }else{
        starCount = [NSNumber numberWithInt:0];;
        [starButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    }
    starView.x = starButton.x + starButton.superview.x + 40;
}

- (void)pinButtonClicked:(UIButton *)sender{
    int count;
    count = (int)[pinCount integerValue];
    
    if (count == 0) {
        pinCount = [NSNumber numberWithInt:1];
        //Armen
//        pinCount = [NSNumber numberWithInteger:[[NSString stringWithFormat:@"%i%@",1,[NSDate dateValue:[NSDate new]]] integerValue]];
        [pinButton setTintColor:[UIColor yellowColor]];
    }else{
        pinCount = [NSNumber numberWithInt:0];
        [pinButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    }
}

- (void)cameraButtonClicked{
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:nil      //  Must be "nil", otherwise a blank title area will appear above our two buttons
                                message:nil
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* button0 = [UIAlertAction
                              actionWithTitle:@"Cancel"
                              style:UIAlertActionStyleCancel
                              handler:^(UIAlertAction * action)
                              {
                              }];
    
    UIAlertAction* button1 = [UIAlertAction
                              actionWithTitle:@"Take photo"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  //  The user tapped on "Take a photo"
                                  UIImagePickerController *imagePickerController= [[UIImagePickerController alloc] init];
                                  imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                                  imagePickerController.delegate = self;
                                  [self presentViewController:imagePickerController animated:YES completion:^{}];
                              }];
    
    UIAlertAction* button2 = [UIAlertAction
                              actionWithTitle:@"Choose Existing"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  //  The user tapped on "Choose existing"
                                  UIImagePickerController *imagePickerController= [[UIImagePickerController alloc] init];
                                  imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                  imagePickerController.delegate = self;
                                  [self presentViewController:imagePickerController animated:YES completion:^{}];
                              }];
    
    [alert addAction:button0];
    [alert addAction:button1];
    [alert addAction:button2];
    alert.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:alert animated:YES completion:nil];
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller traitCollection:(UITraitCollection *)traitCollection {
    // This method is called in iOS 8.3 or later regardless of trait collection, in which case use the original presentation style (UIModalPresentationNone signals no adaptation)
    return UIModalPresentationNone;
}

- (void)searchButtonClicked:(UIButton *)sender{
    SearchViewController *search = [SearchViewController sharedInstance];
    [self addChildViewController:search];
    search.view.frame = self.view.bounds;
    [self.view addSubview:search.view];
    [search didMoveToParentViewController:self];
    [(MainTabBarController *)self.tabBarController hideSliderButtons:NO];
//    [self callTitleMenuPopover];
}

- (void)didCloseSearch{
    if (((MainTabBarController *)self.tabBarController).selectedIndex == 0) {
        [(MainTabBarController *)self.tabBarController hideSliderButtons:YES];
    }
}

- (void)penOrFingerButtonPressed{
    if ([[DrawingViewManager sharedInstance] isPen]) {
        [[DrawingViewManager sharedInstance] setIsPen:NO];
        [penOrFinger setImage:[[UIImage imageNamed:@"finger"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        scrollView.panGestureRecognizer.minimumNumberOfTouches = 2;
    }else{
        [[DrawingViewManager sharedInstance] setIsPen:YES];
        [penOrFinger setImage:[[UIImage imageNamed:@"pen"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        scrollView.panGestureRecognizer.minimumNumberOfTouches = 1;
    }
}

-(void)setisPen{
     [[DrawingViewManager sharedInstance] setIsPen:NO];
    [penOrFinger setImage:[[UIImage imageNamed:@"finger"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
}

- (void)safeButtonPressed{
    [safeButton setSelected:!safeButton.isSelected];
}

// HOLD
- (void)linesButtonClicked:(UIButton *)sender{
}

- (IBAction)clearButtonDidPress:(id)sender {
    [LKAlertView showAlertWithTitle:@""
                            message:@"Do you want to clear this note?"
                  cancelButtonTitle:@"No"
                  otherButtonTitles:@[@"Yes"]
                         completion:^(NSUInteger selectedOtherButtonIndex) {
                             if (selectedOtherButtonIndex == 0) {
                                 if (self.note && [Note getNoteByObject_id:self.note.object_id]) {
                                     [Note removeObject:self.note];
                                     UIPageViewController *pageViewController = self.navigationController.viewControllers[self.navigationController.viewControllers.count - 1];
                                     DrawViewController *viewController;
                                     if (IS_IPHONE) {
                                         viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewControlleriPhone" bundle:nil];
                                     }else{
                                         viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewController" bundle:nil];
                                     }
                                     [[DrawingViewManager sharedInstance] setPrevTitle:@""];
                                     [pageViewController setViewControllers:@[viewController] direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
                                     [self clearTextField];
                                     
                                 }else{
                                     [_drowingView clear];
                                      [self clearTextField];
                                     scrollView.contentOffsetY = 0;
                                     scrollView.contentOffsetX = 0;
                                 }
                             }
                         }];
}

#pragma mark - Text Field

- (void)createCtegoryTextField{
    [categoryTextField setUserInteractionEnabled:YES];
    [categoryTextField setReturnKeyType:UIReturnKeyDone];
    [categoryTextField setTextAlignment:NSTextAlignmentCenter];
    [categoryTextField setTextColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:1]];
    [categoryTextField setTintColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:1]];
    [categoryTextField setFont:[UIFont fontWithName:@"Helvetica-Bold" size:25]];
    UIButton *myButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 30, 35)];
    [myButton setImage:[UIImage imageNamed:@"fill_X_icon"] forState:UIControlStateNormal];
    [myButton setImage:[UIImage imageNamed:@"fill_X_icon"] forState:UIControlStateHighlighted];
    myButton.imageEdgeInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    [myButton addTarget:self action:@selector(clearCategory) forControlEvents:UIControlEventTouchUpInside];
    categoryTextField.rightView = myButton;
    categoryTextField.rightViewMode = UITextFieldViewModeAlways;
    titletextField.delegate = self;
    categoryTextField.layer.borderWidth = 0.5;
    categoryTextField.layer.borderColor = [UIColor colorWithWhite:1 alpha:1].CGColor;
    if ([categoryTextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor colorWithRed:0 green:1 blue:0 alpha:1];
        categoryTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Title" attributes:@{NSForegroundColorAttributeName: color}];
    }
    [categoryTextField setHidden:NO];
    categoryTextField.tag = 100;
}

- (void)createTitleButton{
    [titletextField setUserInteractionEnabled:YES];
    [titletextField setReturnKeyType:UIReturnKeyDone];
    [titletextField setTextAlignment:NSTextAlignmentCenter];
    [titletextField setTextColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:1]];
    [titletextField setTintColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:1]];
    [titletextField setFont:[UIFont fontWithName:@"Helvetica-Bold" size:25]];
    UIButton *myButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 30, 35)];
    [myButton setImage:[UIImage imageNamed:@"fill_X_icon"] forState:UIControlStateNormal];
    [myButton setImage:[UIImage imageNamed:@"fill_X_icon"] forState:UIControlStateHighlighted];
    myButton.imageEdgeInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    [myButton addTarget:self action:@selector(clearTextField) forControlEvents:UIControlEventTouchUpInside];
    titletextField.rightView = myButton;
    titletextField.rightViewMode = UITextFieldViewModeAlways;
    titletextField.delegate = self;
    titletextField.layer.borderWidth = 0.5;
    titletextField.layer.borderColor = [UIColor colorWithWhite:1 alpha:1].CGColor;
    if ([titletextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor colorWithRed:0 green:1 blue:0 alpha:1];
        titletextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Title" attributes:@{NSForegroundColorAttributeName: color}];
    }
    [titletextField setHidden:NO];
    titletextField.tag = 99;
    UITapGestureRecognizer *titleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callTitleMenuPopover)];
    titleTap.numberOfTapsRequired = 2;
    [titletextField addGestureRecognizer:titleTap];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if ([textField isEqual:phoneNumberTextField]) {
        [self.view endEditing:YES];
    }else{
        [textField setPlaceholder:@"Title"];
        [titleTimer invalidate];
        if ([titletextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
            UIColor *color = [UIColor colorWithRed:0 green:1 blue:0 alpha:1];
            titletextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Title" attributes:@{NSForegroundColorAttributeName: color}];
        }
        if (![textField.text isEqualToString:@""]) {
            textField.alpha = 1;
        }else{
            titleTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(toggleLabelAlpha) userInfo:nil repeats:YES];
        }
        [self setTitleTextFieldText:textField.text];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if ([textField isEqual:phoneNumberTextField]) {
        
    }else{
        [titleTimer invalidate];
        textField.alpha = 1;
        [textField setPlaceholder:@""];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField isEqual:phoneNumberTextField]) {

    }else{
        if ([string isEqualToString:@""]) {
            if ([textField.text length] > 0) {
                [[DrawingViewManager sharedInstance] setPrevTitle:[textField.text substringToIndex:[textField.text length] - 1]];
            }
        }else{
            [[DrawingViewManager sharedInstance] setPrevTitle:[textField.text stringByAppendingString:string]];
        }
    }
    return YES;
}

- (void)clearCategory{
    [self setCategoryName:@""];
    [[DrawingViewManager sharedInstance] setPrevCategory:@""];
}

- (void)clearTextField{
    [self setTitleTextFieldText:@""];
    [self textFieldDidEndEditing:titletextField];
    [self textFieldShouldReturn:titletextField];
    [[DrawingViewManager sharedInstance] setPrevTitle:@""];
}

#pragma mark Popover

- (void)hidePopover{
    [UIView animateWithDuration:0.3 animations:^{
        [colorPopoverView setAlpha:0];
        [backgroundColorPopoverView setAlpha:0];
    } completion:^(BOOL finished) {
        [backgroundColorPopoverView removeFromSuperview];
        [colorPopoverView removeFromSuperview];
        colorPopoverView = nil;
        backgroundColorPopoverView = nil;
    }];
}

- (void)changeBackgroundColorButtonPressed{
    if (!backgroundColorPopoverView) {
        [colorPopoverView removeFromSuperview];
        colorPopoverView = nil;
        UIView *poligonView = [[UIView alloc] initWithFrame:CGRectMake(130, -10, 20, 20)];
        backgroundColorPopoverView = [[UIView alloc] initWithFrame:CGRectMake(colorButton.superview.x + colorButton.x - 120, 60, 250, 170)];
        backgroundColorPopoverView.centerX = SCREEN_WIDTH/2;
        [poligonView setHidden:YES];
        ColorCollectionViewController *bgColorCollectionViewController = [[ColorCollectionViewController alloc]initWithNibName:@"ColorCollectionViewController" bundle:nil];
        bgColorCollectionViewController.delegate = self;
        bgColorCollectionViewController.collectionView.tag = 0;
        bgColorCollectionViewController.currentColor = _backgroundViewForColor.backgroundColor;
        [[DrawingViewManager sharedInstance] setBackgroundColor:_backgroundViewForColor.backgroundColor];
        bgColorCollectionViewController.colorsArray = [APPDELEGATE colorsArray];
        [bgColorCollectionViewController.collectionView setFrame:CGRectMake(5, 25, backgroundColorPopoverView.width - 10 > bgColorCollectionViewController.colorsArray.count * 44?bgColorCollectionViewController.colorsArray.count * 44: backgroundColorPopoverView.width - 10, 50)];
        bgColorCollectionViewController.collectionView.centerX = backgroundColorPopoverView.width/2;
        [backgroundColorPopoverView addSubview:bgColorCollectionViewController.collectionView];
        [self addChildViewController:bgColorCollectionViewController];
        ColorCollectionViewController *lineColorCollectionViewController = [[ColorCollectionViewController alloc]initWithNibName:@"ColorCollectionViewController" bundle:nil];
        lineColorCollectionViewController.delegate = self;
        lineColorCollectionViewController.collectionView.tag = 1;
        lineColorCollectionViewController.colorsArray = lineColorsArrray;
        lineColorCollectionViewController.currentColor = _drowingView.lineColor;
        [lineColorCollectionViewController.collectionView setFrame:CGRectMake(5, 110, backgroundColorPopoverView.width - 10, 50)];
        [backgroundColorPopoverView addSubview:lineColorCollectionViewController.collectionView];
        [self addChildViewController:lineColorCollectionViewController];
        backgroundColorPopoverView.alpha = 0.1;
        backgroundColorPopoverView.backgroundColor = [UIColor whiteColor];
        backgroundColorPopoverView.layer.cornerRadius = 5;
        backgroundColorPopoverView.layer.masksToBounds = YES;
        backgroundColorPopoverView.clipsToBounds = NO;
        [backgroundColorPopoverView addShadowToView];
        [poligonView addShadowToView];
        [poligonView setBackgroundColor:[UIColor whiteColor]];
        poligonView.transform = CGAffineTransformMakeRotation(M_PI_4);
        [backgroundColorPopoverView addSubview:poligonView];
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(20, 0, backgroundColorPopoverView.width - 40, 20)];
        [view setBackgroundColor:backgroundColorPopoverView.backgroundColor];
        [backgroundColorPopoverView addSubview:view];
        UILabel *bgColorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, backgroundColorPopoverView.width, 20)];
        [bgColorLabel setTextAlignment:NSTextAlignmentCenter];
        [bgColorLabel setText:@"Background Color"];
        [backgroundColorPopoverView addSubview:bgColorLabel];
        UILabel *lineColorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 90, backgroundColorPopoverView.width, 20)];
        [lineColorLabel setTextAlignment:NSTextAlignmentCenter];
        [lineColorLabel setText:@"Line Color"];
        [backgroundColorPopoverView addSubview:lineColorLabel];
        [self.view addSubview:backgroundColorPopoverView];
        [UIView animateWithDuration:0.3 animations:^{
            [backgroundColorPopoverView setAlpha:1.0];
        } completion:^(BOOL finished) {}];
    }else{
        [self hidePopover];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectObjectAtIndex:(NSInteger)index{
    if (collectionView.tag == 0) {
        [self didSelectBgColor:index];
    }else{
        [self didSelectLineColorColor:index];
    }
}

- (void)didSelectBgColor:(NSInteger)index{
    [self colorPickerDidSelectObjectAtindex:(int)index];
}

- (void)didSelectLineColorColor:(NSInteger)index{
    [DrawingViewManager sharedInstance].lineColor = colorView.backgroundColor = _drowingView.lineColor = lineColorsArrray[index];
}

- (void)colorPickerDidSelectObjectAtindex:(int)index{
    [DrawingViewManager sharedInstance].bgColorIndex = colorIndex = index;
    [_backgroundViewForColor setBackgroundColor:[APPDELEGATE colorsArray][index]];
    [[DrawingViewManager sharedInstance] setBackgroundColor:[APPDELEGATE colorsArray][index]];
    if (index == 3) {
        [[DrawingViewManager sharedInstance] setButtonsColor:[UIColor whiteColor]];
    }else{
        [[DrawingViewManager sharedInstance] setButtonsColor:[UIColor blackColor]];
    }
    [self setButtons:allbuttonsArray wilthColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    const CGFloat * __nullable components = CGColorGetComponents([[DrawingViewManager sharedInstance] lineColor].CGColor);
    if (index == 3 && [_drowingView.lineColor isEqual:[UIColor colorWithRed:0 green:0 blue:0 alpha:1]]) {
        [DrawingViewManager sharedInstance].lineColor = colorView.backgroundColor = _drowingView.lineColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    }else if (index == 4 && [_drowingView.lineColor isEqual:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]]) {
        [DrawingViewManager sharedInstance].lineColor = colorView.backgroundColor = _drowingView.lineColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isDescendantOfView:backgroundColorPopoverView]) {
        return NO;
    }
    return YES;
}

- (void)starCountDidChange:(NSInteger)count{
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    //[scrollView setContentSize:CGSizeMake(SCREEN_WIDTH, _drowingView.height)];
    return _drowingView.superview;
}


#pragma mark Notifications

- (void)plussButtonClickedFromAddressBook:(NSNotification *)notif{
    [self createNewNoteFromList];
}

#pragma mark ImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage *photoTaken = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    [[DrawingViewManager sharedInstance] setTakenPhoto:UIImageJPEGRepresentation(photoTaken, 0.7)];
    [self setBackgroundImage:photoTaken];
    if (_drowingView.lastPoint.y == 0) {
        _drowingView.lastPoint = CGPointMake(0, 1);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setBackgroundImage:(UIImage *)image{
    if (image) {
        //_backgroundImageView.height = image.size.height / (image.size.width / SCREEN_WIDTH);
        //_backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
        _backgroundImageView.y = 0;
        _backgroundImageView.x = 0;
        [_backgroundImageView setImage:image];
    }else{
        [_backgroundImageView setImage:image];
    }

}

- (void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    keyboardHeight = kbSize.height;
    viewForTitleTableView.height = SCREEN_HEIGHT - viewForTitleTableView.y - keyboardHeight;
    titleTableViewController.view.frame = viewForTitleTableView.bounds;
    contactVCView.height = SCREEN_HEIGHT - contactVCView.y - keyboardHeight;
}

- (void)keyboardWillHide:(NSNotification*)notification {
    keyboardHeight = 0;
    contactVCView.height = SCREEN_HEIGHT - contactVCView.y - keyboardHeight;
}

- (void)homeButtonPressed {
    [self createOrUpdateNote];
    [(MainTabBarController *)self.tabBarController homeButtonPressed];
}

- (void)setDate:(NSDate *)dateLocale{
    date = dateLocale;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"M/d/yy H:mm"];
    [dateLabel setText:[dateFormat stringFromDate:dateLocale]];
}

- (void)setLineColor{
    [DrawingViewManager sharedInstance].lineColor = _drowingView.lineColor = [UIColor getColorFromString:_note.line_color_index];
}

- (void) orientationChanged:(NSNotification *)note{
    UIDevice * device = note.object;
    if (device.orientation != lastOrientation) {
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(pushViewControllerAgainForOrientation) userInfo:nil repeats:NO];
        lastOrientation = device.orientation;
    }
}

- (void)pushViewControllerAgainForOrientation{
  // [scrollView setContentSize:CGSizeMake(SCREEN_WIDTH , _drowingView.height )];
    CGFloat minScale = SCREEN_WIDTH/_drowingView.image.size.width;
    if (minScale == 0 || minScale > 1) {
        minScale = 1;
    }
    if (self.drowingView.image != nil) {
         NSData *data = UIImagePNGRepresentation(self.drowingView.image);
         [self loadImageData:data];
    
    }
    //scrollView.minimumZoomScale = minScale;
   // scrollView.zoomScale = minScale;
    drawingViewWidthC.constant =  SCREEN_WIDTH;// / minScale;pakac

    titleMenuPopover.centerX = SCREEN_WIDTH/2;
    scrollView.contentOffsetY = 0;
    [_drowingView setNeedsDisplay];
    [scrollView setNeedsDisplay];
    [self.view layoutIfNeeded];
    titleMenuPopover.frame = CGRectMake(SCREEN_WIDTH/4,50,250, titleMenuItems.count * 40  > SCREEN_HEIGHT - 100 ? SCREEN_HEIGHT - 100 : titleMenuItems.count * 40);
    backgroundColorPopoverView.centerX = self.view.centerX;

}

- (void)loadImageData:(NSData *)imageData{
    if (!imageData) {
        [_drowingView loadImage:[UIImage new]];
    }else{
        UIImage *image = [UIImage imageWithData:imageData];
        image = [UIImage imageWithImage:image convertToSize:CGSizeMake(SCREEN_WIDTH , SCREEN_WIDTH * image.size.height/image.size.width)];
//        if (image.size.width > 1500) {
//            image = [UIImage imageWithImage:image convertToSize:CGSizeMake(image.size.width/2, image.size.height/2)];
//        }
        CGFloat minScale = SCREEN_WIDTH/image.size.width;
        if (minScale == 0 || minScale > 1) {
            minScale = 1;
        }
        scrollView.minimumZoomScale = minScale;
        [scrollView setContentSize:CGSizeMake(SCREEN_WIDTH, _drowingView.height)];
        drawingViewWidthC.constant = SCREEN_WIDTH / minScale;
        [self.view layoutIfNeeded];
//        scrollView.zoomScale = minScale;
        [_drowingView loadImage:image];
    }
}

- (void)setTitleTextFieldText:(NSString *)title{
    titletextField.text = title;
    if ([title isEqualToString:@""]) {
        [searchButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    }else{
        [searchButton setTintColor:[UIColor redColor]];
        
    }
}

- (void)setCategoryName:(NSString *)categoryName{
    categoryTextField.text = categoryName;
    if ([categoryName isEqualToString:@""]) {
        
    }
}

- (void)centerContent:(UIScrollView *)scrollView
{
    CGFloat top = 0, left = 0;
    if (scrollView.contentSize.width < scrollView.bounds.size.width) {
        left = (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5f;
    }
    if (scrollView.contentSize.height < scrollView.bounds.size.height) {
        top = (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5f;
    }
    scrollView.contentInset = UIEdgeInsetsMake(top, left, top, left);
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    //[self centerContent:scrollView];
}

@end

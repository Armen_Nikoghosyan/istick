//
//  AlarmViewController.m
//  iStick
//
//  Created by Levon Kirakosyan on 4/10/17.
//  Copyright © 2017 Levon. All rights reserved.
//

#import "AlarmViewController.h"

@interface AlarmViewController (){
    __weak IBOutlet UILabel *timeLabel;
    __weak IBOutlet UIDatePicker *datepicker;
}

@end

@implementation AlarmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    datepicker.datePickerMode = UIDatePickerModeDateAndTime;
    datepicker.hidden = NO;
    datepicker.date = [NSDate date];
    [datepicker addTarget:self action:@selector(labelChange:) forControlEvents:UIControlEventValueChanged];
    [self labelChange:datepicker];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)selectButtonPressed:(id)sender {
    [self close:nil];
    UILocalNotification *notification = [[UILocalNotification alloc]init];
    //notification.repeatInterval = NSDayCalendarUnit;
    [notification setAlertBody:@"Reminder"];
    [notification setFireDate:datepicker.date];
    [notification setTimeZone:[NSTimeZone  defaultTimeZone]];
    notification.soundName = UILocalNotificationDefaultSoundName;
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyyMMddhhmmss"];
    NSString *strdate = [formatter stringFromDate:_date];
    NSMutableArray *scheduledObjects = [[NSUserDefaults standardUserDefaults] objectForKey:@"scheduled_objects_object_id"];
    if (!scheduledObjects || scheduledObjects.count == 0) {
        scheduledObjects = [[NSMutableArray alloc] init];
    }else{
        scheduledObjects = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"scheduled_objects_object_id"]];
    }
    [scheduledObjects addObject:strdate];
    [[NSUserDefaults standardUserDefaults] setObject:scheduledObjects forKey:@"scheduled_objects_object_id"];
    NSString * str = [self.objectId isEqualToString:@"0"] ? strdate : self.objectId ;
    notification.userInfo = [NSDictionary dictionaryWithObject:str forKey:@"object_id"];
    //notification.applicationIconBadgeNumber++;
    NSMutableArray *oldNotificationsArray = [[[UIApplication sharedApplication] scheduledLocalNotifications] mutableCopy];
    if (oldNotificationsArray && oldNotificationsArray.count > 0) {
        [oldNotificationsArray addObject:notification];
        [[UIApplication sharedApplication] setScheduledLocalNotifications:oldNotificationsArray];
        
    }else{
        [[UIApplication sharedApplication] setScheduledLocalNotifications:[NSArray arrayWithObject:notification]];
    }
    notification.applicationIconBadgeNumber = [[[UIApplication sharedApplication] scheduledLocalNotifications] count] + 1;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LOCALE_NOTIFICATION_STATE_CHANGED" object:datepicker.date];
}

- (void)labelChange:(UIDatePicker *)sender{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"M/d/yy H:mm"];
    NSString *stringFromDate1 = [formatter stringFromDate:sender.date];
    
    [timeLabel setText:stringFromDate1];
}
@end

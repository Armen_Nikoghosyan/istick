#import "BottomImageSliderViewController.h"
#import "ImageSliderCollectionViewCell.h"
#import "DrawingViewManager.h"

@interface BottomImageSliderViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>{
    __weak IBOutlet UICollectionView *mainCollectionView;
    NSMutableArray *notesArray;
    float height;
    NSInteger anIndex;
    CGFloat rightMarge;
}

@end

@implementation BottomImageSliderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    rightMarge = 5;
    [mainCollectionView registerClass:[ImageSliderCollectionViewCell class] forCellWithReuseIdentifier:@"ImageSliderCollectionViewCell"];
    UINib *nib = [UINib nibWithNibName:@"ImageSliderCollectionViewCell" bundle:nil];
    [mainCollectionView registerNib:nib forCellWithReuseIdentifier:@"ImageSliderCollectionViewCell"];
    mainCollectionView.delegate = self;
    mainCollectionView.dataSource = self;
    height = SCREEN_MIN_LENGTH/3;
    anIndex = -1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureWithNotes:(NSArray *)notes withObjetcId:(NSString *)objectId marge:(CGFloat)marge{
    rightMarge = marge;
    notesArray=[[[notes reverseObjectEnumerator] allObjects] mutableCopy];
    NoteMenagedObject *selectedNote = [Note getNoteByObject_id:objectId];
    if (selectedNote) {
        anIndex = [notesArray indexOfObject:selectedNote];
        int currentNumber = -1;
        if(NSNotFound != anIndex) {
            currentNumber = (int)notesArray.count - (int)anIndex;
            [mainCollectionView setContentSize:CGSizeMake(height * notesArray.count, 0)];
            [mainCollectionView setContentOffset:CGPointMake(height * anIndex, mainCollectionView.contentOffset.y) animated:NO];
        }
    }
    [mainCollectionView reloadData];
}

#pragma mark - Collection View

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageSliderCollectionViewCell *cell = (ImageSliderCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ImageSliderCollectionViewCell" forIndexPath:indexPath];
    NoteMenagedObject *note = notesArray[indexPath.row];
    [cell configureCellWithNote:note withSelectedIndex:-1 withButton:NO];
    if (anIndex == indexPath.row) {
        [cell.selectionView setHidden:NO];
    }else{
        [cell.selectionView setHidden:YES];
    }
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return notesArray.count;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, rightMarge, 0, 5);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [[DrawingViewManager sharedInstance] setShowingNote:notesArray[indexPath.row]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_SELECT_NOTE" object:nil];
    anIndex = indexPath.row;
    [collectionView reloadData];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(height - 10, SCREEN_MAX_LENGTH/3.5);
    
}

@end

#import "ImageSliderCollectionViewCell.h"
#import "AppDelegate.h"
#import "UIImage+Resize.h"

@implementation ImageSliderCollectionViewCell{
    __weak IBOutlet UILabel *SelectedNumberLabel;
    __weak IBOutlet UIImageView *imageView;
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UILabel *dateLabel;
    UIImageView *backgroundImageView;
    UIView *viewForColor;
    __weak IBOutlet UIView *bgView;
    __weak IBOutlet UIButton *pinButton;
    NoteMenagedObject *currentNote;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    pinButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [pinButton setImage:[pinButton.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]forState:UIControlStateNormal];
    pinButton.tintColor = [UIColor yellowColor];
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    backgroundImageView = [[UIImageView alloc] initWithFrame:self.contentView.frame];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
    viewForColor = [[UIView alloc] initWithFrame:imageView.frame];
    [self.contentView addSubview:viewForColor];
    [self.contentView addSubview:backgroundImageView];
    [self.contentView bringSubviewToFront:imageView];
    if (SCREEN_MIN_LENGTH < 500) {
        [dateLabel setHidden:YES];
    }else{
        [dateLabel setHidden:NO];
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    viewForColor.frame = imageView.frame;
    backgroundImageView.width = imageView.width;
    backgroundImageView.y = imageView.y;
    backgroundImageView.top = dateLabel.bottom;
}

- (void)configureCellWithNote:(NoteMenagedObject *)note withSelectedIndex:(int)selectedNumber withButton:(BOOL)withButton{
    currentNote = note;
    //pinButton.hidden = !withButton;
    UIImage *cacheImage = [UIImage imageWithData:note.image];
    cacheImage = [UIImage imageWithImage:cacheImage convertToSize:CGSizeMake((SCREEN_MIN_LENGTH)/3, cacheImage.size.height/ (cacheImage.size.width/((SCREEN_MIN_LENGTH)/3)))];
    [imageView setImage:cacheImage];
    [viewForColor setBackgroundColor:[APPDELEGATE colorsArray][[note.color_index integerValue]]];
    [bgView setBackgroundColor:[APPDELEGATE colorsArray][[note.color_index integerValue]]];
    [titleLabel setText:note.title];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"M/d/YY H:mm"];
    NSString *stringDate = [formatter stringFromDate:note.created_at];
    [dateLabel setText:stringDate];
    if (selectedNumber < 0) {
        [SelectedNumberLabel setHidden:YES];
    }else{
        [SelectedNumberLabel setText:[NSString stringWithFormat:@"%d",selectedNumber]];
        [SelectedNumberLabel setHidden:NO];
    }
    const CGFloat *_components = CGColorGetComponents(((UIColor *)[APPDELEGATE colorsArray][[note.color_index intValue]]).CGColor);
    CGFloat red     = _components[0];
    CGFloat green = _components[1];
    CGFloat blue   = _components[2];
    if (red == 0 && green == 0 && blue == 0) {
        [titleLabel setTextColor:[UIColor redColor]];
        [dateLabel setTextColor:[UIColor whiteColor]];
        [SelectedNumberLabel setTextColor:[UIColor whiteColor]];
        [dateLabel setBackgroundColor:[UIColor blackColor]];
        [titleLabel setBackgroundColor:[UIColor whiteColor]];

    }else{
        [titleLabel setTextColor:[UIColor redColor]];
        [dateLabel setTextColor:[UIColor blackColor]];
        [SelectedNumberLabel setTextColor:[UIColor blackColor]];
        [dateLabel setBackgroundColor:[APPDELEGATE colorsArray][[note.color_index intValue]]];
        [titleLabel setBackgroundColor:[UIColor blackColor]];

    }
    if(note.backgroundImage) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            UIImage *backgroundImage = [UIImage imageWithData:note.backgroundImage];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (backgroundImage) {
                    backgroundImageView.image = backgroundImage;
                    backgroundImageView.height = SCREEN_MIN_LENGTH/4 - titleLabel.height - dateLabel.height;
                    backgroundImageView.top = dateLabel.bottom;
                }else{
                    backgroundImageView.image = nil;
                }
            });
        });
    }else{
        backgroundImageView.image = nil;
    }
    [viewForColor setFrame:imageView.frame];
    [self.contentView bringSubviewToFront:dateLabel];
    [self.contentView bringSubviewToFront:titleLabel];
    [self.contentView bringSubviewToFront:pinButton];
}

- (IBAction)pinButtonclicked:(id)sender {
    [Note chnagePinCountInNote:currentNote count:0];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PIN_COUNT_DID_CHANGE" object:nil];
}

@end

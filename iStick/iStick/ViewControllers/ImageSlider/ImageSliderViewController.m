#import "ImageSliderViewController.h"
#import "NoteMenagedObject.h"
#import "AppDelegate.h"
#import "DrawingViewManager.h"
#import "ImageSliderCollectionViewCell.h"
#import "Note.h"

@interface ImageSliderViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate,UICollectionViewDelegateFlowLayout>{
    
    __weak IBOutlet UICollectionView *topCollectionView;
    __weak IBOutlet UICollectionView *mainCollectionView;
    __weak IBOutlet UIScrollView *mainScrollView;
    NSMutableArray *selectedArray;
    NSMutableArray *notesArray;
    BOOL canScroll;
    float height;
    __weak IBOutlet UILabel *infoLabel;
    int selectedIndex;
    NSString *currentObjectId;
}

@end

@implementation ImageSliderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedIndex = -1;
    mainScrollView.delegate = self;
    height = SCREEN_MIN_LENGTH/4;
    selectedArray = [[NSMutableArray alloc] init];
    [mainCollectionView registerClass:[ImageSliderCollectionViewCell class] forCellWithReuseIdentifier:@"ImageSliderCollectionViewCell"];
    [topCollectionView registerClass:[ImageSliderCollectionViewCell class] forCellWithReuseIdentifier:@"ImageSliderCollectionViewCell"];
    UINib *nib = [UINib nibWithNibName:@"ImageSliderCollectionViewCell" bundle:nil];
    [mainCollectionView registerNib:nib forCellWithReuseIdentifier:@"ImageSliderCollectionViewCell"];
    [topCollectionView registerNib:nib forCellWithReuseIdentifier:@"ImageSliderCollectionViewCell"];
    mainCollectionView.tag = 1;
    canScroll = YES;
    mainCollectionView.clipsToBounds = NO;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureWithNotes:(NSArray *)notes withObjetcId:(NSString *)objectId{
    notesArray=[[[notes reverseObjectEnumerator] allObjects] mutableCopy];
    currentObjectId = objectId;
    NoteMenagedObject *selectedNote = [Note getNoteByObject_id:objectId];
    if (selectedNote) {
        [selectedArray insertObject:[Note getNoteByObject_id:objectId] atIndex:0];
        NSInteger anIndex = [notesArray indexOfObject:selectedNote];
        int currentNumber = -1;
        if(NSNotFound != anIndex) {
            currentNumber = (int)notesArray.count - (int)anIndex;
            [mainCollectionView setContentSize:CGSizeMake(height * notesArray.count, 0)];
            [mainCollectionView setContentOffset:CGPointMake(height * anIndex, mainCollectionView.contentOffset.y) animated:NO];
        }
    }
    [mainCollectionView reloadData];
}

- (void)didTap:(UITapGestureRecognizer *)tapGesture{
    [[DrawingViewManager sharedInstance] setShowingNote:notesArray[tapGesture.view.tag]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_SELECT_NOTE" object:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageSliderCollectionViewCell *cell = (ImageSliderCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ImageSliderCollectionViewCell" forIndexPath:indexPath];
    NoteMenagedObject *note;
    if (collectionView.tag == 1) {
        note = notesArray[indexPath.row];
        NSInteger anIndex=[selectedArray indexOfObject:note];
        int currentNumber = -1;
        if(NSNotFound != anIndex) {
            currentNumber = (int)selectedArray.count - (int)anIndex;
        }
        [cell configureCellWithNote:note withSelectedIndex:currentNumber withButton:NO];
    }else{
        note = selectedArray[indexPath.row];
        [cell configureCellWithNote:note withSelectedIndex:-1 withButton:NO];
    }
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (selectedArray.count) {
        [infoLabel setHidden:YES];
    }else{
        [infoLabel setHidden:NO];
    }
    if (collectionView.tag) {
        return notesArray.count;
    }
    return selectedArray.count;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (collectionView.tag == 1) {
        return UIEdgeInsetsMake(10, SCREEN_MIN_LENGTH/3 - 25, 10, SCREEN_MIN_LENGTH/3 - 25);
    }
    return UIEdgeInsetsMake(0, 5, 0, 5);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView.tag == 1) {
        if ([selectedArray containsObject:notesArray[indexPath.row]]) {
            [selectedArray removeObject:notesArray[indexPath.row]];
        }else{
            [selectedArray insertObject:notesArray[indexPath.row] atIndex:0];
        }
        [topCollectionView reloadData];
        [mainCollectionView reloadData];
    }else{
        [[DrawingViewManager sharedInstance] setShowingNote:selectedArray[indexPath.row]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_SELECT_NOTE" object:nil];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView.tag == 1) {
        if (selectedIndex == (int)indexPath.row) {
            return CGSizeMake(height + 200, height + 200);
        }
        return CGSizeMake(height - 10, height - 10);
    }
    return CGSizeMake(SCREEN_MIN_LENGTH - 10, (SCREEN_MAX_LENGTH - height - 10));
 
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{

}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{

}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{

}

- (void)correctCollectionViewPossition:(UIScrollView *)scrollView{
    float pageWidth = height; // width + space
    
    float currentOffset = scrollView.contentOffset.x;
    float targetOffset = currentOffset + 1;
    float newTargetOffset = 0;
    
    if (targetOffset > currentOffset)
        newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth;
    else
        newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth;
    
    if (newTargetOffset < 0)
        newTargetOffset = 0;
    else if (newTargetOffset > scrollView.contentSize.width)
        newTargetOffset = scrollView.contentSize.width;
    [scrollView setContentOffset:CGPointMake(newTargetOffset, scrollView.contentOffset.y) animated:YES];
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{

}

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)animateZoomforCell:(ImageSliderCollectionViewCell*)zoomCell{
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        zoomCell.transform = CGAffineTransformMakeScale(2,2);
    } completion:^(BOOL finished){
    }];
}

@end

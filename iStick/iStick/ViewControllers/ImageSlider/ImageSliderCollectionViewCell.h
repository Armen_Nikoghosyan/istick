#import <UIKit/UIKit.h>
#import "NoteMenagedObject.h"

@interface ImageSliderCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *selectionView;
- (void)configureCellWithNote:(NoteMenagedObject *)note withSelectedIndex:(int)selectedNumber withButton:(BOOL)withButton;
@end

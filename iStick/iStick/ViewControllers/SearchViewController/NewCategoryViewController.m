#import "NewCategoryViewController.h"

@interface NewCategoryViewController ()
@property (weak, nonatomic) IBOutlet UITextField *categoryTextField;

@end

@implementation NewCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_categoryTextField setValue:[UIColor greenColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)donebuttonPressed:(id)sender {
    if (![_categoryTextField.text isEqualToString:@""]) {
        NSArray *categories = [[NSUserDefaults standardUserDefaults] arrayForKey:@"CATEGORIES"];
        if (categories) {
            [[NSUserDefaults standardUserDefaults] setObject:[categories arrayByAddingObject:_categoryTextField.text] forKey:@"CATEGORIES"];
        }else{
            [[NSUserDefaults standardUserDefaults] setObject:@[_categoryTextField.text] forKey:@"CATEGORIES"];
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CREATEDNEWCATEGORY" object:nil];
    }
}

@end

#import "SearchViewController.h"
#import "SearchTableViewCell.h"
#import "SearchTitleTableViewCell.h"
#import "FileManagedObject.h"
#import "Note.h"
#import "BVReorderTableView.h"
#import "NewCategoryViewController.h"
#import "DrawingViewManager.h"
#import "MainTabBarController.h"

@interface SearchViewController ()<UITableViewDelegate,UITableViewDataSource,SearchTitleTableViewCellDelegate,SearchTableViewCellDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoryHeightC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *borderbgView;
@property (weak, nonatomic) IBOutlet UITableView *categoryTableView;
@property (weak, nonatomic) IBOutlet UITableView *titleTableView;
@property (nonatomic, strong) NSArray *categories;
@property (nonatomic, strong) NSArray *titles;
@property (weak, nonatomic) IBOutlet UIButton *queButton;
@property (nonatomic, strong) NSString *changingCategory;
@property (nonatomic, strong) NSString *selectedCategory;
@property (nonatomic) BOOL isTitleInEditMode;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) UIColor *starButtonColor;
@property (strong, nonatomic) UIColor *trashButtonColor;
@property (strong, nonatomic) UIColor *pinButtonColor;


@end

@implementation SearchViewController
@synthesize categoryTableView;
@synthesize titleTableView;
@synthesize categories;
@synthesize titles;
@synthesize selectedCategory;
@synthesize changingCategory;

typedef enum{
    categoryTVTag,
    titleTVTag
} tableViewType;

static SearchViewController *sharedInstance;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    if(sharedInstance) {
        // avoid creating more than one instance
        [NSException raise:@"bug" format:@"tried to create more than one instance"];
    }
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self) {
        // own initialization code here
    }
    return self;
}

+ (SearchViewController *)sharedInstance{
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedInstance = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil];
    });
    return sharedInstance;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    _borderbgView.layer.masksToBounds = NO;
    _borderbgView.layer.borderWidth = 1;
    _borderbgView.layer.borderColor = [UIColor colorWithRed:230/255.0f green:195/255.0f blue:51/255.0f alpha:1].CGColor;
    changingCategory = nil;
    categoryTableView.tag = categoryTVTag;
    titleTableView.tag = titleTVTag;
    
    categories = [Note getAllCategories];
    titles = [Note getAllTitles];
    [categoryTableView registerNib:[UINib nibWithNibName:@"SearchTableViewCell" bundle:nil] forCellReuseIdentifier:@"SearchTableViewCell"];
    [titleTableView registerNib:[UINib nibWithNibName:@"SearchTitleTableViewCell" bundle:nil] forCellReuseIdentifier:@"SearchTitleTableViewCell"];
    categoryTableView.delegate = self;
    categoryTableView.dataSource = self;
    titleTableView.delegate = self;
    titleTableView.dataSource = self;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createdNewCategory) name:@"CREATEDNEWCATEGORY" object:nil];
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateTime) userInfo:nil repeats:YES];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (selectedCategory) {
        titles = [Note getAllTitlesForCategoryName:selectedCategory];
        [titleTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }else{
        titles = [Note getAllTitles];
        [titleTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }
    _starButtonColor = [(MainTabBarController *)self.tabBarController starButtonColor];
    _trashButtonColor = [(MainTabBarController *)self.tabBarController trashButtonColor];
    _pinButtonColor = [(MainTabBarController *)self.tabBarController pinButtonColor];

    [(MainTabBarController *)self.tabBarController setStarButtonColor:[UIColor whiteColor]];
    [(MainTabBarController *)self.tabBarController setPinButtonColor:[UIColor whiteColor]];
    [(MainTabBarController *)self.tabBarController setTrashButtonColor:[UIColor whiteColor]];


    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_SELECT_SEARCH" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (IBAction)close:(id)sender {
    [(MainTabBarController *)self.tabBarController setStarButtonColor:_starButtonColor];
    [(MainTabBarController *)self.tabBarController setPinButtonColor:_pinButtonColor];
    [(MainTabBarController *)self.tabBarController setTrashButtonColor:_trashButtonColor];
    [self close];
}

- (IBAction)queButtonPressed:(id)sender {
    [self close];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PHONE_BUTTON_PRESSED" object:nil];

}

- (IBAction)plussButtonPressed:(id)sender {
    [self close];
    [[DrawingViewManager sharedInstance] setPrevTitle:@""];
    [[DrawingViewManager sharedInstance] setWillCreateNewNote:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_SELECT_TABBAR_ITEM" object:[NSNumber numberWithInt:0]];
}

- (void)close{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_CLOSE_SEARCH" object:nil];
    [self.view removeFromSuperview];
}

- (IBAction)fileCabinetButtonPressed:(id)sender {
    [self close];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MENU_BUTTON_PRESSED" object:nil];
}

- (IBAction)homeButtonPressed:(id)sender {
    [self close];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"HOME_BUTTON_PRESSED" object:nil];

}

- (void)searchDidChange{
    categories = [Note getAllCategories];
    if (selectedCategory) {
        titles = [Note getAllTitlesForCategoryName:selectedCategory];
    }else{
        titles = [Note getAllTitles];
    }
    [titleTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    [categoryTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)createdNewCategory{
    categories = [Note getAllCategories];
    [categoryTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)updateTime{
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    dateFormatter.dateFormat = @"M/d/YY H:mm";
    [_timeLabel setText:[dateFormatter stringFromDate:now]];
}



#pragma mark Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    _mainViewHeightC.constant = MAX(categories.count * 35 + 30, titles.count * 35);
//    [UIView animateWithDuration:0.1
//                     animations:^{
                         [_borderbgView layoutIfNeeded];
//                     }];
    if (tableView.tag == 0) {
        _categoryHeightC.constant = categories.count * 35 + 30;
        [categoryTableView layoutIfNeeded];
        return categories.count;
    }else{
        _titleHeightConstraint.constant = titles.count * 35;
        [titleTableView layoutIfNeeded];
        return titles.count;
    }
}

- (CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 35;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView.tag == categoryTVTag) {
        return 35;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    switch (tableView.tag) {
        case titleTVTag:
            break;
        case categoryTVTag:{
            NewCategoryViewController *newCategoryVC = [[NewCategoryViewController alloc] init];
            [self addChildViewController:newCategoryVC];
            [newCategoryVC.view setFrame:CGRectMake(0, 0, 100, 20)];
            return newCategoryVC.view;
            break;
        }
        default:
            break;
    }
    return [[UIView alloc] init];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (tableView.tag) {
        case categoryTVTag:{
            SearchTableViewCell *cell = (SearchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"SearchTableViewCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            NSString *title = categories[indexPath.row];
            BOOL isInEditMode = (selectedCategory == changingCategory && changingCategory != NSThreadWillExitNotification);
            [cell setEditButtonHidden:![title isEqualToString:selectedCategory] isInEditMode:isInEditMode];
            cell.titleLabel.text = title.uppercaseString;
            cell.titleLabel.textColor = [UIColor whiteColor];
            cell.titleLabel.textColor = [title isEqualToString:selectedCategory]?[UIColor redColor]:[UIColor whiteColor];
            cell.delegate = self;
            return cell;
        }
        case titleTVTag:{
            SearchTitleTableViewCell *cell = (SearchTitleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"SearchTitleTableViewCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            NSString *title = titles[indexPath.row];
            cell.titleLabel.textColor = [UIColor whiteColor];
            cell.titleLabel.text = title.uppercaseString;
            cell.categoryLabel.text = [[Note getCategoryForTitle:title] uppercaseString];
            cell.delegate = self;
            [cell setEditMode:_isTitleInEditMode];
            [cell.checkBox setOn:[Note isTitle:title inCategory:selectedCategory]];
            return cell;
        }
        default:
            break;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (tableView.tag) {
        case categoryTVTag:{
            _isTitleInEditMode = NO;
            changingCategory = nil;
            if (![selectedCategory isEqualToString:categories[indexPath.row]]) {
                [self setSelectedCategory:categories[indexPath.row]];
                titles = [Note getAllTitlesForCategoryName:selectedCategory];
                [titleTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
            }else{
                [self setSelectedCategory:nil];
                titles = [Note getAllTitles];
                [titleTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
            }
            [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case titleTVTag:{
            if (_isTitleInEditMode) {
                SearchTitleTableViewCell *cell = (SearchTitleTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
                [cell.checkBox setOn:!cell.checkBox.on];
                [self SearchTitleTableViewCellChangesCheckBox:cell];
            }else{
                [self close];
                NSMutableArray *array = [@[titles[indexPath.row]] mutableCopy];
                if (selectedCategory) {
                    [array addObject:selectedCategory];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:@"TITLE_ITEM_SELECTED" object:array];
            }
            break;
        }
        default:
            break;
    }
}

- (void)setSelectedCategory:(NSString *)_selectedCategory{
    selectedCategory = _selectedCategory;
    if (selectedCategory) {
        [_titleLabel setText:[selectedCategory uppercaseString]];
    }else{
        [_titleLabel setText:@"ALL"];
    }
}

#pragma mark - CellDelegates

- (void)SearchTitleTableViewCellChangesCheckBox:(UITableViewCell *)cell{
    NSNumber *index = [NSNumber numberWithInteger:[titleTableView indexPathForCell:cell].row];
    SearchTitleTableViewCell *cellLocale = (SearchTitleTableViewCell *)cell;
    if ([cellLocale.checkBox on]) {
        [Note moveAllNotesWithTitle:titles[index.integerValue] tocategory:selectedCategory];
    }else{
        [Note moveAllNotesWithTitle:titles[index.integerValue] tocategory:@""];
    }
}

- (void)editButtonPressed:(BOOL)willEdit{
    _isTitleInEditMode = willEdit;
    if (_isTitleInEditMode) {
        titles = [Note getAllTitles];
        changingCategory = selectedCategory;
    }else{
        titles = [Note getAllTitlesForCategoryName:selectedCategory];
        changingCategory = nil;
        [Note syncData];
    }
    [titleTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}
@end

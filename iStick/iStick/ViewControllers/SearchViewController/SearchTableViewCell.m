#import "SearchTableViewCell.h"

@implementation SearchTableViewCell{
   
    __weak IBOutlet UIButton *editButton;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setBackgroundColor:[UIColor blackColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setEditButtonHidden:(BOOL)hidden isInEditMode:(BOOL)editMode{
    [editButton setHidden:hidden];
    [editButton setTitle:@"Edit" forState:UIControlStateNormal];
    if (editMode) {
        [editButton setTitle:@"Done" forState:UIControlStateNormal];
    }else{
        [editButton setTitle:@"Edit" forState:UIControlStateNormal];
    }
}

- (IBAction)editButtonPressed:(id)sender{
    if ([editButton.titleLabel.text isEqualToString:@"Edit"]) {
        [editButton setTitle:@"Done" forState:UIControlStateNormal];
        [_delegate editButtonPressed:YES];
    }else{
        [editButton setTitle:@"Edit" forState:UIControlStateNormal];
        [_delegate editButtonPressed:NO];
    }
}

@end

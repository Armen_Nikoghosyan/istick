#import "SearchTitleTableViewCell.h"

@implementation SearchTitleTableViewCell{
   
}

- (void)awakeFromNib {
    [super awakeFromNib];
    _moveToButton.layer.masksToBounds = YES;
    _moveToButton.layer.borderColor = [UIColor whiteColor].CGColor;
    _moveToButton.layer.borderWidth = 0.5;
    _moveToButton.layer.cornerRadius = 1.5;
    [self setBackgroundColor:[UIColor blackColor]];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)moveToButtonPressed:(id)sender {
    [_delegate MoveToForCell:self];
}

- (void)setEditMode:(BOOL)isInEditMode{
    [_moveToButton setHidden:!isInEditMode];
    [_checkBox setHidden:!isInEditMode];
}

- (IBAction)checking:(id)sender {
    [_delegate SearchTitleTableViewCellChangesCheckBox:self];
}

@end

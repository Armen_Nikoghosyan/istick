#import <UIKit/UIKit.h>
@protocol SearchTableViewCellDelegate <NSObject>

- (void)MoveToForCell:(UITableViewCell *)cell;
- (void)editButtonPressed:(BOOL)willEdit;


@end
@interface SearchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) id<SearchTableViewCellDelegate> delegate;
- (void)setEditButtonHidden:(BOOL)hidden isInEditMode:(BOOL)editMode;
@end

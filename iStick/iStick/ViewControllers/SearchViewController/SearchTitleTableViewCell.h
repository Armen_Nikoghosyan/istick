#import <UIKit/UIKit.h>
#import "BEMCheckBox.h"

@protocol SearchTitleTableViewCellDelegate <NSObject>

- (void)MoveToForCell:(UITableViewCell *)cell;
- (void)SearchTitleTableViewCellChangesCheckBox:(UITableViewCell *)cell;

@end

@interface SearchTitleTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) id<SearchTitleTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet BEMCheckBox *checkBox;
@property (weak, nonatomic) IBOutlet UIButton *moveToButton;
- (void)setEditMode:(BOOL)isInEditMode;
@end

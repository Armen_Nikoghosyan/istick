#import "HomeViewController.h"
#import "MainTabBarController.h"
#import "DrawingViewManager.h"
#import "BottomImageSliderViewController.h"
#import "Note.h"
#import "FLAnimatedImageView.h"
#import "FLAnimatedImage.h"
#import "MLKMenuPopover.h"
#import "NotesCollectionViewController.h"
#import "SearchViewController.h"
#import <Masonry/Masonry.h>

@interface HomeViewController ()<MLKMenuPopoverDelegate>{
    __weak IBOutlet UIView *navigationView;
    __weak IBOutlet UIButton *manuButton;
    __weak IBOutlet UIButton *plussButton;
    __weak IBOutlet UIView *favoritesView;
    __weak IBOutlet UIButton *searchButton;
    __weak IBOutlet UIButton *homeButton;
    //BottomImageSliderViewController *bootpmImgaeSliderVC;
    NotesCollectionViewController *notesViewController;
    __weak IBOutlet FLAnimatedImageView *addView;
    __weak IBOutlet UILabel *timeLabel;
    __weak IBOutlet UILabel *dateLabel;
    MLKMenuPopover *titleMenuPopover;
    NSArray *titleMenuItems;
   // __weak IBOutlet NSLayoutConstraint *favoriteViewC;
    __weak IBOutlet NSLayoutConstraint *logoHeightC;
    __weak IBOutlet UIImageView *logoView;
    __weak IBOutlet FLAnimatedImageView *addAnimatedImageView;
    __weak IBOutlet UILabel *noFavoriteLabel;
    __weak IBOutlet UIButton *addButton;
    
    UIDeviceOrientation lastOrientation;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    notesViewController.willAllowChangePossition = YES;
    logoHeightC.constant = SCREEN_MIN_LENGTH/10;
    logoView.clipsToBounds = YES;
    [addButton setHidden:YES];
    [logoView layoutIfNeeded];
    //[addAnimatedImageView layoutIfNeeded];
    [favoritesView layoutIfNeeded];
    navigationView.layer.masksToBounds = YES;
    navigationView.layer.borderColor = [UIColor whiteColor].CGColor;
    navigationView.layer.borderWidth = 0;
    notesViewController = [[NotesCollectionViewController alloc] initWithNibName:@"NotesCollectionViewController" bundle:nil];
    [self displayContentController:notesViewController];
    
    [manuButton setImage:[manuButton.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [manuButton setTintColor:[UIColor whiteColor]];
    
    [searchButton setImage:[searchButton.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [searchButton setTintColor:[UIColor whiteColor]];
    
    [homeButton setImage:[homeButton.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [homeButton setTintColor:[UIColor redColor]];
    
    NSURL *imgPath = [[NSBundle mainBundle] URLForResource:@"banner_anim" withExtension:@"gif"];
    NSString*stringPath = [imgPath absoluteString];
    FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:[NSData dataWithContentsOfURL:[NSURL URLWithString:stringPath]]];
   // addView.animatedImage = image;
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateTime) userInfo:nil repeats:YES];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(orientationChanged:)
     name:UIDeviceOrientationDidChangeNotification
     object:[UIDevice currentDevice]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pinsDidChange) name:@"PIN_COUNT_DID_CHANGE" object:nil];


}

- (void)updateTime{
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    dateFormatter.dateFormat = @"M/d/YY H:mm";
    [dateLabel setText:[dateFormatter stringFromDate:now]];
}

- (void)displayContentController:(NotesCollectionViewController*)content{
    [self addChildViewController:content];
    [content didMoveToParentViewController:self];
    content.collectionView.frame = favoritesView.bounds;
    [favoritesView addSubview:content.collectionView];
    [content.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(favoritesView.mas_top);
        make.centerX.equalTo(favoritesView.mas_centerX);
        make.width.equalTo(favoritesView.mas_width);
        make.height.equalTo(favoritesView.mas_height);
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    notesViewController.willAllowChangePossition = YES;
    [self reloadFavorites];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadFavorites) name:@"COLLECTION_DID_CHANGE" object:nil];
    notesViewController.collectionView.contentOffsetY = 0;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMoreNotes) name:@"GETMORENOTES" object:nil];

}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"COLLECTION_DID_CHANGE" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GETMORENOTES" object:nil];
}

- (void)getMoreNotes{
    NSLog(@"WSSSSSssssssSSSSSSSssSSsSSSSSSSSSSSSSSS");
    [NoteHelper sharedInstance].isNewSearch = NO;
    [Note getNotes:SortDescriptortypePossition usingNoteHelperUsingBlock:^(NSArray *notes) {
        if (notes.count) {
            [favoritesView setHidden:NO];
            [noFavoriteLabel setHidden:YES];
        }else{
            [favoritesView setHidden:YES];
            [noFavoriteLabel setHidden:NO];
        }
        [notesViewController setArray:@[notes] willRemove:YES willShowStars:YES];
        [NoteHelper sharedInstance].isNewSearch = YES;
    }];
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    [notesViewController viewWillLayoutSubviews];
}

- (void)reloadFavorites{
    [NoteHelper sharedInstance].title = nil;
    [NoteHelper sharedInstance].fromDate = nil;
    [NoteHelper sharedInstance].toDate = nil;
    [NoteHelper sharedInstance].favorite = nil;
    [NoteHelper sharedInstance].pin = [NSNumber numberWithInt:1];
    [Note getNotes:SortDescriptortypePossition usingNoteHelperUsingBlock:^(NSArray *notes) {
        if (notes.count) {
            [favoritesView setHidden:NO];
            [noFavoriteLabel setHidden:YES];
        }else{
            [favoritesView setHidden:YES];
            [noFavoriteLabel setHidden:NO];
        }
        [notesViewController setArray:@[notes] willRemove:YES willShowStars:YES];
    }];
}

#pragma marg Buttons Action

- (IBAction)menuButtonClicked:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MENU_BUTTON_PRESSED" object:nil];
}

- (IBAction)plussButtonClicked:(id)sender {
    [[DrawingViewManager sharedInstance] setPrevTitle:@""];
    [[DrawingViewManager sharedInstance] setWillCreateNewNote:YES];
    [(MainTabBarController *)self.tabBarController setSelectedIndex:0];
}

- (IBAction)homeButtonPressed:(id)sender {
    [(MainTabBarController *)self.tabBarController setSelectedIndex:4];
}

- (IBAction)searchButtonPressed:(id)sender {
    SearchViewController *search = [SearchViewController sharedInstance];
    [self addChildViewController:search];
    search.view.frame = self.view.bounds;
    [self.view addSubview:search.view];
    [search didMoveToParentViewController:self];
}

- (IBAction)addButtonClicked:(id)sender {
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"http://www.sexynuts.com"]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.sexynuts.com"]];
    }
}

- (void)callTitleMenuPopover{
    titleMenuItems = [[Note getAllTitles] mutableCopy];
    NSMutableArray *upperCaseTitlesArray = [[NSMutableArray alloc] initWithObjects:@"by date",@"by text", nil];
    for (NSString *title in titleMenuItems) {
        [upperCaseTitlesArray addObject:[title uppercaseString]];
    }
    titleMenuPopover = [[MLKMenuPopover alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/4,40,250, titleMenuItems.count * 40  > SCREEN_HEIGHT - 100 ? SCREEN_HEIGHT - 100 : titleMenuItems.count * 40) menuItems:upperCaseTitlesArray];
    titleMenuPopover.tag = 0;
    titleMenuPopover.centerX = SCREEN_WIDTH/2;
    [titleMenuPopover hideArrow];
    titleMenuPopover.menuPopoverDelegate = self;
    [titleMenuPopover showInView:self.view];
}

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex{
    switch (menuPopover.tag) {
        case 0:{
            switch (selectedIndex) {
                case 0:{
                    [(MainTabBarController *)self.tabBarController searchByDateButtonPressed];
                    break;
                }
                case 1:{
                    [(MainTabBarController *)self.tabBarController searchByNameButtonPressed];
                    break;
                }
                default:{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"TITLE_ITEM_SELECTED" object:@[titleMenuItems[selectedIndex - 2]]];
                    break;
                }
            }
        }
        default:
            break;
    }
}

- (void)menuPopoverWillHide:(MLKMenuPopover *)menuPopover{
    
}

- (void) orientationChanged:(NSNotification *)note{
    UIDevice * device = note.object;
    if (device.orientation != lastOrientation) {
        [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(setupNewOrientation) userInfo:nil repeats:NO];
        lastOrientation = device.orientation;
    }
}

- (void)setupNewOrientation{
    titleMenuPopover.frame = CGRectMake(SCREEN_WIDTH/4,40,250, titleMenuItems.count * 40  > SCREEN_HEIGHT - 100 ? SCREEN_HEIGHT - 100 : titleMenuItems.count * 40);
    titleMenuPopover.centerX = SCREEN_WIDTH/2;
}

- (void)pinsDidChange{
//    NSArray * array = [Note getPins];
//    if (array.count) {
//        [favoritesView setHidden:NO];
//    }else{
//        [favoritesView setHidden:YES];
//    }
//    [bootpmImgaeSliderVC configureWithNotes:array withObjetcId:[NSDate dateValue:[NSDate new]] marge:40];
}

@end

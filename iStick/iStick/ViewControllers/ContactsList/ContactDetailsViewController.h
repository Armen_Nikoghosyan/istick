#import <UIKit/UIKit.h>
#import <Contacts/Contacts.h>

@interface ContactDetailsViewController : UIViewController

@property (nonatomic, strong) CNContact *contact;

@end

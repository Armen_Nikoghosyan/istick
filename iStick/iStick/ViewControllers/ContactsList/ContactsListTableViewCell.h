#import <UIKit/UIKit.h>
#import <Contacts/Contacts.h>

@interface ContactsListTableViewCell : UITableViewCell

- (void)configureCellWithContact:(CNContact *)contact;

@end

#import "NumberCollectionViewCell.h"

@implementation NumberCollectionViewCell{
    __weak IBOutlet UILabel *typeLabel;
    __weak IBOutlet UILabel *numberLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [typeLabel setFont:[UIFont fontWithName:typeLabel.font.fontName size:15]];
    // Initialization code
}

- (void)configureCellWithType:(NSString *)type number:(NSString *)number;{
    [typeLabel setText:type];
    [numberLabel setText:number];
}

@end

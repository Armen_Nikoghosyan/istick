#import <UIKit/UIKit.h>
#import <Contacts/Contacts.h>

@protocol ContactsListViewControllerDelegate <NSObject>

- (void)didSelectContact:(CNContact *)contact;

@end

@interface ContactsListViewController : UIViewController
@property (nonatomic, weak) id<ContactsListViewControllerDelegate> delegate;
@end

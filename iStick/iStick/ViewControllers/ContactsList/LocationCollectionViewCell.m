#import "LocationCollectionViewCell.h"

@implementation LocationCollectionViewCell{
    __weak IBOutlet UILabel *countyLabel;
    __weak IBOutlet UILabel *cityLabel;
    __weak IBOutlet UILabel *typeLable;
    __weak IBOutlet UILabel *streetLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureCellWithContact:(CNContact *)contact  index:(NSInteger)index{
    [typeLable setText: [CNLabeledValue localizedStringForLabel:contact.postalAddresses[index].label]];
    [streetLabel setText:contact.postalAddresses[index].value.street];
    [cityLabel setText:[NSString stringWithFormat:@"%@ %@ %@",contact.postalAddresses[index].value.city,contact.postalAddresses[index].value.state,contact.postalAddresses[index].value.postalCode]];
    [countyLabel setText:[NSString stringWithFormat:@"%@",contact.postalAddresses[index].value.country]];
    
}

@end

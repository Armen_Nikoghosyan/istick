#import "ContactsListTableViewCell.h"
#import <MessageUI/MFMessageComposeViewController.h>

@implementation ContactsListTableViewCell{
    CNContact *cnContact;
    __weak IBOutlet UILabel *nameLabel;
    __weak IBOutlet UILabel *phoneNumberLable;
    __weak IBOutlet UIButton *messageButton;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [phoneNumberLable setFont:[UIFont fontWithName:phoneNumberLable.font.fontName size:11]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)phoneButtonPressed:(id)sender {
    if (cnContact.phoneNumbers.count) {
        NSString *phoneNumber = [@"tel://" stringByAppendingString:cnContact.phoneNumbers[0].value.stringValue];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""]]];
    }
}

- (IBAction)messageButtonPressed:(id)sender {
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        controller.recipients = @[cnContact.phoneNumbers[0].value.stringValue];
        //controller.messageComposeDelegate = self;
    }
}

- (void)configureCellWithContact:(CNContact *)contact{
    cnContact = contact;
    if (![contact.familyName isEqualToString:@""] || ![contact.givenName isEqualToString:@""]) {
        nameLabel.text = [NSString stringWithFormat:@"%@ %@",contact.givenName,contact.familyName] ;
    }else{
        if (contact.phoneNumbers.count) {
            nameLabel.text = [contact.phoneNumbers[0].value stringValue];
        }
    }
    if (contact.phoneNumbers.count) {
        [phoneNumberLable setText:[contact.phoneNumbers[0].value stringValue]];
    }
}

@end

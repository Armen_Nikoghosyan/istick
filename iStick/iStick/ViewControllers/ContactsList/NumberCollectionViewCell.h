#import <UIKit/UIKit.h>

@interface NumberCollectionViewCell : UICollectionViewCell

- (void)configureCellWithType:(NSString *)type number:(NSString *)number;

@end

#import "ContactsListViewController.h"
#import <Contacts/Contacts.h>
#import "ContactsListTableViewCell.h"
#import "ContactManagedObject.h"
#import "AppDelegate.h"
#import "Contact.h"
#import "ContactDetailsViewController.h"

@interface ContactsListViewController ()<UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate,UIScrollViewDelegate>{
    CNContactStore *contactStore;
    __weak IBOutlet UITableView *mainTableView;
    NSMutableArray *contacts;
    NSMutableArray *contactsSpletedArray;
    NSArray *contactsSpletedFixArray;
    NSArray *sectionIndextitleArray;
    __weak IBOutlet UISearchBar *mainSearchBar;
}

@end

@implementation ContactsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    sectionIndextitleArray = [NSArray arrayWithObjects:@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", @"#", nil];
    contactsSpletedArray = [[NSMutableArray alloc] init];
    mainSearchBar.delegate = self;
    mainSearchBar.layer.masksToBounds = YES;
    mainSearchBar.layer.borderWidth = 1;
    mainSearchBar.layer.borderColor = [UIColor colorWithRed:201.0f/255.0f green:201.0f/255.0f blue:208.0f/255.0f alpha:1.0].CGColor;
    
    [mainTableView registerClass:[ContactsListTableViewCell class] forCellReuseIdentifier:@"ContactsListTableViewCell"];
    [mainTableView registerNib:[UINib nibWithNibName:@"ContactsListTableViewCell" bundle:nil] forCellReuseIdentifier:@"ContactsListTableViewCell"];
    
    contacts = [[[Contact sharedInstance] getAllContacts] mutableCopy];
    for (NSString *str in sectionIndextitleArray) {
        NSMutableArray *newArray = [[NSMutableArray alloc] init];
        for (CNContact *cont in [NSMutableArray arrayWithArray:contacts]) {
            if ([cont.familyName isEqualToString:@""]) {
                if (![cont.givenName isEqualToString:@""]) {
                    if ([[[cont.givenName substringToIndex:1] lowercaseString] isEqualToString:[str lowercaseString]]) {
                        [newArray addObject:cont];
                        [contacts removeObject:cont];
                    }
                }
            }else{
                if ([[[cont.familyName substringToIndex:1] lowercaseString] isEqualToString:[str lowercaseString]]) {
                    [newArray addObject:cont];
                    [contacts removeObject:cont];
                    
                }
            }
        }
        if (newArray.count) {
            [contactsSpletedArray addObject:newArray];
        }
    }
    [contactsSpletedArray addObject:contacts];
    contactsSpletedFixArray = [NSArray arrayWithArray:contactsSpletedArray];
    
    [mainTableView reloadData];
    [self setAutomaticallyAdjustsScrollViewInsets:NO];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backNButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Table View 

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return contactsSpletedArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *array = contactsSpletedArray[section];
    return array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ContactsListTableViewCell *cell = (ContactsListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ContactsListTableViewCell" forIndexPath:indexPath];
    NSArray *array = contactsSpletedArray[indexPath.section];
    CNContact *contact = array[indexPath.row];
    [cell configureCellWithContact:contact];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *array = contactsSpletedArray[indexPath.section];
    CNContact *contact = array[indexPath.row];
    [self.delegate didSelectContact:contact];
    
//    ContactDetailsViewController *contactDetailsViewController = [[ContactDetailsViewController alloc] initWithNibName:@"ContactDetailsViewController" bundle:nil];
//    contactDetailsViewController.contact = contact;
//    [self.navigationController pushViewController:contactDetailsViewController animated:YES];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return sectionIndextitleArray;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return index;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel *label = [[UILabel alloc] init];
    [label setBackgroundColor:[UIColor colorWithWhite:0.95 alpha:1]];
    NSArray *array = contactsSpletedArray[section];
    if (array.count) {
        CNContact *contact = array[0];
        
        if (![contact.familyName isEqualToString:@""] || ![contact.givenName isEqualToString:@""]) {
            NSString *str = [contact.familyName isEqualToString:@""]?[[contact.givenName substringToIndex:1] uppercaseString]:[[contact.familyName substringToIndex:1] uppercaseString];
            [label setText:str];
            
        }else{
            [label setText:@"#"];
        }
    }
    
    return label;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [mainSearchBar resignFirstResponder];
}

#pragma mark - Search Bar

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if ([searchText isEqualToString:@""]) {
        contactsSpletedArray = [contactsSpletedFixArray mutableCopy];
    }else{
        NSMutableArray *finelArray = [[NSMutableArray alloc] init];
        for (NSArray *array in contactsSpletedFixArray) {
            NSMutableArray *newArray = [[NSMutableArray alloc] init];
            for (CNContact *contact in array) {
                if ([[contact.familyName lowercaseString] containsString:[searchText lowercaseString]] ||
                    [[contact.givenName lowercaseString] containsString:[searchText lowercaseString]] ||
                    (contact.phoneNumbers.count > 0 && [[contact.phoneNumbers[0].value stringValue] containsString:searchText])) {
                    [newArray addObject:contact];
                }
            }
            if (newArray.count) {
                [finelArray addObject:newArray];
            }
        }
        contactsSpletedArray = finelArray;
    }
    [mainTableView reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    searchBar.text = @"";
    [self searchBar:searchBar textDidChange:@""];
    [searchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:NO animated:YES];

}

@end

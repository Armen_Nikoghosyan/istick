#import <UIKit/UIKit.h>

@interface AvatarNameCollectionViewCell : UICollectionViewCell

- (void)configureCellWithData:(NSData *)data firstName:(NSString *)firstName seccondName:(NSString *)seccondName name:(NSString *)name;

@end

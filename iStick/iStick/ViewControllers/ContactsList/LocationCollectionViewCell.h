#import <UIKit/UIKit.h>
#import <Contacts/Contacts.h>

@interface LocationCollectionViewCell : UICollectionViewCell
- (void)configureCellWithContact:(CNContact *)contact index:(NSInteger)index;
@end

#import "ContactDetailsViewController.h"
#import "AvatarNameCollectionViewCell.h"
#import "NumberCollectionViewCell.h"
#import "LocationCollectionViewCell.h"
#import "DrawingViewManager.h"
#import "NotesCollectionViewCell.h"

@interface ContactDetailsViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>{
    __weak IBOutlet UICollectionView *mainCollectionView;
    __weak IBOutlet UIButton *plussIcon;
    NSArray *notesArray;
}

@end

@implementation ContactDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    mainCollectionView.delegate = self;
    mainCollectionView.dataSource = self;
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [mainCollectionView registerClass:[UICollectionReusableView class]
           forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                  withReuseIdentifier:@"HeaderView"];
    
    [mainCollectionView registerClass:[UICollectionReusableView class]
           forSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                  withReuseIdentifier:@"FooterView"];
    [self registerCells];
    [self configure];
    notesArray = [Note getObjectsWithContactId:_contact.identifier];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)configure{
    [plussIcon addShadowToView];
}

- (void)registerCells{
    [mainCollectionView registerClass:[AvatarNameCollectionViewCell class] forCellWithReuseIdentifier:@"AvatarNameCollectionViewCell"];
    UINib *nibAvatar = [UINib nibWithNibName:@"AvatarNameCollectionViewCell" bundle:nil];
    [mainCollectionView registerNib:nibAvatar forCellWithReuseIdentifier:@"AvatarNameCollectionViewCell"];
    
    [mainCollectionView registerClass:[NumberCollectionViewCell class] forCellWithReuseIdentifier:@"NumberCollectionViewCell"];
    UINib *nibNumber = [UINib nibWithNibName:@"NumberCollectionViewCell" bundle:nil];
    [mainCollectionView registerNib:nibNumber forCellWithReuseIdentifier:@"NumberCollectionViewCell"];
    
    [mainCollectionView registerClass:[LocationCollectionViewCell class] forCellWithReuseIdentifier:@"LocationCollectionViewCell"];
    UINib *nibLocation = [UINib nibWithNibName:@"LocationCollectionViewCell" bundle:nil];
    [mainCollectionView registerNib:nibLocation forCellWithReuseIdentifier:@"LocationCollectionViewCell"];
    
    [mainCollectionView registerClass:[NotesCollectionViewCell class] forCellWithReuseIdentifier:@"NotesCollectionViewCell"];
    UINib *nibNote = [UINib nibWithNibName:@"NotesCollectionViewCell" bundle:nil];
    [mainCollectionView registerNib:nibNote forCellWithReuseIdentifier:@"NotesCollectionViewCell"];
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
}

#pragma mark IBActions

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)plussButtonPressed:(id)sender {
    [[DrawingViewManager sharedInstance] setPrevTitle:@""];
    [[DrawingViewManager sharedInstance] setContactId:_contact.identifier];
    [self popViewController];
}

- (void)popViewController{
    if ([self.navigationController.viewControllers[self.navigationController.viewControllers.count - 3] isKindOfClass:[UIPageViewController class]]) {
        UIPageViewController *pageViewController = self.navigationController.viewControllers[self.navigationController.viewControllers.count - 3];
        [self.navigationController popToViewController:pageViewController animated:YES];
    }else{
        UIPageViewController *pageViewController = self.navigationController.viewControllers[self.navigationController.viewControllers.count - 4];
        [self.navigationController popToViewController:pageViewController animated:YES];
    }

}

#pragma mark CollectionView

- (UICollectionViewCell *)collectionView:(UICollectionView *)view cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        AvatarNameCollectionViewCell *cell = (AvatarNameCollectionViewCell *)[view dequeueReusableCellWithReuseIdentifier:@"AvatarNameCollectionViewCell" forIndexPath:indexPath];
        [cell configureCellWithData:_contact.imageData firstName:_contact.givenName seccondName:_contact.familyName name:_contact.organizationName];
        [cell layoutIfNeeded];
        return cell;
    }else if (indexPath.section == 2){
        NumberCollectionViewCell *cell = (NumberCollectionViewCell *)[view dequeueReusableCellWithReuseIdentifier:@"NumberCollectionViewCell" forIndexPath:indexPath];
        CNLabeledValue<CNPhoneNumber *> *phone = _contact.phoneNumbers[indexPath.row];
        [cell configureCellWithType:[CNLabeledValue localizedStringForLabel:phone.label] number:phone.value.stringValue];
        [cell layoutIfNeeded];
        return cell;
    }else if(indexPath.section == 3){
        NumberCollectionViewCell *cell = (NumberCollectionViewCell *)[view dequeueReusableCellWithReuseIdentifier:@"NumberCollectionViewCell" forIndexPath:indexPath];
        [cell configureCellWithType:[CNLabeledValue localizedStringForLabel:_contact.emailAddresses[indexPath.row].label] number:_contact.emailAddresses[indexPath.row].value];
        [cell layoutIfNeeded];
        return cell;
    }else if(indexPath.section == 4){
        NumberCollectionViewCell *cell = (NumberCollectionViewCell *)[view dequeueReusableCellWithReuseIdentifier:@"NumberCollectionViewCell" forIndexPath:indexPath];
        [cell configureCellWithType:[CNLabeledValue localizedStringForLabel:_contact.urlAddresses[indexPath.row].label] number:[_contact.urlAddresses[indexPath.row].value containsString:@"http://"]?_contact.urlAddresses[indexPath.row].value:[NSString stringWithFormat:@"http://%@",_contact.urlAddresses[indexPath.row].value]];
        [cell layoutIfNeeded];
        return cell;
    }else if(indexPath.section == 5){
        LocationCollectionViewCell *cell = (LocationCollectionViewCell *)[view dequeueReusableCellWithReuseIdentifier:@"LocationCollectionViewCell" forIndexPath:indexPath];
        [cell configureCellWithContact:_contact index:indexPath.row];
        [cell layoutIfNeeded];
        return cell;
    }else if(indexPath.section == 6){
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        NumberCollectionViewCell *cell = (NumberCollectionViewCell *)[view dequeueReusableCellWithReuseIdentifier:@"NumberCollectionViewCell" forIndexPath:indexPath];
        [cell configureCellWithType:@"birthday" number:[NSString stringWithFormat:@"%@ %li",[[df monthSymbols] objectAtIndex:_contact.birthday.month - 1],_contact.birthday.day]];
        [cell layoutIfNeeded];
        return cell;
    }else if(indexPath.section == 7){
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        NumberCollectionViewCell *cell = (NumberCollectionViewCell *)[view dequeueReusableCellWithReuseIdentifier:@"NumberCollectionViewCell" forIndexPath:indexPath];
        [cell configureCellWithType:[CNLabeledValue localizedStringForLabel:_contact.dates[indexPath.row].label] number:[NSString stringWithFormat:@"%@ %li",[[df monthSymbols] objectAtIndex:_contact.dates[indexPath.row].value.month - 1],_contact.dates[indexPath.row].value.day]];
        [cell layoutIfNeeded];
        return cell;
    }else if(indexPath.section == 8){
        NumberCollectionViewCell *cell = (NumberCollectionViewCell *)[view dequeueReusableCellWithReuseIdentifier:@"NumberCollectionViewCell" forIndexPath:indexPath];
        [cell configureCellWithType:[CNLabeledValue localizedStringForLabel:_contact.contactRelations[indexPath.row].label] number:_contact.contactRelations[indexPath.row].value.name];
        [cell layoutIfNeeded];
        return cell;
    }else if(indexPath.section == 1){
        NotesCollectionViewCell *cell = (NotesCollectionViewCell *)[view dequeueReusableCellWithReuseIdentifier:@"NotesCollectionViewCell" forIndexPath:indexPath];
        //cell.delegate = self;
        NSLog(@"%ld",(long)indexPath.row);
        [cell configureCellWithNote:(NoteMenagedObject *)notesArray[indexPath.row] isInDeleteState:NO withChakedItems:nil willShowStars:NO];
        [cell layoutIfNeeded];
        return cell;
    }
    
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 2) {
        NSString *phoneNumber = [@"tel://" stringByAppendingString:_contact.phoneNumbers[indexPath.row].value.stringValue];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""]]];
    }else if(indexPath.section == 3){
        NSString *mail = _contact.emailAddresses[indexPath.row].value;
        NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
        NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"mailto:?to=%@&subject=%@",
                                                    [mail stringByAddingPercentEncodingWithAllowedCharacters:set],
                                                    [@"" stringByAddingPercentEncodingWithAllowedCharacters:set]]];
        [[UIApplication sharedApplication] openURL:url];
    }else if(indexPath.section == 4){
        NSURL *url = [NSURL URLWithString:[_contact.urlAddresses[indexPath.row].value containsString:@"http://"]?_contact.urlAddresses[indexPath.row].value:[NSString stringWithFormat:@"http://%@",_contact.urlAddresses[indexPath.row].value]];
        [[UIApplication sharedApplication] openURL:url];
    }else if(indexPath.section == 5){
        NSMutableArray *array = [[NSMutableArray alloc] init];
        [array addObjectsFromArray:[_contact.postalAddresses[indexPath.row].value.country componentsSeparatedByString:@" "]];
        [array addObjectsFromArray:[_contact.postalAddresses[indexPath.row].value.state
                                    componentsSeparatedByString:@" "]];
        [array addObjectsFromArray:[_contact.postalAddresses[indexPath.row].value.street componentsSeparatedByString:@"\n"]];
        NSString * result = [[array valueForKey:@"description"] componentsJoinedByString:@"+"];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/?q=%@",result]];
        [[UIApplication sharedApplication] openURL:url];
    }else if(indexPath.section == 1){
        [[DrawingViewManager sharedInstance] setShowingNote:(NoteMenagedObject *)notesArray[indexPath.row]];
        [self popViewController];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeMake([UIScreen mainScreen].bounds.size.width, 70);
    }else if(indexPath.section == 2){
        return CGSizeMake([UIScreen mainScreen].bounds.size.width, 60);
    }else if(indexPath.section == 3){
        return CGSizeMake([UIScreen mainScreen].bounds.size.width, 60);
    }else if(indexPath.section == 4){
        return CGSizeMake([UIScreen mainScreen].bounds.size.width, 60);
    }else if(indexPath.section == 5){
        return CGSizeMake([UIScreen mainScreen].bounds.size.width, 110);
    }else if(indexPath.section == 6){
        return CGSizeMake([UIScreen mainScreen].bounds.size.width, 60);
    }else if(indexPath.section == 7){
        return CGSizeMake([UIScreen mainScreen].bounds.size.width, 60);
    }else if(indexPath.section == 8){
        return CGSizeMake([UIScreen mainScreen].bounds.size.width, 60);
    }else if(indexPath.section == 1){
        return CGSizeMake([UIScreen mainScreen].bounds.size.width/3.5, [UIScreen mainScreen].bounds.size.height/4);
    }
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, 100);

}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else if (section == 2){
        return _contact.phoneNumbers.count;
    }else if (section == 3){
        return _contact.emailAddresses.count;
    }else if(section == 4){
        return _contact.urlAddresses.count;
    }else if(section == 5){
        return _contact.postalAddresses.count;
    }else if(section == 6){
        return _contact.birthday?1:0;
    }else if(section == 7){
        return _contact.dates.count;
    }else if(section == 8){
        return _contact.contactRelations.count;
    }else{
        return notesArray.count;
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 9;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 3 && _contact.emailAddresses.count){
        return CGSizeMake(SCREEN_MIN_LENGTH, 1);
    }else if(section == 4 && _contact.urlAddresses.count){
        return CGSizeMake(SCREEN_MIN_LENGTH, 1);
    }else if(section == 5 && _contact.postalAddresses.count){
        return CGSizeMake(SCREEN_MIN_LENGTH, 1);
    }else if(section == 6 && _contact.birthday){
        return CGSizeMake(SCREEN_MIN_LENGTH, 1);
    }else if(section == 7 && _contact.dates.count){
        return CGSizeMake(SCREEN_MIN_LENGTH, 1);
    }else if(section == 8 && _contact.contactRelations.count){
        return CGSizeMake(SCREEN_MIN_LENGTH, 1);
    }else if(section == 2 && _contact.phoneNumbers.count){
        return CGSizeMake(SCREEN_MIN_LENGTH, 1);
    }
    return CGSizeMake(SCREEN_MIN_LENGTH, 0);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(15, 0, headerView.width, headerView.height)];
        [view setBackgroundColor:[UIColor lightGrayColor]];
        [headerView addSubview:view];
        reusableview = headerView;
    }
    if (kind == UICollectionElementKindSectionFooter) {
        
    }
    return reusableview;
}

@end

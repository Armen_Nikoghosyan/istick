#import "AvatarNameCollectionViewCell.h"

@implementation AvatarNameCollectionViewCell{
    __weak IBOutlet UIImageView *avatarImageView;
    __weak IBOutlet UILabel *fullNameLabel;
    __weak IBOutlet UILabel *nameLabel;
    __weak IBOutlet UILabel *imageLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [imageLabel setFont:[UIFont fontWithName:imageLabel.font.fontName size:30]];
    [imageLabel setBackgroundColor:[UIColor lightGrayColor]];
    [imageLabel setTextAlignment:NSTextAlignmentCenter];
    [imageLabel setTextColor:[UIColor whiteColor]];
    // Initialization code
}

- (void)configureCellWithData:(NSData *)data firstName:(NSString *)firstName seccondName:(NSString *)seccondName name:(NSString *)name{
    [avatarImageView setImage:[UIImage imageWithData:data]];
    [fullNameLabel setText:[NSString stringWithFormat:@"%@ %@",firstName,seccondName]];
    [nameLabel setText:name];
    [imageLabel setText:[NSString stringWithFormat:@"%@%@",[firstName isEqualToString:@""]?@"":[[firstName substringToIndex:1] uppercaseString],[seccondName isEqualToString:@""]?@"":[[seccondName substringToIndex:1] uppercaseString]]];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [avatarImageView.layer setMasksToBounds:YES];
    avatarImageView.layer.cornerRadius = avatarImageView.height/2;
    [imageLabel.layer setMasksToBounds:YES];
    imageLabel.layer.cornerRadius = imageLabel.height/2;
}

@end

#import "MainTabBarController.h"
#import "DrawViewController.h"
#import "NotesViewController.h"
#import "ConnectionsViewController.h"
#import "ContactsListViewController.h"
#import "DrawingViewManager.h"
#import "MLKMenuPopover.h"
#import "HomeViewController.h"
#import <Speech/Speech.h>
#import "LoginViewController.h"
#import "PreviewImageVC.h"

@interface MainTabBarController ()<SFSpeechRecognizerDelegate, UIImagePickerControllerDelegate, PreviewImageVCDDelegate,UINavigationControllerDelegate>{
    UIButton *hideShowButton;
    NSMutableArray *mainButtonsArray;
    NSMutableArray *allbuttonsArray;
    UIScrollView * sliderButtonsView;
    UIButton *sortButton;
    UIButton *menuButton;
    UIButton *trashButton;
    UIPageViewController *mainPageViewController;
    UIButton *starButton;
    UIButton *pinButton;
    UIButton *openedButton;
    UIButton *penOrFinger;
    UIButton *phoneButton;
    SFSpeechRecognizer *speechRecognizer;
    SFSpeechAudioBufferRecognitionRequest *recognitionRequest;
    SFSpeechRecognitionTask *recognitionTask;
    AVAudioEngine *audioEngine;
    int count;
    float sliderHeight;
}

@end

@implementation MainTabBarController
#pragma mark - SuperClass Functions

- (void)viewDidLoad {
    [super viewDidLoad];
    sliderHeight = [UIScreen mainScreen].bounds.size.height + 20;
    self.tabBar.y = 100;// SCREEN_HEIGHT;
    [self createFunctionButtons];
    [self configureViewControllers];
    //[self configureSpeach];
    [sliderButtonsView setHidden:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectTabBarItem:) name:@"DID_SELECT_TABBAR_ITEM" object:nil];
}
-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"MainTabBarController viewWillAppear");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    sliderHeight = [UIScreen mainScreen].bounds.size.height + 20;
    CGRect frame = sliderButtonsView.frame;
    frame.size.height = sliderHeight;
    sliderButtonsView.frame = frame;
    sliderButtonsView.bottom = 40;
    sliderButtonsView.contentOffsetY = 40 * (count + 1) + 20 - sliderHeight;
    [self hideSliderMenu:sliderButtonsView];
}

- (void)didSelectTabBarItem:(NSNotification *)notif{
    NSNumber *index = notif.object;
    [self setSelectedIndex:[index integerValue]];
}

#pragma mark - Configure Functions

- (void)configureViewControllers{
    //Creating Drawing View Contrller
    NSDate *startDate = [NSDate new];
    mainPageViewController = [[UIPageViewController alloc] init];
    DrawViewController *viewController;
    if (IS_IPHONE) {
        viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewControlleriPhone" bundle:nil];
    }else{
        viewController = [[DrawViewController alloc] initWithNibName:@"DrawViewController" bundle:nil];
    }
[mainPageViewController setViewControllers:@[viewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    NSLog(@"Seconds -------->01 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    UINavigationController *drawNavigationController = [[UINavigationController alloc] initWithRootViewController:mainPageViewController];
    [drawNavigationController.navigationBar removeFromSuperview];
    
    //Creating File Cabinet View Controller
    NotesViewController *notesViewController = [[NotesViewController alloc] initWithNibName:@"NotesViewController" bundle:nil]; 
    NSLog(@"Seconds -------->02 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    //Creating Login View Controller
    ContactsListViewController *contactsListViewController = [[ContactsListViewController alloc] initWithNibName:@"ContactsListViewController" bundle:nil];
    UINavigationController *contactNavigationController = [[UINavigationController alloc] initWithRootViewController:contactsListViewController];
    [contactNavigationController.navigationBar removeFromSuperview];
    NSLog(@"Seconds -------->03 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    //Creating Contacts View Controller
    ConnectionsViewController *connectionsViewController = [[ConnectionsViewController alloc]initWithNibName:@"Connections" bundle:nil];
    connectionsViewController.view.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.9];
    UINavigationController *connectionNavigationController = [[UINavigationController alloc] initWithRootViewController:connectionsViewController];
    [connectionNavigationController.navigationBar removeFromSuperview];
    NSLog(@"Seconds -------->04 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    //Creating Home View Controller
    
    HomeViewController *homeViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    [contactNavigationController.navigationBar removeFromSuperview];

    
    [self setViewControllers:@[drawNavigationController,notesViewController,contactNavigationController,connectionNavigationController,homeViewController]];
    //[[self.tabBar.items objectAtIndex:0] setTitle:@"Hello"];
    NSLog(@"Seconds -------->05 %f",[[NSDate date] timeIntervalSinceDate: startDate]);

    self.selectedIndex = 1;

}

- (void)createFunctionButtons{
    allbuttonsArray = [[NSMutableArray alloc] init];
    mainButtonsArray = [[NSMutableArray alloc] init];
    

    UIButton *localeShareButton = [[UIButton alloc] init];
    [localeShareButton setImage:[UIImage imageNamed:@"locale_share_icon"] forState:UIControlStateNormal];
    localeShareButton.tag = 3;
    [localeShareButton addTarget:self action:@selector(logInButtonPressed) forControlEvents:UIControlEventTouchUpInside];

    menuButton = [[UIButton alloc] init];
    [menuButton setImage:[[UIImage imageNamed:@"menu_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [menuButton setTintColor:[UIColor whiteColor]];
    menuButton.tag = 1;
    [menuButton addTarget:self action:@selector(buttonIsSelected:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *addressBookButton = [[UIButton alloc] init];
    [addressBookButton setImage:[[UIImage imageNamed:@"adress_book_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [addressBookButton setTintColor:[UIColor colorWithRed:199.0f/255.0f green:92.0f/255.0f blue:92.0f/255.0f alpha:1.0f]];
    addressBookButton.tag = 2;
    
    sortButton = [[UIButton alloc] init];
    [sortButton setImage:[[UIImage imageNamed:@"sort"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [sortButton setTintColor:[UIColor whiteColor]];
    sortButton.tag = 4;
    //[sortButton addTarget:self action:@selector(buttonIsSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    trashButton = [[UIButton alloc] init];
    [trashButton setTitle:@"Arc" forState:UIControlStateNormal];
    [trashButton setTintColor:[UIColor whiteColor]];
    [trashButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [trashButton addTarget:self action:@selector(trashButtonSelected) forControlEvents:UIControlEventTouchUpInside];
    
    phoneButton = [[UIButton alloc] init];
    [phoneButton setImage:[[UIImage imageNamed:@"phone_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [phoneButton addTarget:self action:@selector(phonButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [phoneButton setTintColor:[UIColor whiteColor]];

    UIButton *shareButton = [[UIButton alloc] init];
    [shareButton setImage:[UIImage imageNamed:@"share_icon"] forState:UIControlStateNormal];

    
    UIButton *alarmButton = [[UIButton alloc] init];
    if ([[UIApplication sharedApplication] scheduledLocalNotifications].count > 0) {
        [alarmButton setImage:[UIImage imageNamed:@"alarm_icon_selected"] forState:UIControlStateNormal];
    }else{
        [alarmButton setImage:[UIImage imageNamed:@"alarm_icon"] forState:UIControlStateNormal];
    }
    UIButton *colorButton = [[UIButton alloc] init];
    [colorButton setImage:[[UIImage imageNamed:@"color_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [colorButton setTintColor:[UIColor whiteColor]];


    starButton = [[UIButton alloc] init];
    [starButton setImage:[[UIImage imageNamed:@"star_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [starButton setTintColor:[UIColor whiteColor]];
    [starButton addTarget:self action:@selector(starButtonPressed) forControlEvents:UIControlEventTouchUpInside];

    pinButton = [[UIButton alloc] init];
    [pinButton setImage:[[UIImage imageNamed:@"icon_pin"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [pinButton setTintColor:[UIColor whiteColor]];
    [pinButton addTarget:self action:@selector(pinButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    pinButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    openedButton = [[UIButton alloc] init];
    [openedButton setTitle:@"__" forState:UIControlStateNormal];
    [openedButton.titleLabel setFont:[UIFont boldSystemFontOfSize:22]];
    [openedButton setTintColor:[UIColor whiteColor]];
    [openedButton addTarget:self action:@selector(openButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [openedButton setUserInteractionEnabled:NO];
    
    UIButton *cameraButton = [[UIButton alloc] init];
    [cameraButton setImage:[[UIImage imageNamed:@"camera_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [cameraButton addTarget:self action:@selector(cameraButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [cameraButton setTintColor:[UIColor whiteColor]];


    UIButton *artButton = [[UIButton alloc] init];
    [artButton setTitle:@"Art" forState:UIControlStateNormal];
    [artButton setTintColor:[UIColor whiteColor]];
    [artButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    UIButton *galButton = [[UIButton alloc] init];
    //[galButton setTitle:@"Gal" forState:UIControlStateNormal];
    [galButton setImage:[[UIImage imageNamed:@"img1"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [galButton setTintColor:[UIColor whiteColor]];
    [galButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [galButton addTarget:self action:@selector(galleryButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    penOrFinger = [[UIButton alloc] init];
    if ([[DrawingViewManager sharedInstance] isPen]) {
        [penOrFinger setImage:[[UIImage imageNamed:@"finger"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    }else{
        [penOrFinger setImage:[[UIImage imageNamed:@"pen"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    }
    [penOrFinger addTarget:self action:@selector(penOrFingerButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    //[penOrFinger setTintColor:[UIColor redColor]];
    UIButton *showImageSliderButton = [[UIButton alloc] init];
    [showImageSliderButton setImage:[[UIImage imageNamed:@"next_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [showImageSliderButton setTintColor:[[DrawingViewManager sharedInstance] buttonsColor]];
    showImageSliderButton.transform = CGAffineTransformMakeRotation(-M_PI_2);
    //[sliderButtonsArray addObject:toDoButton];
    [mainButtonsArray addObject:penOrFinger];
    
    UIButton *safeButton = [[UIButton alloc] init];
    [safeButton setImage:[[UIImage imageNamed:@"unlocked"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [safeButton setImage:[[UIImage imageNamed:@"locked"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate ] forState:UIControlStateSelected];
    [mainButtonsArray addObject:safeButton];
    [mainButtonsArray addObject:shareButton];
   // [mainButtonsArray addObject:galButton];
    [mainButtonsArray addObject:alarmButton];
    [mainButtonsArray addObject:colorButton];
    [mainButtonsArray addObject:starButton];
    [mainButtonsArray addObject:pinButton];
    [mainButtonsArray addObject:phoneButton];
    [mainButtonsArray addObject:trashButton];
    [mainButtonsArray addObject:openedButton];
    [mainButtonsArray addObject:localeShareButton];
    [mainButtonsArray addObject:sortButton];
    [mainButtonsArray addObject:showImageSliderButton];
    [mainButtonsArray addObject:artButton];
   
    [mainButtonsArray addObject:cameraButton];
    [mainButtonsArray addObject:addressBookButton];

    
    
    [self createSlideButtonsWithArray:mainButtonsArray];
    [allbuttonsArray setArray:mainButtonsArray];
    [allbuttonsArray addObject:hideShowButton];
    [allbuttonsArray addObject:menuButton];
    [allbuttonsArray addObject:sortButton];
    [allbuttonsArray addObject:trashButton];
    [allbuttonsArray addObject:sortButton];
    [allbuttonsArray addObject:trashButton];
    [allbuttonsArray addObject:pinButton];
    [self setButtons:allbuttonsArray wilthColor:[UIColor whiteColor]];
}

- (void)penOrFingerButtonPressed{
    if ([[DrawingViewManager sharedInstance] isPen]) {
        [[DrawingViewManager sharedInstance] setIsPen:NO];
        [penOrFinger setImage:[[UIImage imageNamed:@"finger"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    }else{
        [[DrawingViewManager sharedInstance] setIsPen:YES];
        [penOrFinger setImage:[[UIImage imageNamed:@"pen"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    }
    [penOrFinger setTintColor:[UIColor whiteColor]];
}

-(void)galleryButtonClicked {
    
}


- (void)cameraButtonClicked{
    hideShowButton.tag = 1;
    [self hideshowSliderMenuButtons:hideShowButton];
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:nil      //  Must be "nil", otherwise a blank title area will appear above our two buttons
                                message:nil
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* button0 = [UIAlertAction
                              actionWithTitle:@"Cancel"
                              style:UIAlertActionStyleCancel
                              handler:^(UIAlertAction * action)
                              {
                              }];
    
    UIAlertAction* button1 = [UIAlertAction
                              actionWithTitle:@"Take photo"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  //  The user tapped on "Take a photo"
                                  UIImagePickerController *imagePickerController= [[UIImagePickerController alloc] init];
                                  imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                                  imagePickerController.delegate = self;
                                 
                                  [self presentViewController:imagePickerController animated:YES completion:^{}];
                              }];
    
    UIAlertAction* button2 = [UIAlertAction
                              actionWithTitle:@"Choose Existing"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  //  The user tapped on "Choose existing"
                                  UIImagePickerController *imagePickerController= [[UIImagePickerController alloc] init];
                                  imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                  imagePickerController.delegate = self;
                                  [self presentViewController:imagePickerController animated:YES completion:^{}];
                              }];
    
    [alert addAction:button0];
    [alert addAction:button1];
    [alert addAction:button2];
    alert.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:alert animated:YES completion:nil];
}



#pragma mark ImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage *photoTaken = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    PreviewImageVC *previewImageVC = [[PreviewImageVC alloc]initWithNibName:@"PreviewImageVC" bundle:nil];
    previewImageVC.view.layer.borderColor = [[UIColor blackColor]CGColor];
    previewImageVC.takedImage.image = photoTaken;
    previewImageVC.delegate = self;
   
    [self dismissViewControllerAnimated:YES completion:nil];
    [self presentViewController:previewImageVC animated:true completion:nil];
}
-(void)cameraButtonPressed {
    UIImagePickerController *imagePickerController= [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.delegate = self;
    [self presentViewController:imagePickerController animated:YES completion:^{}];
    
}

-(void)saveButtonPressed:(UIImage *)image type:(NSNumber *)type {
    NSDictionary *notoficationDictionary = [NSDictionary dictionaryWithObjectsAndKeys:image, @"image",type, @"type", nil];
    [self performSelector:@selector(cameraNoteTakenNotification:) withObject:notoficationDictionary afterDelay:0.5];
}

-(void)cameraNoteTakenNotification:(NSDictionary*)obj {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_SAVED_NOTE" object:obj];
}

- (void)starButtonPressed{
    self.selectedIndex = 1;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STAR_BUTTON_PRESSED" object:nil];
}

- (void)phonButtonClicked{
    self.selectedIndex = 1;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PHONE_BUTTON_PRESSED" object:nil];
}

- (void)pinButtonPressed{
    self.selectedIndex = 1;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PIN_BUTTON_PRESSED" object:nil];
}

- (void)openButtonPressed{
    self.selectedIndex = 1;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OPED_BUTTON_PRESSED" object:nil];
}

- (void)trashButtonSelected{
    self.selectedIndex = 1;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TRASH_BUTTON_PRESSED" object:nil];
}

- (void)searchByDateButtonPressed{
    self.selectedIndex = 1;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SEARCH_BY_DATE_BUTTON_PRESSED" object:nil];
}

- (void)searchByNameButtonPressed{
    self.selectedIndex = 1;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SEARCH_BY_NAME_BUTTON_PRESSED" object:nil];
}

- (void)setButtons:(NSMutableArray *)buttons wilthColor:(UIColor *)color{
    for (UIButton *btn in buttons) {
        if ([btn isKindOfClass:[UIButton class]] && btn.tag != 99) {
            [btn setImage:[btn.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            if (![btn.tintColor isEqual:[UIColor redColor]]) {
                [btn setTintColor:color];
                [btn setTitleColor:color forState:UIControlStateNormal];
            }
        }
    }
    [hideShowButton setTintColor:color];
}

- (void)createSlideButtonsWithArray:(NSArray *)array{
    count = (int)array.count;
    sliderButtonsView = [[UIScrollView alloc] initWithFrame:CGRectMake(-30, -sliderHeight + 40, 70,sliderHeight)];// 40 * (count + 1) + 20)];
    sliderButtonsView.contentSize = CGSizeMake(0, 0);
    sliderButtonsView.contentOffsetY = 40 * (count + 1) + 20 - sliderHeight;
    for (int i = 0; i < count; ++i) {
        UIButton *btn = array[i];
        [btn addShadowToView];
        [btn setFrame:CGRectMake(33, i*40 + 20, 30, 40)];
        [sliderButtonsView addSubview:btn];
    }
    hideShowButton = [[UIButton alloc] initWithFrame:CGRectMake(33, count * 40 + 21, 30, 40)];
    [hideShowButton setImage:[[UIImage imageNamed:@"down_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [hideShowButton setTintColor:[UIColor whiteColor]];
    [hideShowButton addShadowToView];
    [hideShowButton addTarget:self action:@selector(hideshowSliderMenuButtons:) forControlEvents:UIControlEventTouchUpInside];
    [sliderButtonsView addSubview:hideShowButton];
    sliderButtonsView.layer.masksToBounds = YES;
    sliderButtonsView.layer.cornerRadius = 25;
    [sliderButtonsView setBackgroundColor:[UIColor colorWithWhite:0.7 alpha:0.0]];
    [self.view addSubview:sliderButtonsView];
}

- (void)changeSliderButtonsWithArray:(NSArray *)array{
    count = (int)array.count;
    for (UIView *view in sliderButtonsView.subviews) {
        [view removeFromSuperview];
    }
    sliderButtonsView.frame = CGRectMake(-30, count*(-40) - 20, 70, 40 * (count + 1) + 20);
    [sliderButtonsView setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:0.7]];
    for (int i = 0; i < count; ++i) {
        UIButton *btn = array[i];
        [btn setFrame:CGRectMake(33, i*40 + 20, 30, 40)];
        [sliderButtonsView addSubview:btn];
    }
    hideShowButton.frame = CGRectMake(33, count * 40 + 21, 30, 40);
    [self setSliderPosition];
    [sliderButtonsView addSubview:hideShowButton];
}

- (void)setSliderPosition{
    if (hideShowButton.tag == 1) {
        sliderButtonsView.y = -20;
    }
}

- (void)hideshowSliderMenuButtons:(UIButton *)sender{
    if (sender.tag == 0) {
        [self showSliderMenu:sender.superview];
    }else{
        [self hideSliderMenu:sender.superview];
    }
}

- (void)showSliderMenu:(UIView *)menuView{
    hideShowButton.tag = 1;
    sliderButtonsView.contentSize = CGSizeMake(0, 40 * (count + 1) + 20);
    sliderButtonsView.contentOffsetY = 40 * (count + 1) + 20 - sliderHeight;
    [UIView animateWithDuration:0.4 animations:^{
        menuView.y = -20;
        //menuView.alpha = 0.5;
        hideShowButton.transform = CGAffineTransformMakeRotation(M_PI);
    }];
}

- (void)hideSliderMenu:(UIView *)menuView{
    hideShowButton.tag = 0;
    sliderButtonsView.contentSize = CGSizeMake(0, 0);
    [UIView animateWithDuration:0.4 animations:^{
        menuView.bottom = 40;
        //menuView.alpha = 1;
        hideShowButton.transform = CGAffineTransformMakeRotation(0);
        sliderButtonsView.contentOffsetY = 40 * (count + 1) + 20 - sliderHeight;
    } completion:^(BOOL finished) {
    }];
}
#pragma mark - Buttons Functions

- (void)buttonIsSelected:(UIButton *)sender{
    if (self.selectedIndex == 1) {
        if (sender.tag == 4){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ORDER_BUTTON_PRESSED" object:nil];
        }else if (sender.tag == 5){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TRASH_BUTTON_PRESSED" object:nil];
        }else{
            [self setSelectedIndex:sender.tag];
        }
    }else{
        [self setSelectedIndex:sender.tag];

    }
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex{
    if ([[DrawingViewManager sharedInstance] isPen]) {
        [penOrFinger setImage:[[UIImage imageNamed:@"pen"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    }else{
        [penOrFinger setImage:[[UIImage imageNamed:@"finger"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];

    }
    if (selectedIndex != 2 && selectedIndex != 3){
        [super setSelectedIndex:selectedIndex];
    }
    [self setButtons:allbuttonsArray wilthColor:[UIColor whiteColor]];
    NotesViewController *viewController = (NotesViewController *)[self viewControllers][1];
    [viewController setNavigationColor:[[DrawingViewManager sharedInstance] backgroundColor]];
    [self hideSliderButtons:NO];
    if (selectedIndex == 0) {
    [self setButtons:mainButtonsArray wilthColor:[[DrawingViewManager sharedInstance] buttonsColor]];
        [self hideSliderButtons:YES];
    }else if (selectedIndex == 1){
//        NSMutableArray *ar = [@[trashButton] mutableCopy];
//        [ar addObject:sortButton];
//        [ar addObjectsFromArray:mainButtonsArray];
//        [self changeSliderButtonsWithArray:ar];
    }else if (selectedIndex == 2 || selectedIndex == 3){
        //NSMutableArray *ar = [[NSMutableArray alloc] initWithArray:mainButtonsArray];
        //[ar addObject:menuButton];
        //[self changeSliderButtonsWithArray:ar];
    }else if (selectedIndex == 5){

    }
}

- (void)hideSliderButtons:(BOOL)hidde{
    [sliderButtonsView setHidden:hidde];

}

- (UIColor *)starButtonColor{
    return starButton.tintColor;
}

- (UIColor *)trashButtonColor{
    return trashButton.tintColor;
}

- (UIColor *)pinButtonColor{
    return pinButton.tintColor;
}

- (void)setTrashButtonColor:(UIColor *)color{
    if ([color isEqual:[UIColor redColor]]) {
        [trashButton setTintColor:color];
        [trashButton setTitleColor:color forState:UIControlStateNormal];
    }else{
        [trashButton setTintColor:[UIColor whiteColor]];
        [trashButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

- (void)setOpenedColor:(UIColor *)color{
    if ([color isEqual:[UIColor redColor]]) {
        [openedButton setTintColor:color];
        [openedButton setTitleColor:color forState:UIControlStateNormal];
    }else{
        [openedButton setTintColor:[UIColor whiteColor]];
        [openedButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

- (void)homeButtonPressed{
    NSMutableArray *ar = [[NSMutableArray alloc] initWithArray:mainButtonsArray];
    //[ar addObject:menuButton];
    //[self changeSliderButtonsWithArray:ar];
    self.selectedIndex = 4;
}

- (void)setStarButtonColor:(UIColor *)color{
    const CGFloat* colors = CGColorGetComponents( color.CGColor );
    if (colors[0] == 1) {
        [starButton setTintColor:color];
        [starButton setTitleColor:color forState:UIControlStateNormal];
    }else{
        [starButton setTintColor:[UIColor whiteColor]];
        [starButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

- (void)setPinButtonColor:(UIColor *)color{
    if ([color isEqual:[UIColor redColor]]) {
        [pinButton setTintColor:color];
        [pinButton setTitleColor:color forState:UIControlStateNormal];
    }else{
        [pinButton setTintColor:[UIColor whiteColor]];
        [pinButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

- (void)setPhoneButtonColor:(UIColor *)color{
    if ([color isEqual:[UIColor redColor]]) {
        [phoneButton setTintColor:color];
        [phoneButton setTitleColor:color forState:UIControlStateNormal];
    }else{
        [phoneButton setTintColor:[UIColor whiteColor]];
        [phoneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

- (void)logInButtonPressed{
    LoginViewController *loginViewController = [[LoginViewController alloc] init];
    loginViewController.view.backgroundColor = [UIColor colorWithWhite:0.2 alpha:1];
    loginViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    loginViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self hideSliderMenu:sliderButtonsView];
    [self presentViewController:loginViewController animated:YES completion:nil];
}
#pragma mark -Speach Rec.

- (void)configureSpeach{
    speechRecognizer = [[SFSpeechRecognizer alloc] initWithLocale:[NSLocale localeWithLocaleIdentifier:@"en-US"]];
    audioEngine = [[AVAudioEngine alloc] init];
    speechRecognizer.delegate = self;
    [SFSpeechRecognizer requestAuthorization:^(SFSpeechRecognizerAuthorizationStatus status) {
        BOOL isButtonEnabled = NO;
        switch (status) {
            case SFSpeechRecognizerAuthorizationStatusAuthorized:
                isButtonEnabled = YES;
                break;
            case SFSpeechRecognizerAuthorizationStatusDenied:
                isButtonEnabled = NO;
                break;
            case SFSpeechRecognizerAuthorizationStatusRestricted:
                isButtonEnabled = NO;
                break;
            case SFSpeechRecognizerAuthorizationStatusNotDetermined:
                isButtonEnabled = NO;
                break;
            default:
                break;
        }
        [self isMicraphoneButtonEnabled:isButtonEnabled];
//        OperationQueue.main.addOperation() {
//            self.microphoneButton.isEnabled = isButtonEnabled
//        }
    }];
}

- (void)microphoneTapped{
    self.selectedIndex = 0;
    if (audioEngine.isRunning) {
        [self endVoice];
    } else {
        [self startRecording];
    }
}

- (void)endVoice{
    [audioEngine stop];
    [recognitionRequest endAudio];
    [self isMicraphoneButtonEnabled:false];
}

- (void)isMicraphoneButtonEnabled:(BOOL)isMicraphoneButtonEnabled{
    
}

- (void)startRecording{
    if (recognitionTask != nil) {  //1
        [recognitionTask cancel];
        recognitionTask = nil;
    }
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];  //2
    NSError *error1;
    NSError *error2;
    NSError *error3;
    [audioSession setCategory:AVAudioSessionCategoryRecord error:&error1];
    [audioSession setMode:AVAudioSessionModeMeasurement error:&error2];
    [audioSession setActive:YES error:&error3];
    if (error3 || error2 || error1) {
        NSLog(@"audioSession properties weren't set because of an error.--%@,%@,%@",error1,error2,error3);
    }
    recognitionRequest = [[SFSpeechAudioBufferRecognitionRequest alloc] init];
    AVAudioInputNode *inputNode = audioEngine.inputNode;
    if (!inputNode) {
        NSAssert(NO,@"Audio engine has no input node");
    }
    
    if (!recognitionRequest) {
        NSAssert(NO,@"Unable to create an SFSpeechAudioBufferRecognitionRequest object");
    }
    recognitionRequest.shouldReportPartialResults = YES;  //6
    recognitionTask = [speechRecognizer recognitionTaskWithRequest:recognitionRequest resultHandler:^(SFSpeechRecognitionResult * _Nullable result, NSError * _Nullable error) {
        BOOL isFinal = NO;  //8
        if (result != nil) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"VOICE_SENT_TEXT" object:result.bestTranscription.formattedString];
            isFinal = result.isFinal;
        }
        if (error != nil || isFinal) {  //10
            [audioEngine stop];
            [inputNode removeTapOnBus:0];
            recognitionRequest = nil;
            recognitionTask = nil;
            [self isMicraphoneButtonEnabled:YES];
        }
    }];
    AVAudioFormat *recordingFormat = [inputNode outputFormatForBus:0];  //11
    [inputNode installTapOnBus:0 bufferSize:1024 format:recordingFormat block:^(AVAudioPCMBuffer * _Nonnull buffer, AVAudioTime * _Nonnull when) {
        [recognitionRequest appendAudioPCMBuffer:buffer];
    }];
    NSError *error;
    [audioEngine prepare];  //12
    [audioEngine startAndReturnError:&error];
    if (error){
        NSLog(@"audioEngine couldn't start because of an error. -%@",error);
    }
}

- (void)speechRecognizer:(SFSpeechRecognizer *)speechRecognizer availabilityDidChange:(BOOL)available{
    if (available) {
        [self isMicraphoneButtonEnabled:YES];
    } else {
        [self isMicraphoneButtonEnabled:NO];
    }
}

@end

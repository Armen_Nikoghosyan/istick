#import <UIKit/UIKit.h>

@interface MainTabBarController : UITabBarController
- (void)logInButtonPressed;
- (void)setTrashButtonColor:(UIColor *)color;
- (void)setStarButtonColor:(UIColor *)color;
- (void)setPinButtonColor:(UIColor *)color;
- (void)setPhoneButtonColor:(UIColor *)color;
- (void)setOpenedColor:(UIColor *)color;
- (void)homeButtonPressed;
- (void)microphoneTapped;
- (void)endVoice;
- (void)searchByDateButtonPressed;
- (void)searchByNameButtonPressed;
- (void)hideSliderButtons:(BOOL)hidde;
- (UIColor *)starButtonColor;
- (UIColor *)trashButtonColor;
- (UIColor *)pinButtonColor;

@end
